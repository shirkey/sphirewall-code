Sphirewall Source Code:

Sphirewall is split into modules, everything depends on sphirewall-libs, but what you install after that is up to you.

Sometimes you need to run ldconfig to resync the linker
On systems with systemd sphirewall-core will only look in /usr/lib/systemd/system before giving up on installing the unit.
On systems without dpkg you will need to run depmod manually after installing sphirewall-kernel. 

See http://sphirewall.net for more information
