#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

import string
import uuid 
import os 
import click
from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallapi.sphirewall_connection import SessionTimeoutException, ApiError

sphirewall_client_connection = None

@click.group()
@click.option('--fork', type=bool, default=False, help='fork command as a daemon, defaults to False')
@click.option('--connect', type=bool, default=True, help='connect to sphirewall, defaults to True')
@click.option('--username', default="admin", help='username for connection to sphirewalld, defaults to admin')
@click.option('--password', default="admin", help='password for connection to sphirewalld, defaults to admin')
@click.option('--host', default="localhost", help='sphirewalld node hostname')
@click.option('--port', default=8001, help='sphirewalld management port')
def cli(fork, connect, username, password, host, port):
    if fork:
         import daemon
         daemon.basic_daemonize()

    if not connect:
        return

    global sphirewall_client_connection
    try:
        sphirewall_client_connection = SphirewallClient(hostname=host, port=port, username=username, password=password)

        #This is a bit of a hack to check that the api connection is working properly, and we can continue
        sphirewall_client_connection.general().active_users_list()
    except SessionTimeoutException as session_exception:
        raise click.ClickException(session_exception.message)
    except ApiError as generic_api_error:
        raise click.ClickException(generic_api_error.message)
    except Exception as generic_exception:
        raise click.ClickException(str(generic_exception))


@click.group()
def config():
    pass


@click.command()
def stats():
    click.echo("sphirewalld active statistics")

    click.echo("tracked connections: %d" % sphirewall_client_connection.firewall().connections_size())
    click.echo("active users: %d" % len(sphirewall_client_connection.general().active_users_list()))
    click.echo("active devices: %d" % sphirewall_client_connection.general().hosts_size())


@click.command()
def version():
    click.echo("sphirewalld version: %s" % sphirewall_client_connection.version()["version"])


@click.group()
def show():
    pass


def print_interface(interface):
    interface_description_line = ""
    interface_description_line += interface["interface"]
    if interface.get("name"):
        interface_description_line += " " + interface["name"]

    if interface.get("vlan"):
        interface_description_line += " vlan id %s interface %s " % (interface.get("vlanId"), interface.get("vlanInterface"))
    if interface.get("bridge"):
        interface_description_line += " bridge interfaces %s " % (string.join(interface.get("bridgeDevices"), ","))
    if interface.get("physical"):
        interface_description_line += " physical"
    interface_description_line += " up" if interface.get("state") else " down"

    click.echo(interface_description_line)

    interface_description_line = ""
    interface_description_line += " hw %s" % interface.get("mac")
    click.echo(interface_description_line)

    if interface.get("dhcp"):
        click.echo(" address mode dhcp")
    if not interface.get("dhcp"):
        click.echo(" address mode static")

    ipv4_addresses = interface.get("ipv4")
    for ipv4 in ipv4_addresses:
        click.echo(" ipv4 %s/%s bcast %s" % (ipv4.get("ip"), ipv4.get("mask"), ipv4.get("broadcast")))


@click.command("interfaces")
def show_interfaces():
    interfaces = sphirewall_client_connection.network().devices(configured=True)
    for interface in interfaces:
        print_interface(interface)


@click.command("wan")
def show_wan():
    firewall__autowan = sphirewall_client_connection.firewall().autowan()
    print firewall__autowan
    click.echo("automatic wan")
    if firewall__autowan["mode"] == 0:
        click.echo("mode: single link")
        click.echo("devices: %s" % string.join([interface["interface"] for interface in firewall__autowan.get("interfaces", [])], ", "))

    if firewall__autowan["mode"] == 1:
        click.echo("mode: multipath")
        click.echo("devices: %s" % string.join([interface["interface"] for interface in firewall__autowan.get("interfaces", [])], ", "))

    if firewall__autowan["mode"] == 2:
        click.echo("mode: failover")
        click.echo("devices: %s" % string.join([interface["interface"] for interface in firewall__autowan.get("interfaces", [])], ", "))


@click.command("wan")
@click.argument("device", required=False)
def set_wan(device=None):
    if not device:
        sphirewall_client_connection.firewall().autowan_set(-1, None)
        return
    sphirewall_client_connection.firewall().autowan_set(0, [{"interface": device, "failover_index": 0}])


@click.group("interfaces")
def set_interfaces():
    pass


@click.group("addr")
def set_interface_address():
    pass


@click.command("add")
@click.argument("interface")
@click.argument("ip")
@click.argument("mask")
@click.argument("bcast")
def set_interface_address_add(interface, ip, mask, bcast):
    current_device_entity = sphirewall_client_connection.network().devices(interface)
    new_address = {
        "ip": ip,
        "mask": mask,
        "broadcast": bcast,
    }

    current_device_entity["ipv4"].append(new_address)
    sphirewall_client_connection.network().devices_set(interface, {}, address_params=current_device_entity)


@click.command("del")
@click.argument("interface")
@click.argument("ip")
@click.argument("mask")
@click.argument("bcast")
def set_interface_address_del(interface, ip, mask, bcast):
    current_device_entity = sphirewall_client_connection.network().devices(interface)
    new_addresses = []
    for ipv4_address in current_device_entity["ipv4"]:
        if not (ipv4_address.get("ip") == ip and ipv4_address.get("mask") == mask and ipv4_address.get("broadcast") == bcast):
            new_addresses.append(ipv4_address)

    current_device_entity["ipv4"] = new_addresses
    sphirewall_client_connection.network().devices_set(interface, {}, address_params=current_device_entity)


@click.command("method")
@click.argument("interface")
@click.argument("method")
def set_interface_method(interface, method):
    address_params = {
        "dhcp": method == "dhcp",
    }
    sphirewall_client_connection.network().devices_set(interface, {}, address_params=address_params)

@click.command("gateway")
@click.argument("interface")
@click.argument("gateway")
def set_interface_gateway(interface, gateway):
    address_params = {
        "gateway": gateway,
    }
    sphirewall_client_connection.network().devices_set(interface, {}, address_params=address_params)


@click.command("state")
@click.argument("interface")
@click.argument("state")
def set_interface_state(interface, state):
    if state == "up":
        print "bringing device up"
        sphirewall_client_connection.network().devices_up(interface)
    elif state == "down":
        sphirewall_client_connection.network().devices_down(interface)
    else:
        click.echo("unknown state")

@click.command("persisted")
@click.argument("interface")
@click.argument("persisted")
def set_interface_persisted(interface, persisted):
    device_config = {
        "persisted": persisted == "yes"
    }
    sphirewall_client_connection.network().devices_set(interface, device_config)

@click.group("add")
def set_interface_add():
    pass


@click.command("vlan")
@click.argument("interface")
@click.argument("vlan_id")
def set_interface_add_vlan(interface, vlan_id):
    device_params = {"vlan": True, "vlanId": int(vlan_id), "vlanInterface": interface}
    sphirewall_client_connection.network().devices_set(device_params=device_params)


@click.command("bridge")
@click.argument("devices", nargs=-1)
def set_interface_add_bridge(devices):
    device_params = {"bridge": True, "bridgeDevices": devices}
    sphirewall_client_connection.network().devices_set(device_params=device_params)

@click.group("bridge")
def set_interface_bridge():
    pass

@click.command("setif")
@click.argument("interface")
@click.argument("devices", nargs=-1)
def set_interface_bridge_setif(interface, devices):
    device_params = {"bridge": True, "bridgeDevices": devices}
    sphirewall_client_connection.network().devices_set(interface, device_params=device_params)

@click.command("del")
@click.argument("interface")
def set_interface_del(interface):
    sphirewall_client_connection.network().devices_remove(interface)


@click.command("save")
def set_interface_save():
    sphirewall_client_connection.network().devices_save()


@click.command()
@click.argument("interface")
@click.argument("mode")
def set_interface_net(interface, mode):
    pass


@click.group()
def set():
    pass


@click.group()
def disable():
    pass


@click.command("cloud")
@click.argument('device_id')
@click.argument('peer_key')
@click.argument('verify')
def set_cloud(device_id, peer_key, verify):
    sphirewall_client_connection.general().configuration("cloud:deviceid", value=device_id)
    sphirewall_client_connection.general().configuration("cloud:key", value=peer_key)
    sphirewall_client_connection.general().configuration("cloud:enabled", value=True)
    sphirewall_client_connection.general().configuration("cloud:verify", value=verify == "true" or verify == "True" or verify == "yes")


@click.command("cloud")
def show_cloud():
    click.echo("cloud management connection")
    enabled = sphirewall_client_connection.general().configuration("cloud:enabled")
    device_id = sphirewall_client_connection.general().configuration("cloud:deviceid")
    peer_key = sphirewall_client_connection.general().configuration("cloud:key")
    verify = sphirewall_client_connection.general().configuration("cloud:verify")

    click.echo("enabled   : %s" % enabled)
    click.echo("device_id : %s" % device_id)
    click.echo("peer_key  : %s" % peer_key)
    click.echo("verify    : %s" % verify)


@click.command("dump")
def config_dump():
    click.echo(sphirewall_client_connection.general().configuration_dump())


@click.command("load")
@click.argument("content", required=False)
def config_load(content):
    if not content:
        content = click.get_text_stream('stdin').read()
    sphirewall_client_connection.general().configuration_load(content)


@click.command("load-file")
@click.argument("filename", required=True)
def config_load_file(filename):
    fp = open(filename)
    file_contents = fp.read()
    sphirewall_client_connection.general().configuration_load(file_contents)


@click.command("cloud")
def disable_cloud():
    sphirewall_client_connection.general().configuration("cloud:enabled", value=False)


@click.command("masquerading")
def disable_masquerading():
    sphirewall_client_connection.firewall().autowan_set(False, [])

@show.command("connections")
def show_connections():
    connections = sphirewall_client_connection.firewall().connections_list()
    for connection in connections:
        print connection

@click.command("update")
def update():
    click.echo("performing update, note: this must executed on the target machine")
    click.echo("-- halting sphirewall daemons")
    os.system("service sphirewall-core stop")
    click.echo("-- updating apt repositories")
    os.system("export DEBIAN_FRONTEND=noninteractive && export PATH=/usr/bin:/bin:/usr/sbin:/sbin && apt-get -yf --force-yes update")

    click.echo("-- upgrading....here we go boys...hope for the best")
    os.system("export DEBIAN_FRONTEND=noninteractive && export PATH=/usr/bin:/bin:/usr/sbin:/sbin && apt-get -yf --force-yes upgrade")
    click.echo("-- starting sphirewall daemon again")
    os.system("service sphirewall-core start")

def upload_dump(dump_file, upload_url):
    os.system("export DEBIAN_FRONTEND=noninteractive && export PATH=/usr/bin:/bin:/usr/sbin:/sbin && apt-get -yf --force-yes install curl")
    os.system("curl -v -T %s %s" % (dump_file, upload_url))


@click.command("dump")
@click.option('--upload', default=None, help='upload url')
def dump(upload):
    click.echo("collecting debugging information")
    dump_id = str(uuid.uuid4())
    temp_directory = "/tmp/%s" % dump_id
    click.echo("-- dump id: %s" % dump_id)
    click.echo("-- temp directory: %s" % (temp_directory))

    click.echo("--- copying configuration files")
    os.system("mkdir %s" % temp_directory)
    os.system("cp /etc/sphirewall.conf %s" % temp_directory)
    
    click.echo("--- copying log files")
    os.system("cp /var/log/syslog* %s" % temp_directory)
    os.system("cp /var/log/kern* %s" % temp_directory)

    click.echo("--- copying core dumps")
    os.system("cp /core %s" % temp_directory)

    click.echo("--- copying debian network configuration")
    os.system("cp /etc/network/interfaces %s" % temp_directory)

    os.system("export DEBIAN_FRONTEND=noninteractive && export PATH=/usr/bin:/bin:/usr/sbin:/sbin && apt-get -yf --force-yes install bzip2")
    click.echo("-- compressing files")
    os.system("tar jcf %s.tar.gz %s" % (dump_id, temp_directory))

    if upload:
        click.echo("-- uploading to %s" % upload)
        upload_dump("%s.tar.gz" % dump_id, upload)
        click.echo("-- upload finished, let the support team know, your id is %s" % dump_id)
    click.echo("finished")	
    
cli.add_command(config)
cli.add_command(stats)
cli.add_command(update)
cli.add_command(dump)

config.add_command(show)
config.add_command(set)
config.add_command(disable)
config.add_command(config_dump)
config.add_command(config_load)
config.add_command(config_load_file)

set.add_command(set_cloud)
set.add_command(set_interfaces)
set.add_command(set_wan)

set_interface_address.add_command(set_interface_address_add)
set_interface_address.add_command(set_interface_address_del)

set_interfaces.add_command(set_interface_add)
set_interfaces.add_command(set_interface_del)
set_interfaces.add_command(set_interface_address)
set_interfaces.add_command(set_interface_method)
set_interfaces.add_command(set_interface_state)
set_interfaces.add_command(set_interface_persisted)
set_interfaces.add_command(set_interface_save)
set_interfaces.add_command(set_interface_gateway)

set_interfaces.add_command(set_interface_bridge)
set_interface_bridge.add_command(set_interface_bridge_setif)

set_interface_add.add_command(set_interface_add_bridge)
set_interface_add.add_command(set_interface_add_vlan)

show.add_command(show_cloud)
show.add_command(show_interfaces)
show.add_command(show_wan)
cli.add_command(version)

disable.add_command(disable_cloud)
disable.add_command(disable_masquerading)

if __name__ == '__main__':
    cli()
