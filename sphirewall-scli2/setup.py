from setuptools import setup

setup(
    name='scli2',
    version='0.9.9.68',
    py_modules=['scli2'],
    install_requires=[
        'Click', 'sphirewallapi'
    ],
    entry_points='''
        [console_scripts]
        scli2=scli2:cli
    ''',
)

