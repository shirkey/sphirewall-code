from flask import request, session, redirect, flash
import flask
from flask.ext.login import current_user, login_required, logout_user
from sphirewallapi.sphirewall_connection import SessionTimeoutException
from sphirewallwmi import app
from sphirewallwmi.api.utils import get_class


def standard_error_handler(error):
    flash(str(error))
    return redirect("/login?error=" + str(error))


@app.errorhandler(500)
def on_error(error):
    return get_class(app.config["STANDARD_ERROR_HANDLER"])(error)


@app.errorhandler(SessionTimeoutException)
def on_error_timeout(e):
    logout_user()
    return redirect("/")


@app.before_request
def preheader():
    if current_user.is_authenticated():
        flask.g.preheader = get_class(app.config["PREHEADER_GENERATOR"])()
        flask.g.analytics_available = get_class(app.config["ANALYTICS_PROVIDER"])().available()


@app.route("/")
@login_required
def home():
    return redirect("/device")


def standard_preheader():
    return ""