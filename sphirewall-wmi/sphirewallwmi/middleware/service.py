from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallwmi import app
from sphirewallwmi.api.utils import get_class
from sphirewallwmi.routes_login import get_session_sphirewall_hostname, get_session_sphirewall_port, get_session_sphirewall_token


class SphirewallAnalyticsProvider():
    def __init__(self):
        pass

    def available(self):
        return False

    def statistics_transfer(self, params):
        return []

    def statistics_devices(self, params):
        return []

    def get_category(self, type):
        return []

    def statistics_types(self, params):
        return []

    def statistics_types_users(self, type, params):
        return []

    def statistics_types_devices(self, type, params):
        return []

    def statistics_users(self, params):
        return []

    def statistics_websites(self, params):
        return []

    def statistics_websites_subdomains(self, website, params):
        return []

    def statistics_metrics(self, params):
        return []

    def statistics_metrics_specific(self, metric, params):
        return []

    def statistics_device_types(self, mac, ip, params):
        return []

    def statistics_device_transfer(self, mac, ip, params):
        return []

    def statistics_device_websites(self, mac, ip, params):
        return []

    def statistics_device_websites_subdomains(self, mac, ip, website, params):
        return []

    def statistics_user_types(self, username, params):
        return []

    def statistics_user_transfer(self, username, params):
        return []

    def statistics_user_websites(self, username, params):
        return []

    def statistics_user_websites_subdomains(self, user, website, params):
        return []

    def events(self, params):
        return get_sphirewall_direct().general().events_list()

    def create_configuration_snapshot(self, account_email, description, snapshot):
        pass

    def get_configuration_snapshots(self):
        return []


def standard_analytics_provider():
    return SphirewallAnalyticsProvider()


def sphirewall():
    path = app.config["SPHIREWALL_CONNECTION_PROVIDER"]
    return get_class(path)()


def get_sphirewall_direct():
    client = SphirewallClient(
        hostname=get_session_sphirewall_hostname(), port=get_session_sphirewall_port(), token=get_session_sphirewall_token()
    )
    return client


def get_service_provider():
    return get_class(app.config["ANALYTICS_PROVIDER"])()
