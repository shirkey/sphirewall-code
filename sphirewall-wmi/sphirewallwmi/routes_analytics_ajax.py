from datetime import datetime
import json
from flask import session, jsonify
from flask.ext.login import login_required, current_user
from sphirewallwmi.api.utils import arg, get_class, DateHelper
from sphirewallwmi.middleware import service
from sphirewallwmi import app
from sphirewallwmi.middleware.service import get_service_provider
from sphirewallwmi.routes_device_status import process_params

@app.route("/device/analytics/transfer")
@login_required
def analytics_transfer():
    return jsonify(get_service_provider().statistics_transfer(process_params()))


@app.route("/device/analytics/transfer/devices")
@login_required
def analytics_transfer_devices():
    return jsonify(get_service_provider().statistics_devices(process_params()))


def resolve_traffic_type_categories(entity):
    resolved_type = get_service_provider().get_category(entity["type_port"])
    if resolved_type:
        entity["type_name"] = resolved_type["name"]
        entity["type_description"] = resolved_type["description"]
    else:
        entity["type_name"] = "Unidentified on port %s" % entity["type_port"]


@app.route("/device/analytics/transfer/types")
@login_required
def analytics_transfer_types():
    transfer = get_service_provider().statistics_types(process_params())
    for entity in transfer:
        resolve_traffic_type_categories(entity)
    return jsonify(result=transfer)


@app.route("/device/analytics/transfer/types/<type>/users")
@login_required
def analytics_transfer_types_users(type):
    return jsonify(result=get_service_provider().statistics_types_users(type, process_params()))


@app.route("/device/analytics/transfer/types/<type>/devices")
@login_required
def analytics_transfer_types_devices(type):
    return jsonify(result=get_service_provider().statistics_types_devices(type, process_params()))


@app.route("/device/analytics/transfer/users")
@login_required
def analytics_transfer_users():
    return jsonify(get_service_provider().statistics_users(process_params()))


@app.route("/device/analytics/transfer/websites")
@login_required
def analytics_transfer_websites():
    return jsonify(get_service_provider().statistics_websites(process_params()))


@app.route("/device/analytics/metrics")
@login_required
def analytics_metrics_all():
    return jsonify(get_service_provider().statistics_metrics(process_params()))


@app.route("/device/analytics/metrics/<metric>")
@login_required
def analytics_metrics(metric):
    return jsonify(get_service_provider().statistics_metrics_specific(metric, process_params()))


@app.route("/device/analytics/transfer/device/<mac>/<ip>/types")
@login_required
def analytics_transfer_types_device(mac, ip):
    transfer = get_service_provider().statistics_device_types(mac, ip, process_params())
    for entity in transfer:
        resolve_traffic_type_categories(entity)
    return jsonify(result=transfer)


@app.route("/device/analytics/transfer/device/<mac>/<ip>")
@login_required
def analytics_transfer_device(mac, ip):
    transfer = get_service_provider().statistics_device_transfer(mac, ip, process_params())
    return jsonify(transfer)


@app.route("/device/analytics/transfer/device/<mac>/<ip>/websites")
@login_required
def analytics_transfer_device_websites(mac, ip):
    transfer = get_service_provider().statistics_device_websites(mac, ip, process_params())
    return jsonify(transfer)


@app.route("/device/analytics/transfer/device/<mac>/<ip>/website/<website>")
@login_required
def analytics_transfer_device_website_spec(mac, ip, website):
    transfer = get_service_provider().statistics_device_websites_subdomains(mac, ip, website, process_params())
    return jsonify(transfer)


@app.route("/device/analytics/transfer/user/<user>/types")
@login_required
def analytics_transfer_types_user(user):
    transfer = get_service_provider().statistics_user_types(user, process_params())
    for entity in transfer:
        resolve_traffic_type_categories(entity)
    return jsonify(result=transfer)


@app.route("/device/analytics/transfer/user/<user>")
@login_required
def analytics_transfer_user(user):
    transfer = get_service_provider().statistics_user_transfer(user, process_params())
    return jsonify(transfer)


@app.route("/device/analytics/transfer/user/<user>/websites")
@login_required
def analytics_transfer_user_websites(user):
    transfer = get_service_provider().statistics_user_websites(user, process_params())
    return jsonify(transfer)


@app.route("/device/analytics/transfer/user/<user>/website/<website>")
@login_required
def analytics_transfer_user_website_spec(user, website):
    transfer = get_service_provider().statistics_user_websites_subdomains(user, website, process_params())
    return jsonify(transfer)


@app.route("/device/analytics/transfer/website/<website>")
@login_required
def analytics_transfer_website(website):
    return jsonify(get_service_provider().statistics_websites_subdomains(website, process_params()))


@app.route("/device/analytics/connections")
@login_required
def analytics_connections():
    transfer = service.sphirewall().firewall().connections_list()
    return jsonify(result=transfer)


@app.route("/device/analytics/events")
@login_required
def analytics_events():
    provider__events = get_service_provider().events(process_params())
    for event in provider__events:
        event["message"] = json.dumps(event["params"])
        event["time"] = datetime.fromtimestamp(event["time"]).strftime('%Y-%m-%d %H:%M:%S')

    return jsonify(result=provider__events)


@app.route("/device/analytics/clients")
@login_required
def analytics_clients():
    general__hosts_list = service.sphirewall().general().hosts_list()
    users_list = service.sphirewall().general().active_users_list()

    return jsonify(result=dict(users=users_list, hosts=general__hosts_list))


@app.route("/device/analytics/logs")
@login_required
def analytics_logs():
    log = service.sphirewall().general().logs(filter=arg("filter"))
    return jsonify(result=log)

@app.route("/device/vpn/deviceinformation")
@login_required
def vpn_deviceinformation():
    devices = []
    for device in current_user.devices:
        try:
            device = {
                "deviceid": device["deviceid"],
                "user_defined_name": device["user_defined_name"]
            }

            device["interfaces"] = service.sphirewall(device["deviceid"]).network().devices()
            devices.append(device)
        except Exception as e:
            pass
    return jsonify(result=devices)
