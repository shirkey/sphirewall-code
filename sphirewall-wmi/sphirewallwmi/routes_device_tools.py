from flask import render_template, jsonify
from sphirewallwmi  import app
from sphirewallwmi.middleware import service


@app.route("/device/tools/ajax/ping/<hostname>")
def ping(hostname):
    return jsonify(online=service.sphirewall().network().ping(hostname))

@app.route("/device/tools/ping")
def tools_ping():
    return render_template("device_tools_ping.html")