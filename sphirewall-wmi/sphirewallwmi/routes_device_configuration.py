import json

import operator
from flask import flash
from flask.ext.login import login_required

from flask.globals import request, session
from flask.templating import render_template
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import checkbox_value, nint, text_value
from sphirewallwmi.middleware import service
from sphirewallapi.sphirewall_api_general import QuotaInfo
from sphirewallwmi.middleware.service import get_service_provider


@app.route("/device/configuration/snapshots", methods=["GET", "POST"])
@login_required
def configuration_snapshots():
    if request.method == "POST":
        snapshot = service.sphirewall().general().configuration_dump()
        get_service_provider().create_configuration_snapshot("edgewize", text_value("description"), snapshot)

    snapshots = get_service_provider().get_configuration_snapshots()
    return render_template("device_configuration_snapshots.html", snapshots=snapshots)


def find_snapshot_by_id(id):
    snapshots = get_service_provider().get_configuration_snapshots()
    for snapshot in snapshots:
        if snapshot["id"] == int(id):
            return snapshot
    return None


@app.route("/device/configuration/snapshots/<snapshot_id>/restore", methods=["GET"])
@login_required
def configuration_snapshots_restore(snapshot_id):
    snapshot = find_snapshot_by_id(snapshot_id)
    service.sphirewall().general().configuration_load(snapshot["snapshot"])
    flash("Configuration snapshot was restored")
    return redirect("/device/configuration/snapshots")


@app.route("/device/configuration/snapshots/<snapshot_id>/view", methods=["GET"])
@login_required
def configuration_snapshots_view(snapshot_id):
    snapshot = find_snapshot_by_id(snapshot_id)
    snapshot_dict = json.loads(snapshot["snapshot"])
    return render_template("device_configuration_snapshots_view.html", snapshot=snapshot, data=json.dumps(snapshot_dict, indent=5))


@app.route("/device/configuration/general", methods=["POST", "GET"])
@login_required
def configuration_general():
    if request.method == "POST":
        service.sphirewall().general().configuration("general:disable_remote_management", checkbox_value("disable_remote_management"))
        service.sphirewall().general().configuration("general:snort_enabled", checkbox_value("snort_enabled"))
        service.sphirewall().general().configuration("general:snort_file", request.form["snort_file"])
        service.sphirewall().general().configuration("general:disableGoogleSearchSSL", checkbox_value("enforcegss"))

    drm = service.sphirewall().general().configuration("general:disable_remote_management")
    snort_enabled = service.sphirewall().general().configuration("general:snort_enabled")
    snort_file = service.sphirewall().general().configuration("general:snort_file")
    enforcegss = service.sphirewall().general().configuration("general:disableGoogleSearchSSL")
    return render_template("device_configuration_general.html", disable_remote_management=drm,
                           snort_enabled=snort_enabled, snort_file=snort_file, enforcegss=enforcegss,
                           pools=service.sphirewall().firewall().aliases())


@app.route("/device/configuration/authentication", methods=["GET", "POST"])
@login_required
def configuration_authentication():
    if request.method == "POST":
        service.sphirewall().general().configuration("authentication:session_timeout", nint(request.form["session_timeout"]))
        service.sphirewall().general().configuration("authentication:cp_url", request.form["cp_url"])
        service.sphirewall().general().configuration("authentication:remote_authentication_group", nint(request.form["remote_authentication_group"]))
        service.sphirewall().general().configuration("authentication:ldap:enabled", checkbox_value("ldap_enabled"))
        service.sphirewall().general().configuration("authentication:wmic:enabled", checkbox_value("wmic_enabled"))
        service.sphirewall().general().configuration("authentication:pam:enabled", checkbox_value("pam_enabled"))
        service.sphirewall().general().configuration("authentication:basic:enabled", checkbox_value("http_enabled"))
        service.sphirewall().general().configuration("authentication:basic:baseurl", request.form["http_baseurl"])

        force_login_setting = 1 if checkbox_value("force_login") else 0
        service.sphirewall().general().advanced(key="CAPTURE_PORTAL_FORCE", value=force_login_setting)
        service.sphirewall().general().configuration("general:rpc", checkbox_value("enable_rpc"))

        force_auth_source = request.form.getlist('force_auth_sources')
        force_auth_website_exceptions = request.form.getlist('force_auth_website_exceptions')
        force_auth_destination_exceptions = request.form.getlist('force_auth_destination_exceptions')
        service.sphirewall().firewall().force_auth_settings(force_auth_source, force_auth_website_exceptions,
                                                            force_auth_destination_exceptions)

    session_timeout = service.sphirewall().general().configuration("authentication:session_timeout")
    cp_url = service.sphirewall().general().configuration("authentication:cp_url")
    groups = service.sphirewall().general().groups()
    remote_authentication_group = service.sphirewall().general().configuration(
        "general:remote_authentication_group")
    ldap_enabled = service.sphirewall().general().configuration("authentication:ldap:enabled")
    pam_enabled = service.sphirewall().general().configuration("authentication:pam:enabled")
    wmic_enabled = service.sphirewall().general().configuration("authentication:wmic:enabled")
    http_enabled = service.sphirewall().general().configuration("authentication:basic:enabled")
    http_baseurl = service.sphirewall().general().configuration("authentication:basic:baseurl")

    #Force authentication stuff
    force_login = True if service.sphirewall().general().advanced_value(
        key="CAPTURE_PORTAL_FORCE") == 1 else False

    return render_template("device_configuration_authentication.html",
                           cp_url=cp_url, session_timeout=session_timeout, groups=groups,
                           remote_authentication_group=remote_authentication_group, ldap_enabled=ldap_enabled, pam_enabled=pam_enabled,
                           force_login=force_login,
                           force_auth_settings=service.sphirewall().firewall().force_auth_settings(),
                           pools=service.sphirewall().firewall().aliases(), wmic_enabled=wmic_enabled, http_enabled=http_enabled,
                           http_baseurl=http_baseurl)


@app.route("/device/configuration/authentication/wmi", methods=["GET", "POST"])
@login_required
def configuration_authentication_wmi():
    if request.method == "POST":
        service.sphirewall().general().configuration("authentication:wmic:username", request.form["wmic_username"])
        service.sphirewall().general().configuration("authentication:wmic:password", request.form["wmic_password"])
        service.sphirewall().general().configuration("authentication:wmic:domain", request.form["wmic_domain"])
        service.sphirewall().general().configuration("authentication:wmic:hostname", request.form["wmic_host"])

        service.sphirewall().general().configuration("authentication:wmic:targetEventId", request.form["wmic_eventid"])
        service.sphirewall().general().configuration("authentication:wmic:hostAttribute", request.form["wmic_host_attribute"])
        service.sphirewall().general().configuration("authentication:wmic:usernameAttribute", request.form["wmic_username_attribute"])

    wmic_eventid = service.sphirewall().general().configuration("authentication:wmic:targetEventId")
    wmic_host_attribute = service.sphirewall().general().configuration("authentication:wmic:hostAttribute")
    wmic_username_attribute = service.sphirewall().general().configuration("authentication:wmic:usernameAttribute")

    wmic_username = service.sphirewall().general().configuration("authentication:wmic:username")
    wmic_password = service.sphirewall().general().configuration("authentication:wmic:password")
    wmic_domain = service.sphirewall().general().configuration("authentication:wmic:domain")
    wmic_host = service.sphirewall().general().configuration("authentication:wmic:hostname")

    return render_template("device_configuration_wmi.html",
                           wmic_username=wmic_username,
                           wmic_password=wmic_password,
                           wmic_domain=wmic_domain,
                           wmic_host=wmic_host,
                           wmic_eventid=wmic_eventid,
                           wmic_host_attribute=wmic_host_attribute,
                           wmic_username_attribute=wmic_username_attribute)


@app.route("/device/configuration/general/ldap", methods=["POST", "GET"])
@login_required
def configuration_general_ldap():
    if request.method == "POST":
        service.sphirewall().general().configuration("authentication:ldap:hostname", text_value("hostname"))
        service.sphirewall().general().configuration("authentication:ldap:port", nint(request.form["port"]))
        service.sphirewall().general().configuration("authentication:ldap:ldapusername", request.form["username"])
        service.sphirewall().general().configuration("authentication:ldap:ldappassword", request.form["password"])

        #Schema changes
        service.sphirewall().general().configuration("authentication:ldap:basedn", text_value("basedn"))
        service.sphirewall().general().configuration("authentication:ldap:userownedgroupmembership", checkbox_value("userownedgroupmembership"))
        service.sphirewall().general().configuration("authentication:ldap:usernameattribute", text_value("usernameattribute"))
        service.sphirewall().general().configuration("authentication:ldap:userbase", text_value("userbase"))
        service.sphirewall().general().configuration("authentication:ldap:usergroupattribute", text_value("usergroupattribute"))
        service.sphirewall().general().configuration("authentication:ldap:userdnattribute", text_value("userdnattribute"))
        service.sphirewall().general().configuration("authentication:ldap:authenticateuserusingsupplieddn", checkbox_value("authenticateuserusingsupplieddn"))
        service.sphirewall().general().configuration("authentication:ldap:groupbase", text_value("groupbase"))
        service.sphirewall().general().configuration("authentication:ldap:groupnameattribute", text_value("groupnameattribute"))
        service.sphirewall().general().configuration("authentication:ldap:groupmembernameattribute", text_value("groupmembernameattribute"))

        service.sphirewall().general().ldap_sync_groups()

    return render_template("device_configuration_general_ldap.html",
                           server=service.sphirewall().general().configuration("authentication:ldap:hostname"),
                           port=service.sphirewall().general().configuration("authentication:ldap:port"),
                           baseDn=service.sphirewall().general().configuration("authentication:ldap:basedn"),
                           bindUsername=service.sphirewall().general().configuration("authentication:ldap:ldapusername"),
                           bindPassword=service.sphirewall().general().configuration("authentication:ldap:ldappassword"),
                           ldap=service.sphirewall().general().ldap(),

                           userownedgroupmembership=service.sphirewall().general().configuration("authentication:ldap:userownedgroupmembership"),
                           usernameattribute=service.sphirewall().general().configuration("authentication:ldap:usernameattribute"),
                           userbase=service.sphirewall().general().configuration("authentication:ldap:userbase"),
                           usergroupattribute=service.sphirewall().general().configuration("authentication:ldap:usergroupattribute"),
                           userdnattribute=service.sphirewall().general().configuration("authentication:ldap:userdnattribute"),
                           authenticateuserusingsupplieddn=service.sphirewall().general().configuration("authentication:ldap:authenticateuserusingsupplieddn"),
                           groupbase=service.sphirewall().general().configuration("authentication:ldap:groupbase"),
                           groupnameattribute=service.sphirewall().general().configuration("authentication:ldap:groupnameattribute"),
                           groupmembernameattribute=service.sphirewall().general().configuration("authentication:ldap:groupmembernameattribute")
    )


@app.route("/device/configuration/events", methods=["POST", "GET"])
@login_required
def configuration_events():
    if request.method == "POST":
        if checkbox_value("log"):
            service.sphirewall().general().event_handler_add("event", "handler.log")
        else:
            service.sphirewall().general().event_handler_delete("event", "handler.log")

        if checkbox_value("ids"):
            service.sphirewall().general().event_handler_add("event.ids", "handler.firewall.block")
        else:
            service.sphirewall().general().event_handler_delete("event.ids", "handler.firewall.block")
    logging = service.sphirewall().general().event_handler_exists("event", "handler.log")
    ids = service.sphirewall().general().event_handler_exists("event.ids", "handler.firewall.block")

    return render_template(
        "device_configuration_events.html",
        logging=logging,
        ids=ids
    )


@app.route("/device/configuration/users", methods=["GET", "POST"])
@login_required
def configuration_users():
    if request.method == "POST":
        service.sphirewall().general().users_add(request.form["username"], "", "")

    unsorted_users = service.sphirewall().general().users()
    users = sorted(unsorted_users, key=operator.itemgetter("username"))
    return render_template("device_configuration_users.html", users=users,
                           groups=service.sphirewall().general().groups())


@app.route("/device/configuration/users/delete/<username>")
@login_required
def configuration_users_delete(username):
    service.sphirewall().general().users_delete(username)
    return redirect("/device/configuration/users")


@app.route("/device/configuration/users/<username>", methods=["GET", "POST"])
@login_required
def configuration_users_edit(username):
    if request.method == "POST":
        quota = QuotaInfo()
        quota.dailyQuota = checkbox_value("dailyQuota")
        quota.dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))

        quota.weeklyQuota = checkbox_value("weeklyQuota")
        quota.weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))

        quota.monthQuota = checkbox_value("monthQuota")
        quota.monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        quota.timeQuota = checkbox_value("timeQuota")
        quota.timeQuotaLimit = nint(text_value("timeQuotaLimit"))

        quota.totalQuota = checkbox_value("totalQuota")
        quota.totalQuotaLimit = nint(text_value("totalQuotaLimit"))

        #configure groups:
        group_list = request.form.getlist('groups')
        groups = []
        for id in group_list:
            groups.append(id)

        service.sphirewall().general().users_groups_merge(username, groups)
        service.sphirewall().general().users_save(username, request.form["fname"], request.form["lname"],
                                                  request.form["email"], quota)

        if len(request.form["password"]) > 0 and request.form["password"] == request.form["repassword"]:
            service.sphirewall().general().user_set_password(username, request.form["password"])

    return render_template("device_configuration_users_edit.html",
                           user=service.sphirewall().general().users(username),
                           groups=service.sphirewall().general().groups())


@app.route("/device/configuration/users/<username>/disable")
@login_required
def configuration_users_disable(username):
    service.sphirewall().general().user_disable(username)
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/users/<username>/enable")
@login_required
def configuration_users_enable(username):
    service.sphirewall().general().user_enable(username)
    return redirect("/device/configuration/users/" + username)


@login_required
@app.route("/device/configuration/users/<username>/groups/remove/<groupid>", methods=["GET", "POST"])
def configuration_users_remove_group(username, groupid):
    service.sphirewall().general().user_groups_remove(username, groupid)
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/users/<username>/groups/add", methods=["POST"])
@login_required
def configuration_users_add_group(username):
    service.sphirewall().general().user_groups_add(username, request.form["group"])
    return redirect("/device/configuration/users/" + username)


@app.route("/device/configuration/groups", methods=["GET", "POST"])
@login_required
def configuration_groups():
    if request.method == "POST":
        service.sphirewall().general().group_add(request.form["name"])

    unsorted_groups = service.sphirewall().general().groups()
    groups = sorted(unsorted_groups, key=operator.itemgetter("name"))
    return render_template("device_configuration_groups.html", groups=groups)


@app.route("/device/configuration/groups/delete/<id>")
@login_required
def configuration_groups_delete(id):
    service.sphirewall().general().groups_delete(id)
    return redirect("/device/configuration/groups")


@app.route("/device/configuration/groups/<id>", methods=["GET", "POST"])
@login_required
def configuration_groups_edit(id):
    if request.method == "POST":
        quota = QuotaInfo()

        quota.dailyQuota = checkbox_value("dailyQuota")
        quota.dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))

        quota.weeklyQuota = checkbox_value("weeklyQuota")
        quota.weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))

        quota.monthQuota = checkbox_value("monthQuota")
        quota.monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        quota.timeQuota = checkbox_value("timeQuota")
        quota.timeQuotaLimit = nint(text_value("timeQuotaLimit"))

        quota.totalQuota = checkbox_value("totalQuota")
        quota.totalQuotaLimit = nint(text_value("totalQuotaLimit"))

        timeout = text_value("timeout")
        try:
            if timeout is None or timeout == "":
                timeout = -1
            elif timeout.lower() == "disabled":
                timeout = 0
            timeout = int(timeout)
        except ValueError:
            timeout = -1

        service.sphirewall().general().groups_save(id, request.form["manager"], checkbox_value("mui"), request.form["desc"], quota, timeout)

    group = service.sphirewall().general().groups(id)
    if "timeout" not in group or group["timeout"] == -1:
        group["timeout"] = ""

    return render_template("device_configuration_groups_edit.html", group=group)


@app.route("/device/configuration/advanced", methods=["GET", "POST"])
@login_required
def configuration_advanced():
    if request.method == "POST":
        for fieldname, value in request.form.items():
            service.sphirewall().general().advanced(fieldname, nint(value))

    return render_template("device_configuration_advanced.html", vars=service.sphirewall().general().advanced())


@app.route("/device/configuration/quotas", methods=["GET", "POST"])
@login_required
def configuration_quotas():
    if request.method == "POST":
        dailyQuotaLimit = nint(text_value("dailyQuotaLimit"))
        weeklyQuotaLimit = nint(text_value("weeklyQuotaLimit"))
        monthQuotaBillingDay = nint(text_value("monthQuotaBillingDay"))
        monthQuotaLimit = nint(text_value("monthQuotaLimit"))

        service.sphirewall().general().quotas_set(checkbox_value("dailyQuota"),
                                                  dailyQuotaLimit, checkbox_value("weeklyQuota"),
                                                  weeklyQuotaLimit, checkbox_value("monthQuota"),
                                                  monthQuotaBillingDay,
                                                  checkbox_value("monthQuotaIsSmart"), monthQuotaLimit)

    return render_template("device_configuration_quotas.html", settings=service.sphirewall().general().quotas())


@app.route("/device/configuration/watchdog", methods=["GET", "POST"])
@login_required
def configuration_watchdog():
    if request.method == "POST":
        service.sphirewall().general().configuration("watchdog:monitorGoogle", checkbox_value("monitorGoogle"))

    monitorGoogle = service.sphirewall().general().configuration("watchdog:monitorGoogle")
    return render_template("device_configuration_watchdog.html", monitorGoogle=monitorGoogle)


@app.route("/device/configuration/logging", methods=["GET", "POST"])
@login_required
def configuration_logging():
    if request.method == "POST":
        service.sphirewall().general().logging(text_value("context"), nint(text_value("level")))

    logging = service.sphirewall().general().logging()
    return render_template("device_configuration_logging.html", logging=logging,
                           critical=service.sphirewall().general().logging_critical())


@app.route("/device/configuration/logging/<context>/delete", methods=["GET", "POST"])
@login_required
def configuration_logging_delete(context):
    service.sphirewall().general().logging_delete(context)
    return redirect("/device/configuration/logging")


@app.route("/device/configuration/logging/critical", methods=["POST"])
@login_required
def configuration_logging_critical():
    service.sphirewall().general().logging_critical(checkbox_value("critical"))
    return redirect("/device/configuration/logging")


@app.route("/device/configuration/sessions/persist", methods=["POST"])
@login_required
def status_clients_persist():
    service.sphirewall().general().create_persisted_session(text_value("username"), text_value("hw"))
    return redirect("/device/configuration/sessions")


@app.route("/device/configuration/sessions/persist/<username>/<mac>/remove")
@login_required
def status_clients_persist_remove(username, mac):
    service.sphirewall().general().delete_persisted_session(username, mac)
    return redirect("/device/configuration/sessions")


@app.route("/device/configuration/sessions")
@login_required
def configuration_sessions():
    persisted_sessions = service.sphirewall().general().list_persisted_session()
    return render_template("device_configuration_persisted_sessions.html",
                           available_users=service.sphirewall().general().users(),
                           persisted_sessions=persisted_sessions)


@app.route("/device/configuration/authentication/timeouts")
@login_required
def configuration_authentication_timeouts():
    timeouts = service.sphirewall().general().session_network_based_timeouts()
    return render_template("device_configuration_authentication_timeouts.html",
                           networks=service.sphirewall().firewall().aliases(),
                           timeouts=timeouts)


@app.route("/device/configuration/authentication/timeouts/manage", methods=["POST", "GET"])
@app.route("/device/configuration/authentication/timeouts/<id>/manage", methods=["POST", "GET"])
@login_required
def configuration_authentication_timeouts_manage(id=None):
    if request.method == "POST":
        service.sphirewall().general().session_network_based_timeouts_create_or_modify(id, int(text_value("timeout")), request.form.getlist("networks"))
        return redirect("/device/configuration/authentication/timeouts")

    #Find the targetted timeout
    target_timeout = None
    for timeout in service.sphirewall().general().session_network_based_timeouts():
        if timeout["id"] == id:
            target_timeout = timeout
            break

    return render_template(
        "device_configuration_authentication_manage.html",
        timeout=target_timeout,
        networks=service.sphirewall().firewall().aliases()
    )


@app.route("/device/configuration/authentication/timeouts/<id>/delete")
@login_required
def configuration_authentication_timeouts_delete(id):
    service.sphirewall().general().session_network_based_timeouts_remove(id)
    return redirect("/device/configuration/authentication/timeouts")
