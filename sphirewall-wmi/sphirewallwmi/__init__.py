import logging

from flask.app import Flask
import os
from flask.ext.login import LoginManager

app = Flask(__name__)
app.secret_key = "key123"

default_config = os.path.join(app.root_path, "configuration.cfg")
if os.path.exists(default_config):
    app.config.from_pyfile(default_config)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

logging.basicConfig(level=logging.DEBUG)

with app.test_request_context():
    from sphirewallwmi import routes_base
    from sphirewallwmi import routes_login
    from sphirewallwmi import routes_device_configuration
    from sphirewallwmi import routes_device_firewall
    from sphirewallwmi import routes_device_network
    from sphirewallwmi import routes_device_status
    from sphirewallwmi import routes_analytics_ajax
    from sphirewallwmi import routes_device_tools
