#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from datetime import date, timedelta
from flask import request


class DateHelper:
    @staticmethod
    def today():
        return date.today()

    @staticmethod
    def yesterday():
        return date.today() - timedelta(1)

    @staticmethod
    def week():
        return date.today() - timedelta(date.today().weekday())

    @staticmethod
    def month():
        return date.today().replace(day=1)

def nint(value):
    if value is None or len(value) == 0:
        return 0
    else:
        return int(value)


def checkbox_value(key):
    if key in request.form:
        return True
    return False

def text_value(key):
    return request.form.get(key)

def int_value(key, default=-1):
    if key in request.form and len(request.form[key]) > 0:
        return int(request.form[key])
    return default


def arg(key):
    if key in request.args:
        return request.args[key]
    return ""

def get_class(clazz):
    parts = clazz.split('.')
    module = ".".join(parts[:-1])
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m
