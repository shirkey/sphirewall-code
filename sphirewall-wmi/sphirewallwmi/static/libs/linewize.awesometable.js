function AwesomeTable(id, datapath, pagesize, appendregion, childtype, optional_replace_data) {
    this.id = id;
    this.datapath = datapath;
    this.pagesize = pagesize
    this.replaceWith = optional_replace_data || false;
    if (appendregion != undefined) {
        this.appendregion = appendregion
    } else {
        this.appendregion = "tbody";
    }

    if (childtype != undefined) {
        this.childtype = childtype
    } else {
        this.childtype = "tr";
    }
}

AwesomeTable.prototype.offset = function () {
    return parseInt(sessionStorage[this.id + "_offset"]);
}

AwesomeTable.prototype.setOffset = function (offset) {
    sessionStorage[this.id + "_offset"] = offset;
}

AwesomeTable.prototype.getOffsetKey = function () {
    return "offset";
}

AwesomeTable.prototype.getSearchKey = function () {
    return "search";
}

AwesomeTable.prototype.getSearchValue = function () {
    return $(this.id + " .search").val();
}

AwesomeTable.prototype.getPageSizeKey = function () {
    return "limit";
}

AwesomeTable.prototype.getPageSizeValue = function () {
    return this.pagesize;
}

AwesomeTable.prototype.buildUrl = function () {
    var url = "";
    url += this.datapath;
    url += "&";

    url += this.getPageSizeKey() + "=" + this.getPageSizeValue() + "&";
    url += this.getOffsetKey() + "=" + this.offset() + "&";
    url += this.getSearchKey() + "=" + this.getSearchValue() + "&";

    return url;
}

AwesomeTable.prototype.beforeRenderData = function (data) {
}

AwesomeTable.prototype.afterRenderData = function (data) {
}

AwesomeTable.prototype.doRenderData = function (data) {
    $(this.id + " #tabletemplate").tmpl(data).appendTo(this.id + " " + this.appendregion);
}

AwesomeTable.prototype.loadingIndicator = function() {
    return $(this.id + " .loadingindicator");
}

AwesomeTable.prototype.showLoadingIndicator = function(){
    this.loadingIndicator().show();
}

AwesomeTable.prototype.hideLoadingIndicator = function(){
    this.loadingIndicator().hide();
}

AwesomeTable.prototype.nextButton = function(){
    return $(this.id + " .next");
}

AwesomeTable.prototype.prevButton = function(){
    return $(this.id + " .prev");
}

AwesomeTable.prototype.render_websites_table = function () {
    var table_this = this;
    this.showLoadingIndicator();
    if (sessionStorage["offset"] == undefined) {
        sessionStorage["offset"] = 0;
    }

    var clj = function (data) {
        var result_data_points = data.result;
        table_this.beforeRenderData(result_data_points);
        table_this.doRenderData(result_data_points);
        table_this.afterRenderData(result_data_points);

        if (result_data_points.length >= table_this.pagesize) {
            table_this.nextButton().show();
        } else {
            table_this.nextButton().hide();
        }

        if (table_this.offset() !== 0) {
            table_this.prevButton().show();
        } else {
            table_this.prevButton().hide();
        }

        table_this.hideLoadingIndicator();
    };

    if (this.replaceWith !== false) {
        clj(this.replaceWith);
    } else {
        $.getJSON(this.buildUrl(), clj);
    }
}

AwesomeTable.prototype.update = function (datapath) {
    this.setOffset(0);
    var table_this = this;
    table_this.datapath = datapath
    var target_id = table_this.id + " " + table_this.appendregion + " " + table_this.childtype;
    $(table_this.id + " " + table_this.appendregion + " " + table_this.childtype).remove();
    table_this.render_websites_table();
}

AwesomeTable.prototype.init = function () {
    this.setOffset(0);
    var table_this = this;

    this.render_websites_table();
    this.nextButton().click(function () {
        table_this.setOffset(table_this.offset() + table_this.pagesize);
        $(table_this.id + " " + table_this.appendregion + " " + table_this.childtype).remove();
        table_this.render_websites_table();
    });

    this.prevButton().click(function () {
        table_this.setOffset(table_this.offset() - table_this.pagesize);
        $(table_this.id + " " + table_this.appendregion + " " + table_this.childtype).remove();
        table_this.render_websites_table();
    });

    $(table_this.id + " .search").change(function () {
        table_this.setOffset(0);
        $(table_this.id + " " + table_this.appendregion + " " + table_this.childtype).remove();
        table_this.render_websites_table();
    });
    return table_this
}
