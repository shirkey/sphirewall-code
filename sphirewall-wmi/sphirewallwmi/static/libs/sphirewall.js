window.MEGABYTE = 1048576;
window.MEGABIT = 104857;

function setInvisible(elem) {
    var qosElement = document.getElementById(elem);
    qosElement.style.display = 'none';
}

function setVisible(elem) {
    var qosElement = document.getElementById(elem);
    qosElement.style.display = 'block';
}

/*Toggle Tab System*/
function toggleTab(event, contentRootId) {
    var target = event.target;
    var idOfDivTarget = target.getAttribute("data-panelid");

    return toggleTabWithId(contentRootId, idOfDivTarget);
}

function toggleTabWithId(contentRootId, idOfDivTarget) {
    var menuBaseElement = document.getElementById(contentRootId);
    len = menuBaseElement.children.length;
    for (i = 0; i < len; i++) {
        var child = menuBaseElement.children[i];
        setInvisible(child.id);
    }

    setVisible(idOfDivTarget);
    sessionStorage["lastTab-" + contentRootId] = idOfDivTarget;

    var buttonsList = document.getElementsByClassName("tab-button");
    for (i = 0; i < buttonsList.length; i++) {
        var item = buttonsList[i];
        if (item.getAttribute("data-panelid") == idOfDivTarget) {
            item.style.fontWeight = 'bold';
        } else {
            item.style.fontWeight = 'normal';
        }
    }
}

function initToggleTab(contentRootId, defaultOption) {
    if (sessionStorage["lastTab-" + contentRootId] != undefined) {
        toggleTabWithId(contentRootId, sessionStorage["lastTab-" + contentRootId]);
    } else {
        toggleTabWithId(contentRootId, defaultOption);
    }
}

/*Dropdown menu system*/
function dropdownmenu_toggle(dropdownId) {
    var toClear = document.getElementsByClassName("dropdownmenu_options");
    for (var z = 0; z < toClear.length; z++) {
        var tar = toClear[z];
        var parent = tar.parentNode;
        if (parent.id != dropdownId) {
            tar.style.display = "none";
        }
    }

    var dropdowns = $("#" + dropdownId).children()
    for (var x = 0; x < dropdowns.length; x++) {
        var t = dropdowns[x];
        if (t.className == "dropdownmenu_options") {
            if (t.style.display == "block") {
                t.style.display = "none";
            } else {
                t.style.display = "block";
            }

        }
    }
}

function dropdownmenu_init() {
    var dropdowns = $("div.dropdownmenu").children()
    for (var x = 0; x < dropdowns.length; x++) {
        var t = dropdowns[x];
        if (t.className == "dropdownmenu_button") {
            var parent = t.parentNode;
            var parentId = parent.id;

            t.setAttribute('onclick', "dropdownmenu_toggle('" + parentId + "')");
            t.setAttribute('onblur', "dropdownmenu_toggle('" + parentId + "')");

        }
    }
}

function populateField(name, data, method) {
    $.post("index.php?function=api", JSON.stringify(data)).done(function (data) {
        var parse = JSON.parse(data);
        method(parse);
    });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return ""; else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Works out based on a return code whether the analytics service is available, this is helpful for ajax api calls
function checkAnalyticsIsRunning() {
    var strUrl = "index.php?function=api", strReturn = "";
    var data = {request: "analytics/version", args: {}};
    jQuery.ajax({
        url: strUrl,
        success: function (html) {
            strReturn = html;
        },
        type: "post",
        data: JSON.stringify(data),
        async: false
    });

    if (JSON.parse(strReturn).version != undefined) {
        return true;
    }

    var output = JSON.parse(JSON.parse(strReturn));
    if (output.code == -3) {
        return false;
    }

    return true;
}

function formatDate(d) {
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1;
    var curr_year = d.getFullYear();
    return curr_year + "-" + curr_month + "-" + curr_date;
}

function getTimeWithoutMinutes(string) {
    var re = new RegExp("([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+)");
    var m = re.exec(string);
    if (m == null) {
        return new Date(string).getTime();
    } else {
        var date = new Date(m[1] + "/" + m[2] + "/" + "/" + m[3]);
        date.setHours(m[4]);
        return date.getTime();
    }
}

function getTime(string) {
    var re = new RegExp("([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+)");
    var m = re.exec(string);
    if (m == null) {
        return getTimeWithoutMinutes(string);
    } else {
        var date = new Date(m[1] + "/" + m[2] + "/" + "/" + m[3]);
        date.setHours(m[4]);
        date.setMinutes(m[5]);
        date.setSeconds(m[6]);
        return date.getTime();
    }
}

$(function () {
    window.setupDateTimeDecorators = function () {
        var $dateTimes = $(".datetimedecorator");
        for (var i = 0; i < $dateTimes.length; i++) {
            var time = $($dateTimes[i]).html() * 1000;
            var newValue = new Date(time);
            $($dateTimes[i]).html(newValue);
        }

        $(".timedecorator").each(function () {
            var timestamp = $(this).html();
            var d = new Date(timestamp * 1000);

            var year = d.getYear();
            var month = d.getUTCMonth() + 1;
            $(this).html("" + d.getFullYear() + "/" + month + "/" + d.getUTCDate() + " " + d.getUTCHours() + ":" + d.getUTCMinutes());
        });
    }
});

$(function () {
    $(document).tooltip();
});

$(function () {
    function setupMenu() {
        $(".operations").first().addClass("btn");
        $(".operations").first().html("<i class='icon-cog'></i> Actions");
        if ($("#dropdownmenu").children().length == 0) {
            $("#banner-menu").hide();
        } else {
            $(".operations").first().attr("onclick", "");
            $(".operations").first().click(function () {
                var elementById = document.getElementById("dropdownmenu");
                var ob = elementById.style;
                ob.display = (ob.display == 'block') ? 'none' : 'block';
            });
        }
    }

    setupMenu();

    jQuery.fn.dataTableExt.oSort['numeric-comma-asc'] = function (a, b) {
        console.log("trying to sort shit");
        x = parseFloat(a);
        y = parseFloat(b);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    };

    jQuery.fn.dataTableExt.oSort['numeric-comma-desc'] = function (a, b) {
        x = parseFloat(a);
        y = parseFloat(b);
        console.log("sorting: " + a + " and " + b);
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    };


    window.setupDatatables = function () {
        $(".amountdecorator").each(function () {
            var current_value = parseInt($(this).html());

            $(this).html((current_value / window.MEGABYTE).toFixed(3));
        });

        window.setupDateTimeDecorators();
        $.fn.dataTableExt.sErrMode = 'throw';
        $('.list').each(function () {
            var sortby = $(this).attr("data-sortby") == undefined ? 0 : $(this).attr("data-sortby");
            var pagesize = $(this).attr("data-pagesize") == undefined ? 10 : parseInt($(this).attr("data-pagesize"));
            //If the table does not have a paging attribute, we set this value very high:
            if (!$(this).hasClass("paging")) {
                pagesize = 10000;
            }

            var args = {
                "aaSorting": [
                    [sortby, 'desc']
                ],
                "bFilter": $(this).hasClass("filtered"),
                "bSmart": true,
                "bInfo": false,
                "bLengthChange": false,
                "iDisplayLength": pagesize,
                "oLanguage": {
                    "sInfoEmpty": "No entries to show"
                },
                "aoColumnDefs": [
                    {"sType": "numeric-comma", "bSortable": true, "aTargets": [ "numberSort" ] }
                ]
            };

            if ($(this).hasClass("export")) {
                var additional = {
                    "sDom": 'T<"clear">lfrtip',
                    "oTableTools": {
                        "aButtons": [
                            {
                                "sExtends": "collection",
                                "sButtonText": '<i class=icon-download-alt /> <span class="caret" />',
                                "sButtonText": '<i class=icon-download-alt /> <span class="caret" />',
                                "aButtons": [ "csv", "xls", "pdf" ]
                            }
                        ]}
                };

                args = $.extend({}, args, additional);
            }

            $(this).dataTable(args);
            if ($(this).hasClass("export")) {
                var wrapperId = $(this).attr('id') + "_wrapper";
                var selector = "#" + wrapperId + " .DTTT_container";
                $(selector).append("<span class=filter_title>&nbsp;&nbsp;Export/Download</span>")
            }

            if (!$(this).hasClass("paging")) {
                var wrapperId = $(this).attr('id') + "_paginate";
                $("#" + wrapperId).hide();
            }
        });


        $(".hidezerorow .dataTables_empty").hide();
    }
});

var render_metric = function (metric_key, graph_id, converter) {
    $.getJSON("/device/analytics/metrics/" + metric_key, function (dataset) {
        data = [];
        for (var i = 0; i < dataset.result.length; i++) {
            var value = dataset.result[i].value;
            if (converter !== undefined) {
                value = converter(value);
            }

            var items = [new Date(dataset.result[i].time * 1000), value];
            data.push(items);
        }

        $.plot($(graph_id),
            [
                {label: name, data: data}
            ], {xaxis: {mode: 'time'}, lines: {fill: true},
                grid: {
                    borderWidth: 1,
                    borderColor: '#ccc'
                }

            });
    });
};
