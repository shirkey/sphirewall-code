import json
from string import replace
import uuid
from flask.ext.login import login_required

from flask.globals import request, session
from flask.templating import render_template
from flask.wrappers import Response
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import checkbox_value, text_value
from sphirewallwmi.middleware import service


@login_required
@app.route("/device/network/devices")
def network_devices():
    return render_template("device_network_devices.html", routes=service.sphirewall().network().routes(),
                           devices=service.sphirewall().network().devices(configured=True))


@login_required
@app.route("/device/network/devices/<device>/up")
def network_devices_up(device):
    service.sphirewall().network().devices_up(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/down")
def network_devices_down(device):
    service.sphirewall().network().devices_down(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/delete")
def network_devices_delete(device):
    service.sphirewall().network().devices_remove(device)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/create", methods=["GET", "POST"])
@app.route("/device/network/devices/<device>", methods=["GET", "POST"])
def network_devices_edit(device=None):
    if request.method == "POST":
        device_params = {}

        existing_device_configuration = None
        if device:
            existing_device_configuration = service.sphirewall().network().devices(device, configured=True)

        device_params["name"] = text_value("name")
        device_params["persisted"] = checkbox_value("persisted")
        if text_value("type") == "physical":
            device_params["physical"] = True
            device_params["physicalInterface"] = text_value("device")

        if text_value("type") == "vlan":
            device_params["vlan"] = True
            device_params["vlanId"] = int(text_value("vlanId"))
            device_params["vlanInterface"] = text_value("vlanInterface")

        if text_value("type") == "bridge" or (existing_device_configuration and existing_device_configuration.get("bridge")):
            device_params["bridge"] = True
            device_params["bridgeDevices"] = request.form.getlist('bridgeDevices')

        if text_value("type") == "lacp" or (existing_device_configuration and existing_device_configuration.get("lacp")):
            device_params["lacp"] = True
            device_params["lacpDevices"] = request.form.getlist('lacpDevices')

            device_params["lacpMode"] = int(text_value("lacpMode", "0", False))
            device_params["lacpLinkStatePollFrequency"] = int(text_value("lacpLinkStatePollFrequency", "0", False))
            device_params["lacpLinkStateDownDelay"] = int(text_value("lacpLinkStateDownDelay", "0", False))
            device_params["lacpLinkStateUpDelay"] = int(text_value("lacpLinkStateUpDelay", "0", False))

        ipv4_addresses = []
        if text_value("aliases"):
            for raw_address in text_value("aliases").split("\n"):
                if raw_address and len(raw_address.split("/")) == 3:
                    address = {
                        "ip": raw_address.split("/")[0].strip(),
                        "mask": raw_address.split("/")[1].strip(),
                        "broadcast": raw_address.split("/")[2].strip()
                    }
                    ipv4_addresses.append(address)

        address_params = {
            "ipv4": ipv4_addresses,
            "dhcp": checkbox_value("dhcp"),
            "gateway": text_value("gateway"),
            "dhcpServerEnabled": checkbox_value("dhcpServerEnabled"),
            "dhcpServerStart": text_value("dhcpServerStart"),
            "dhcpServerEnd": text_value("dhcpServerEnd")
        }

        service.sphirewall().network().devices_set(device, device_params=device_params, address_params=address_params)
        return redirect("/device/network/devices")

    current_device_stored = None
    if device:
        current_device_stored = service.sphirewall().network().devices(device, configured=True)
    return render_template("device_network_devices_edit.html", device=current_device_stored,
                           devices=service.sphirewall().network().devices(configured=True))


@login_required
@app.route("/device/network/devices/save")
def network_devices_save():
    service.sphirewall().network().devices_save()
    return redirect("device/network/devices")


@login_required
@app.route("/device/network/devices/addbridge")
def network_devices_createbridge():
    service.sphirewall().network().devices_createbridge()
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/<device>/aliases", methods=["POST"])
def network_devices_aliases(device):
    service.sphirewall().network().devices_aliases_add(device, request.form["alias"]);
    return redirect("/device/network/devices/" + device)


@login_required
@app.route("/device/network/devices/<device>/aliases/delete/<alias>")
def network_devices_aliases_delete(device, alias):
    service.sphirewall().network().devices_aliases_del(device, alias)
    return redirect("/device/network/devices/" + device)


@login_required
@app.route("/device/network/devices/<device>/leases", methods=["GET", "POST"])
def network_devices_dhcp(device):
    if request.method == "POST":
        service.sphirewall().network().devices_dhcp_static_add(device, text_value("ip"), text_value("mac"))
    device = service.sphirewall().network().devices(device=device, configured=True)
    return render_template("device_network_devices_edit_dhcp.html", device=device)


@login_required
@app.route("/device/network/devices/<device>/leases/delete/<ip>/<mac>", methods=["GET"])
def network_devices_dhcp_lease_delete(device, ip, mac):
    service.sphirewall().network().devices_dhcp_static_del(device, ip, mac)
    return redirect("/device/network/devices/%s/leases" % device)


@login_required
@app.route("/device/network/devices/routes", methods=["GET", "POST"])
def network_devices_routes():
    service.sphirewall().network().routes_add(text_value("destination"), text_value("destination_cidr"), text_value("route_device"), text_value("route_nexthop"))
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/devices/routes/del/<id>", methods=["GET"])
def network_devices_routes_del(id):
    service.sphirewall().network().routes_del(id)
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/connections", methods=["GET", "POST"])
def network_connections():
    if request.method == "POST":
        service.sphirewall().network().connections_add(text_value("name"), text_value("type"))

    return render_template("device_network_connections.html", connections=service.sphirewall().network().connections())


@login_required
@app.route("/device/network/connections/delete/<name>", methods=["GET"])
def network_connections_del(name):
    service.sphirewall().network().connections_del(name)
    return redirect("/device/network/connections")


@login_required
@app.route("/device/network/connections/<name>", methods=["GET", "POST"])
def network_connections_edit(name):
    connection = service.sphirewall().network().connections(name)
    if request.method == "POST":
        if connection["type"] == 0:
            service.sphirewall().network().connections_pppoe_save(name, text_value("username"),
                                                                  text_value("password"), text_value("authenticationType"), text_value("device"))
        if connection["type"] == 1:
            service.sphirewall().network().connections_openvpn_save(name, text_value("keyfile"))

    connection = service.sphirewall().network().connections(name)
    return render_template("device_network_connection_edit.html", connection=connection,
                           log=replace(connection["log"], "\n", "<br>"))


@login_required
@app.route("/device/network/connections/<name>/connect")
def network_connections_connect(name):
    service.sphirewall().network().connection_connect(name)
    return redirect("/device/network/connections/" + name)


@login_required
@app.route("/device/network/connections/<name>/disconnect")
def network_connections_disconnect(name):
    service.sphirewall().network().connection_disconnect(name)
    return redirect("/device/network/connections/" + name)


@login_required
@app.route("/device/network/dns", methods=["GET", "POST"])
def network_dns():
    if request.method == "POST":
        service.sphirewall().general().configuration("dnsmasq:forwarding", checkbox_value("enabled"))
        service.sphirewall().general().configuration("dnsmasq:domain", text_value("domain"))
        service.sphirewall().general().configuration("dnsmasq:primary", text_value("primary"))
        service.sphirewall().general().configuration("dnsmasq:secondary", text_value("secondary"))

        service.sphirewall().network().dnsmasq_stop()
        service.sphirewall().network().dnsmasq_publish()
        service.sphirewall().network().dnsmasq_start()

    enabled = service.sphirewall().general().configuration("dnsmasq:forwarding")
    domain = service.sphirewall().general().configuration("dnsmasq:domain")
    primary = service.sphirewall().general().configuration("dnsmasq:primary")
    secondary = service.sphirewall().general().configuration("dnsmasq:secondary")

    return render_template("device_network_dns.html", enabled=enabled, domain=domain, primary=primary, secondary=secondary,
                           running=service.sphirewall().network().dnsmasq_running())


@login_required
@app.route("/device/network/vpn", methods=["GET", "POST"])
def network_vpn():
    if request.method == "POST":
        if text_value("type") == "static":
            service.sphirewall().network().vpns_openvpnstatic_create(text_value("name"))
            return redirect("/device/network/vpn/%s" % text_value("name"))
        if text_value("type") == "tls":
            return redirect("/device/network/vpn/tls/%s/create" % text_value("name"))
        if text_value("type") == "ipsec_gateway_to_gateway":
            service.sphirewall().network().vpns_ipsec_create(text_value("name"))
            return redirect("/device/network/vpn/%s" % text_value("name"))

    return render_template("device_network_vpn.html", vpns=service.sphirewall().network().vpns())


@login_required
@app.route("/device/network/vpn/tls/<name>/create", methods=["GET", "POST"])
def network_vpn_tls_create(name):
    if request.method == "POST":
        service.sphirewall().network().vpns_openvpntls_create(
            name, text_value("certcountry"), text_value("certcity"), text_value("certprovince"), text_value("certorg"), text_value("certemail")
        )
        return redirect("/device/network/vpn/%s" % name)
    return render_template("device_network_vpn_tls_create.html")


@login_required
@app.route("/device/network/vpn/<name>/delete", methods=["GET", "POST"])
def network_vpn_delete(name):
    service.sphirewall().network().vpns_delete(name)
    return redirect("/device/network/vpn")


def network_vpn_edit_openvpn_static_post(name):
    remoteServiceIp = request.form["remoteserverip"]
    remotePort = request.form["remoteport"]
    lzoCompression = checkbox_value("compression")
    customOptions = request.form["customopts"]

    serverIp = request.form["serverip"]
    clientIp = request.form["clientip"]
    service.sphirewall().network().vpns_static_save(
        name, remoteServiceIp, remotePort, lzoCompression, customOptions, serverIp, clientIp
    )


def network_vpn_edit_openvpn_tls_post(name):
    remoteServiceIp = request.form["remoteserverip"]
    remotePort = request.form["remoteport"]
    lzoCompression = checkbox_value("compression")
    customOptions = request.form["customopts"]

    serverIp = request.form["serverip"]
    serverMask = request.form["servermask"]
    service.sphirewall().network().vpns_tls_save(
        name, remoteServiceIp, remotePort, lzoCompression, customOptions, serverIp, serverMask
    )


def network_vpn_edit_ipsec_gw_gw_post(name, mode):
    if mode == "manual":
        options = {
            "local_host": text_value("local_host"),
            "local_subnet": text_value("local_subnet"),
            "local_id": text_value("local_id"),
            "remote_host": text_value("remote_host"),
            "remote_subnet": text_value("remote_subnet"),
            "remote_id": text_value("remote_id"),

            "auth_mode": int(text_value("auth_mode")),
            "secret_key": text_value("secret_key"),
            "auto_start": checkbox_value("auto_start"),

            "meta": json.dumps({"cloud_type": "manual"})
        }
        service.sphirewall().network().vpns_options(name, options)


@login_required
@app.route("/device/network/vpn/<name>", methods=["GET", "POST"])
def network_vpn_edit(name):
    vpn = service.sphirewall().network().vpns(name)
    if request.method == "POST":
        if vpn["type"] == 1:
            network_vpn_edit_openvpn_static_post(name)
        if vpn["type"] == 2:
            network_vpn_edit_ipsec_gw_gw_post(name, text_value("mode"))
        if vpn["type"] == 3:
            network_vpn_edit_openvpn_tls_post(name)

    vpn = service.sphirewall().network().vpns(name)
    network_devices = service.sphirewall().network().devices()
    if vpn["type"] == 1:
        return render_template(
            "device_network_vpn_edit_openvpn_static.html", log=replace(vpn["log"], "\n", "<br>"), vpn=vpn
        )
    elif vpn["type"] == 3:
        return render_template(
            "device_network_vpn_edit_openvpn_tls.html", log=replace(vpn["log"], "\n", "<br>"), vpn=vpn,
            clients=service.sphirewall().network().vpns_clients(name)["clients"]
        )
    elif vpn["type"] == 2:
        if vpn.get("meta") and len(vpn.get("meta")) > 0:
            vpn["meta"] = json.loads(vpn["meta"])

        return render_template(
            "device_network_vpn_edit_ipsec.html", vpn=vpn,
            log=replace(vpn["log"], "\n", "<br>"), local_devices=network_devices
        )


@login_required
@app.route("/device/network/vpn/<name>/client")
@app.route("/device/network/vpn/<name>/client/<client_name>")
def network_vpn_clientconfig(name, client_name=None):
    vpn = service.sphirewall().network().vpns(name)
    config_string = ""
    if vpn["type"] == 1:
        client_name = "%s_configuration" % name
        config_string += service.sphirewall().network().vpns_clients(name)["key"]
    if vpn["type"] == 3:
        config_string += service.sphirewall().network().vpns_clients(name, client_name)["conf"]

    return Response(config_string, mimetype="text/plain", headers={"Content-Disposition": "attachment;filename=" + client_name + ".conf"})


@login_required
@app.route("/device/network/vpn/<name>/client", methods=["POST"])
def network_vpn_client_create(name):
    service.sphirewall().network().vpns_clients_add(name, text_value("clientname"))
    return redirect("/device/network/vpn/" + name)


@login_required
@app.route("/device/network/vpn/<name>/client/<client>/delete")
def network_vpn_client_delete(name, client):
    service.sphirewall().network().vpns_clients_delete(name, client)
    return redirect("/device/network/vpn/" + name)


@login_required
@app.route("/device/network/vpn/<name>/start")
def network_vpn_start(name):
    vpn = service.sphirewall().network().vpns(name)

    if "meta" in vpn:
        metadata = json.loads(vpn["meta"])
        if metadata.get("cloud_type") == "automatic":
            service.sphirewall(metadata.get("remote_deviceid")).network().vpns_start(name)

    service.sphirewall().network().vpns_start(name)
    return redirect("/device/network/vpn/" + name)


@login_required
@app.route("/device/network/vpn/<name>/stop")
def network_vpn_stop(name):
    vpn = service.sphirewall().network().vpns(name)
    if "meta" in vpn:
        metadata = json.loads(vpn["meta"])
        if metadata.get("cloud_type") == "automatic":
            service.sphirewall(metadata.get("remote_deviceid")).network().vpns_stop(name)

    service.sphirewall().network().vpns_stop(name)
    return redirect("/device/network/vpn/" + name)


@login_required
@app.route("/device/network/devices/publish")
def network_devices_publish():
    service.sphirewall().network().devices_publish()
    return redirect("/device/network/devices")


@login_required
@app.route("/device/network/wireless", methods=["GET", "POST"])
def network_wireless():
    if request.method == "POST":
        service.sphirewall().general().configuration("wireless:enabled", checkbox_value("enabled"))
        service.sphirewall().general().configuration("wireless:interface", text_value("interface"))
        service.sphirewall().general().configuration("wireless:ssid", text_value("ssid"))
        service.sphirewall().general().configuration("wireless:channel", int(text_value("channel")))
        service.sphirewall().general().configuration("wireless:driver", text_value("driver"))

        if checkbox_value("enabled"):
            service.sphirewall().network().wireless_start()
        else:
            service.sphirewall().network().wireless_stop()

    enabled = service.sphirewall().general().configuration("wireless:enabled")
    interface = service.sphirewall().general().configuration("wireless:interface")
    channel = service.sphirewall().general().configuration("wireless:channel")
    ssid = service.sphirewall().general().configuration("wireless:ssid")
    driver = service.sphirewall().general().configuration("wireless:driver")
    online = service.sphirewall().network().wireless_online()
    devices = service.sphirewall().network().devices()

    return render_template("device_network_wireless.html", enabled=enabled, interface=interface, channel=channel, ssid=ssid,
                           online=online, driver=driver, devices=devices)


@login_required
@app.route("/device/network/dyndns", methods=["GET", "POST"])
def network_dyndns():
    if request.method == "POST":
        service.sphirewall().network().dyndns_configure(checkbox_value("enabled"), text_value("username"),
                                                        text_value("password"), text_value("domain"), text_value("type"))

    return render_template("device_network_dyndns.html", config=service.sphirewall().network().dyndns_get())
