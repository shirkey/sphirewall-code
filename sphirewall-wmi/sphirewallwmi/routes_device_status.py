import json
from string import replace
from flask.ext.login import login_required

from flask.globals import request
from flask.templating import render_template
from werkzeug.utils import redirect

from sphirewallwmi import app
from sphirewallwmi.api.utils import DateHelper, checkbox_value, text_value
from sphirewallwmi.middleware import service


@login_required
@app.route('/device')
def main():
    return render_template(
        'device_home.html',
        totalUsers=len(service.sphirewall().general().active_users_list()),
        totalHosts=service.sphirewall().general().hosts_size(),
        totalConnection=service.sphirewall().firewall().connections_size(),
        version=service.sphirewall().version(),
        events=service.sphirewall().general().events_size(),
        hostname=service.sphirewall().get_hostname(), port=service.sphirewall().get_port(),
        token=service.sphirewall().get_token())


@login_required
@app.route("/device/status/events")
def status_events():
    return render_template("device_status_events.html", params=process_params())


@app.route("/device/status/users")
@login_required
def status_users():
    return render_template("device_status_activeusers.html", params=process_params())


@app.route("/device/status/hosts")
@login_required
def status_hosts():
    return render_template("device_status_activehosts.html", params=process_params())


@app.route("/device/status/hosts/<mac>/<ip>", methods=["GET", "POST"])
@login_required
def status_hosts_edit(mac, ip):
    if request.method == "POST":
        service.sphirewall().general().host_quarantine(mac, ip, checkbox_value("quarantined"))
    host = service.sphirewall().general().host(mac, ip)
    return render_template("device_status_activehosts_edit.html", host=host)


@login_required
@app.route("/device/status/clients/disconnect/<address>")
def status_clients_disconnect(address):
    service.sphirewall().general().disconnect_session(address)
    return redirect("/device/status/clients")


@login_required
@app.route("/device/status/connections")
def status_connections():
    return render_template("device_status_connections.html", params=process_params())


@login_required
@app.route("/device/status/connections/terminate/<sourceIp>/<sourcePort>/<destIp>/<destPort>")
def status_connections_terminate(sourceIp, sourcePort, destIp, destPort):
    service.sphirewall().firewall().connection_terminate(sourceIp, sourcePort, destIp, destPort)
    return redirect("/status/connections")


@login_required
@app.route("/device/status/reporting/address", methods=["POST", "GET"])
def status_reporting_address():
    return render_template("device_status_reporting_address.html", params=process_params())


@login_required
@app.route("/device/status/reporting")
def status_reporting():
    return render_template("device_status_reporting_summary.html", params=process_params())


@login_required
@app.route("/device/status/reporting/address/<mac>/<ip>")
def status_reporting_address_spec(mac, ip):
    return render_template("device_status_reporting_address_spec.html", mac=mac, ip=ip, params=process_params())


def safedivide(numerator, divisor):
    if divisor == 0:
        return 0
    return numerator / divisor


@login_required
@app.route("/device/status/reporting/websites")
def status_reporting_website():
    filter_mac = None
    filter_ip = None
    filter_user = None

    if "filter_mac" in request.args:
        filter_mac = request.args["filter_mac"]
    if "filter_ip" in request.args:
        filter_ip = request.args["filter_ip"]
    if "filter_user" in request.args:
        filter_user = request.args["filter_user"]

    return render_template("device_status_reporting_websites.html", filter_mac=filter_mac, filter_user=filter_user, filter_ip=filter_ip, params=process_params())


@login_required
@app.route("/device/status/reporting/users")
def status_reporting_users():
    return render_template("device_status_reporting_users.html", params=process_params())


@login_required
@app.route("/device/status/reporting/users/<user>")
def status_reporting_users_spec(user):
    return render_template("device_status_reporting_user_spec.html", user=user, params=process_params())


@login_required
@app.route("/device/status/reporting/types")
def status_reporting_types():
    filter_mac = None
    filter_ip = None
    filter_user = None

    if "filter_mac" in request.args:
        filter_mac = request.args["filter_mac"]
    if "filter_ip" in request.args:
        filter_ip = request.args["filter_ip"]
    if "filter_user" in request.args:
        filter_user = request.args["filter_user"]

    return render_template("device_status_reporting_types.html", filter_mac=filter_mac, filter_user=filter_user, filter_ip=filter_ip, params=process_params())


@login_required
@app.route("/device/status/reporting/types/<type>")
def status_reporting_types_spec(type):
    category = service.get_category_description(type)
    return render_template("device_status_reporting_types_spec.html", item=type, type_description=category, params=process_params())


@login_required
@app.route("/device/status/reporting/websites/<website>")
def status_reporting_websites_spec(website):
    filter_mac = None
    filter_ip = None
    filter_user = None

    if "filter_mac" in request.args:
        filter_mac = request.args["filter_mac"]
    if "filter_ip" in request.args:
        filter_ip = request.args["filter_ip"]
    if "filter_user" in request.args:
        filter_user = request.args["filter_user"]

    return render_template("device_status_reporting_websites_spec.html", params=process_params(), website=website, filter_mac=filter_mac, filter_ip=filter_ip, filter_user=filter_user)


@login_required
@app.route("/device/status/metrics")
def status_metrics():
    params = process_params()
    return render_template(
        "device_status_metrics.html", params=params)


@login_required
@app.route("/device/status/metrics/all")
def status_metrics_all():
    params = process_params()
    return render_template("device_status_metrics_all.html", params=params)


@login_required
@app.route("/device/status/logs", methods=["POST", "GET"])
def status_logs():
    filter = text_value("filter")
    if filter and len(filter) > 0:
        general__logs = service.sphirewall().general().logs(filter=filter)
    else:
        general__logs = service.sphirewall().general().logs()

    return render_template("device_status_logs.html", logs=replace(general__logs, "\n", "<br>"), filter=filter)


@login_required
@app.route("/device/status/connections/states")
def status_connections_states():
    return render_template("device_status_connections_states.html")


def process_params():
    params = {}
    if "startDate" in request.args and len(request.args["startDate"]) > 0:
        params["startDate"] = request.args["startDate"]
    elif cookie_value("startDate") is not None:
        params["startDate"] = cookie_value("startDate")
    else:
        params["startDate"] = DateHelper.yesterday().isoformat()

    if "endDate" in request.args and len(request.args["endDate"]) > 0:
        params["endDate"] = request.args["endDate"]
    elif cookie_value("endDate") is not None:
        params["endDate"] = cookie_value("endDate")
    else:
        params["endDate"] = DateHelper.today().isoformat()

    if "offset" in request.args:
        params["offset"] = request.args["offset"]
    if "limit" in request.args:
        params["limit"] = request.args["limit"]
    if "search" in request.args:
        params["search"] = request.args["search"]

    return params


def cookie_value(key):
    return request.cookies.get(key)
