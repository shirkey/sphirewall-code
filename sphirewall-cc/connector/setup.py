from setuptools import setup, find_packages

setup(
    name='sphirewallcc',
    version='0.9.9.68',
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=['Flask', 'gunicorn', 'sphirewallapi', 'simplejson']
)

