from flask import request, render_template, redirect, flash
import flask
from flask.ext.login import login_required, logout_user, login_user
from sphirewallapi.sphirewall_api import SphirewallClient
from sphirewallapi.sphirewall_connection import SessionTimeoutException, ApiError
from sphirewallconnector import app, login_manager
from sphirewallconnector import get_sphirewall_client


class FakeUser:
    token = None

    def __init__(self, token):
        self.token = token

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.token


@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        username = "admin"
        password = request.form["password"]
        token = get_sphirewall_client().connection.authenticate(username=username, password=password)
        if token:
            setattr(flask.g, "current_token", token)
            login_user(FakeUser(token))
            return redirect("/")

        flash("Login failed, invalid password")
    return render_template("login.html")


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/")


@login_manager.user_loader
def load_user(userid):
    if not hasattr(flask.g, "current_token"):
        token = userid
        setattr(flask.g, "current_token", token)
    return FakeUser(flask.g.current_token)


@app.errorhandler(SessionTimeoutException)
def error_handlers_session_timedout(e):
    setattr(flask.g, "current_token", None)
    logout_user()
    return redirect("/")


@app.errorhandler(ApiError)
def error_handlers_generic(e):
    return render_template("error.html", error=e.message)


@app.errorhandler(500)
def error_handlers_generic(e):
    return render_template("error.html", error=str(e))