from flask import Flask
from flask.ext.login import LoginManager
from sphirewallapi.sphirewall_api import SphirewallClient

__author__ = 'michaellawson'

app = Flask(__name__)
app.secret_key = "1234"

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

HOST, PORT = "localhost", 8001

def get_sphirewall_client(token=None):
    return SphirewallClient(hostname=HOST, port=PORT, token=token)

from sphirewallconnector import routes
from sphirewallconnector import routes_authentication
