#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

# sautorun is a small daemon that monitors udev for new block devices and 
# mounts them and attempts to run scli2 commands from autorun.sphirewall 
# in the root directory

#
# autorun.sphirewall sytax
# -- {MOUNT_POINT} == The uuid/random drive mount point

import uuid
import os.path
import logging
import pyudev
import subprocess 

logger = logging.getLogger('sautorun')
hdlr = logging.FileHandler('/var/log/sautorun.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

def mount_partition(partition_name):
	partition_mount_point = str(uuid.uuid4())
	logger.info("creating mount directory %s" % partition_mount_point)
	subprocess.check_call(["mkdir", "/mnt/%s" % partition_mount_point])

	logger.info("mounting %s on %s " % (partition_name, partition_mount_point))
	subprocess.check_call(["mount", partition_name, "/mnt/%s" % partition_mount_point])
	return "/mnt/%s" % partition_mount_point

def autorun(partition_mount_point):
	logger.info("searching for autorun.sphirewall")
	autorun_filename = "%s/autorun.sphirewall" % partition_mount_point

	logger.info("attempting to open %s" % autorun_filename)
	if os.path.isfile(autorun_filename):
		fp = open(autorun_filename)
		for line in fp.readlines():
			trimmed_line = line.strip()
			trimmed_line = trimmed_line.replace("{MOUNT_POINT}", partition_mount_point)
			logger.info("running scli2 %s" % trimmed_line)
			os.system("scli2 %s" % trimmed_line)

def umount_partition(partition_name):
	subprocess.check_call(["umount", partition_name])

def beep_started():
	os.system("beep -l 1000")

def beep_finished():
	os.system("beep -l 1000 -r 5")

def handle_event(action, device):
	if action == "add" and device.get("DEVTYPE") == "partition":
		logger.info("found a partition we can mount %s" % (device["DEVNAME"]))
                beep_started()
                partition_mount_point = mount_partition(device["DEVNAME"])
                autorun(partition_mount_point)
                umount_partition(partition_mount_point)
                beep_finished()
		logger.info("finished processing %s" % (device["DEVNAME"]))


def main():
	logger.info("starting sautorun udev listener")
	context = pyudev.Context()
	monitor = pyudev.Monitor.from_netlink(context)
	monitor.filter_by(subsystem='block')
	for action, device in monitor:
		handle_event(action, device)

if __name__ == "__main__": 
	main()

