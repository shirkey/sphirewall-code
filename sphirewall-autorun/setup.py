from setuptools import setup

setup(
    name='sautorun',
    version='0.9.9.68',
    py_modules=['sautorun'],
    install_requires=[
	'pyudev', 'scli2'
    ],
    entry_points='''
        [console_scripts]
        sautorun-runner=sautorun:main
    ''',
)
