#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class QuotaInfo:
    dailyQuota = False
    dailyQuotaLimit = 0
    weeklyQuota = False
    weeklyQuotaLimit = 0
    monthQuota = False
    monthQuotaLimit = 0
    totalQuota = False
    totalQuotaLimit = 0
    timeQuota = False
    timeQuotaLimit = 0


class GeneralSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def events_list(self, timestamp=None):
        args = {}
        if timestamp:
            args["timestamp"] = timestamp
        return self.connection.request("events/list", args)["events"]

    def events_size(self):
        return self.connection.request("events/size", None)["size"]

    def hosts_list(self):
        return self.connection.request("network/arp/list", None)["hosts"]

    def hosts_size(self):
        return self.connection.request("network/arp/size", None)["size"]

    def host(self, mac, ip):
        all_active_hosts = self.hosts_list()
        for host in all_active_hosts:
            if host["host"] == ip and host["hw"] == mac:
                return host
        return None

    def host_quarantine(self, mac, ip, quarantined):
       self.connection.request("network/arp/quarantined", {"mac": mac, "ip": ip, "quarantined": quarantined})

    def active_users_list(self):
        return self.connection.request("auth/sessions/list", None)["sessions"]

    def list_persisted_session(self):
        return self.connection.request("auth/sessions/list", None)["persisted"]

    def create_persisted_session(self, username, mac):
        self.connection.request("auth/sessions/persist", {'username': username, 'hw': mac})

    def delete_persisted_session(self, username, mac):
        self.connection.request("auth/sessions/persist/remove", {'username': username, 'hw': mac})

    def configuration(self, key, value=None):
        if value is not None:
            arr = {"key": key, "value": value}
            self.connection.request("general/config/set", arr)
        else:
            arr = {"key": key, "hideExceptions": True}
            retval = self.connection.request("general/config/get", arr)

            if retval is not None:
                return retval["value"]
            return False

    def configuration_dump(self):
        return self.connection.request("general/config", {})["dump"]

    def configuration_load(self, snapshot):
        return self.connection.request("general/config/load", {"snapshot": snapshot})

    def event_handler(self, eventType=None, handler=None):
        if eventType is not None and handler is not None:
            handlers = self.connection.request("events/config/list", None)["handlers"]
            for h in handlers:
                if h["key"] == eventType and h["handler"] == handler:
                    return h
        else:
            return self.connection.request("events/config/list", None)["handlers"]

    def event_handler_add(self, eventType, handlerType, data=None):
        args = {}

        if data is not None:
            for dkey in data.iterkeys():
                k = dkey
                v = data[k]
                args[k] = v

        args["key"] = eventType
        args["handler"] = handlerType
        return self.connection.request("events/config/add", args)

    def event_handler_delete(self, eventType, handlerType):
        args = {"key": eventType, "handler": handlerType}
        return self.connection.request("events/config/del", args)

    def event_handler_exists(self, eventType, handlerType):
        handlers = self.event_handler()
        for handler in handlers:
            if handler["key"] == eventType and handler["handler"] == handlerType:
                return True

        return False

    def groups(self, groupid=None):
        if groupid is not None:
            allGroups = self.groups()
            for group in allGroups:
                if group["id"] == int(groupid):
                    return group

        return self.connection.request("auth/groups/list", None)["groups"]

    def groups_get_by_name(self, name):
        if name is not None:
            allGroups = self.groups()
            for group in allGroups:
                if group["name"] == name:
                    return group
        return None

    def users(self, username=None):
        if username is not None:
            return self.connection.request("auth/users/get", {"username": username})
        return self.connection.request("auth/users/list", None)["users"]

    def users_add(self, username, firstName, lastName):
        args = {"username": username}
        self.connection.request("auth/users/add", args)

    def users_delete(self, username):
        args = {"username": username}
        self.connection.request("auth/users/del", args)

    def users_save(self, username, fname, lname, email, quota, temp_user=False, expiry_timestamp=-1):
        args = {"username": username,
                "fname": fname,
                "lname": lname,
                "email": email,
                "usePam": False,
                "quota": 0,
                "temp_user": temp_user,
                "expiry_timestamp": expiry_timestamp,
                "dailyQuota": quota.dailyQuota,
                "dailyQuotaLimit": quota.dailyQuotaLimit,
                "weeklyQuota": quota.weeklyQuota,
                "weeklyQuotaLimit": quota.weeklyQuotaLimit,
                "monthQuota": quota.monthQuota,
                "monthQuotaLimit": quota.monthQuotaLimit,
                "totalQuota": quota.totalQuota,
                "totalQuotaLimit": quota.totalQuotaLimit,
                "timeQuota": quota.timeQuota,
                "timeQuotaLimit": quota.timeQuotaLimit}
        self.connection.request("auth/users/save", args)

    def user_set_password(self, username, password):
        args = {"username": username, "password": password}
        self.connection.request("auth/users/setpassword", args)

    def group_add(self, name, timeout=-1):
        args = {"name": name, "timeout": timeout}
        self.connection.request("auth/groups/create", args)

    def groups_delete(self, id=None, name=None):
        args = {}
        if id is not None:
            args["id"] = int(id)
        if name is not None:
            args["name"] = name
        self.connection.request("auth/groups/del", args)

    def groups_create_bulk(self, groups):
        self.connection.request("auth/groups/create/bulk", {"values": groups})

    def user_createormodify(self, username, firstname, lastname, password, groups):
        self.connection.request("auth/user/merge",
                                {"username": username, "fname": firstname, "lname": lastname, "password": password, "groups": groups})

    def groups_save(self, groupId, manager, isAdmin, description, quota, sessionTimeout=-1, metadata=""):
        args = {
            "id": int(groupId),
            "desc": description,
            "manager": manager,
            "mui": isAdmin,
            "dailyQuota": quota.dailyQuota,
            "dailyQuotaLimit": quota.dailyQuotaLimit,
            "weeklyQuota": quota.weeklyQuota,
            "weeklyQuotaLimit": quota.weeklyQuotaLimit,
            "monthQuota": quota.monthQuota,
            "monthQuotaLimit": quota.monthQuotaLimit,
            "totalQuota": quota.totalQuota,
            "totalQuotaLimit": quota.totalQuotaLimit,
            "timeQuota": quota.timeQuota,
            "timeQuotaLimit": quota.timeQuotaLimit,
            "timeout": int(sessionTimeout),
            "metadata": metadata
        }

        self.connection.request("auth/groups/save", args)

    def disconnect_session(self, address):
        args = {"ipaddress": address}
        self.connection.request("auth/logout", args)

    def events_purge(self):
        self.connection.request("events/purge", None)

    def smtp_test(self):
        self.connection.request("general/smtp/testconnection", None)

    def smtp_publish(self):
        self.connection.request("general/smtp/publish", None)

    def users_groups_merge(self, username, groups):
        args = {"username": username, "groups": groups}
        self.connection.request("auth/groups/mergeoveruser", args)

    def user_groups_add(self, username, groupid):
        args = {"username": username, "group": int(groupid)}
        self.connection.request("auth/users/groups/add", args)

    def user_groups_remove(self, username, groupid):
        args = {"username": username, "group": int(groupid)}
        self.connection.request("auth/users/groups/del", args)

    def user_quotas(self, username):
        args = {"username": username}
        return self.connection.request("auth/users/quotas/list", args)["items"]

    def user_quotas_add(self, username, period, limit):
        args = {"username": username, "period": period, "quotaLimit": int(limit)}
        self.connection.request("auth/users/quotas/set", args)

    def user_quotas_del(self, username, period):
        args = {"username": username, "period": period}
        self.connection.request("auth/users/quotas/del", args)

    def user_enable(self, username):
        args = {"username": username}
        self.connection.request("auth/users/enable", args)

    def user_disable(self, username):
        args = {"username": username}
        self.connection.request("auth/users/disable", args)

    def advanced(self, key=None, value=None):
        if key is None and value is None:
            args = {}
            return self.connection.request("general/runtime/list", args)["keys"]
        else:
            args = {"key": key, "value": value}
            self.connection.request("general/runtime/set", args)

    def advanced_value(self, key):
        all_items = self.advanced()
        for item in all_items:
            if item["key"] == key:
                return item["value"]

        return None

    def quotas(self):
        args = {}
        return self.connection.request("general/quotas", args)

    def quotas_set(self, dailyQuota, dailyQuotaLimit, weeklyQuota, weeklyQuotaLimit, monthQuota, monthQuotaBillingDay,
                   monthQuotaIsSmart, monthQuotaLimit):
        args = {}
        args["dailyQuota"] = dailyQuota
        args["dailyQuotaLimit"] = dailyQuotaLimit
        args["weeklyQuota"] = weeklyQuota
        args["weeklyQuotaLimit"] = weeklyQuotaLimit

        args["monthQuota"] = monthQuota
        args["monthQuotaBillingDay"] = monthQuotaBillingDay
        args["monthQuotaIsSmart"] = monthQuotaIsSmart
        args["monthQuotaLimit"] = monthQuotaLimit

        return self.connection.request("general/quotas/set", args)

    def logs(self, filter=None):
        args = {}
        if filter is not None:
            args["filter"] = filter
        return self.connection.request("general/logs", args)["log"]

    def defaultpasswordset(self):
        return self.connection.request("auth/users/defaultpasswordset", None)["value"]

    def ldap(self):
        return self.connection.request("auth/ldap", None)

    def ldap_sync_groups(self):
        return self.connection.request("auth/ldap/sync", None)

    def logging(self, context=None, priority=None):
        if context is not None and priority is not None:
            self.connection.request("general/logging/set", {"context": context, "level": priority})
        return self.connection.request("general/logging/list", {})["levels"]

    def logging_critical(self, value=None):
        if value is not None:
            self.connection.request("general/logging/critical", {"critical": value})
        return self.connection.request("general/logging/critical", {})["critical"]

    def logging_delete(self, context):
        self.connection.request("general/logging/remove", {"context": context})

    def configuration_bulk(self, values):
        self.connection.request("general/config/set/bulk", {"values": values})

    def create_session(self, ip, username, timeout=0, absolute_timeout=False, create_user_if_missing=False):
        req = {
            "ipaddress": ip,
            "username": username,
            "create_user": create_user_if_missing,
            "timeout": timeout,
            "absoluteTimeout": absolute_timeout
        }
        self.connection.request("auth/createsession", req)

    def session_network_based_timeouts(self):
        return self.connection.request("auth/sessions/networktimeouts", None)["timeouts"]

    def session_network_based_timeouts_create_or_modify(self, id=None, timeout=0, networks=[]):
        args = {"timeout": timeout, "networks": networks}
        if id: args["id"] = id
        return self.connection.request("auth/sessions/networktimeouts/manage", args)

    def session_network_based_timeouts_remove(self, id=None):
        return self.connection.request("auth/sessions/networktimeouts/remove", {"id": id})

    def upgrade(self):
        return self.connection.request("general/upgrade", None)

    def cloud_connected(self):
        return self.connection.request("general/cloud/connected", None)["status"]