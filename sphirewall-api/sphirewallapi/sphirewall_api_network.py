#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class NetworkSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def devices(self, device=None, configured=False):
        if device is not None:
            allDevices = self.devices(configured=configured)
            for d in allDevices:
                if d["interface"] == device:
                    return d

        return self.connection.request("network/devices/list", {"configuredDevices": configured})["devices"]

    def devices_remove(self, device):
        self.connection.request("network/devices/delete", {"device": device})

    def devices_up(self, device):
        self.connection.request("network/devices/up", {"device": device})

    def devices_down(self, device):
        self.connection.request("network/devices/down", {"device": device})

    def devices_save(self):
        self.connection.request("network/devices/save", {})

    def devices_set(self, device=None, device_params=None, address_params=None):
        args = {}
        if device:
            args["interface"] = device

        #Deal with interface types
        if device_params.get("vlan", False):
            args["vlan"] = True
            args["vlanId"] = device_params["vlanId"]
            args["vlanInterface"] = device_params["vlanInterface"]

        if device_params.get("bridge", False):
            args["bridge"] = True
            args["bridgeDevices"] = device_params["bridgeDevices"]

        if device_params.get("lacp", False):
            args["lacp"] = True
            args["lacpDevices"] = device_params["lacpDevices"]

            args["lacpMode"] = device_params["lacpMode"]
            args["lacpLinkStatePollFrequency"] = device_params["lacpLinkStatePollFrequency"]
            args["lacpLinkStateDownDelay"] = device_params["lacpLinkStateDownDelay"]
            args["lacpLinkStateUpDelay"] = device_params["lacpLinkStateUpDelay"]

        if device_params.get("physical", False):
            args["physical"] = True
            args["physicalInterface"] = device_params.get("physicalInterface")

        #Deal with address configuration
        if address_params:

            if "ipv4" in address_params:
                args["ipv4"] = address_params.get("ipv4")

            if "dhcp" in address_params:
                args["dhcp"] = address_params.get("dhcp")

            if "gateway" in address_params:
                args["gateway"] = address_params.get("gateway")

            #Dhcp Server Configuration:
            if "dhcpServerEnabled" in address_params:
                args["dhcpServerEnabled"] = address_params.get("dhcpServerEnabled")
                args["dhcpServerStart"] = address_params.get("dhcpServerStart")
                args["dhcpServerEnd"] = address_params.get("dhcpServerEnd")

        if "name" in device_params:
            args["name"] = device_params.get("name", "")
        if "persisted" in device_params:
            args["persisted"] = device_params.get("persisted", False)
        self.connection.request("network/devices/set", args)

    def devices_aliases_add(self, device, ip):
        args = {}
        args["interface"] = device
        args["ip"] = ip
        self.connection.request("network/devices/alias/add", args)

    def devices_aliases_del(self, device, ip):
        args = {}
        args["interface"] = device
        args["ip"] = ip
        self.connection.request("network/devices/alias/delete", args)

    def devices_ddns(self, device, ddenabled, ddusername, ddpassword, dddomain):
        args = {}
        args["interface"] = device
        args["ddenabled"] = ddenabled
        args["ddusername"] = ddusername
        args["ddpassword"] = ddpassword
        args["dddomain"] = dddomain
        self.connection.request("network/devices/dynamicdns/set", args)

    def devices_dhcp_static_add(self, device, ip, mac):
        args = {"device": device, "ip": ip, "mac": mac}
        self.connection.request("network/devices/leases/add", args)

    def devices_dhcp_static_del(self, device, ip, mac):
        args = {"device": device, "ip": ip, "mac": mac}
        self.connection.request("network/devices/leases/del", args)

    def dnsmasq_start(self):
        self.connection.request("network/dnsmasq/start", {})

    def dnsmasq_stop(self):
        self.connection.request("network/dnsmasq/stop", {})

    def dnsmasq_publish(self):
        self.connection.request("network/dnsmasq/publish", {})

    def dnsmasq_running(self):
        return self.connection.request("network/dnsmasq/running", {})["running"]

    def devices_toggle(self, device):
        args = {}
        args["interface"] = device
        self.connection.request("network/devices/toggle", args)

    def routes(self):
        return self.connection.request("network/routes/list", None)["routes"]

    def routes_add(self, destination=None, destination_cidr=None, route_device=None, route_nexthop=None):
        args = {}
        if destination and destination_cidr:
            args["destination"] = destination
            args["destination_cidr"] = int(destination_cidr)

        if route_device:
            args["route_device"] = route_device

        if route_nexthop:
            args["route_nexthop"] = route_nexthop
        self.connection.request("network/routes/add", args)

    def routes_del(self, id):
        args = {"id": id}
        self.connection.request("network/routes/del", args)

    def connections(self, name=None):
        if name is not None:
            allConnections = self.connections()
            for conn in allConnections:
                if conn["name"] == name:
                    return conn

        args = {}
        return self.connection.request("network/connections/list", args)["items"]

    def connections_add(self, name, type):
        args = {}
        args["name"] = name
        args["type"] = int(type)
        self.connection.request("network/connections/add", args)

    def connections_del(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/connections/del", args)

    def connections_pppoe_save(self, name, username, password, authenticationType, interface):
        args = {}
        args["name"] = name
        args["username"] = username
        args["password"] = password
        args["authenticationType"] = int(authenticationType)
        args["device"] = interface
        self.connection.request("network/connections/save", args)

    def connections_openvpn_save(self, name, configuration):
        args = {}
        args["name"] = name
        args["keyfile"] = configuration
        self.connection.request("network/connections/save", args)

    def connection_disconnect(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/connections/disconnect", args)

    def connection_connect(self, name):
        args = {}
        args["name"] = name
        self.connection.request("network/connections/connect", args)

    def dns_config(self):
        return self.connection.request("network/dns/get", None)

    def dns_config_set(self, domain, search, ns1, ns2):
        args = {}
        args["domain"] = domain
        args["search"] = search
        args["ns1"] = ns1
        args["ns2"] = ns2
        self.connection.request("network/dns/set", args)

    def vpns(self, name=None):
        if name is not None:
            allVpns = self.vpns()
            for vpn in allVpns:
                if vpn["name"] == name:
                    return vpn
            return None
        return self.connection.request("network/vpn", None)["items"]

    def vpns_start(self, name):
        args = {"name": name}
        self.connection.request("network/vpn/start", args)

    def vpns_stop(self, name):
        args = {"name": name}
        self.connection.request("network/vpn/stop", args)

    def vpns_ipsec_create(self, name):
        args = {"name": name, "type": 2}
        self.connection.request("network/vpn/create", args)

    def vpns_openvpnstatic_create(self, name):
        args = {"name": name, "type": 1}
        self.connection.request("network/vpn/create", args)

    def vpns_openvpntls_create(self, name, certcountry, certcity, certprovince, certorg, certemail):
        args = {"name": name, "type": 3, "certcountry": certcountry, "certcity": certcity, "certprovince": certprovince, "certorg": certorg, "certemail": certemail}
        self.connection.request("network/vpn/create", args)

    def vpns_delete(self, name):
        args = {"name": name}
        self.connection.request("network/vpn/remove", args)

    def vpns_set_option(self, name, key, value):
        args = {"name": name, key: value}
        self.connection.request("network/openvpn/setoption", args)

    def vpns_options(self, name, options):
        args = {"name": name}
        self.connection.request("network/vpn/options", dict(args.items() + options.items()))

    def vpns_tls_save(self, name, remoteServiceIp, remotePort, lzoCompression, customOptions, serverIp, serverMask):
        options = {
            "customopts": customOptions,
            "compression": lzoCompression,
            "remoteserverip": remoteServiceIp,
            "remoteport": int(remotePort),
            "serverip": serverIp,
            "servermask": serverMask
        }
        self.vpns_options(name, options)

    def vpns_static_save(self, name, remoteServiceIp, remotePort, lzoCompression, customOptions, serverIp, clientIp):
        options = {
            "customopts": customOptions,
            "compression": lzoCompression,
            "remoteserverip": remoteServiceIp,
            "remoteport": int(remotePort),

            "serverip": serverIp,
            "clientip": clientIp
        }
        self.vpns_options(name, options)

    def vpns_clients(self, name, clientName=None):
        if clientName is not None:
            allClients = self.vpns_clients(name)["clients"]
            for c in allClients:
                if c["name"] == clientName:
                    return c
        args = {"name": name}
        return self.connection.request("network/openvpn/client/list", args)

    def vpns_clients_add(self, name, clientName):
        args = {"name": name, "clientname": clientName}
        self.connection.request("network/openvpn/client/add", args)

    def vpns_clients_delete(self, name, clientName):
        args = {}
        args["name"] = name
        args["clientname"] = clientName
        self.connection.request("network/openvpn/client/delete", args)

    def devices_publish(self):
        self.connection.request("network/devices/publish", None)

    def wireless_start(self):
        self.connection.request("network/wireless/start", None)

    def wireless_stop(self):
        self.connection.request("network/wireless/stop", None)

    def wireless_online(self):
        return self.connection.request("network/wireless/online", None)["status"]

    def dyndns_configure(self, enabled, username, password, domain, type):
        args = {}
        args["enabled"] = enabled
        args["username"] = username
        args["password"] = password
        args["domain"] = domain
        args["type"] = int(type)
        self.connection.request("network/dyndns/set", args)

    def dyndns_get(self):
        return self.connection.request("network/dyndns", None)

    def isSynced(self):
        return self.connection.request("network/devices/synced", None)["synced"]

    def devices_createbridge(self):
        return self.connection.request("network/devices/createbridge", None)

    def ping(self, hostname):
        connection_request = self.connection.request("network/ping", {"host": hostname})
        print connection_request
        return connection_request["result"]