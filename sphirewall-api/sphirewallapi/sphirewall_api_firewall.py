#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

class FirewallSettings:
    connection = None

    def __init__(self, connection):
        self.connection = connection

    def connections_list(self):
        return self.connection.request("firewall/tracker/list", None)["connections"]

    def connections_size(self):
        return self.connection.request("firewall/tracker/size", None)["size"]

    def aliases(self, id=None, name=None):
        if id or name:
            allAliases = self.aliases()
            for alias in allAliases:
                if alias["id"] == id:
                    return alias
                elif alias["name"] == name:
                    return alias
            return None

        return self.connection.request("firewall/aliases/list", None)["aliases"]

    def aliases_create(self, name, type, source, detail):
        args = {}
        args["name"] = name
        args["type"] = int(type)
        args["source"] = int(source)
        args["detail"] = detail

        self.connection.request("firewall/aliases/add", args)

    def aliases_bulk_create(self, aliases):
        self.connection.request("firewall/aliases/add/bulk", {"items": aliases})

    def aliases_delete(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/aliases/del", args)

    def aliases_delete_all(self):
        self.connection.request("firewall/aliases/del/all", {})

    def aliases_sync(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/aliases/load", args)

    def aliases_list(self, id):
        args = {}
        args["id"] = id
        return self.connection.request("firewall/aliases/alias/list", args)["items"]

    def aliases_list_add(self, id, value):
        args = {}
        args["id"] = id
        args["value"] = value
        self.connection.request("firewall/aliases/alias/add", args)

    def aliases_list_search(self, id, search_expression):
        args = {"id": id, "search": search_expression}
        return self.connection.request("firewall/aliases/alias/search", args)["matched"]

    def aliases_list_del(self, id, value):
        args = {}
        args["id"] = id
        args["value"] = value
        self.connection.request("firewall/aliases/alias/del", args)

    def webfilter(self, id=None):
        if id is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            return next((rule for rule in all_rules if rule.get("id") == id), None)
        return self.connection.request("firewall/webfilter/rules/list", None)["rules"]

    def webfilter_get(self, id):
        if id is not None:
            all_rules = self.connection.request("firewall/webfilter/rules/list", None)["rules"]
            return next((rule for rule in all_rules if rule.get("id") == id), None)


    def webfilter_add(self, id, name, criteria, action, fireEvent=False, redirect=False, redirectUrl="", exclusive=False, metadata=None, temp_rule=False, expiry_timestamp=-1):
        args = {}
        if id is not None:
            args["id"] = id
        
        args["action"] = int(action)
        args["fireEvent"] = fireEvent
        args["redirect"] = redirect
        args["redirectUrl"] = redirectUrl
        args["criteria"] = criteria
        args["name"] = name

        args["temp_rule"] = temp_rule
        args["expiry_timestamp"] = expiry_timestamp

        args["exclusive"] = exclusive
        if metadata: args["metadata"] = metadata

        self.connection.request("firewall/webfilter/rules/add", args)

    def webfilter_add_bulk(self, rules):
        self.connection.request("firewall/webfilter/rules/add/bulk", {"items": rules})

    def webfilter_delete(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/remove", args)

    def webfilter_delete_all(self):
        self.connection.request("firewall/webfilter/rules/deleteall", None)

    def webfilter_moveup(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/moveup", args)

    def webfilter_movedown(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/movedown", args)

    def webfilter_enable(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/enable", args)

    def webfilter_disable(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/webfilter/rules/disable", args)

    def denylist(self):
        return self.connection.request("firewall/deny/list", None)["list"]

    def denylist_delete(self, host):
        args = {}
        args["ip"] = host
        return self.connection.request("firewall/deny/del", args)

    def qos(self, id=None):
        all_rules = self.connection.request("firewall/acls/list/trafficshaper", None)["items"]
        for rule in all_rules:
            if rule["id"] == id:
                return rule
        return all_rules

    def qos_add(self, id=None, name="", criteria=[], cumulative=True, upload=1024, download=1024):
        args = {
            "criteria": criteria,
            "upload": int(upload),
            "download": int(download),
            "cumulative": cumulative,
            "name": name
        }

        if id: args["id"] = id
        self.connection.request("firewall/acls/add/trafficshaper", args)

    def qos_del(self, id):
        args = {"id": id}
        self.connection.request("firewall/acls/del/trafficshaper", args)

    def normal_add(self, id=None, criteria=[], ignoreconntrack=False,
                   comment="",action="ACCEPT", log=False, nice=None):
        args = {}
        if id is not None:
            args["id"] = id
        if log is not None:
            args["log"] = log

        if action == "ACCEPT":
            args["action"] = 1
        if action == "DROP":
            args["action"] = 0
        if action == "PRIORITISE":
            args["action"] = 4
            args["nice"] = int(nice)
        args["comment"] = comment
        args["criteria"] = criteria
        args["ignoreconntrack"] = ignoreconntrack

        return self.connection.request("firewall/acls/add", args)


    def acls(self):
        return self.connection.request("firewall/acls/list", None)["normal"]

    def priority(self):
        return self.connection.request("firewall/acls/list", None)["priority"]

    def get(self, id):
        all_rules = self.acls()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        all_rules = self.nat()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        all_rules = self.priority()
        for rule in all_rules:
            if rule["id"] == id:
                return rule

        return None

    def nat(self):
        return self.connection.request("firewall/acls/list", None)["nat"]

    def normal_delete(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/acls/filter/delete", args)

    def autowan(self):
        return self.connection.request("firewall/autowan", {})

    def signatures(self):
        return self.connection.request("firewall/signatures", {})["signatures"]

    def signatures_add(self, id=None, type=None, name=None, description=None, expression=None):
        d = {
            "type": type,
            "name": name,
            "description": description,
            "expression": expression
             }

        if id is not None: d["id"] = id
        return self.connection.request("firewall/signatures/add", d)

    def signatures_del(self, id):
        return self.connection.request("firewall/signatures/del", {"id": id})

    def autowan_set(self, mode, devices):
        self.connection.request("firewall/autowan/set", {"mode": mode, "interfaces": devices})

    def normal_up(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/acls/filter/up", args)

    def normal_down(self, id):
        args = {}
        args["id"] = id
        self.connection.request("firewall/acls/filter/down", args)

    def normal_enable(self, id):
        args = {}
        args["id"] = id
        args["ruleType"] = 0
        self.connection.request("firewall/acls/enable", args)

    def normal_disable(self, id):
        args = {}
        args["id"] = id
        args["ruleType"] = 0
        self.connection.request("firewall/acls/disable", args)

    def connection_terminate(self, sourceIp, sourcePort, destIp, destPort):
        args = {}
        args["source"] = sourceIp
        args["sourcePort"] = int(sourcePort)
        args["dest"] = destIp
        args["destPort"] = int(destPort)

        self.connection.request("firewall/tracker/terminate", args)

    def defaultruleset(self):
        current_acls = self.acls()
        print current_acls
        if len(current_acls) == 2:
            if current_acls[0]["id"] == "default1" and current_acls[1]["id"] == "default2":
                return True
        return False

    def force_auth_settings(self, source=None, websites=None, destExceptions=None):
        if source is not None and websites is not None and destExceptions is not None:
            self.connection.request("firewall/rewrite/forceauthranges/set",
                    {"ranges": source, "websites": websites, "destExceptions": destExceptions})

        return self.connection.request("firewall/rewrite/forceauthranges", {})

    def periods(self, id=None):
        if id:
            for period in self.periods():
                if period["id"] == id:
                    return period
            return None
        return self.connection.request("firewall/periods/list", {})["periods"]

    def periods_create(self, name):
        return self.connection.request("firewall/periods/add", {"name": name})["id"]

    def periods_modify(self, id, any=False, mon=False, tue=False, wed=False, thu=False, fri=False, sat=False, sun=False,
                       startTime=0, endTime=2400, startDate=-1, endDate=-1):
        self.connection.request("firewall/periods/add", dict(
                id=id,
                startTime=startTime,
                endTime=endTime,
                startDate=startDate,
                endDate=endDate,
                any=any,
                mon=mon,
                tue=tue,
                wed=wed,
                thu=thu,
                fri=fri,
                sat=sat,
                sun=sun
        ))

    def periods_del(self, id):
        self.connection.request("firewall/periods/delete", {"id": id})

    def balancer_set(self, enabled, devices):
        self.connection.request("firewall/balancer/set", {"enabled": enabled, "devices": devices})

    def balancer(self):
        return self.connection.request("firewall/balancer", {})

    def forwarding_rules(self, id=None):
        if id:
            all_rules = self.forwarding_rules()
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None
        return self.connection.request("firewall/acls/forwarding", {})["rules"]

    def forwarding_rules_add(self, criteria, forwardingDestination, forwardingDestinationPort, id=None):
        args = {
            "forwardingDestination": forwardingDestination,
            "forwardingDestinationPort": forwardingDestinationPort,
            "criteria": criteria
        }

        if id: args["id"] = id

        return self.connection.request("firewall/acls/forwarding/add", args)

    def forwarding_rules_delete(self, id):
        self.connection.request("firewall/acls/forwarding/delete", {"id": id})

    def forwarding_rules_disable(self, id):
        self.connection.request("firewall/acls/forwarding/disable", {"id": id})

    def forwarding_rules_enable(self, id):
        self.connection.request("firewall/acls/forwarding/enable", {"id": id})

    def masquerading_rules(self, id=None):
        if id:
            all_rules = self.masquerading_rules()
            for rule in all_rules:
                if rule["id"] == id:
                    return rule
            return None
        return self.connection.request("firewall/acls/masquerading", {})["rules"]

    def masquerading_rules_add(self, criteria, natTargetDevice=None, natTargetIp=None, id=None):
        args = {
            "criteria": criteria,
        }

        if natTargetDevice:
            args["natTargetDevice"] = natTargetDevice
        if natTargetIp:
            args["natTargetIp"] = natTargetIp
        if id: args["id"] = id

        return self.connection.request("firewall/acls/masquerading/add", args)

    def masquerading_rules_delete(self, id):
        self.connection.request("firewall/acls/masquerading/delete", {"id": id})

    def masquerading_rules_disable(self, id):
        self.connection.request("firewall/acls/masquerading/disable", {"id": id})

    def masquerading_rules_enable(self, id):
        self.connection.request("firewall/acls/masquerading/enable", {"id": id})

