#Copyright Michael Lawson
#This file is part of Sphirewall.
#
#Sphirewall is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Sphirewall is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from sphirewallapi.sphirewall_api_firewall import FirewallSettings
from sphirewallapi.sphirewall_api_general import GeneralSettings
from sphirewallapi.sphirewall_api_network import NetworkSettings
from sphirewallapi.sphirewall_connection import ConnectionDetail

class SphirewallClient:
    connection = None
    hostname = None
    port = None

    def __init__(self, hostname=None, port=None, username=None, password=None, token=None, connection_detail=None):
        self.hostname = hostname
        self.port = port

        if connection_detail:
            self.connection = connection_detail
        else:
            self.connection = ConnectionDetail(self.hostname, self.port)
            if username is None and password is None:
                self.connection.token = token
            else:
                self.connection.authenticate(username, password)

    def version(self):
        return self.connection.request("general/version", None)

    def get_hostname(self):
        return self.hostname

    def get_port(self):
        return self.port

    def get_token(self):
        return self.connection.token

    def statistics_list(self, key, startDate, endDate):
        args = {
                "startDate": startDate,
                "endDate": endDate,
                "key": key
                }
        try:
            connection_request = self.connection.request("analytics/stats/metrics/get", args)
        except:
            return []

        if connection_request is not None:
            return connection_request["items"]
        else:
            return []

    def statistics_list_available(self):
        try:
            connection_request = self.connection.request("analytics/stats/metrics", {})
        except:
            return []

        if connection_request is not None:
            return connection_request["available"]
        else:
            return []

    def authenticate(self, username, password, ipaddress, timeout=None, absolute_timeout=None):
        args = {"username": username, "password": password, "ipaddress": ipaddress}
        if timeout: args["timeout"] = timeout
        if absolute_timeout: args["absoluteTimeout"] = absolute_timeout

        response = self.connection.request("auth/login", args)
        return response

    def general(self):
        return GeneralSettings(self.connection)

    def firewall(self):
        return FirewallSettings(self.connection)

    def network(self):
        return NetworkSettings(self.connection)
