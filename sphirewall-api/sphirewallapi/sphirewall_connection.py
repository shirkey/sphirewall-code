# Copyright Michael Lawson
# This file is part of Sphirewall.
#
# Sphirewall is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# Sphirewall is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.

from _socket import SOCK_STREAM, AF_INET, socket
import json


class SessionTimeoutException(Exception):

    def __init__(self, message):
        self.message = message


class ApiError(Exception):

    def __init__(self, message, error_code=None):
        self.message = message


class ConnectionDetail:
    token = None
    hostname = None
    port = None
    echo = False

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port

    def authenticate(self, username, password):
        response = self.send({'request': 'auth', 'username': username, 'password': password})
        jresponse = json.loads(response.decode('utf8', 'ignore'))
        if jresponse["code"] == 0:
            self.token = jresponse["token"]
            return self.token

    def send(self, data):
        s = socket(AF_INET, SOCK_STREAM)
        s.connect((self.hostname, int(self.port)))
        s.sendall(json.dumps(data) + "\n")

        responseBuffer = ""

        while 1:
            data = s.recv(1024)

            if not data:
                break

            responseBuffer += data
        return responseBuffer

    def request(self, type, args):
        req = {'request': type, 'token': self.token, 'args': args}
        if self.echo:
            print req
        try:
            response = self.send(req)
        except:
            raise ApiError("Connection error")

        if self.echo:
            print response
        json_loads = json.loads(response.decode('utf8', 'ignore'))
        if json_loads['code'] == 0:
            return json_loads['response']
        if json_loads['code'] == -2:
            raise SessionTimeoutException(json_loads['message'])
        else:
            raise ApiError(json_loads["message"])
