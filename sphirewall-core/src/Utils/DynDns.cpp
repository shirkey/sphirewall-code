/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>

#include "Core/System.h"
#include "Core/Logger.h"
#include "Utils/DynDns.h"
#include "Utils/HttpRequestWrapper.h"
#include "Core/ConfigurationManager.h"
#include "Utils/Hash.h"
#include "Utils/StringUtils.h"

bool DynamicDnsClient::update() {
	static int attempts = 0;

	if (type == DYNDNS) {
		Logger::instance()->log("sphirewalld.dyndns", INFO, "Updating dynamic dns domain for '%s'", domain.c_str());

		stringstream url;
		url << "http://" << username << ":" << password << "@members.dyndns.org/nic/update?hostname=" << domain;
		HttpRequestWrapper *request = new HttpRequestWrapper(url.str(), GET);
		request->useragent = "Sphirewall Labs - Sphirewall - 0.9.9.4";

		try {
			string buffer = request->execute();
			delete request;

			Logger::instance()->log("sphirewalld.dyndns", DEBUG, "Dyndns responded with message '%s'", buffer.c_str());
			if (buffer.find("good") != std::string::npos) {
				active = true;
				Logger::instance()->log("sphirewalld.dyndns", INFO,  "Dyndns update succeeded");
				return true;
			}
			else {
				Logger::instance()->log("sphirewalld.dyndns", ERROR, "Dyndns update failed for reason '%s'", buffer.c_str());
			}

		}
		catch (const HttpRequestWrapperException &exception) {
			Logger::instance()->log("sphirewalld.dyndns", ERROR, "Updating device for dynamic dns failed with a connection error");

			active = false;
			attempts++;

			if (attempts++ < 5) {
				return update();
			}
		}

	}
	else if (type == FREEDNS) {
		stringstream us;
		us << username << "|" << password;
		Hash h;
		string sha1Password = h.create_hash(us.str(), us.str().size());

		stringstream ss;
		ss << "http://freedns.afraid.org/api/?action=getdyndns&sha=" << sha1Password;
		HttpRequestWrapper *request = new HttpRequestWrapper(ss.str(), GET);

		try {
			string info = request->execute();
			vector<string> domains;
			split(info, '\n', domains);

			for (uint x = 0; x < domains.size(); x++) {
				vector<string> pieces;
				split(domains[x], '|', pieces);

				if (pieces.size() == 3) {
					if (pieces[0].compare(domain) == 0) {
						HttpRequestWrapper *updateRequest = new HttpRequestWrapper(pieces[2], GET);
						updateRequest->execute();
						active = true;

						Logger::instance()->log("sphirewalld.dyndns", INFO, "Updated freedns service domain '%s'", domain.c_str());
						delete updateRequest;
						return true;
					}
				}
			}

			Logger::instance()->log("sphirewalld.dyndns", ERROR, "Could not update freedns service, domain was not found");
		}
		catch (const HttpRequestWrapperException &exception) {
			active = false;

			Logger::instance()->log("sphirewalld.dyndns", ERROR, "Could not update freedns service, an error was found");
		}

		delete request;
	}

	attempts = 0;
	return false;
}

void GlobalDynDns::run() {
	run(false);
}

void GlobalDynDns::run(bool force) {
	if (isEnabled()) {
		client->update();
	}
}

bool GlobalDynDns::load() {
	if (configurationManager->has("dyndns")) {
		ObjectContainer *root = configurationManager->getElement("dyndns");
		setUsername(root->get("username")->string());
		setPassword(root->get("password")->string());
		setDomain(root->get("domain")->string());
		setEnabled(root->get("enabled")->boolean());
		setType((DynamicDnsClientProvider) root->get("type")->number());
	}
	return true;
}

void GlobalDynDns::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	root->put("username", new ObjectWrapper((string) getUsername()));
	root->put("password", new ObjectWrapper((string) getPassword()));
	root->put("domain", new ObjectWrapper((string) getDomain()));
	root->put("enabled", new ObjectWrapper((bool) isEnabled()));
	root->put("type", new ObjectWrapper((double) getType()));

	configurationManager->setElement("dyndns", root);
	configurationManager->save();
}

