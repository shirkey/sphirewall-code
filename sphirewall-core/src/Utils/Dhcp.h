#ifndef DHCP_H
#define DHCP_H

enum {
        DHCPDISCOVER = 1,
        DHCPOFFER = 2,
        DHCPREQUEST = 3,

        DHCPACK =5,
        DHCPNAK = 6,
        DHCPDECLINE = 4,
        DHCPRELEASE = 7,
        DHCPINFORM = 8
};

struct dhcp_packet {
        u_int8_t op;
        u_int8_t htype;
        u_int8_t hlen;
        u_int8_t hops;
        u_int32_t xid;
        u_int16_t secs;
        u_int16_t flags;

        u_int32_t ciaddr;
        u_int32_t yiaddr;
        u_int32_t siaddr;
        u_int32_t giaddr;

        char chaddr[16];
        char sname[64];
        char file[128];
};

struct dhcp_packet_option {
        u_int8_t len;
        char* data;
};

struct dhcp_builder_context {
        struct dhcp_packet header;
        struct dhcp_packet_option options[255];
};

int dhcp_open_raw_socket(int ifindex);
int dhcp_udp_send(const char* ifname, unsigned char* data, unsigned int len);
int dhcp_udp_recv(int sd, unsigned char* data, unsigned int buffer_len);
struct dhcp_builder_context* dhcp_init_context();
void dhcp_free_context(struct dhcp_builder_context* context);
void dhcp_set_blob_options(struct dhcp_builder_context* context, u_int8_t type, char* blob, u_int8_t size);
void dhcp_set_int8_options(struct dhcp_builder_context* context, u_int8_t type, u_int8_t value);
void dhcp_set_int32_options(struct dhcp_builder_context* context, u_int8_t type, u_int32_t value);
int dhcp_get_int8_option(struct dhcp_builder_context* context, u_int8_t type, u_int8_t* value);
int dhcp_get_int32_option(struct dhcp_builder_context* context, u_int8_t type, unsigned int* value);
int dhcp_serialize(struct dhcp_builder_context* context, char* buffer, size_t len);
int dhcp_deserialize(struct dhcp_builder_context* context, char* buffer, size_t len);

#endif
