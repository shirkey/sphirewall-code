/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_MONITOR_H_INCLUDED
#define SPHIREWALL_MONITOR_H_INCLUDED

#include <netinet/in.h>
#include <string>
#include <vector>

std::vector<in_addr_t> hostToIPAddresses(const std::string host);

class NetworkUtils {
	public:
		/*!Return 0 if the host is down, -1 if an error occured, otherwise returns the time to host*/
		static float ping(std::string host); /*One ping*/
		static int ipToHost(std::string ip, std::string &hostname);
};


#endif
