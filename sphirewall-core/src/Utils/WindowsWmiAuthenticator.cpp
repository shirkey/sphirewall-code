#include <iostream>
#include "Utils/WindowsWmiAuthenticator.h"
#include "Core/System.h"
#include <boost/regex.hpp>

#include "Core/Event.h"
#include "Core/ConfigurationManager.h"
#include "Auth/AuthenticationHandler.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Utils/IP4Addr.h"
#include "Auth/Session.h"
#include "Auth/SessionDb.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/LinuxUtils.h"

using namespace std;

void WinWmiAuth::poll() {
	if (!isEnabled()) {
		return;
	}

	Logger::instance()->log("sphirewalld.wmic.poll", DEBUG, "Polling for new events");

	try {
		seedCursor();
		list<std::string> ids = findEventsSince("");

		for (list<string>::iterator iter = ids.begin(); iter != ids.end(); iter++) {
			WmiLoginEvent *event = getRawEvent((*iter));

			if (event->isValidUser()) {
				Logger::instance()->log("sphirewalld.wmic.poll", INFO, "Found a kerberos user login event '%s'", event->toString().c_str());

				string macAddress = arp->get(event->getIpv4Address());
				if (macAddress.size() == 0) {
					Logger::instance()->log("sphirewalld.wmic.poll", ERROR, "Could not find mac address for new session '%s'", event->toString().c_str());

					updateCursor((*iter));
					delete event;
					continue;
				}

				UserPtr user = authManager->resolveUser(event->getPlainUsername());
				if (!user) {
					Logger::instance()->log("sphirewalld.wmic.poll", ERROR, "Could not find user account for new session '%s'", event->toString().c_str());
					updateCursor((*iter));
					delete event;
					continue;
				}

				if (eventDb) {
					EventParams params;
					params["user"] = event->getPlainUsername();
					params["ip"] = event->getIpv4Address();
					params["hw"] = macAddress;
					eventDb->add(new Event(USERDB_LOGIN_SUCCESS, params));
				}

				sessionDb->holdLock();
				SessionPtr session = sessionDb->get(macAddress, IP4Addr::stringToIP4Addr(event->getIpv4Address()));

				if (session) {
					if (session->getUser()->getUserName().compare(event->getPlainUsername()) == 0) {
						sessionDb->releaseLock();

						updateCursor((*iter));
						delete event;
						continue;
					}
					else {
						sessionDb->deleteSession(session);
					}
				}

				sessionDb->create(user, macAddress, IP4Addr::stringToIP4Addr(event->getIpv4Address()));
				sessionDb->releaseLock();

			}

			updateCursor((*iter));
		}

		updateStartCursor();
	}
	catch (const WmiException &e) {
		Logger::instance()->log("sphirewalld.wmic.poll", ERROR, e.message());
	}
}

void WinWmiAuth::run() {
	while (true) {
		poll();
		sleep(2);
	}
}

std::string WinWmiAuth::query(std::string input) {
	stringstream m;
	m << "Executing query against wmic client, " << input;
	Logger::instance()->log("sphirewalld.wmic.query", DEBUG, m.str());

	stringstream queryIds;
	queryIds << "wmic -U " << getDomain() << "/" << getUsername() << "%" << getPassword() << " //" << getHost() << " \"" << input << "\"";

	string ret;

	if (LinuxUtils::exec(queryIds.str(), ret) != 0) {
		throw WmiException(ret);
	}

	return ret;
}

bool WinWmiAuth::check() {
	try {
		Logger::instance()->log("sphirewalld.wmic.check", INFO, "Polling wmic to check connection details");
		query("Select RecordNumber from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = " + getTargetEventId());

	}
	catch (const WmiException &e) {
		Logger::instance()->log("sphirewalld.wmic.check", ERROR, "Could not connect, reason: " + e.message());
		return false;
	}

	Logger::instance()->log("sphirewalld.wmic.check", INFO, "Could connection to wmic service");
	return true;
}

list<string> WinWmiAuth::findEventsSince(std::string last) {
	stringstream q;
	q << "Select RecordNumber from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = "<< getTargetEventId()<< " And RecordNumber > " << cursor;
	string ret = query(q.str());
	boost::regex reg("Security|([0-9]+)");
	boost::smatch what;

	std::string::const_iterator start, end;
	start = ret.begin();
	end = ret.end();

	list<std::string> items;

	while (boost::regex_search(start, end, what, reg)) {
		items.push_back(what[1]);
		start = what[0].second;
	}

	items.remove("");

	return items;
}

void WinWmiAuth::seedCursor() {
	if(cursor != 0){
		return;
	}

	if(getStartCursor() != 0){
		cursor = getStartCursor();
	}

	stringstream q;
	q << "Select RecordNumber from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = " << getTargetEventId() << " And RecordNumber > " << cursor;
	string ret = query(q.str());
	boost::regex reg("Security|([0-9]+)");
	boost::smatch what;

	std::string::const_iterator start, end;
	start = ret.begin();
	end = ret.end();

	while (boost::regex_search(start, end, what, reg)) {
		string is = what[1];
		int i = atoi(is.c_str());
		if(i > cursor){
			cursor = i;
		}
		start = what[0].second;
	}
}

void WinWmiAuth::updateCursor(std::string input) {
	int value = stoi(input);

	if (value > cursor) {
		cursor = value;
	}
}

WmiLoginEvent *WinWmiAuth::getRawEvent(std::string id) {
	stringstream ss;
	ss << "Select Message from Win32_NTLogEvent Where Logfile = 'Security' And EventIdentifier = "<< getTargetEventId() << " and RecordNumber = ";
	ss << id;
	string ret = query(ss.str());

	WmiLoginEvent *event = new WmiLoginEvent();
	event->eventId = id;
	event->user = attribute(ret, getUsernameAttribute());
	event->host = attribute(ret, getHostAttribute());
	return event;
}

string WinWmiAuth::attribute(string event, string key) {
	stringstream exp;
	exp << key << ":" << "([\\s]+)([:,a-z,A-Z,0-9\\.,@,\\$,-]+)";
	boost::regex req(exp.str());
	boost::smatch what2;

	while (boost::regex_search(event, what2, req)) {
		return what2[2];
	}

	return "";
}

bool WmiLoginEvent::isValidUser() {
	return getPlainUsername().size() > 0;
}

std::string WmiLoginEvent::getPlainUsername() {
	boost::regex req("([a-z,A-Z,0-9,\\.]+)");
	boost::smatch what2;

	if (boost::regex_search(user, what2, req)) {
		return what2[1];
	}

	return "";
}

std::string WmiLoginEvent::getIpv4Address() {
	boost::regex req("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	boost::smatch what2;

	if (boost::regex_search(host, what2, req)) {
		stringstream ss;
		ss << what2[1] << "." << what2[2] << "." << what2[3] << "." << what2[4];
		return ss.str();
	}

	return "";
}

void WinWmiAuth::updateStartCursor() {
	static int persistCount = 0;
	config->put("authentication:wmic:cursor", (double) cursor);

	persistCount++;
	if(persistCount % 100){
		config->save();
	}
}

std::string WinWmiAuth::getUsernameAttribute(){
	if (config->hasByPath("authentication:wmic:usernameAttribute")) {
		string value = config->get("authentication:wmic:usernameAttribute")->string();
		if(value.size() > 0){
			return value;
		}
	}

	return "Account Name";
}

std::string WinWmiAuth::getHostAttribute(){
	if (config->hasByPath("authentication:wmic:hostAttribute")) {
		string value = config->get("authentication:wmic:hostAttribute")->string();
		if(value.size() > 0){
			return value;
		}
	}

	return "Client Address";
}

std::string WinWmiAuth::getTargetEventId(){
	if (config->hasByPath("authentication:wmic:targetEventId")) {
		string value = config->get("authentication:wmic:targetEventId")->string();
		if(value.size() > 0){
			return value;
		}
	}

	return "4768";
}


int WinWmiAuth::getStartCursor() {
	if (config->hasByPath("authentication:wmic:cursor")) {
		return config->get("authentication:wmic:cursor")->number();
	}

	return 0;
}

bool WinWmiAuth::isEnabled() {
	if (config->hasByPath("authentication:wmic:enabled")) {
		return config->get("authentication:wmic:enabled")->boolean();
	}

	return false;
}
std::string WinWmiAuth::getUsername() {
	if (config->hasByPath("authentication:wmic:username")) {
		return config->get("authentication:wmic:username")->string();
	}

	return "";
}

std::string WinWmiAuth::getDomain() {
	if (config->hasByPath("authentication:wmic:domain")) {
		return config->get("authentication:wmic:domain")->string();
	}

	return "";
}

std::string WinWmiAuth::getPassword() {
	if (config->hasByPath("authentication:wmic:password")) {
		return config->get("authentication:wmic:password")->string();
	}

	return "";
}

std::string WinWmiAuth::getHost() {
	if (config->hasByPath("authentication:wmic:hostname")) {
		return config->get("authentication:wmic:hostname")->string();
	}

	return "";
}


