/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

//SNI Retrieval
/*
 * Copyright (c) 2011 and 2012, Dustin Lundquist <dustin@null-ptr.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * This is a minimal TLS implementation indented only to parse the server name extension.
 * This was created based primarily on Wireshark dissection of a TLS handshake and RFC4366.
 */

#include <stdlib.h>
#include <string.h>
#include "TlsUtils.h"

#define true 1
#define false 0


static const char *parse_server_name_extension(const char *buf, int buf_len) {
	static char server_name[SERVER_NAME_LEN];
	const char *p = buf;
	char name_type;
	int name_len;

	if (p - buf + 1 > buf_len) {
		return NULL;
	}

	p += 2;

	while (1) {
		if (p - buf >= buf_len) {
			return NULL;
		}

		name_type = *p;
		p ++;

		switch (name_type) {
		case (0x00):
			if (p - buf + 1 > buf_len) {
				return NULL;
			}

			name_len = ((unsigned char)p[0] << 8) + (unsigned char)p[1];
			p += 2;

			if (p - buf + name_len > buf_len) {
				return NULL;
			}

			if (name_len >= SERVER_NAME_LEN - 1) {
				return NULL;
			}

			strncpy(server_name, p, name_len);
			server_name[name_len] = '\0';
			return server_name;

		default:
			break;
		}
	}
}

const char *parse_tls_header(const char *data, int data_len) {
	char tls_content_type;
	char tls_version_major;
	char tls_version_minor;
	int tls_length;
	const char *p = data;
	int len;

	/* Check that our TCP payload is at least large enough for a TLS header */
	if (data_len < TLS_HEADER_LEN) {
		return NULL;
	}

	tls_content_type = p[0];

	if (tls_content_type != TLS_HANDSHAKE_CONTENT_TYPE) {
		return NULL;
	}

	tls_version_major = p[1];
	tls_version_minor = p[2];

	if (tls_version_major < 3) {
		return NULL;
	}

	if (tls_version_major == 3 && tls_version_minor < 1) {
		return NULL;
	}

	tls_length = ((unsigned char)p[3] << 8) + (unsigned char)p[4];

	if (data_len < tls_length + TLS_HEADER_LEN) {
		return NULL;
	}

	/* Advance to first TLS payload */
	p += TLS_HEADER_LEN;

	if (p - data >= data_len) {
		return NULL;
	}

	if (*p != TLS_HANDSHAKE_TYPE_CLIENT_HELLO) {
		return NULL;
	}

	/* Skip past:
	   1    Handshake Type
	   3    Length
	   2    Version (again)
	   32   Random
	   to   Session ID Length
	 */
	p += 38;

	if (p - data >= data_len) {
		return NULL;
	}

	len = (unsigned char) * p; /* Session ID Length */
	p += 1 + len; /* Skip session ID block */

	if (p - data >= data_len) {
		return NULL;
	}

	len = (unsigned char) * p << 8; /* Cipher Suites length high byte */
	p ++;

	if (p - data >= data_len) {
		return NULL;
	}

	len += (unsigned char) * p; /* Cipher Suites length low byte */

	p += 1 + len;

	if (p - data >= data_len) {
		return NULL;
	}

	len = (unsigned char) * p; /* Compression Methods length */

	p += 1 + len;


	if (p - data >= data_len) {
		return NULL;
	}


	len = (unsigned char) * p << 8; /* Extensions length high byte */
	p++;

	if (p - data >= data_len) {
		return NULL;
	}

	len += (unsigned char) * p; /* Extensions length low byte */
	p++;

	while (1) {
		if (p - data + 4 >= data_len) { /* 4 bytes for the extension header */
			return NULL;
		}

		/* Parse our extension header */
		len = ((unsigned char)p[2] << 8) + (unsigned char)p[3]; /* Extension length */

		if (p[0] == 0x00 && p[1] == 0x00) { /* Check if it's a server name extension */
			/* There can be only one extension of each type, so we break
			   our state and move p to beinnging of the extension here */
			p += 4;

			if (p - data + len > data_len) {
				return NULL;
			}

			return parse_server_name_extension(p, len);
		}

		p += 4 + len; /* Advance to the next extension header */
	}

	return NULL;
}
