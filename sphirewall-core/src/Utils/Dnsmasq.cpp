#include <iostream>
#include "Core/ConfigurationManager.h"
#include "Utils/Interfaces.h"
#include "Utils/Dnsmasq.h"
#include "Core/System.h"
#include "Utils/FileUtils.h"
#include "Utils/LinuxUtils.h"
#include "Utils/IP4Addr.h"

using namespace std;

int DnsmasqManager::DNSMASQ_ENABLED = 1;
DnsmasqManager::DnsmasqManager(IntMgr* interfaceManager, ConfigurationManager *config) :
	interfaceManager(interfaceManager), config(config) {

	System::getInstance()->config.getRuntime()->loadOrPut("DNSMASQ_ENABLED", &DNSMASQ_ENABLED);
}

std::string DnsmasqManager::generateConfigurationFile() {
	stringstream ss;

	if (isDnsForwarderEnabled()) {
		ss << "domain-needed\n";
		ss << "bogus-priv\n";
		ss << "domain=" << getDnsFowarderDomain() << endl;
		ss << "expand-hosts\n";
		ss << "local=/" << getDnsFowarderDomain() << "/\n";

		ss << "listen-address=127.0.0.1\n";
		ss << "bind-interfaces\n";

		ss << "server=" << getDnsForwarderServerPrimary() << endl;
		ss << "server=" << getDnsForwarderServerSecondary() << endl;
	}

	for (InterfacePtr device : interfaceManager->get_all_interfaces()) {
		if(device->configuration_entity && device->configuration_entity->dhcpServerEnabled){
			ss << "dhcp-authoritative\n";
			ss << "interface=" << device->name << endl;
			ss << "dhcp-range=" << device->name << "," << device->configuration_entity->dhcpServerStart << "," << device->configuration_entity->dhcpServerEnd << endl;

			if (device->get_primary_ipv4_address() != 0) {
				ss << "dhcp-option=" << device->name << ",3," << IP4Addr::ip4AddrToString(device->get_primary_ipv4_address())  << "\n";
				ss << "dhcp-option=" << device->name << ",6," << IP4Addr::ip4AddrToString(device->get_primary_ipv4_address())  << "\n";
			}

			for (DhcpStaticLease lease : device->configuration_entity->dhcpServerStaticLeases) {
				ss << "dhcp-host=" << lease.mac << "," << lease.ip << endl;
			}
		}
	}

	return ss.str();
}

void DnsmasqManager::publish() {
	config->save();
	string config = generateConfigurationFile();
	string filename = "/etc/dnsmasq.conf";

	if (FileUtils::checkExists(filename)) {
		ofstream configFile;
		configFile.open(filename.c_str());
		configFile << "#/etc/dnsmasq.conf created by Sphirewall\n\n";
		configFile << config;
	}
}

bool DnsmasqManager::isDnsForwarderEnabled() {
	if (config->hasByPath("dnsmasq:forwarding")) {
		return config->get("dnsmasq:forwarding")->boolean();
	}

	return false;
}

string DnsmasqManager::getDnsFowarderDomain() {
	if (config->hasByPath("dnsmasq:domain")) {
		return config->get("dnsmasq:domain")->string();
	}

	return "local";
}

string DnsmasqManager::getDnsForwarderServerPrimary() {
	if (config->hasByPath("dnsmasq:primary")) {
		return config->get("dnsmasq:primary")->string();
	}

	return "127.0.0.1";
}

string DnsmasqManager::getDnsForwarderServerSecondary() {
	if (config->hasByPath("dnsmasq:secondary")) {
		return config->get("dnsmasq:secondary")->string();
	}

	return "127.0.0.1";
}


void DnsmasqManager::start() {
	stringstream ss;
	ss << "start-stop-daemon --start --oknodo --quiet --exec /usr/sbin/dnsmasq --pidfile /var/run/dnsmasq/dnsmasq.pid -- -x /var/run/dnsmasq/dnsmasq.pid >/dev/null";
	LinuxUtils::execWithNoFds(ss.str());
}

void DnsmasqManager::stop() {
	stringstream ss;
	ss << "killall -e -9 dnsmasq";
	LinuxUtils::execWithNoFds(ss.str());
	sleep(3);

	if (system("rm -Rf /var/run/dnsmasq/dnsmasq.pid"));
}

bool DnsmasqManager::running() {
	return FileUtils::checkExists("/var/run/dnsmasq/dnsmasq.pid");
}

void DnsmasqManager::initalize() {
	if (DNSMASQ_ENABLED == 1) {
		stop();
		publish();
		start();
	}
}
