#include <iostream>
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <map>
#include <poll.h>
#include <linux/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <dirent.h>

#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <sys/ioctl.h>
#include <linux/rtnetlink.h>
#include <curl/curl.h>
#include <linux/if_bridge.h>
#include <linux/if_vlan.h>
#include  <linux/sockios.h>

#include <sstream>
#include "autoconf.h"
#include "Utils/IP4Addr.h"
#include "Core/System.h"

extern "C" {
	#include <linux/netfilter.h>          
	#include <libnfnetlink/libnfnetlink.h>
}

using namespace std;

#include "Utils/Interfaces.h"
#include "Utils/NRoute.h"
#include "Utils/Dhcp.h"
#include "Utils/Dump.h"
#include "Core/Logger.h"
#include "Utils/LinuxUtils.h"

IntMgr::IntMgr(){
	for (int x = 0; x < MAX_DEVICES; x++) {
		active_interfaces[x] = InterfacePtr();
	}

	listening = false;
}

IntMgrCronJob::IntMgrCronJob(IntMgr* manager) 
	: CronJob(60 * 2, "INTERFACE_MANAGER_CRON", true){

	this->manager = manager;
}

void IntMgr::set_persisted(InterfacePtr interface, bool persisted){
	if(persisted && !interface->configuration_entity){
		interface->configuration_entity = InterfaceConfigurationPtr(new InterfaceConfiguration());		
		configured_interfaces.push_back(interface->configuration_entity);
		return;
	}

	if(!persisted){
		if(interface->configuration_entity){
			configured_interfaces.remove(interface->configuration_entity);
			interface->configuration_entity = InterfaceConfigurationPtr();
		}
	}
}

Interface::Interface(std::string name){
	this->name = name;
	memset(&dhcp, 0, sizeof(struct dhcp_state));
	gateway = 0;
	ifid = 0;
	state = false;
	deleting= false;
}

unsigned int Interface::get_primary_ipv4_address(){
	for(IPv4AddressPtr ip : ipv4_addresses){
		if(ip->ip != 0){
			return ip->ip;
		}
	}

	return 0;
}

unsigned int Interface::get_primary_ipv4_netmask(){
	for(IPv4AddressPtr ip : ipv4_addresses){
		if(ip->mask != 0){
			return ip->mask;
		}
	}

	return 0;
}

unsigned int Interface::get_primary_ipv4_broadcast(){
        for(IPv4AddressPtr ip : ipv4_addresses){
                if(ip->bcast != 0){
                        return ip->bcast;
                }
        }

        return 0;
}

void IntMgrCronJob::run(){
	manager->holdLock();
	for(InterfacePtr interface : manager->get_all_interfaces()){
		if(interface->addr_mode == INTERFACE_ADDR_MODE_DHCP){
			if(interface->dhcp.state == DHCP_TRANSACTION_FINISHED){
				//Has lease
				if(interface->dhcp.lease.expirey_time < time(NULL)){
					Logger::instance()->log("sphirewalld.intmgr", INFO, "DHCP lease on interface %s has expired", interface->name.c_str());
					interface->refresh_address_configuration();
					continue;
				}
			}

			if(interface->dhcp.state == DHCP_TRANSACTION_FAILED || interface->dhcp.state == DHCP_TRANSACTION_RETRY_REQUIRED){
				Logger::instance()->log("sphirewalld.intmgr", INFO, "DHCP lease required as interface %s is in DHCP_TRANSACTION_FAILED or DHCP_TRANSACTION_RETRY_REQUIRED state", interface->name.c_str());
				interface->refresh_address_configuration();
				continue;
			}
		}
	}
	manager->releaseLock();
}

bool IntMgr::load(){
	state_known = false;
	configured_interfaces.clear();
	if (configurationManager->has("network")) {
		ObjectContainer *configRoot = configurationManager->getElement("network");
		ObjectContainer *configDevices = configRoot->get("devices")->container();

		for (int x = 0; x < configDevices->size(); x++) {
			ObjectContainer *configDevice = configDevices->get(x)->container();
			InterfaceConfiguration* target = new InterfaceConfiguration();
			target->interface = configDevice->get("interface")->string();
			target->dhcp = configDevice->get("dhcp")->boolean();
			target->label = configDevice->get("name")->string();
			target->gateway = IP4Addr::stringToIP4Addr(configDevice->get("gateway")->string());

			if(configDevice->has("bridge")){
				target->bridge = configDevice->get("bridge")->boolean();
				if(target->bridge){
					ObjectContainer *bridgeDevices = configDevice->get("bridgeDevices")->container();

					for (int y = 0; y < bridgeDevices->size(); y++) {
						target->bridgeDevices.push_back(bridgeDevices->get(y)->string());
					}
				}
			}

			if(configDevice->has("vlan")){
				target->vlan = configDevice->get("vlan")->boolean();
				if(target->vlan){
					target->vlanId = configDevice->get("vlanId")->number();
					target->vlanInterface = configDevice->get("vlanInterface")->string();
				}
			}

			if(configDevice->has("lacp")){
				target->lacp= configDevice->get("lacp")->boolean();
				if(target->lacp){
					target->lacpMode = configDevice->get("lacpMode")->number();
					target->lacpLinkStatePollFrequency= configDevice->get("lacpLinkStatePollFrequency")->number();
					target->lacpLinkStateDownDelay= configDevice->get("lacpLinkStateDownDelay")->number();
					target->lacpLinkStateUpDelay= configDevice->get("lacpLinkStateUpDelay")->number();

					ObjectContainer *bondedDevices= configDevice->get("bondedDevices")->container();
					for (int y = 0; y < bondedDevices->size(); y++) {
						target->bondedDevices.push_back(bondedDevices->get(y)->string());
					}
				}
			}
			if(!target->bridge && !target->lacp && !target->vlan){
				target->physical = true;
			}

			//Dhcp server options:
			if (configDevice->has("dhcpServerEnabled")) {
				target->dhcpServerEnabled =  configDevice->get("dhcpServerEnabled")->boolean();
				target->dhcpServerStart =  configDevice->get("dhcpServerStart")->string();
				target->dhcpServerEnd =  configDevice->get("dhcpServerEnd")->string();

				ObjectContainer *leases = configDevice->get("dhcpServerStaticLeases")->container();

				for (int x = 0; x < leases->size(); x++) {
					ObjectContainer *lease = leases->get(x)->container();

					DhcpStaticLease slease;
					slease.ip = lease->get("ip")->string();
					slease.mac = lease->get("mac")->string();
					target->dhcpServerStaticLeases.push_back(slease);
				}
			}

			if (configDevice->has("ipv4")) {
				ObjectContainer *configAliases = configDevice->get("ipv4")->container();

				for (int y = 0; y < configAliases->size(); y++) {
					ObjectContainer* wrapper = configAliases->get(y)->container();

					IPv4AddressPtr ip = IPv4AddressPtr(new IPv4Address());
					ip->ip = IP4Addr::stringToIP4Addr(wrapper->get("ip")->string());				
					ip->mask = IP4Addr::stringToIP4Addr(wrapper->get("mask")->string());				
					ip->bcast = IP4Addr::stringToIP4Addr(wrapper->get("bcast")->string());				
					target->ipv4_addresses.push_back(ip);
				}
			}

			configured_interfaces.push_back(InterfaceConfigurationPtr(target));
		}
	}

	notifier.register_listener(this);
	notifier.connect();

	notifier.pollfirst();
	while(notifier.handleEvent() == NC_REQUEST_SEGMENT);

	notifier.pollfirst_addresses();
	while(notifier.handleEvent() == NC_REQUEST_SEGMENT);

	state_known = true;
	//Go through the configured interfaces, and find what is missing
	for(InterfaceConfigurationPtr interface : configured_interfaces){
		InterfacePtr target = get_interface(interface->interface);
		if(!target){
			//Create the bloody thing
			if(interface->vlan){
				create_vlan_interface(interface->vlanInterface, interface->vlanId);
			}else if(interface->lacp){
				create_bonding_interface(interface->interface);
			}else if(interface->bridge){
				create_bridge_interface(interface->interface);
			}
		}
	}

	notifier.pollfirst();
	while(notifier.handleEvent() == NC_REQUEST_SEGMENT);

	notifier.pollfirst_addresses();
	while(notifier.handleEvent() == NC_REQUEST_SEGMENT);

	for(InterfaceConfigurationPtr interface : configured_interfaces){
		InterfacePtr target = get_interface(interface->interface);
		if(target){
			target->bring_up();
		}
	}

	//Kick of listener
	if(!listening){
		listening = true;

		new boost::thread(boost::bind(&NetworkChangeNotifier::run, &notifier));

		//Kick of cron job as well
		System::getInstance()->getCronManager()->registerJob(new IntMgrCronJob(this));
	}
	return true;
}

void IntMgr::save(){
	for(InterfacePtr interface : get_all_interfaces()){
		if(interface->configuration_entity){
			interface->save_state_to_configuration_entity();			
		}
	} 

	holdLock();
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *confDevices = new ObjectContainer(CARRAY);

	for (InterfaceConfigurationPtr target : configured_interfaces) {
		ObjectContainer *jsonTarget = new ObjectContainer(CREL);

		jsonTarget->put("interface", new ObjectWrapper((string) target->interface));
		jsonTarget->put("gateway", new ObjectWrapper((string) IP4Addr::ip4AddrToString(target->gateway)));
		jsonTarget->put("dhcp", new ObjectWrapper((bool) target->dhcp));
		jsonTarget->put("name", new ObjectWrapper((string) target->label));
		jsonTarget->put("bridge", new ObjectWrapper((bool) target->bridge));

		//Dhcp server options
		jsonTarget->put("dhcpServerEnabled", new ObjectWrapper((bool) target->dhcpServerEnabled));
		jsonTarget->put("dhcpServerStart", new ObjectWrapper((string) target->dhcpServerStart));
		jsonTarget->put("dhcpServerEnd", new ObjectWrapper((string) target->dhcpServerEnd));

		jsonTarget->put("vlan", new ObjectWrapper((bool) target->vlan));
		jsonTarget->put("lacp", new ObjectWrapper((bool) target->lacp));
		jsonTarget->put("bridge", new ObjectWrapper((bool) target->bridge));
		jsonTarget->put("physical", new ObjectWrapper((bool) target->physical));

		if(target->vlan){
			jsonTarget->put("vlanId", new ObjectWrapper((double) target->vlanId));
			jsonTarget->put("vlanInterface", new ObjectWrapper((string) target->vlanInterface));
		}

		ObjectContainer *leases = new ObjectContainer(CARRAY);
		for (DhcpStaticLease lease : target->dhcpServerStaticLeases) {
			ObjectContainer *olease = new ObjectContainer(CREL);
			olease->put("ip", new ObjectWrapper((string) lease.ip));
			olease->put("mac", new ObjectWrapper((string) lease.mac));
			leases->put(new ObjectWrapper(olease));
		}
		jsonTarget->put("dhcpServerStaticLeases", new ObjectWrapper(leases));

		if (target->bridge) {
			ObjectContainer *bridgeNodes = new ObjectContainer(CARRAY);
			list<string> &devs = target->bridgeDevices;

			for (list<string>::iterator iter = devs.begin(); iter != devs.end(); iter++) {
				bridgeNodes->put(new ObjectWrapper((*iter)));
			}

			jsonTarget->put("bridgeDevices", new ObjectWrapper(bridgeNodes));
		}

		if(target->lacp){
			ObjectContainer *bridgeNodes = new ObjectContainer(CARRAY);
			list<string> &devs = target->bondedDevices;

			for (list<string>::iterator iter = devs.begin(); iter != devs.end(); iter++) {
				bridgeNodes->put(new ObjectWrapper((*iter)));
			}

			jsonTarget->put("bondedDevices", new ObjectWrapper(bridgeNodes));
			jsonTarget->put("lacpMode", new ObjectWrapper((double) target->lacpMode));
			jsonTarget->put("lacpLinkStatePollFrequency", new ObjectWrapper((double) target->lacpLinkStatePollFrequency));
			jsonTarget->put("lacpLinkStateDownDelay", new ObjectWrapper((double) target->lacpLinkStateDownDelay));
			jsonTarget->put("lacpLinkStateUpDelay", new ObjectWrapper((double) target->lacpLinkStateUpDelay));
		}

		ObjectContainer *aliases = new ObjectContainer(CARRAY);
		for (IPv4AddressPtr ip : target->ipv4_addresses) {
			ObjectContainer* ip_saved = new ObjectContainer(CREL);			
			ip_saved->put("ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(ip->ip)));		
			ip_saved->put("mask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(ip->mask)));		
			ip_saved->put("bcast", new ObjectWrapper((string) IP4Addr::ip4AddrToString(ip->bcast)));		

			aliases->put(new ObjectWrapper(ip_saved));
		}

		jsonTarget->put("ipv4", new ObjectWrapper(aliases));
		confDevices->put(new ObjectWrapper(jsonTarget));
	}

	root->put("devices", new ObjectWrapper(confDevices));
	configurationManager->setElement("network", root);
	configurationManager->save();
	releaseLock();
}

InterfacePtr IntMgr::get_interface(std::string name){
	for(int x =0; x < MAX_DEVICES; x++){
		if(active_interfaces[x] && active_interfaces[x]->name.compare(name) == 0){
			return active_interfaces[x];	
		}
	}

	return InterfacePtr(); 
}

InterfacePtr IntMgr::get_interface_by_id(int id){
	if(id < MAX_DEVICES){
		return active_interfaces[id];
	}

	return InterfacePtr(); 
}

void IntMgr::put_interface_by_id(int id, InterfacePtr interface){
	if(id < MAX_DEVICES){
		active_interfaces[id] = interface;
	}
}

list<InterfacePtr> IntMgr::get_all_interfaces(){
	list<InterfacePtr> ret;
	for(int x =0; x < MAX_DEVICES; x++){
		if(active_interfaces[x]){
			ret.push_back(active_interfaces[x]);
		}
	}

	return ret;
}

void IntMgr::handle_rtm_newlink(struct ifinfomsg *info, size_t len){
	int interface_index = info->ifi_index;
	bool interface_state = (info->ifi_flags & IFF_UP);
	std::string interface_name;
	struct rtattr *a = IFLA_RTA(info);
	while (RTA_OK(a, len)) {
		switch (a->rta_type) {
			case IFLA_IFNAME:
				interface_name = string((char *) RTA_DATA(a));
				break;

			default:
				break;
		}

		a = RTA_NEXT(a, len);
	}

	holdLock();
	if(!get_interface_by_id(info->ifi_index)){
		Interface* new_interface = NULL;
		if(is_bridge(interface_name)){
			new_interface = new BridgeInterface(interface_name);
			new_interface->configuration_entity = get_interface_configuration(interface_name);

			if(new_interface->configuration_entity){
				for(std::string bridge : ((BridgeInterface*) new_interface)->get_bridged_devices()){
					((BridgeInterface*) new_interface)->remove_bridged_device(bridge);
				}

				for(std::string bridge : new_interface->configuration_entity->bridgeDevices){
					((BridgeInterface*) new_interface)->add_bridged_device(bridge);	
				}
			}

		}else if(is_vlan(interface_name)){
			new_interface = new VlanInterface(interface_name);
			new_interface->configuration_entity = get_interface_configuration(interface_name);
		}else if(is_bonding(interface_name)){
			new_interface = new BondingInterface(interface_name);
			new_interface->configuration_entity = get_interface_configuration(interface_name);
			if(new_interface->configuration_entity){
				if(!interface_state){
					((BondingInterface*) new_interface)->set_bonding_mode(new_interface->configuration_entity->lacpMode);
				}

				((BondingInterface*) new_interface)->set_link_state_poll_frequency(new_interface->configuration_entity->lacpLinkStatePollFrequency);
				((BondingInterface*) new_interface)->set_link_down_delay(new_interface->configuration_entity->lacpLinkStateDownDelay);
				((BondingInterface*) new_interface)->set_link_up_delay(new_interface->configuration_entity->lacpLinkStateUpDelay);

				for(string slave : ((BondingInterface*) new_interface)->configuration_entity->bondedDevices){
					//Do we know this device?
					InterfacePtr target = get_interface(slave);
					if(target){
						target->bring_down();
					}

					((BondingInterface*) new_interface)->add_slave(slave);
				}
			}			
		}else{
			new_interface = new Interface(interface_name);
			new_interface->configuration_entity = get_interface_configuration(interface_name);
		}

		if(new_interface->configuration_entity){
			new_interface->label = new_interface->configuration_entity->label;
			if(new_interface->configuration_entity->dhcp){
				new_interface->addr_mode = INTERFACE_ADDR_MODE_DHCP;
			}else{
				new_interface->addr_mode = INTERFACE_ADDR_MODE_STATIC;
			}
		}

		new_interface->ifid = interface_index;
		put_interface_by_id(interface_index, InterfacePtr(new_interface));
		Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newlink", INFO, 
				"new network interface found, name: %s ifid: %d", interface_name.c_str(), interface_index);
	}

	//Update state
	InterfacePtr interface = get_interface_by_id(interface_index);
	if(interface && interface->state != interface_state){
		if(interface_state){
			Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newlink", INFO, 
					"interface state changed to online, name: %s ifid: %d", interface_name.c_str(), interface_index);

			//Lets check the configuration to see if this requires a dhcp lease
			if(state_known){
				interface->refresh_address_configuration();
			}
		}else{
			Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newlink", INFO,
					"interface state changed to offline, name: %s ifid: %d", interface_name.c_str(), interface_index);
		}
	}

	if(interface && !interface_state && interface->deleting && interface->is_bonding()){
		put_interface_by_id(info->ifi_index, InterfacePtr()); 
	} 

	interface->state = interface_state;
	releaseLock();
	trigger_change_listeners();
}

void IntMgr::handle_rtm_dellink(struct ifinfomsg *info, size_t len){
	holdLock();
	InterfacePtr interface = get_interface_by_id(info->ifi_index);
	if(interface){
		Logger::instance()->log("sphirewalld.intmgr.handle_rtm_dellink", INFO,
				"network interface deleted, name: %s ifid: %d", active_interfaces[info->ifi_index]->name.c_str(), info->ifi_index);
		interface->deleting = true;
		if(!interface->is_bonding() && !interface->is_physical()){
			put_interface_by_id(info->ifi_index, InterfacePtr());	
		}
	}

	releaseLock();
	trigger_change_listeners();
}

void IntMgr::handle_rtm_newaddr(struct ifaddrmsg *info, size_t len){
	holdLock();

	int interface_index = info->ifa_index;
	InterfacePtr interface = get_interface_by_id(interface_index); 
	if(interface){
		//Determine address family
		if(info->ifa_family == AF_INET){
			IPv4Address* ipv4 = new IPv4Address();

			struct rtattr *a = IFLA_RTA(info);
			while (RTA_OK(a, len)) {
				switch (a->rta_type) {
					case IFA_ADDRESS:
						//Need to work out what this is
						break;
					case IFA_LOCAL:
						memcpy(&ipv4->ip, RTA_DATA(a), 4);
						break;
					case IFA_BROADCAST:
						memcpy(&ipv4->bcast, RTA_DATA(a), 4);
						break;
					default:
						break;
				}

				a = RTA_NEXT(a, len);
			}

			ipv4->mask = ~(~uint32_t(0) >> info->ifa_prefixlen);
			ipv4->ip = ntohl(ipv4->ip);
			ipv4->bcast = ntohl(ipv4->bcast);

			//Check that an existing entry on this ip does not exist
			for(IPv4AddressPtr existing_ip : interface->ipv4_addresses){
				if(existing_ip->ip == ipv4->ip && existing_ip->mask == ipv4->mask && existing_ip->bcast == ipv4->bcast){
					releaseLock();
					return;
				}
			}

			interface->ipv4_addresses.push_back(IPv4AddressPtr(ipv4));

			Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newaddr", INFO,
					"added ipv4 address %s to interface %s", ipv4->to_string().c_str(), interface->name.c_str());

		}else if(info->ifa_family == AF_INET6){
			//ipv6 address
		}	
	}else{
		Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newaddr", ERROR,
				"could not find interface");
	}
	releaseLock();
	trigger_change_listeners();
}

void IntMgr::handle_rtm_deladdr(struct ifaddrmsg *info, size_t len){
	holdLock();

	int interface_index = info->ifa_index;
	InterfacePtr interface = get_interface_by_id(interface_index);
	if(interface){
		//Determine address family
		if(info->ifa_family == AF_INET){
			unsigned int target_ip = 0;
			unsigned int target_bcast = 0;
			unsigned int target_mask = 0;

			struct rtattr *a = IFLA_RTA(info);
			while (RTA_OK(a, len)) {
				switch (a->rta_type) {
					case IFA_ADDRESS:
						break;
					case IFA_LOCAL:
						memcpy(&target_ip, RTA_DATA(a), 4);
						break;
					case IFA_BROADCAST:
						memcpy(&target_bcast, RTA_DATA(a), 4);
						break;
					default:
						break;
				}

				a = RTA_NEXT(a, len);
			}

			target_mask = ~(~uint32_t(0) >> info->ifa_prefixlen);
			for(IPv4AddressPtr ip : interface->ipv4_addresses){
				if(ip->ip == ntohl(target_ip) && ip->bcast == ntohl(target_bcast) && ip->mask == target_mask){
					Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newaddr", INFO,
							"removed ipv4 address %s to interface %s", ip->to_string().c_str(), interface->name.c_str());

					interface->ipv4_addresses.remove(ip);
					break;
				}
			}

		}else if(info->ifa_family == AF_INET6){
			//ipv6 address
		}
	}else{
		Logger::instance()->log("sphirewalld.intmgr.handle_rtm_newaddr", ERROR,
				"could not find interface");
	}
	releaseLock();
	trigger_change_listeners();
}

int IntMgr::handle_network_change_notification(struct nlmsghdr *nh, size_t len){
	/* This method receives netlink messages and must update our device state and information */
	switch(nh->nlmsg_type){
		case RTM_NEWLINK:
			handle_rtm_newlink((struct ifinfomsg *) NLMSG_DATA(nh), NLMSG_PAYLOAD(nh, sizeof(struct ifaddrmsg)));
			break;
		case RTM_DELLINK:
			handle_rtm_dellink((struct ifinfomsg *) NLMSG_DATA(nh), NLMSG_PAYLOAD(nh, sizeof(struct ifaddrmsg)));
			break;
		case RTM_NEWADDR:
			handle_rtm_newaddr((struct ifaddrmsg *) NLMSG_DATA(nh), NLMSG_PAYLOAD(nh, sizeof(struct ifaddrmsg)));
			break;
		case RTM_DELADDR:
			handle_rtm_deladdr((struct ifaddrmsg *) NLMSG_DATA(nh), NLMSG_PAYLOAD(nh, sizeof(struct ifaddrmsg)));
			break;
		default:
			break;

	};
}

int addattr_l(struct nlmsghdr *n, int type, const void *data, int alen)
{
	int len = RTA_LENGTH(alen);
	struct rtattr *rta;

	rta = (struct rtattr*) NLMSG_TAIL(n);
	rta->rta_type = type;
	rta->rta_len = len;
	memcpy(RTA_DATA(rta), data, alen);
	n->nlmsg_len = NLMSG_ALIGN(n->nlmsg_len) + RTA_ALIGN(len);
	return 0;
}

in_addr_t netmask( int prefix ) {
	if ( prefix == 0 ){
		return( ~((in_addr_t) -1) );
	}
	return( ~((1 << (32 - prefix)) - 1) );

}

void Interface::bring_up(){
	int fd;
	struct ifreq ifr;
	memset(&ifr, 0, sizeof(struct ifreq));
	if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) >= 0) {
		ifr.ifr_addr.sa_family = AF_INET;
		strcpy(ifr.ifr_name, name.c_str());

		if (ioctl(fd, SIOCGIFFLAGS, &ifr) == 0) {
			ifr.ifr_flags |= IFF_UP;
			ioctl(fd, SIOCSIFFLAGS, &ifr);
		}
	}

	close(fd);
}

void Interface::bring_down(){
	int fd;
	struct ifreq ifr;
	memset(&ifr, 0, sizeof(struct ifreq));
	if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) >= 0) {
		ifr.ifr_addr.sa_family = AF_INET;
		strcpy(ifr.ifr_name, name.c_str());

		if (ioctl(fd, SIOCGIFFLAGS, &ifr) == 0) {
			ifr.ifr_flags &= ~IFF_UP;
			ioctl(fd, SIOCSIFFLAGS, &ifr);
		}
	}

	close(fd);
}

void Interface::add_ipv4_address(unsigned int ip, unsigned int mask, unsigned int bcast){
	struct {
		struct nlmsghdr  nh;
		struct ifaddrmsg ifinfo;
		char             attrbuf[512];
	} req;

	struct rtattr *rta;
	int rtnetlink_sk = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);

	memset(&req, 0, sizeof(req));
	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	req.nh.nlmsg_flags = NLM_F_REQUEST|NLM_F_CREATE|NLM_F_EXCL;
	req.nh.nlmsg_type = RTM_NEWADDR;
	req.ifinfo.ifa_family= AF_INET;
	req.ifinfo.ifa_prefixlen = __builtin_popcount(mask);
	req.ifinfo.ifa_index= ifid;

	unsigned int ip_networkorder = htonl(ip);
	unsigned int ip_bcast = htonl(bcast);

	addattr_l(&req.nh, IFA_LOCAL, &ip_networkorder, sizeof(ip));
	addattr_l(&req.nh, IFA_BROADCAST, &ip_bcast, sizeof(ip));

	send(rtnetlink_sk, &req, req.nh.nlmsg_len, 0);
}

void Interface::remove_ipv4_address(unsigned int ip, unsigned int mask, unsigned int bcast){
	struct {
		struct nlmsghdr  nh;
		struct ifaddrmsg ifinfo;
		char             attrbuf[512];
	} req;

	struct rtattr *rta;
	int rtnetlink_sk = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);

	memset(&req, 0, sizeof(req));
	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	req.nh.nlmsg_flags = NLM_F_REQUEST|NLM_F_CREATE|NLM_F_EXCL;
	req.nh.nlmsg_type = RTM_DELADDR;
	req.ifinfo.ifa_family= AF_INET;
	req.ifinfo.ifa_prefixlen = __builtin_popcount(mask);
	req.ifinfo.ifa_index= ifid;

	unsigned int ip_networkorder = htonl(ip);
	unsigned int ip_bcast = htonl(bcast);

	addattr_l(&req.nh, IFA_LOCAL, &ip_networkorder, sizeof(ip));
	addattr_l(&req.nh, IFA_BROADCAST, &ip_bcast, sizeof(ip));

	send(rtnetlink_sk, &req, req.nh.nlmsg_len, 0);
}

int Interface::get_hardware_addr(char* addr){
	int fd;
	struct ifreq ifr;
	struct sockaddr_in *sin;

	memset(&ifr, 0, sizeof(struct ifreq));
	if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) >= 0) {
		ifr.ifr_addr.sa_family = AF_INET;
		strcpy(ifr.ifr_name, name.c_str());

		if (ioctl(fd, SIOCGIFHWADDR, &ifr) == 0) {
			memcpy(addr, ifr.ifr_hwaddr.sa_data, 6);
		}else{
			return -1;
		}
	} else {
		return -1;
	}

	close(fd);
	return 0;
}

std::string Interface::to_string(){
	std::stringstream ss;
	ss << " flags [";
	if(state){
		ss << " UP " ;
	}

	if(is_vlan()) ss << "VLAN ";
	if(is_bridge()) ss << "BRIDGE ";
	if(is_bonding()) ss << "BONDING ";

	ss << "]";
	return ss.str();
}

void Interface::request_lease_dhcp_discover(){
	struct dhcp_builder_context* context = dhcp_init_context();
	context->header.xid = dhcp.xid; 

	get_hardware_addr((char*) &context->header.chaddr);
	dhcp_set_int8_options(context, DHCP_OPTION_MESSAGE_TYPE, DHCPDISCOVER);

	char* buffer = (char*) malloc(1501);
	memcpy(buffer, (char*) &context->header, sizeof(context->header));

	int cursor = dhcp_serialize(context, buffer, 1501);
	dhcp_udp_send(name.c_str(), (unsigned char*) buffer, cursor);

	free(buffer);
	dhcp_free_context(context);
}

void Interface::request_lease_dhcp_request(){
	struct dhcp_builder_context* context = dhcp_init_context();
	context->header.xid = 100; /*TODO: Make this random, its used as a transaction id*/

	get_hardware_addr((char*) &context->header.chaddr);
	dhcp_set_int8_options(context, DHCP_OPTION_MESSAGE_TYPE, DHCPREQUEST);
	dhcp_set_int32_options(context, 54, dhcp.server_ip);
	dhcp_set_int32_options(context, 50, dhcp.requested_ip);

	char params[] = {
		DHCP_OPTION_SUBNET_MASK, DHCP_OPTION_ROUTER, DHCP_OPTION_DNS, DHCP_OPTION_BROADCAST
	};

	dhcp_set_blob_options(context, DHCP_OPTION_PARAM_REQUEST_LIST, (char*) &params, sizeof(params));

	char* buffer = (char*) malloc(1501);
	memcpy(buffer, (char*) &context->header, sizeof(context->header));

	int cursor = dhcp_serialize(context, buffer, 1501);
	dhcp_udp_send(name.c_str(), (unsigned char*) buffer, cursor);

	free(buffer);
	dhcp_free_context(context);
}

void Interface::request_lease_dhcp_release(){
	struct dhcp_builder_context* context = dhcp_init_context();
	context->header.xid = 100; /*TODO: Make this random, its used as a transaction id*/

	get_hardware_addr((char*) &context->header.chaddr);
	dhcp_set_int8_options(context, DHCP_OPTION_MESSAGE_TYPE, DHCPRELEASE);
	dhcp_set_int32_options(context, 54, dhcp.server_ip);
	dhcp_set_int32_options(context, 50, dhcp.requested_ip);

	char* buffer = (char*) malloc(1501);
	memcpy(buffer, (char*) &context->header, sizeof(context->header));

	int cursor = dhcp_serialize(context, buffer, 1501);
	dhcp_udp_send(name.c_str(), (unsigned char*) buffer, cursor);

	free(buffer);
	dhcp_free_context(context);
}

void Interface::request_lease_handle_message(char* response_message, size_t len){
	struct dhcp_builder_context* response_context = dhcp_init_context();
	if(dhcp_deserialize(response_context, (char*) response_message, len)){
		//Check the transaction type
		//Check the transaction id
		if(response_context->header.op != 2 || response_context->header.xid != dhcp.xid){
			return;
		}

		//Check that we are getting an offer:
		u_int8_t dhcp_message_type = 0;
		dhcp_get_int8_option(response_context, DHCP_OPTION_MESSAGE_TYPE, &dhcp_message_type);
		switch(dhcp_message_type){
			case DHCPOFFER:
				dhcp.requested_ip = response_context->header.yiaddr;
				dhcp_get_int32_option(response_context, 54, &dhcp.server_ip);
				dhcp.state = DHCP_TRANSACTION_REQUEST_SENT;

				Logger::instance()->log("sphirewalld.intmgr.dhcpclient", INFO, 
						"DHCPOFFER received from %s on interface %s, sending DHCPREQUEST",IP4Addr::ip4AddrToString(dhcp.server_ip).c_str(), name.c_str());

				request_lease_dhcp_request();
				break;

			case DHCPACK:
				dhcp.state = DHCP_TRANSACTION_FINISHED;
				dhcp_get_int32_option(response_context, DHCP_OPTION_SUBNET_MASK, &dhcp.lease.mask);
				dhcp_get_int32_option(response_context, DHCP_OPTION_ROUTER, &dhcp.lease.gw);
				dhcp_get_int32_option(response_context, DHCP_OPTION_DNS, &dhcp.lease.dns);
				dhcp_get_int32_option(response_context, DHCP_OPTION_BROADCAST, &dhcp.lease.broadcast);
				dhcp_get_int32_option(response_context, DHCP_OPTION_LEASE_TIME, &dhcp.lease.expirey_time);
				dhcp.lease.expirey_time += time(NULL);
				dhcp.lease.ip = dhcp.requested_ip;	

				Logger::instance()->log("sphirewalld.intmgr.dhcpclient", INFO, 
						"DHCPACK received from %s on interface %s, returning lease", IP4Addr::ip4AddrToString(dhcp.server_ip).c_str(), name.c_str());

				break;

			case DHCPNAK:
				dhcp.state = DHCP_TRANSACTION_RETRY_REQUIRED;
			default:
				break;
		};
	}
}

int Interface::dhcp_request_lease(){
	stop_dhclient_if_running();

	int raw_fd = dhcp_open_raw_socket(ifid);

	//Send discover request
	dhcp.state = DHCP_TRANSACTION_DISCOVER_SENT;
	dhcp.xid = 100;
	dhcp.retries = 0;

	Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", INFO, "Sending DHCPDISCOVER to 255.255.255.255 on interface %s", name.c_str());
	request_lease_dhcp_discover();
	while(dhcp.state != DHCP_TRANSACTION_FINISHED){
		char response[MAX_PACKET_SIZE];
		memset(&response, 0, sizeof(response));

		int response_len = dhcp_udp_recv(raw_fd, (unsigned char*) &response, sizeof(response));
		if(response_len < 0){
			if((dhcp.state == DHCP_TRANSACTION_RETRY_REQUIRED || dhcp.state == DHCP_TRANSACTION_DISCOVER_SENT) && dhcp.retries < MAX_RETRIES){
				dhcp.state = DHCP_TRANSACTION_DISCOVER_SENT;
				dhcp.xid = 100;
				dhcp.retries++;

				Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", ERROR, "DHCPDISCOVER on %s timed out, retrying", name.c_str());

				request_lease_dhcp_discover();
			}else if(dhcp.state == DHCP_TRANSACTION_REQUEST_SENT && dhcp.retries < MAX_RETRIES){
				Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", ERROR, "DHCPREQUEST on %s timed out, retrying", name.c_str());
				request_lease_dhcp_request();
				dhcp.retries++;
			}else{
				Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", INFO, "Failed to get a valid dhcp lease for interface %s, timed out", name.c_str());

				dhcp.state = DHCP_TRANSACTION_FAILED;
				break;
			}
		}

		request_lease_handle_message((char*) &response, sizeof(response));
	}

	//Set interface details
	if(dhcp.state == DHCP_TRANSACTION_FINISHED){
		return 0;
	}
	return -1;
}

int Interface::dhcp_release_lease(){
	request_lease_dhcp_release();	
	return 0;
}

void Interface::__configure_interface_routing(unsigned int ip, unsigned int mask, unsigned int gw){
	//Lets add a default route
	NRoute* route = new NRoute();
	route->route_device = ifid;
	route->route_nexthop = gw;
	System::getInstance()->get_route_manager()->add_route_entry(NRoutePtr(route));

	//We also need to configure the routes on the other route tables
	NRoute* default_route_table = new NRoute();
	default_route_table->route_device = ifid;
	default_route_table->route_nexthop = gw; 
	default_route_table->route_table_id = ifid;

	System::getInstance()->get_route_manager()->add_route_entry(NRoutePtr(default_route_table));

	NRoute* interface_route_table = new NRoute();
	interface_route_table->destination_cidr = __builtin_popcount(mask);
	interface_route_table->destination = (ip & mask);
	interface_route_table->route_device = ifid;
	interface_route_table->route_table_id = ifid;
	interface_route_table->link_scope= true;

	System::getInstance()->get_route_manager()->add_route_entry(NRoutePtr(interface_route_table));

	NRoutingRule* mark_rule = new NRoutingRule();
	mark_rule->fw_mark = ifid;
	mark_rule->route_table_id = ifid;
	mark_rule->source_ip = 0;
	System::getInstance()->get_route_manager()->add_route_rule(mark_rule);

	NRoutingRule* ip_rule = new NRoutingRule();
	ip_rule->source_ip = gw; 
	ip_rule->route_table_id = ifid;

	System::getInstance()->get_route_manager()->add_route_rule(ip_rule);
}

void Interface::refresh_address_configuration(){	
	if(addr_mode == INTERFACE_ADDR_MODE_DHCP){
		//Request a dhcp lease
		Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", INFO, "Requesting a dhcp lease for device %s", name.c_str());
		if(dhcp_request_lease() == 0){
			Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", INFO, "A lease was offerred, erasing current addresses and replacing");
			for(IPv4AddressPtr c : ipv4_addresses){	
				remove_ipv4_address(c->ip, c->mask, c->bcast);
			}

			add_ipv4_address(ntohl(dhcp.lease.ip), ntohl(dhcp.lease.mask), ntohl(dhcp.lease.broadcast));

			//We must also add a default route if provided:
			if(dhcp.lease.gw != 0){
				//Lets add a default route
				__configure_interface_routing(ntohl(dhcp.lease.ip), ntohl(dhcp.lease.mask), ntohl(dhcp.lease.gw));
			}

			if(dhcp.lease.dns != 0){
				//Do we have a dns server? Lets get this configured
				DNSConfig* dns_config = System::getInstance()->get_dns_config();
				dns_config->setNS1(IP4Addr::ip4AddrToString(ntohl(dhcp.lease.dns)));
				dns_config->save();
			}


			Logger::instance()->log("sphirewalld.interface.refresh_address_configuration", INFO, "Finished setting the dhcp lease address");
		}	
	}else{
		if(configuration_entity){
			for(IPv4AddressPtr c : ipv4_addresses){
				remove_ipv4_address(c->ip, c->mask, c->bcast);
			}

			for(IPv4AddressPtr c : configuration_entity->ipv4_addresses){
				add_ipv4_address(c->ip, c->mask, c->bcast);

				if(gateway != 0){
					__configure_interface_routing(c->ip, c->mask, gateway);
				}
			}
		}
	}	
}

int IntMgr::create_bridge_interface(std::string name){
	int ret;
	int br_socket_fd;
	if ((br_socket_fd = socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return -1;
	}
#ifdef SIOCBRADDBR
	ret = ioctl(br_socket_fd, SIOCBRADDBR, name.c_str());
	if (ret < 0)
#endif
	{
		char _br[IFNAMSIZ];
		unsigned long arg[3] 
			= { BRCTL_ADD_BRIDGE, (unsigned long) _br };

		strncpy(_br, name.c_str(), IFNAMSIZ);
		ret = ioctl(br_socket_fd, SIOCSIFBR, arg);
	} 

	close(br_socket_fd);
	return ret < 0 ? errno : 0;
}

int IntMgr::delete_bridge_interface(std::string name){
	//First remove all interfaces on the bridge:
	InterfacePtr to_delete = get_interface(name);
	BridgeInterface* target_bridge = (BridgeInterface*) to_delete.get();
	for(string interface : target_bridge->get_bridged_devices()){
		target_bridge->remove_bridged_device(interface);
	}

	int ret;
	int br_socket_fd;
	if ((br_socket_fd = socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return -1;
	}

#ifdef SIOCBRDELBR	
	ret = ioctl(br_socket_fd, SIOCBRDELBR, name.c_str());
	if (ret < 0)
#endif
	{
		char _br[IFNAMSIZ];
		unsigned long arg[3] 
			= { BRCTL_DEL_BRIDGE, (unsigned long) _br };

		strncpy(_br, name.c_str(), IFNAMSIZ);
		ret = ioctl(br_socket_fd, SIOCSIFBR, arg);
	} 

	close(br_socket_fd);
	return ret < 0 ? errno : 0;
}

bool IntMgr::is_bridge(std::string name){
	stringstream ss; ss << "/sys/class/net/" << name << "/bridge";
	struct stat s;
	int err = stat(ss.str().c_str(), &s);
	if(err == -1) {
		return false;
	}
	return true;
}

#define MAX_PORTS 256
list<std::string> BridgeInterface::get_bridged_devices(){
	list<string> ret;	

	int i, err, count;
	struct ifreq ifr;
	char ifname[IFNAMSIZ];
	int ifindices[MAX_PORTS];
	unsigned long args[4] = { BRCTL_GET_PORT_LIST,
		(unsigned long)ifindices, MAX_PORTS, 0 };

	int br_socket_fd;
	if ((br_socket_fd = socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return ret;
	}

	memset(ifindices, 0, sizeof(ifindices));
	strncpy(ifr.ifr_name, name.c_str(), IFNAMSIZ);
	ifr.ifr_data = (char *) &args;

	err = ioctl(br_socket_fd, SIOCDEVPRIVATE, &ifr);
	if (err < 0) {
		return ret;
	}

	count = 0;
	for (i = 0; i < MAX_PORTS; i++) {
		if (!ifindices[i])
			continue;

		if (!if_indextoname(ifindices[i], ifname)) {
			continue;
		}

		++count;
		ret.push_back(ifname);
	}

	close(br_socket_fd);
	return ret;
}

int BridgeInterface::add_bridged_device(std::string interface){
	struct ifreq ifr;
	int err;
	int ifindex = if_nametoindex(interface.c_str());

	if (ifindex == 0) 
		return ENODEV;

	int br_socket_fd;
	if ((br_socket_fd = socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return -1;
	}

	strncpy(ifr.ifr_name, name.c_str(), IFNAMSIZ);
#ifdef SIOCBRADDIF
	ifr.ifr_ifindex = ifindex;
	err = ioctl(br_socket_fd, SIOCBRADDIF, &ifr);
	if (err < 0)
#endif
	{
		unsigned long args[4] = { BRCTL_ADD_IF, ifindex, 0, 0 };

		ifr.ifr_data = (char *) args;
		err = ioctl(br_socket_fd, SIOCDEVPRIVATE, &ifr);
	}

	close(br_socket_fd);
	return err < 0 ? errno : 0;
}

int BridgeInterface::remove_bridged_device(std::string interface){
	struct ifreq ifr;
	int err;
	int ifindex = if_nametoindex(interface.c_str());

	if (ifindex == 0) 
		return ENODEV;

	int br_socket_fd;
	if ((br_socket_fd = socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return -1;
	}

	strncpy(ifr.ifr_name, name.c_str(), IFNAMSIZ);
#ifdef SIOCBRDELIF
	ifr.ifr_ifindex = ifindex;
	err = ioctl(br_socket_fd, SIOCBRDELIF, &ifr);
	if (err < 0)
#endif		
	{
		unsigned long args[4] = { BRCTL_DEL_IF, ifindex, 0, 0 };

		ifr.ifr_data = (char *) args;
		err = ioctl(br_socket_fd, SIOCDEVPRIVATE, &ifr);
	}

	close(br_socket_fd);
	return err < 0 ? errno : 0;
}

int IntMgr::create_vlan_interface(std::string binding_interface, int vlan_id){
	int fd;
	if ((fd= socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return -1;
	}

	struct vlan_ioctl_args if_request;
	memset(&if_request, 0, sizeof(struct vlan_ioctl_args));

	strcpy(if_request.device1, binding_interface.c_str());
	if_request.u.VID = vlan_id;
	if_request.cmd = ADD_VLAN_CMD;

	if (ioctl(fd, SIOCSIFVLAN, &if_request) < 0) {
		perror("ioctl");
		return -1;
	}

	close(fd);
	return 0;
}

int IntMgr::delete_vlan_interface(std::string binding_interface, int vlan_id){
	int fd;
	if ((fd= socket(AF_LOCAL, SOCK_STREAM, 0)) < 0){
		return -1;
	}

	struct vlan_ioctl_args if_request;
	memset(&if_request, 0, sizeof(struct vlan_ioctl_args));

	sprintf(if_request.device1, "%s.%d", binding_interface.c_str(), vlan_id);

	if_request.u.VID = vlan_id;
	if_request.cmd = DEL_VLAN_CMD;

	if (ioctl(fd, SIOCSIFVLAN, &if_request) < 0) {
		perror("ioctl");
		return -1;
	}

	close(fd);
	return 0;
}

bool IntMgr::is_vlan(std::string name){
	if(name.find(".") != string::npos){
		return true;
	}

	return false;
}

int VlanInterface::get_vlan_id(){
	string vlan_id = name.substr(name.find(".") + 1, name.size());
	return atoi(vlan_id.c_str());
}

std::string VlanInterface::get_vlan_interface(){
	return name.substr(0, name.find("."));
}

bool IntMgr::is_bonding(std::string name){
	stringstream ss; ss << "/sys/class/net/" << name << "/bonding";
	struct stat s;
	int err = stat(ss.str().c_str(), &s);
	if(err == -1) {
		return false;
	}
	return true;
}

#define SYSFS_BONDING_FILE "/sys/class/net/bonding_masters"
int IntMgr::create_bonding_interface(std::string name){
	FILE *f = fopen(SYSFS_BONDING_FILE, "w"); 
	fprintf(f, "+%s", name.c_str());
	fclose(f);
}

int IntMgr::delete_bonding_interface(std::string name){
	FILE *f = fopen(SYSFS_BONDING_FILE, "w");
	fprintf(f, "-%s", name.c_str());
	fclose(f);
}

std::list<std::string> BondingInterface::get_slaves(){
	list<string> ret;
	stringstream ss; ss << "/sys/class/net/" << name << "/";

	struct dirent *entry;
	DIR *dir;
	dir = opendir (ss.str().c_str());
	if(dir){
		while ((entry = readdir (dir)) != NULL) {
			string filename = entry->d_name;
			if(filename.find("slave_") != string::npos){
				ret.push_back(filename.substr(6, filename.size()));
			}
		}

		closedir(dir);
	}
	return ret;
}

int BondingInterface::add_slave(std::string slave){
	stringstream ss; ss << "/sys/class/net/" << name << "/bonding/slaves";
	FILE *f = fopen(ss.str().c_str(), "w");
	fprintf(f, "+%s", slave.c_str());
	fclose(f);
}

int BondingInterface::remove_slave(std::string slave){
	stringstream ss; ss << "/sys/class/net/" << name << "/bonding/slaves";
	FILE *f = fopen(ss.str().c_str(), "w");
	fprintf(f, "-%s", slave.c_str());
	fclose(f);
}

int BondingInterface::get_variable(std::string key){
	stringstream ss; ss << "/sys/class/net/" << name << "/bonding/" << key;
	FILE *f = fopen(ss.str().c_str(), "r");
	int i = 0;
	fscanf (f, "%d", &i);    
	fclose(f);
	return i;
}

void BondingInterface::set_variable(std::string key, int value){
	stringstream ss; ss << "/sys/class/net/" << name << "/bonding/" << key;
	FILE *f = fopen(ss.str().c_str(), "w");
	fprintf(f, "%d", value);
	fclose(f);
}


void BondingInterface::set_bonding_mode(int value){
	set_variable("mode", value);
}

void BondingInterface::set_link_state_poll_frequency(int value){
	set_variable("miimon", value);
}

void BondingInterface::set_link_down_delay(int value){
	set_variable("downdelay", value);
}

void BondingInterface::set_link_up_delay(int value){
	set_variable("updelay", value);
}

int BondingInterface::get_bonding_mode(){
	stringstream ss; ss << "/sys/class/net/" << name << "/bonding/mode";
	FILE *f = fopen(ss.str().c_str(), "r");
	int i = 0;
	char buffer[1024];
	fscanf (f, "%s %d", &buffer, &i);
	fclose(f);

	return i; 
}

int BondingInterface::get_link_state_poll_frequency(){
	return get_variable("miimon");
}

int BondingInterface::get_link_down_delay(){
	return get_variable("downdelay");
}

int BondingInterface::get_link_up_delay(){
	return get_variable("updelay");
}

void Interface::save_state_to_configuration_entity(){
	if(configuration_entity){
		configuration_entity->interface= name;
		configuration_entity->label= label;
		configuration_entity->gateway = gateway;
		configuration_entity->dhcp = addr_mode == INTERFACE_ADDR_MODE_DHCP;
		if(!configuration_entity->dhcp){
			configuration_entity->ipv4_addresses = ipv4_addresses;
		}

		configuration_entity->vlan = is_vlan();
		configuration_entity->lacp = is_bonding();
		configuration_entity->bridge = is_bridge();
		configuration_entity->physical = !(is_bridge() || is_bonding() || is_vlan());
	}
}

void BondingInterface::save_state_to_configuration_entity(){
	Interface::save_state_to_configuration_entity();
	if(configuration_entity){
		configuration_entity->bondedDevices = get_slaves();
		configuration_entity->lacpMode = get_bonding_mode();
		configuration_entity->lacpLinkStatePollFrequency = get_link_state_poll_frequency();
		configuration_entity->lacpLinkStateDownDelay = get_link_down_delay();
		configuration_entity->lacpLinkStateUpDelay = get_link_up_delay();
	}
}

void VlanInterface::save_state_to_configuration_entity(){
	Interface::save_state_to_configuration_entity();
	if(configuration_entity){
		configuration_entity->vlanId = get_vlan_id();
		configuration_entity->vlanInterface = get_vlan_interface();
	}
}

void BridgeInterface::save_state_to_configuration_entity(){
	Interface::save_state_to_configuration_entity();
	if(configuration_entity){
		configuration_entity->bridgeDevices= get_bridged_devices();
	}
}

void IntMgr::delete_interface(InterfacePtr ptr){
	//Remove the configuration entity if it exists
	if(ptr->configuration_entity){
		configured_interfaces.remove(ptr->configuration_entity);
	}

	if(ptr->is_vlan()){
		VlanInterface* vlan = (VlanInterface*) ptr.get();
		delete_vlan_interface(vlan->get_vlan_interface(), vlan->get_vlan_id());
	}

	if(ptr->is_bridge()){
		delete_bridge_interface(ptr->name);
	}

	if(ptr->is_bonding()){
		delete_bonding_interface(ptr->name);
	}
}

DNSConfig::DNSConfig() {
	LinuxUtils::exec("cat /etc/resolv.conf | grep domain | cut -d' ' -f2", domain);
	LinuxUtils::exec("cat /etc/resolv.conf | grep search | cut -d' ' -f2", search);

	string dnsServers;
	LinuxUtils::exec("cat /etc/resolv.conf | grep nameserver | cut -d' ' -f2", dnsServers);
	nameserver1 = dnsServers.substr(0, dnsServers.find("\n"));
	nameserver2 = dnsServers.substr(dnsServers.find("\n") + 1, dnsServers.size());
}

void DNSConfig::save() {
	ofstream fp;
	fp.open("/etc/resolv.conf", fstream::out);

	if (fp.is_open()) {
		if(search.size() != 0){
			fp << "search " << search << endl;
		}

		if(domain.size() != 0){
			fp << "domain " << domain << endl;
		}

		if(nameserver1.size() != 0){
			fp << "nameserver " << nameserver1 << endl;
		}

		if(nameserver2.size() != 0){
			fp << "nameserver " << nameserver2 << endl;
		}
		fp.close();
	}
}

void Interface::stop_dhclient_if_running(){
	system("killall -9 dhclient");
}

