/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HTTPREQUEST_WRAPPER_H
#define HTTPREQUEST_WRAPPER_H

#include <exception>
#include <string>

class HttpRequestWrapperException : public std::exception {
	public:

		HttpRequestWrapperException(std::string message) {
			w = message;
		}

		~HttpRequestWrapperException() throw () {

		}

		virtual const char *what() const throw () {
			return "HttpRequestWrapperException";
		}

		std::string message() {
			return w;
		}

	private:
		std::string w;
};

enum HttpRequestType {
	GET, POST
};

class HttpRequestWrapper {
	public:
		HttpRequestWrapper(std::string url, HttpRequestType type);
		~HttpRequestWrapper();
		std::string execute();

		std::string useragent;
		std::string postdata;
	private:
		std::string url;
		HttpRequestType type;
};

#endif
