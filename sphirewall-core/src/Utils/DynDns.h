/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DYNAMIC_DNS
#define DYNAMIC_DNS
#include "Core/Cron.h"
#include "Core/ConfigurationManager.h"

enum DynamicDnsClientProvider {
	DYNDNS = 0, FREEDNS = 1
};

class DynamicDnsClient {
	public:
		bool update();

		std::string username;
		std::string password;
		std::string domain;
		DynamicDnsClientProvider type;
		bool active;
};

class GlobalDynDns : public CronJob, public Configurable {
	public:
		GlobalDynDns() :
			CronJob(60 * 5, "GLOBAL_DNS_CRON", true){
			this->client = new DynamicDnsClient();
			enabled = false;
		}

		void run();
		void run(bool force);

		bool load();
		void save();

		std::string getUsername() const {
			return client->username;
		}

		std::string getPassword() const {
			return client->password;
		}

		std::string getDomain() const {
			return client->domain;
		}

		DynamicDnsClientProvider getType() const {
			return client->type;
		}

		void setUsername(std::string username) {
			client->username = username;
		}

		void setPassword(std::string password) {
			client->password = password;
		}

		void setDomain(std::string domain) {
			client->domain = domain;
		}

		bool isEnabled() {
			return enabled;
		}

		void setEnabled(bool enabled) {
			this->enabled = enabled;
		}

		void setType(DynamicDnsClientProvider type) {
			this->client->type = type;
		}

		DynamicDnsClientProvider getType() {
			return client->type;
		}

		bool active() {
			return client->active;
		}

		std::string getLastIp() const {
			return lastip;
		}

                const char* getConfigurationSystemName(){
                        return "Dynamic dns";
                }

	private:
		DynamicDnsClient *client;
		std::string lastip;
		bool enabled;
};

#endif
