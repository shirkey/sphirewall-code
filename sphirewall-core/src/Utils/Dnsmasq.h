#ifndef DNSMASQ_H
#define DNSMASQ_H

#include "Utils/Interfaces.h"

class ConfigurationManager;
class DnsmasqManager : public IntMgrChangeListener {
	public:
		DnsmasqManager(IntMgr* interfaceManager, ConfigurationManager* config);
		void publish();
		std::string generateConfigurationFile();

		bool isDnsForwarderEnabled();
		string getDnsFowarderDomain();
		string getDnsForwarderServerPrimary();
		string getDnsForwarderServerSecondary();

		void start();
		void stop();
		bool running();

		void initalize();

		//Change listener for interface manager
		void interface_change() {
			initalize();
		}

		static int DNSMASQ_ENABLED;
	private:
		IntMgr* interfaceManager;
		ConfigurationManager* config;
};

#endif
