#ifndef DUMP_H
#define DUMP_H
void hexdump(std::ostream &output, char *buffer, size_t len, size_t line_len);
#endif
