#include <iostream>
#include <unistd.h>
#include "Utils/LinuxUtils.h"

using namespace std;

int LinuxUtils::exec(string command, string &ret) {
        string sbuf;

        char cbuf[1024];

        FILE *fp;
        fp = popen(command.c_str(), "r");
	if(!fp){
		return -1;
	}

        while (fgets(cbuf, 1024, fp) != NULL)
                sbuf += cbuf;

	int exitCode = pclose(fp);
        ret = sbuf;
        return exitCode;
}

int LinuxUtils::execWithNoFds(std::string s) {
        if (!fork()) {
                int fd_limit = sysconf(_SC_OPEN_MAX);

                for (int x = 3; x < fd_limit; x++) {
                        close(x);
                }

                if (system(s.c_str()));

                exit(-1);
        }

        return 0;
}

