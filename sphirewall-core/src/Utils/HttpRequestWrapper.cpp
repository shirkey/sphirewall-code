/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <curl/curl.h>

using namespace std;
#include "Utils/HttpRequestWrapper.h"
#include "Utils/Lock.h"

/*
size_t httprequestwrapper_function(char *ptr, size_t size, size_t nmemb, void *userdata) {
    ((std::string*)userdata)->append((char*)ptr, size * nmemb);
    return size * nmemb;
}

HttpRequestWrapper::HttpRequestWrapper(std::string url, HttpRequestType type)
	: url(url), type(type) {
}

HttpRequestWrapper::~HttpRequestWrapper() {
}

std::string HttpRequestWrapper::execute() {
	CURL *c;
	string buffer;

	c = curl_easy_init();
	curl_easy_setopt(c, CURLOPT_URL, url.c_str());
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, httprequestwrapper_function);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, &buffer);

	if (useragent.size() > 0) {
		curl_easy_setopt(c, CURLOPT_USERAGENT, useragent.c_str());
	}

	if (postdata.size() > 0) {
		curl_easy_setopt(c, CURLOPT_POSTFIELDS, postdata.c_str());
	}

	curl_easy_setopt(c, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(c, CURLOPT_SSL_VERIFYHOST, 0L);

	int err = curl_easy_perform(c);
	if (err != 0) {
		curl_easy_cleanup(c);
		throw HttpRequestWrapperException("an error was thrown from curl");
	}

	curl_easy_cleanup(c);
	return buffer;
}
*/

static Lock *lock = new Lock();
std::string buf;
size_t httprequestwrapper_function(char *ptr, size_t size, size_t nmemb, void *userdata) {
	buf += string(ptr, size * nmemb);
	return size * nmemb;
}

HttpRequestWrapper::HttpRequestWrapper(std::string url, HttpRequestType type)
	: url(url), type(type) {
}

HttpRequestWrapper::~HttpRequestWrapper() {
}

std::string HttpRequestWrapper::execute() {
	CURL *c;
	c = curl_easy_init();
	curl_easy_setopt(c, CURLOPT_URL, url.c_str());
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, httprequestwrapper_function);

	if (useragent.size() > 0) {
		curl_easy_setopt(c, CURLOPT_USERAGENT, useragent.c_str());
	}

	if (postdata.size() > 0) {
		curl_easy_setopt(c, CURLOPT_POSTFIELDS, postdata.c_str());
	}

	curl_easy_setopt(c, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(c, CURLOPT_SSL_VERIFYHOST, 0L);

	lock->lock();
	buf.clear();
	int err = curl_easy_perform(c);

	if (err != 0) {
		lock->unlock();
		curl_easy_cleanup(c);
		throw HttpRequestWrapperException("an error was thrown from curl");
	}

	lock->unlock();
	curl_easy_cleanup(c);
	return buf;
}
