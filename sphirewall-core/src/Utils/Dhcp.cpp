#include <iostream>
#include <net/if.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <map>
#include <poll.h>
#include <linux/types.h>
#include <sys/socket.h>
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <sys/ioctl.h>
#include <linux/rtnetlink.h>
#include <curl/curl.h>
#include <sstream>
#include "autoconf.h"
#include "Utils/IP4Addr.h"

using namespace std;

#include "Utils/Dhcp.h"
#include "Utils/Dump.h"

#define SOCKET_TIMEOUT_PERIOD 5000

int dhcp_open_raw_socket(int ifindex)
{
	int fd;
	struct sockaddr_ll sock;

	if ((fd = socket(PF_PACKET, SOCK_DGRAM, htons(ETH_P_IP))) < 0) {
		return -1;
	}

	sock.sll_family = AF_PACKET;
	sock.sll_protocol = htons(ETH_P_IP);
	sock.sll_ifindex = ifindex;
	if (bind(fd, (struct sockaddr *) &sock, sizeof(sock)) < 0) {
		close(fd);
		return -1;
	}

	return fd;

}

int dhcp_udp_send(const char* ifname, unsigned char* data, unsigned int len){
	struct ip ip;
	struct udphdr udp;
	const int on = 1;
	struct sockaddr_in sin;
	unsigned char* packet = (unsigned char*)malloc(sizeof(struct ip) + sizeof(struct udphdr) + len);

	ip.ip_hl = 0x5;
	ip.ip_v = 0x4;
	ip.ip_tos = 0x0;
	ip.ip_len = htons(sizeof(struct ip) + sizeof(struct udphdr) + len);
	ip.ip_id = htons(12830);
	ip.ip_off = 0x0;
	ip.ip_ttl = 64;
	ip.ip_p = IPPROTO_UDP;
	ip.ip_sum = 0x0;
	ip.ip_src.s_addr = inet_addr("0.0.0.0");
	ip.ip_dst.s_addr = inet_addr("255.255.255.255");
	memcpy(packet, &ip, sizeof(struct ip));

	udp.source = htons(68);
	udp.dest = htons(67);
	udp.len = htons(sizeof(struct udphdr) + len);
	udp.check = 0;
	memcpy(packet + 20, &udp, sizeof(struct udphdr));
	memcpy(packet + sizeof(struct ip) + sizeof(struct udphdr), data, len);

	int sd = 0;
	if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_UDP)) < 0) {
		perror("raw socket");
		return -1;
	}

	if (setsockopt(sd, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0) {
		perror("setsockopt");
		return -1;
	}

	if(setsockopt(sd, SOL_SOCKET, SO_BROADCAST, &on, sizeof on) < 0){
		perror("setsockopt");
		return -1;
	}

	if (setsockopt (sd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on) < 0) {
		perror("dhcp_socket/setsockopt: SO_REUSEADDR");
		return -1;
	}


	struct ifreq ifr;
	memset(&ifr, 0, sizeof(ifr));
	strncpy(ifr.ifr_name, ifname, sizeof(ifr.ifr_name));
	if (setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0) {
		perror("SO_BINDTODEVICE failed");
		return -1;
	}

	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = ip.ip_dst.s_addr;

	if (sendto(sd, packet, sizeof(struct ip) + sizeof(struct udphdr) + len, 0, (struct sockaddr *)&sin, sizeof(struct sockaddr)) < 0)
	{
		perror("sendto");
		return -1;
	}

	free(packet);
	close(sd);
}

int dhcp_udp_recv(int sd, unsigned char* data, unsigned int buffer_len){
	struct pollfd events[1];
	events[0].fd = sd;
	events[0].events = POLLIN;

	if(poll(events, 1, SOCKET_TIMEOUT_PERIOD) > 0){
		socklen_t fromlen = 0;
		char* recv_buffer = (char*) malloc(buffer_len);
		int ret = recvfrom(sd, recv_buffer, buffer_len, 0, NULL, &fromlen);

		int header_len = sizeof(struct ip) + sizeof(struct udphdr);
		struct iphdr* ip_header = (struct iphdr*) recv_buffer;
		struct udphdr* udp_header = (struct udphdr*) (recv_buffer+ sizeof(struct ip));
		if(ret < header_len){
			free(recv_buffer);
			return -1;
		}

		if(ip_header->protocol != IPPROTO_UDP || 
				udp_header->dest != htons(68)){
			free(recv_buffer);
			return -1;
		}

		int payload_len = ret - header_len;
		memcpy(data, (char*) recv_buffer + header_len, payload_len);

		free(recv_buffer);
		return payload_len;
	}

	return -1;
}

struct dhcp_builder_context* dhcp_init_context(){
	struct dhcp_builder_context* ctx = (struct dhcp_builder_context*) malloc(sizeof(struct dhcp_builder_context));
	memset(&ctx->header, 0, sizeof(struct dhcp_packet));

	for(int x= 0; x < 255; x++){
		ctx->options[x].len = 0;	
		ctx->options[x].data= NULL;	
	}

	//Setup resonible defaults
        ctx->header.op = 1; /* Boot Request*/
        ctx->header.htype = 1; /* 10m Ethernet*/
        ctx->header.hlen = 6; /* mac addresses are 6 octlets long*/

	return ctx;
}

void dhcp_free_context(struct dhcp_builder_context* context){
	for(int x= 0; x < 255; x++){
		free(context->options[x].data);
	}

	free(context);
}


void dhcp_set_blob_options(struct dhcp_builder_context* context, u_int8_t type, char* blob, u_int8_t size){
	context->options[type].len = size;
	context->options[type].data = (char*) malloc(size);
        memcpy(context->options[type].data, blob, size);
}

void dhcp_set_int8_options(struct dhcp_builder_context* context, u_int8_t type, u_int8_t value){
	context->options[type].len = sizeof(u_int8_t);
	context->options[type].data = (char*) malloc(sizeof(value));
	context->options[type].data[0] = value;
} 

void dhcp_set_int32_options(struct dhcp_builder_context* context, u_int8_t type, u_int32_t value){
	context->options[type].len = sizeof(u_int32_t);
	context->options[type].data = (char*) malloc(sizeof(value));
	memcpy(context->options[type].data, &value, sizeof(value));
} 

int dhcp_get_int8_option(struct dhcp_builder_context* context, u_int8_t type, u_int8_t* value){
	if(context->options[type].data){
		memcpy(value, context->options[type].data, sizeof(u_int8_t));
		return sizeof(u_int8_t);
	}
	return -1;
}

int dhcp_get_int32_option(struct dhcp_builder_context* context, u_int8_t type, unsigned int* value){
	if(context->options[type].data){
		memcpy(value, context->options[type].data, sizeof(unsigned int));
		return sizeof(unsigned int);
	}
	return -1;
}

int dhcp_serialize(struct dhcp_builder_context* context, char* buffer, size_t len){
	memcpy(buffer, (char*) &context->header, sizeof(context->header));

	//Process options
	int cursor = sizeof(context->header);
	buffer[cursor++] = 99;
	buffer[cursor++] = 130;
	buffer[cursor++] = 83;
	buffer[cursor++] = 99;

	for(u_int8_t x = 0; x < 255; x++){
		struct dhcp_packet_option* option = &context->options[x];
		if(option->len > 0){
			buffer[cursor++] = x;
			buffer[cursor++] = option->len;
			for(u_int8_t y = 0; y < option->len; y++){
				buffer[cursor++] = option->data[y];
			}
		}
	}

	return cursor;
}

int dhcp_deserialize(struct dhcp_builder_context* context, char* buffer, size_t len){
	if(len < sizeof(context->header)){
		return -1;	
	}	

	int cursor = 0;
	memcpy((void*) &context->header, (void*) buffer, sizeof(context->header));
	cursor += sizeof(context->header);
	cursor += 4;

	//Lets work out the opts
	while(cursor < len){
		u_int8_t op = buffer[cursor++];
		u_int8_t oplen = buffer[cursor++];

		if(op == 255){
			break;
		}

		context->options[op].len = oplen;
		context->options[op].data = (char*) malloc(oplen);
		memcpy(context->options[op].data, buffer + cursor, oplen);
		cursor += oplen;
	}	

	return 1;
}

