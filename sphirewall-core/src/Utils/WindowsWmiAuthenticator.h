#ifndef WINWMIAUTH_H
#define WINWMIAUTH_H

#include <sstream>
#include <list>
using namespace std;

class WmiException: public exception {
	public:

		WmiException(std::string message) {
			w = message;
		}

		~WmiException() throw () {
		}

		virtual const char *what() const throw () {
			return "WMIC Exception";
		}

		const std::string &message() const {
			return w;
		}

	private:
		std::string w;
};

class WmiLoginEvent {
	public:
		std::string eventId;
		std::string user;
		std::string host;

		bool isValidUser();

		std::string toString() {
			std::stringstream ss;
			ss << "id:" << eventId << " user:" << getPlainUsername() << " host:" << getIpv4Address();
			return ss.str();
		}

		std::string getPlainUsername();
		std::string getIpv4Address();
};


class SessionDb;
class EventDb;
class AuthenticationManager;
class HostDiscoveryService;
class ConfigurationManager;

class WinWmiAuth {
	public:
		WinWmiAuth(SessionDb *sessionDb, EventDb *eventDb, AuthenticationManager *authManager, HostDiscoveryService *arp, ConfigurationManager *config):
			sessionDb(sessionDb), eventDb(eventDb), authManager(authManager), arp(arp), config(config) {
			cursor = 0;
		}

		WinWmiAuth() {
			cursor = 0;
		}

		bool isEnabled();
		std::string getUsername();
		std::string getDomain();
		std::string getPassword();
		std::string getHost();

		void poll();
		bool check();
		void run();
	private:
		int cursor;

		WmiLoginEvent *getRawEvent(std::string id);
		void seedCursor();
		int getStartCursor();
		std::string attribute(std::string raw, std::string key);
		std::list<std::string> findEventsSince(std::string id);
		std::string query(std::string input);
		void updateCursor(std::string input);
		void updateStartCursor();

		//Event Mapping Properties:
		std::string getUsernameAttribute();
		std::string getHostAttribute();
		std::string getTargetEventId();

		SessionDb *sessionDb;
		EventDb *eventDb;
		AuthenticationManager *authManager;
		HostDiscoveryService *arp;
		ConfigurationManager *config;
};

#endif
