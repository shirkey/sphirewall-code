#ifndef LINUX_UTILS
#define LINUX_UTILS

class LinuxUtils {
	public:
		static int execWithNoFds(std::string command);
		static int exec(std::string command, std::string &ret);
};

#endif
