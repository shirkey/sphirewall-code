/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

#include "Api/JsonManagementService.h"
#include "Api/Delegate.h"
#include "Api/Exceptions.h"
#include "Api/Delegates/GeneralDelegate.h"
#include "Core/Config.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/SysMonitor.h"
#include "Core/ConfigurationManager.h"

using namespace std;

class TestDelegate : public Delegate {
	public:

		TestDelegate() {
			hit = false;
		}

		JSONObject process(std::string url, JSONObject object) {
			hit = true;

			JSONObject obj;
			obj.put(L"blah", new JSONValue(StringToWString("testing")));

			return obj;
		}

		string rexpression() {
			return "test/process";
		}

		bool hit;
};

class MockSysMonitor : public virtual SysMonitor {
	public:

		MockSysMonitor() {

		}

};

TEST(JsonManagementService, seqAck) {
	TestDelegate *del = new TestDelegate();

	JsonManagementService *svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(del);

	string output = svc->process("{\"seq\":10,\"request\":\"test/process\",\"args\":null}", "").first;
	EXPECT_TRUE(del->hit == true);

	string expected = "{\"ack\":10,\"code\":0,\"response\":{\"blah\":\"testing\"}}\n";

	EXPECT_TRUE(expected.compare(output) == 0);
}


TEST(JsonManagementService, parseToDelegate) {
	TestDelegate *del = new TestDelegate();

	JsonManagementService *svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(del);

	string output = svc->process("{\"request\":\"test/process\",\"args\":null}", "").first;
	EXPECT_TRUE(del->hit == true);

	string expected = "{\"code\":0,\"response\":{\"blah\":\"testing\"}}\n";

	EXPECT_TRUE(expected.compare(output) == 0);
}

TEST(JsonManagementService, jsonErrorHandling_seqAck) {
	JsonManagementService *svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(new GeneralDelegate(new Config()));

	string output = svc->process("{\"seq\":20,\"request\":\"general/config/get\",\"args\":{\"vv\":null}}", "").first;
	string expected = "{\"code\":-1,\"message\":\"The field <key> could not be found or parsed correctly.\",\"ack\":20}\n";
	EXPECT_STREQ(expected.c_str(), output.c_str());
}

TEST(JsonManagementService, jsonErrorHandling) {
	JsonManagementService *svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(new GeneralDelegate(new Config()));

	string output = svc->process("{\"request\":\"general/config/get\",\"args\":{\"vv\":null}}", "").first;
	string expected = "{\"code\":-1,\"message\":\"The field <key> could not be found or parsed correctly.\"}\n";
	EXPECT_STREQ(expected.c_str(), output.c_str());
}

TEST(JsonManagementService, delegateNotFound) {
	JsonManagementService *svc = new JsonManagementService();
	svc->ignoreAuthentication(true);
	svc->registerDelegate(new GeneralDelegate(new Config()));

	string output = svc->process("{\"request\":\"dude\",\"args\":{\"vv\":null}}", "").first;

	string expected = "{\"code\":-1,\"message\":\"Could not find the requested delegate.\"}\n";
	EXPECT_STREQ(expected.c_str(), output.c_str());
}

TEST(GeneralDelegate, runtime_list) {
	int a = 0, b = 1;
	Config *config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	config->getRuntime()->loadOrPut("key1", &a);
	config->getRuntime()->loadOrPut("key2", &b);

	GeneralDelegate *del = new GeneralDelegate(config);
	JSONObject pass;
	JSONObject obj = del->process("general/runtime/list", pass);

	JSONArray elems = obj[L"keys"]->AsArray();
	JSONObject o1 = elems[0]->AsObject();
	JSONObject o2 = elems[1]->AsObject();
	EXPECT_TRUE(o1[L"key"]->String().compare("key1") == 0);
	EXPECT_TRUE(o2[L"key"]->String().compare("key2") == 0);
}

TEST(GeneralDelegate, runtime_get) {
	int a = 0, b = 1;
	Config *config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	config->getRuntime()->loadOrPut("key1", &a);
	config->getRuntime()->loadOrPut("key2", &b);

	GeneralDelegate *del = new GeneralDelegate(config);
	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));

	JSONObject obj = del->process("general/runtime/get", pass);

	EXPECT_TRUE(obj[L"value"]->AsNumber() == 0);
}

TEST(GeneralDelegate, runtime_set) {
	int a = 0, b = 1;
	Config *config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	config->getRuntime()->loadOrPut("key1", &a);
	config->getRuntime()->loadOrPut("key2", &b);

	GeneralDelegate *del = new GeneralDelegate(config);
	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));
	pass.put(L"value", new JSONValue((double) 10));

	del->process("general/runtime/set", pass);

	EXPECT_TRUE(a == 10);
}

TEST(GeneralDelegate, runtime_set_invalid_entry) {
	Config *config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	GeneralDelegate *del = new GeneralDelegate(config);
	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));
	pass.put(L"value", new JSONValue((double) 10));

	del->process("general/runtime/set", pass);
}

TEST(GeneralDelegate, runtime_get_invalid_entry) {
	Config *config = new Config();
	config->setConfigurationManager(new ConfigurationManager());
	GeneralDelegate *del = new GeneralDelegate(config);

	JSONObject pass;
	pass.put(L"key", new JSONValue((std::string) "key1"));

	bool exception = false;

	try {
		del->process("general/runtime/get", pass);
	}
	catch (DelegateGeneralException e) {
		exception = true;
	}

	EXPECT_TRUE(exception);
}

