/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/algorithm/string.hpp>

using namespace std;

#include "Api/Delegates/NetworkSettingsDelegate.h"

#include "Json/JSON.h"
#include "Api/Exceptions.h"
#include "Utils/StringUtils.h"
#include "Core/HostDiscoveryService.h"
#include "Core/ConnectionManager.h"
#include "Core/ConnectionManager.h"
#include "Core/Openvpn.h"
#include "Core/System.h"
#include "Core/Wireless.h"
#include "Utils/DynDns.h"
#include "Core/Event.h"
#include "Utils/IP4Addr.h"
#include "Utils/Interfaces.h"
#include "Utils/NRoute.h"
#include "Utils/NetworkUtils.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &NetworkSettingsDelegate::y))

NetworkSettingsDelegate::NetworkSettingsDelegate(IntMgr* interfaceManager, DNSConfig *dnsConfig, HostDiscoveryService *arpTable, Dhcp3ConfigurationManager *dhcp, DnsmasqManager *dns)
	: interfaceManager(interfaceManager), dnsConfig(dnsConfig), arp(arpTable), dhcpConfiguration(dhcp), eventDb(NULL), dnsmasq(dns) {

	installHandler("network/devices/list", devices_list);
	installHandler("network/devices/set", devices_set);
	installHandler("network/devices/delete", devices_delete);
	installHandler("network/devices/up", devices_up);
	installHandler("network/devices/down", devices_down);
	installHandler("network/devices/save", devices_save);

	installHandler("network/dns/get", dns_get);
	installHandler("network/dns/set", dns_set);
	installHandler("network/routes/list", routes_list);
	installHandler("network/routes/add", routes_add);
	installHandler("network/routes/del", routes_del);
	installHandler("network/arp/list", arp_list);
	installHandler("network/arp/size", arp_size);

	installHandler("network/dnsmasq/running", dnsmasq_running);
	installHandler("network/dnsmasq/start", dnsmasq_start);
	installHandler("network/dnsmasq/stop", dnsmasq_stop);
	installHandler("network/dnsmasq/publish", dnsmasq_publish);

	installHandler("network/devices/leases/add", devices_leases_add);
	installHandler("network/devices/leases/del", devices_leases_del);

	installHandler("network/connections/list", connections_list);
	installHandler("network/connections/add", connectons_add);
	installHandler("network/connections/del", connections_del);
	installHandler("network/connections/save", connections_save);
	installHandler("network/connections/connect", connections_connect);
	installHandler("network/connections/disconnect", connections_disconnect);

        installHandler("network/vpn", vpn_instances_list);
	installHandler("network/vpn/start", vpn_start);
	installHandler("network/vpn/stop", vpn_stop);
	installHandler("network/vpn/status", vpn_status);
        installHandler("network/vpn/create", vpn_instance_create);
        installHandler("network/vpn/remove", vpn_instance_remove);
        installHandler("network/vpn/options", vpn_options);

	installHandler("network/openvpn/log", openvpn_log);
	installHandler("network/openvpn/client/add", openvpn_clients_add);
	installHandler("network/openvpn/client/delete", openvpn_clients_delete);
	installHandler("network/openvpn/client/list", openvpn_clients_list);
	installHandler("network/openvpn/daemonexists", openvpn_daemonexists);

	installHandler("network/wireless/start", wireless_start);
	installHandler("network/wireless/stop", wireless_stop);
	installHandler("network/wireless/online", wireless_online);
	installHandler("network/dyndns", dyndns);
	installHandler("network/dyndns/set", dyndns_set);

	installHandler("network/ping", ping);
	installHandler("network/arp/quarantined", set_quarantined);
}

std::string NetworkSettingsDelegate::rexpression() {
	return "network/(.*)";
}

void NetworkSettingsDelegate::setConnectionManager(ConnectionManager *connectionManager) {
	this->connectionManager = connectionManager;
}

void NetworkSettingsDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

void NetworkSettingsDelegate::setOpenvpnManager(OpenvpnManager *o) {
	this->openvpn = o;
}

JSONObject NetworkSettingsDelegate::process(std::string uri, JSONObject obj) {
	try {
		return invokeHandler(uri, obj);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}
}

JSONObject NetworkSettingsDelegate::wireless_online(JSONObject obj) {
	JSONObject ret;
	ret.put(L"status", new JSONValue((bool)System::getInstance()->getWirelessConfiguration()->online()));
	return ret;
}

JSONObject NetworkSettingsDelegate::ping(JSONObject obj) {
	JSONObject ret;
	float i = NetworkUtils::ping(obj[L"host"]->String());
	
	ret.put(L"result", new JSONValue((double) i));
	return ret;
}

JSONObject NetworkSettingsDelegate::wireless_start(JSONObject obj) {
	System::getInstance()->getWirelessConfiguration()->start();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::wireless_stop(JSONObject obj) {
	System::getInstance()->getWirelessConfiguration()->stop();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_leases_add(JSONObject obj) {
	InterfacePtr device = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
	if (device && device->configuration_entity) {
		DhcpStaticLease lease;
		lease.ip = obj[L"ip"]->String();
		lease.mac = obj[L"mac"]->String();
		device->configuration_entity->dhcpServerStaticLeases.push_back(lease);
	}

	System::getInstance()->get_interface_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_leases_del(JSONObject obj) {
        InterfacePtr device = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
		if (device && device->configuration_entity) {
			for (list<DhcpStaticLease>::iterator iter = device->configuration_entity->dhcpServerStaticLeases.begin();
				iter != device->configuration_entity->dhcpServerStaticLeases.end(); iter++) {
			DhcpStaticLease lease = (*iter);

			if (lease.ip.compare(obj[L"ip"]->String()) == 0 && lease.mac.compare(obj[L"mac"]->String()) == 0) {
				device->configuration_entity->dhcpServerStaticLeases.erase(iter);
				break;
			}
		}
	}

	System::getInstance()->get_interface_manager()->save();
	return JSONObject();
}


JSONObject NetworkSettingsDelegate::devices_list(JSONObject obj) {
	JSONObject ret;
	JSONArray devices;

	for (InterfacePtr d : System::getInstance()->get_interface_manager()->get_all_interfaces()) {
		JSONObject interface;

		if (d) {
			interface.put(L"interface", new JSONValue((string) d->name));
			JSONArray ipv4_array;
			for(IPv4AddressPtr ip : d->ipv4_addresses){
				JSONObject address;
				address.put(L"ip", new JSONValue((string) IP4Addr::ip4AddrToString(ip->ip)));
				address.put(L"mask", new JSONValue((string) IP4Addr::ip4AddrToString(ip->mask)));
				address.put(L"broadcast", new JSONValue((string) IP4Addr::ip4AddrToString(ip->bcast)));
				ipv4_array.push_back(new JSONValue(address));
			}

			interface.put(L"ipv4", new JSONValue(ipv4_array));
			char hw[6];
			d->get_hardware_addr((char*) &hw);
			interface.put(L"mac", new JSONValue((string) IP4Addr::convertHw((unsigned char*) &hw)));
			interface.put(L"state", new JSONValue((bool) d->state));
			interface.put(L"name", new JSONValue((string) d->label));
			
			if(d->addr_mode == INTERFACE_ADDR_MODE_DHCP){
				interface.put(L"dhcp",  new JSONValue((bool) true));
			}else{
				interface.put(L"dhcp",  new JSONValue((bool) false));
			}

			interface.put(L"id", new JSONValue((double) d->ifid));
			interface.put(L"gateway", new JSONValue((string) IP4Addr::ip4AddrToString(d->gateway)));
			interface.put(L"persisted", new JSONValue((bool) d->configuration_entity));

			//Type information
			interface.put(L"bridge", new JSONValue((bool) d->is_bridge()));
			interface.put(L"vlan", new JSONValue((bool) d->is_vlan()));
			interface.put(L"lacp", new JSONValue((bool) d->is_bonding()));
			interface.put(L"physical", new JSONValue((bool) !d->is_bridge() && !d->is_vlan() && !d->is_bonding()));

			if(d->configuration_entity){
				interface.put(L"dhcpServerEnabled", new JSONValue((bool) d->configuration_entity->dhcpServerEnabled));
				interface.put(L"dhcpServerStart", new JSONValue((string) d->configuration_entity->dhcpServerStart));
				interface.put(L"dhcpServerEnd", new JSONValue((string) d->configuration_entity->dhcpServerEnd));

				JSONArray leases;
				for (DhcpStaticLease lease : d->configuration_entity->dhcpServerStaticLeases) {
					JSONObject object;
					object.put(L"ip", new JSONValue((string) lease.ip));
					object.put(L"mac", new JSONValue((string) lease.mac));

					leases.push_back(new JSONValue(object));
				}

				interface.put(L"dhcpServerLeases", new JSONValue(leases));
			}

			//We must override the settings if another ip is set:
			if (d->is_bridge()) {
				BridgeInterface* bridge = (BridgeInterface*) d.get();

				JSONArray barr;
				for (string name : bridge->get_bridged_devices()) {
					barr.push_back(new JSONValue(name));
				}
				interface.put(L"bridgeDevices", new JSONValue(barr));
			}

			if (d->is_bonding()) {
				BondingInterface* lacp = (BondingInterface*) d.get();

				JSONArray barr;
				for (string bond : lacp->get_slaves()) {
					barr.push_back(new JSONValue(bond));
				}
				interface.put(L"lacpDevices", new JSONValue(barr));

				interface.put(L"lacpMode", new JSONValue((double) lacp->get_bonding_mode()));
				interface.put(L"lacpLinkStatePollFrequency", new JSONValue((double) lacp->get_link_state_poll_frequency()));
				interface.put(L"lacpLinkStateDownDelay", new JSONValue((double) lacp->get_link_down_delay()));
				interface.put(L"lacpLinkStateUpDelay", new JSONValue((double) lacp->get_link_up_delay()));
			}

			if (d->is_vlan()) {
				VlanInterface* vlan = (VlanInterface*) d.get();

				interface.put(L"vlanId", new JSONValue((double) vlan->get_vlan_id()));
				interface.put(L"vlanInterface", new JSONValue((string) vlan->get_vlan_interface()));
			}


		}
		else {
			continue;
		}

		devices.push_back(new JSONValue(interface));
	}

	ret.put(L"devices", new JSONValue(devices));
	return ret;
}

JSONObject NetworkSettingsDelegate::devices_set(JSONObject obj) {
	JSONObject interface;
	InterfacePtr dcp;

	//If we are editing an interface, then we have an interface already, otherwise we wont
	if(obj.has(L"interface")){	
		dcp = System::getInstance()->get_interface_manager()->get_interface(obj[L"interface"]->String());
		if(!dcp){
			return JSONObject();	
		}
	}else{
		//Determine debian interface name
		if(obj.has(L"vlan") && obj[L"vlan"]->AsBool()){
			System::getInstance()->get_interface_manager()->create_vlan_interface(obj[L"vlanInterface"]->String(), obj[L"vlanId"]->AsNumber());	
			return JSONObject();
		}else if(obj.has(L"lacp") && obj[L"lacp"]->AsBool()){
			//We need to find a suitable interface name
			for(int x = 0; x < 255; x++){
				stringstream interface;
				interface << "bond" << x;

				if(!System::getInstance()->get_interface_manager()->get_interface(interface.str())){	
					System::getInstance()->get_interface_manager()->create_bonding_interface(interface.str());
					return JSONObject();
				}
			}
		}else if(obj.has(L"bridge") && obj[L"bridge"]->AsBool()){
			//We need to find a suitable interface name
			for(int x = 0; x < 255; x++){
				stringstream interface;
				interface << "br" << x;

				if(!System::getInstance()->get_interface_manager()->get_interface(interface.str())){
					System::getInstance()->get_interface_manager()->create_bridge_interface(interface.str());
					return JSONObject();
				}
			}
		}else{
			return JSONObject();
		}
	}

	//Deal with difference modes:
	if (dcp->is_bridge() && obj.has(L"bridgeDevices")) {
		BridgeInterface* bridge = dynamic_cast<BridgeInterface*>(dcp.get());
		for(std::string bridge_member : bridge->get_bridged_devices()){
			bridge->remove_bridged_device(bridge_member);
		}

		JSONArray &barr = obj[L"bridgeDevices"]->AsArray();
		for (JSONValue * value : barr) {
			bridge->add_bridged_device(value->String());
		}
	}else if(dcp->is_bonding()){
		BondingInterface* bonding = dynamic_cast<BondingInterface*>(dcp.get());
		for(std::string slave : bonding->get_slaves()){
			bonding->remove_slave(slave);
		}

		bool was_up = bonding->state;
		bonding->bring_down();	
		bonding->set_bonding_mode(obj[L"lacpMode"]->AsNumber());
		bonding->set_link_state_poll_frequency(obj[L"lacpLinkStatePollFrequency"]->AsNumber());
		bonding->set_link_down_delay(obj[L"lacpLinkStateDownDelay"]->AsNumber());
		bonding->set_link_up_delay(obj[L"lacpLinkStateUpDelay"]->AsNumber());

		if(was_up){
			bonding->bring_up();
		}

		JSONArray &barr = obj[L"lacpDevices"]->AsArray();
		for (JSONValue * value : barr) {
			//We must make sure the interface target is down:
			InterfacePtr target = System::getInstance()->get_interface_manager()->get_interface(value->String());
			if(target){
				target->bring_down();	
				bonding->add_slave(value->String());
			}
		}
	}

	//Deal with addresses
	if (obj.has(L"dhcp")) {
		if(obj[L"dhcp"]->AsBool()){
			dcp->addr_mode = INTERFACE_ADDR_MODE_DHCP;
		}else{
			dcp->addr_mode = INTERFACE_ADDR_MODE_STATIC;
		}
	}

	//Remove the current addresses:
	//Add the new addresses
	if(obj.has(L"ipv4")){
		if(dcp->addr_mode != INTERFACE_ADDR_MODE_DHCP){
			list<IPv4AddressPtr> copy_of_ipv4_addresses = dcp->ipv4_addresses;
			for(IPv4AddressPtr ip : copy_of_ipv4_addresses){
				dcp->remove_ipv4_address(ip->ip, ip->mask, ip->bcast);
			}		

			JSONArray ipv4 = obj[L"ipv4"]->AsArray();
			for(int x= 0; x < ipv4.size(); x++){
				JSONObject value = ipv4[x]->AsObject();
				dcp->add_ipv4_address(IP4Addr::stringToIP4Addr(value[L"ip"]->String()), IP4Addr::stringToIP4Addr(value[L"mask"]->String()), IP4Addr::stringToIP4Addr(value[L"broadcast"]->String()));
			}
		}
	}	

	if (obj.has(L"name")) {
		dcp->label = obj[L"name"]->String();
	}

	if (obj.has(L"gateway")) {
		dcp->gateway = IP4Addr::stringToIP4Addr(obj[L"gateway"]->String());
	}

	if (obj.has(L"persisted")) {
		System::getInstance()->get_interface_manager()->set_persisted(dcp, obj[L"persisted"]->AsBool());
	}

	if (obj.has(L"dhcpServerEnabled") && dcp->configuration_entity) {
		dcp->configuration_entity->dhcpServerEnabled = obj[L"dhcpServerEnabled"]->AsBool();
		dcp->configuration_entity->dhcpServerStart = obj[L"dhcpServerStart"]->String();
		dcp->configuration_entity->dhcpServerEnd = obj[L"dhcpServerEnd"]->String();

		System::getInstance()->get_interface_manager()->trigger_change_listeners();
	}

	System::getInstance()->get_interface_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_up(JSONObject obj) {
	InterfacePtr dcp;
	if(obj.has(L"device")){
		dcp = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
		if(dcp){
			dcp->bring_up();
		}

	}		
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_down(JSONObject obj) {
	InterfacePtr dcp;
	if(obj.has(L"device")){
		dcp = System::getInstance()->get_interface_manager()->get_interface(obj[L"device"]->String());
		if(dcp){
			dcp->bring_down();
		}

	}
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_save(JSONObject obj) {
	System::getInstance()->get_interface_manager()->save();
	return JSONObject();
}


JSONObject NetworkSettingsDelegate::dns_get(JSONObject) {
	JSONObject ret;
	ret.put(L"ns1", new JSONValue((string) dnsConfig->getNS1()));
	ret.put(L"ns2", new JSONValue((string) dnsConfig->getNS2()));
	ret.put(L"domain", new JSONValue((string) dnsConfig->getDomain()));
	ret.put(L"search", new JSONValue((string) dnsConfig->getSearch()));

	return ret;
}

JSONObject NetworkSettingsDelegate::dns_set(JSONObject obj) {
	dnsConfig->setNS1(obj[L"ns1"]->String());
	dnsConfig->setNS2(obj[L"ns2"]->String());
	dnsConfig->setSearch(obj[L"search"]->String());
	dnsConfig->setDomain(obj[L"domain"]->String());

	dnsConfig->save();

	if (eventDb) {
		EventParams params;
		eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_DNS_SET, params));
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::routes_list(JSONObject) {
	JSONObject ret;
	JSONArray routes;

	for (PersistedNRoutePtr route : System::getInstance()->get_route_manager()->list_persisted_routes()) {
		JSONObject r;
		r.put(L"id", new JSONValue((string) route->id));

		if(route->route->destination != 0){
			r.put(L"destination", new JSONValue((string) IP4Addr::ip4AddrToString(route->route->destination)));
			r.put(L"destination_cidr", new JSONValue((double) route->route->destination_cidr));
		}

		if(route->route->route_device != 0){
			InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface_by_id(route->route->route_device);
			if(interface){
				r.put(L"route_device", new JSONValue((string) interface->name));
			}
		}

		if(route->route->route_nexthop != 0){
			r.put(L"route_nexthop", new JSONValue((string) IP4Addr::ip4AddrToString(route->route->route_nexthop)));
		}

		routes.push_back(new JSONValue(r));
	}

	ret.put(L"routes", new JSONValue(routes));
	return ret;
}

JSONObject NetworkSettingsDelegate::routes_add(JSONObject args) {
	PersistedNRoutePtr route(new PersistedNRoute());
	route->route = NRoutePtr(new NRoute());
	route->id = StringUtils::genRandom();
	
	if(args.has(L"destination")){
		route->route->destination = IP4Addr::stringToIP4Addr(args[L"destination"]->String());
		route->route->destination_cidr = args[L"destination_cidr"]->AsNumber();
	}

	if(args.has(L"route_device")){
		InterfacePtr interface = System::getInstance()->get_interface_manager()->get_interface(args[L"route_device"]->String());
		if(interface){
			route->route->route_device = interface->ifid;
		}
	}

	if(args.has(L"route_nexthop")){
		route->route->route_nexthop = IP4Addr::stringToIP4Addr(args[L"route_nexthop"]->String());
	}
	
	System::getInstance()->get_route_manager()->add_persisted_route_entry(route);
	System::getInstance()->get_route_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::routes_del(JSONObject args) {
	PersistedNRoutePtr target = System::getInstance()->get_route_manager()->get_persisted_route_entry(args[L"id"]->String());	
	if (target) {
		System::getInstance()->get_route_manager()->del_persisted_route_entry(target);
	}

	System::getInstance()->get_route_manager()->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::arp_size(JSONObject args) {
	JSONObject ret;
	ret.put(L"size", new JSONValue((double) arp->size()));
	return ret;
}

JSONObject NetworkSettingsDelegate::arp_list(JSONObject args) {
	JSONObject ret;
	JSONArray hosts;

	arp->holdLock();
	std::list<HostPtr> a;
	arp->list(a);

	for (HostPtr entry : a) {
		JSONObject obj;
		obj.put(L"host", new JSONValue((string) entry->getIp()));
		obj.put(L"hw", new JSONValue((string) entry->mac));
		obj.put(L"loginTime", new JSONValue((double) entry->firstSeen));
		obj.put(L"hostname", new JSONValue((string) entry->hostname()));
		obj.put(L"quarantined", new JSONValue((bool) entry->quarantined));

		hosts.push_back(new JSONValue(obj));
	}

	arp->releaseLock();

	ret.put(L"hosts", new JSONValue(hosts));
	return ret;
}

JSONObject NetworkSettingsDelegate::set_quarantined(JSONObject args){
	HostPtr host = arp->getByMac(args[L"mac"]->String(), IP4Addr::stringToIP4Addr(args[L"ip"]->String()));
	if(host){
		host->quarantined = args[L"quarantined"]->AsBool();	
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dnsmasq_running(JSONObject) {
	JSONObject ret;
	ret.put(L"running", new JSONValue((bool) dnsmasq->running()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dnsmasq_publish(JSONObject) {
	dnsmasq->publish();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dnsmasq_start(JSONObject) {
	dnsmasq->start();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::dnsmasq_stop(JSONObject) {
	dnsmasq->stop();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	list<Connection *> connections = connectionManager->getConnections();
	list<Connection *>::iterator iter;

	for (iter = connections.begin();
			iter != connections.end();
			iter++) {

		Connection *target = (*iter);
		JSONObject o;
		o.put(L"name", new JSONValue((string) target->getName()));
		o.put(L"device", new JSONValue((string) target->getDevice()));
		o.put(L"username", new JSONValue((string) target->getAuthenticationCredentials().username));
		o.put(L"password", new JSONValue((string) target->getAuthenticationCredentials().password));

		o.put(L"authenticationType", new JSONValue((double) target->getAuthenticationCredentials().authenticationType));
		o.put(L"type", new JSONValue((double) target->type()));
		o.put(L"state", new JSONValue((bool) target->isConnected()));
		o.put(L"keyfile", new JSONValue((string) target->getAuthenticationCredentials().keyfile));
		o.put(L"log", new JSONValue((string) target->log()));
		o.put(L"connected", new JSONValue((bool) target->isConnected()));
		arr.push_back(new JSONValue(o));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::connectons_add(JSONObject obj) {
	connectionManager->createConnection(obj[L"name"]->String(), (ConnectionType) obj[L"type"]->AsNumber());
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_del(JSONObject obj) {
	Connection *target = connectionManager->getConnection(obj[L"name"]->String());

	if (target) {
		connectionManager->deleteConnection(target);
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_save(JSONObject obj) {
	Connection *connection = connectionManager->getConnection(obj[L"name"]->String());

	if (connection) {
		//Set the details:
		AuthenticationCredentials &auth = connection->getAuthenticationCredentials();

		if (connection->type() == PPPoE) {
			connection->setDevice(obj[L"device"]->String());
			auth.username = obj[L"username"]->String();
			auth.password = obj[L"password"]->String();
			auth.authenticationType = (ConnectionAuthenticationType) obj[L"authenticationType"]->AsNumber();
		}
		else {
			auth.keyfile = obj[L"keyfile"]->String();
		}

		connectionManager->save();

		connectionManager->publishAuthenticationCredentials();
		connection->publishSettings();
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_connect(JSONObject o) {
	Connection *connection = connectionManager->getConnection(o[L"name"]->String());

	if (connection) {
		connection->connect();

		if (eventDb) {
			EventParams params;
			params["name"] = o[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_CONNECTION_START, params));
		}
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::connections_disconnect(JSONObject o) {
	Connection *connection = connectionManager->getConnection(o[L"name"]->String());

	if (connection) {
		connection->disconnect();

		if (eventDb) {
			EventParams params;
			params["name"] = o[L"name"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP, params));
		}
	}
	else {
		throw DelegateGeneralException("could not find connection");
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_start(JSONObject obj) {
	VpnInstancePtr instance = vpnManager->get_instance(obj[L"name"]->String());

	if (instance) {
		vpnManager->do_connect(instance);
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_stop(JSONObject obj) {
	VpnInstancePtr instance = vpnManager->get_instance(obj[L"name"]->String());

	if (instance) {
		vpnManager->do_disconnect(instance);
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_status(JSONObject obj) {
	VpnInstancePtr instance = vpnManager->get_instance(obj[L"name"]->String());

	if (instance) {
		JSONObject ret;
		ret.put(L"status", new JSONValue((double) vpnManager->status(instance)));
		return ret;
	}

	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_log(JSONObject obj) {
	JSONObject ret;
	OpenvpnPtr instance = openvpn->getInstance(obj[L"name"]->String());

	if (instance) {
		ret.put(L"log", new JSONValue((string) instance->log()));
	}

	return ret;
}

JSONObject NetworkSettingsDelegate::openvpn_clients_list(JSONObject obj) {
	OpenvpnPtr instance = openvpn->getInstance(obj[L"name"]->String());
	JSONObject ret;

	if (instance && instance->type() == VPN_OPENVPN_SERVER_TLS) {
		OpenvpnTlsKeys* ovtk = dynamic_cast<OpenvpnTlsKeys*>(instance.get());	

		JSONArray arr;
		for (OpenvpnClientConf* conf : ovtk->getClients()) {
			JSONObject obj;
			obj.put(L"name", new JSONValue((string) conf->name));
			obj.put(L"conf", new JSONValue((string) ovtk->generateUserFile(conf->name)));

			arr.push_back(new JSONValue(obj));
		}

		ret.put(L"clients", new JSONValue(arr));
	}else if(instance && instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS){
		OpenvpnStaticKeys* ovtk = dynamic_cast<OpenvpnStaticKeys*>(instance.get());
		ret.put(L"key", new JSONValue(ovtk->generate_client_configuration()));
	}

	return ret;
}

JSONObject NetworkSettingsDelegate::openvpn_clients_add(JSONObject obj) {
	OpenvpnPtr instance = openvpn->getInstance(obj[L"name"]->String());

	if (instance && instance->type() == VPN_OPENVPN_SERVER_TLS) {
		OpenvpnTlsKeys* ovtk = dynamic_cast<OpenvpnTlsKeys*>(instance.get()); 
		ovtk->createClient(obj[L"clientname"]->String());
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_clients_delete(JSONObject obj) {
	OpenvpnPtr instance = openvpn->getInstance(obj[L"name"]->String());

	if (instance && instance->type() == VPN_OPENVPN_SERVER_TLS) {
		OpenvpnTlsKeys* ovtk = dynamic_cast<OpenvpnTlsKeys*>(instance.get());
		for (OpenvpnClientConf* client :  ovtk->getClients()) {
			if (client->name.compare(obj[L"clientname"]->String()) == 0) {
				ovtk->deleteClient(client);
				break;
			}
		}
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_instances_list(JSONObject obj) {
	JSONObject ret;
	JSONArray arr;

	for (VpnInstancePtr instance : vpnManager->list_instances()) {
		JSONObject i;
		i.put(L"name", new JSONValue((string) instance->name));
		i.put(L"type", new JSONValue((double) instance->type()));

		if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
			OpenvpnPtr target = dynamic_pointer_cast<Openvpn>(instance);

			i.put(L"serverip", new JSONValue((string) target->serverip));
			i.put(L"clientip", new JSONValue((string) target->clientip));
			i.put(L"customopts", new JSONValue((string) target->customopts));
			i.put(L"servermask", new JSONValue((string) target->servermask));
			i.put(L"remoteserverip", new JSONValue((string) target->remoteserverip));
			i.put(L"remoteport", new JSONValue((double) target->remoteport));

			i.put(L"remoteserverip", new JSONValue((string) target->remoteserverip));
			i.put(L"compression", new JSONValue((bool) target->compression));

		}else if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
			IPSecGatewayToGatewayInstance* target = dynamic_cast<IPSecGatewayToGatewayInstance*>(instance.get());

			i.put(L"local_host", new JSONValue((std::string) target->local_host));
			i.put(L"local_subnet", new JSONValue((std::string) target->local_subnet));
			i.put(L"local_id", new JSONValue((string) target->local_id));
			i.put(L"remote_host", new JSONValue((string) target->remote_host));
			i.put(L"remote_subnet", new JSONValue((string) target->remote_subnet));
			i.put(L"remote_id", new JSONValue((string) target->remote_id));
			i.put(L"auth_mode", new JSONValue((double) target->auth_mode));
			i.put(L"secret_key", new JSONValue((string) target->secret_key));
			i.put(L"auto_start", new JSONValue((bool) target->auto_start));
			i.put(L"meta", new JSONValue((string) target->meta));
		}

		i.put(L"status", new JSONValue((double) vpnManager->status(instance)));
		i.put(L"log", new JSONValue((string) vpnManager->log(instance)));

		arr.push_back(new JSONValue(i));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject NetworkSettingsDelegate::vpn_instance_create(JSONObject obj) {
	string name = obj[L"name"]->String();
	int type = obj[L"type"]->AsNumber();

	vpnManager->add_instance(name, type);

	if(type == 3){
		OpenvpnTlsKeys* target = dynamic_cast<OpenvpnTlsKeys*>(vpnManager->get_instance(name).get()); 

		target->certcountry = obj[L"certcountry"]->String();
		target->certprovince = obj[L"certprovince"]->String();
		target->certcity = obj[L"certcity"]->String();
		target->certorg = obj[L"certorg"]->String();
		target->certemail = obj[L"certemail"]->String();
	}

	if(type == 1 || type == 3){
		OpenvpnPtr oi = dynamic_pointer_cast<Openvpn>(vpnManager->get_instance(name));
		oi->installbase();
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_instance_remove(JSONObject obj) {
	VpnInstancePtr instance = vpnManager->get_instance(obj[L"name"]->String());
	if (instance) {
		vpnManager->delete_instance(instance);
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::vpn_options(JSONObject obj) {
	VpnInstancePtr instance = vpnManager->get_instance(obj[L"name"]->String());
	if (instance) {
		if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
			OpenvpnPtr oi = dynamic_pointer_cast<Openvpn>(instance);

			if (obj.has(L"serverip")) {
				oi->serverip = obj[L"serverip"]->String();
			}

			if (obj.has(L"clientip")) {
				oi->clientip = obj[L"clientip"]->String();
			}

			if (obj.has(L"servermask")) {
				oi->servermask = obj[L"servermask"]->String();
			}

			if (obj.has(L"remoteserverip")) {
				oi->remoteserverip = obj[L"remoteserverip"]->String();
			}

			if (obj.has(L"remoteport")) {
				oi->remoteport = obj[L"remoteport"]->AsNumber();
			}

			if (obj.has(L"compression")) {
				oi->compression = obj[L"compression"]->AsBool();
			}

			if (obj.has(L"customopts")) {
				oi->customopts = obj[L"customopts"]->String();
			}
		}else if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
			IPSecGatewayToGatewayInstance* igtg = dynamic_cast<IPSecGatewayToGatewayInstance*>(instance.get());

			igtg->local_host = obj[L"local_host"]->String();
			igtg->local_subnet = obj[L"local_subnet"]->String();
			igtg->local_id = obj[L"local_id"]->String();

			igtg->remote_host = obj[L"remote_host"]->String();
			igtg->remote_subnet = obj[L"remote_subnet"]->String();
			igtg->remote_id = obj[L"remote_id"]->String();
			igtg->auth_mode = obj[L"auth_mode"]->AsNumber();
			igtg->secret_key = obj[L"secret_key"]->String();
			igtg->meta = obj[L"meta"]->String();
			igtg->auto_start = obj[L"auto_start"]->AsBool();
		}
	}

	vpnManager->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::openvpn_daemonexists(JSONObject obj) {
	JSONObject ret;
	ret.put(L"exists", new JSONValue((bool) openvpn->daemonExists()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dyndns(JSONObject obj) {
	GlobalDynDns *dns = System::getInstance()->getGlobalDynDns();
	JSONObject ret;
	ret.put(L"username", new JSONValue((string) dns->getUsername()));
	ret.put(L"password", new JSONValue((string) dns->getPassword()));
	ret.put(L"domain", new JSONValue((string) dns->getDomain()));
	ret.put(L"enabled", new JSONValue((bool) dns->isEnabled()));
	ret.put(L"type", new JSONValue((double)(DynamicDnsClientProvider) dns->getType()));
	ret.put(L"active", new JSONValue((bool) dns->active()));
	ret.put(L"lastip", new JSONValue((string) dns->getLastIp()));
	return ret;
}

JSONObject NetworkSettingsDelegate::dyndns_set(JSONObject obj) {
	GlobalDynDns *dns = System::getInstance()->getGlobalDynDns();
	dns->setUsername(obj[L"username"]->String());
	dns->setPassword(obj[L"password"]->String());
	dns->setDomain(obj[L"domain"]->String());
	dns->setEnabled(obj[L"enabled"]->AsBool());
	dns->setType((DynamicDnsClientProvider) obj[L"type"]->AsNumber());
	dns->run(true);

	dns->save();
	return JSONObject();
}

JSONObject NetworkSettingsDelegate::devices_delete(JSONObject obj) {
	IntMgr* mgr = System::getInstance()->get_interface_manager();
	InterfacePtr interface = mgr->get_interface(obj[L"device"]->String());
	if(interface){
		interface->bring_down();
		mgr->delete_interface(interface);
	}
	return JSONObject();
}

