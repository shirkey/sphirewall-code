/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
#include "test.h"

using namespace std;
using namespace SFwallCore;
#include "SFwallCore/Alias.h"
#include "SFwallCore/Firewall.h"
#include "Api/Delegates/FirewallDelegate.h"
#include "Core/Logger.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"

TEST(FirewallDelegate, alias_list) {
	MockFactory *tester = new MockFactory();
	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	firewall->aliases->setResolve(false);

	Alias *alias = new IpRangeAlias();
	alias->name = "google.com";

	Alias *alias2 = new IpRangeAlias();
	alias2->name = "yahoo.com";
	firewall->aliases->create(AliasPtr(alias));
	firewall->aliases->create(AliasPtr(alias2));

	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);
	JSONObject ret = del->process("firewall/aliases/list", JSONObject());

	JSONArray aliases = ret[L"aliases"]->AsArray();
	EXPECT_TRUE(aliases.size() == 2);
}

TEST(FirewallDelegate, alias_add) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);
	firewall->aliases->setResolve(false);

	JSONObject args;
	args.put(L"name", new JSONValue((string) "Blah Name"));
	args.put(L"type", new JSONValue((double) 0));
	JSONObject ret = del->process("firewall/aliases/add", args);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 1);
}

TEST(FirewallDelegate, alias_createwithurl) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);
	firewall->aliases->setResolve(false);

	JSONObject args;
	args.put(L"name", new JSONValue((string) "google.com"));
	args.put(L"detail", new JSONValue((string) "http://mirror.sphirewall.net/resources/list.txt"));
	args.put(L"type", new JSONValue((double) 2));

	JSONObject ret = del->process("firewall/aliases/add", args);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 1);
}

TEST(FirewallDelegate, alias_del) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, firewall->aliases, NULL));
	firewall->aliases->setResolve(false);

	Alias *alias = new IpRangeAlias();
	alias->name = "google.com";

	Alias *alias2 = new IpRangeAlias();
	alias2->name = "yahoo.com";
	firewall->aliases->create(AliasPtr(alias));
	firewall->aliases->create(AliasPtr(alias2));

	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 2);

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	JSONObject ret = del->process("firewall/aliases/del", args);

	EXPECT_TRUE(firewall->aliases->aliases.size() == 1);
}

TEST(FirewallDelegate, alias_add_address) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, firewall->aliases, NULL));
	firewall->aliases->setResolve(false);

	Alias *alias = new IpRangeAlias();
	alias->name = "google.com";

	firewall->aliases->create(AliasPtr(alias));
	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	args.put(L"value", new JSONValue((string) "10.1.1.1-10.1.1.2"));
	JSONObject ret = del->process("firewall/aliases/alias/add", args);

	EXPECT_TRUE(alias->listEntries().size() == 1);
}


TEST(FirewallDelegate, alias_del_address) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL, NULL, NULL, firewall->aliases, NULL));
	firewall->aliases->setResolve(false);

	Alias *alias = new IpRangeAlias();
	alias->name = "google.com";

	firewall->aliases->create(AliasPtr(alias));
	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);
	alias->addEntry("10.1.1.1-10.1.1.10");

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	args.put(L"value", new JSONValue((string) "10.1.1.1-10.1.1.10"));
	JSONObject ret = del->process("firewall/aliases/alias/del", args);

	EXPECT_TRUE(alias->listEntries().size() == 0);
}

TEST(FirewallDelegate, alias_list_address) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	firewall->aliases = new SFwallCore::AliasDb();
	firewall->aliases->setAclStore(new SFwallCore::ACLStore(NULL,  NULL, NULL, firewall->aliases, NULL));
	firewall->aliases->setResolve(false);

	Alias *alias = new IpRangeAlias();
	alias->name = "google.com";
	firewall->aliases->create(AliasPtr(alias));

	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);

	alias->addEntry("10.1.1.1-10.1.1.10");
	alias->addEntry("10.1.2.1-10.1.2.10");

	JSONObject args;
	args.put(L"id", new JSONValue((string) alias->id));
	JSONObject ret = del->process("firewall/aliases/alias/list", args);

	EXPECT_TRUE(ret[L"items"]->AsArray().size() == 2);
}

TEST(FirewallDelegate, acls_list_BASIC) {
	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore *store = new SFwallCore::ACLStore();
	firewall->acls = store;

	SFwallCore::FilterRule *rule1 = new SFwallCore::FilterRule();
	store->loadAclEntry(rule1);

	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);

	JSONObject ret = del->process("firewall/acls/list", JSONObject());
	JSONArray filterRules = ret[L"normal"]->AsArray();

	EXPECT_TRUE(filterRules.size() == 1);
}

TEST(FirewallDelegate, acls_filter_delete) {
	MockFactory *tester = new MockFactory();

	SFwallCore::Firewall *firewall = new SFwallCore::Firewall();
	SFwallCore::ACLStore *store = new SFwallCore::ACLStore(NULL, NULL, NULL, NULL, NULL);
	store->setConfigurationManager(new ConfigurationManager());
	firewall->acls = store;

	FirewallDelegate *del = new FirewallDelegate(firewall, NULL, NULL);

	SFwallCore::FilterRule *rule1 = new SFwallCore::FilterRule();
	rule1->id = "1";
	SFwallCore::FilterRule *rule2 = new SFwallCore::FilterRule();
	rule2->id = "2";

	store->loadAclEntry(rule1);
	store->loadAclEntry(rule2);

	JSONObject args;
	args.put(L"id", new JSONValue((string)rule1->id));
	JSONObject ret = del->process("firewall/acls/filter/delete", args);

	EXPECT_TRUE(store->listFilterRules().size() == 1);
}

TEST(FirewallDelegate, acls_delete_trafficshaper) {
	MockFactory *mocks = new MockFactory();
	FirewallDelegate *del = new FirewallDelegate(mocks->givenFw(), NULL, NULL);
	TsRulePtr rule = TsRulePtr(new TsRule("id"));

	mocks->givenFw()->trafficShaper->add(rule);

	JSONObject args;
	args.put(L"id", new JSONValue((string) "id"));

	del->process("firewall/acls/del/trafficshaper", args);
	EXPECT_TRUE(mocks->givenFw()->trafficShaper->list().size() == 0);
}

TEST(FirewallDelegate, acls_delete_trafficshaper_invalid) {
	MockFactory *mocks = new MockFactory();
	FirewallDelegate *del = new FirewallDelegate(mocks->givenFw(), NULL, NULL);
	TsRulePtr rule = TsRulePtr(new TsRule("id"));

	mocks->givenFw()->trafficShaper->add(rule);

	JSONObject args;
	args.put(L"id", new JSONValue((string) "xxid"));

	expectException(del, "firewall/acls/del/trafficshaper", args);
}
