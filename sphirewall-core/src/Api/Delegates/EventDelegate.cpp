/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>
#include <string>

using namespace std;

#include "Api/Delegates/EventDelegate.h"
#include "Api/Exceptions.h"
#include "Core/Event.h"
#include "Core/EventHandler.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &EventDelegate::y))

EventDelegate::EventDelegate(EventDb *eventDb)
	: eventDb(eventDb) {
	installHandler("events/list", list);
	installHandler("events/config/add", config_add);
	installHandler("events/config/del", config_del);
	installHandler("events/config/list", config_list);
	installHandler("events/size", size);
	installHandler("events/purge", purge);
}

string EventDelegate::rexpression() {
	return "events/(.*)";
}

JSONObject EventDelegate::process(string uri, JSONObject args) {
	try {
		return invokeHandler(uri, args);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}

	return JSONObject();
}

JSONObject EventDelegate::size(JSONObject args) {
	JSONObject ret;
	ret.put(L"size", new JSONValue((double) eventDb->size()));
	return ret;
}

JSONObject EventDelegate::list(JSONObject object) {
	int minTimestamp = -1;

	if (object.has(L"time")) {
		minTimestamp = object[L"time"]->AsNumber();
	}

	JSONObject ret;
	JSONArray array;

	for (EventPtr target : eventDb->list()) {
		if (minTimestamp != -1 && target->t < minTimestamp) {
			continue;
		}

		JSONObject e;
		e.put(L"time", new JSONValue((double) target->t));
		e.put(L"key", new JSONValue((string) target->key));

		JSONArray data;
		map<string, Param> params = target->params.getParams();

		for (map<string, Param>::iterator piter = params.begin(); piter != params.end(); piter++) {
			Param p = piter->second;
			JSONObject po;

			if (p.isString()) {
				po.put(StringToWString(piter->first), new JSONValue((string) p.string()));
			}

			if (p.isNumber()) {
				po.put(StringToWString(piter->first), new JSONValue((double) p.number()));
			}

			data.push_back(new JSONValue(po));
		}

		e.put(L"params", new JSONValue(data));
		array.push_back(new JSONValue(e));
	}

	ret.put(L"events", new JSONValue(array));
	return ret;
}

JSONObject EventDelegate::purge(JSONObject object) {
	eventDb->purgeEvents();

	return JSONObject();
}

JSONObject EventDelegate::config_add(JSONObject object) {
	string key = object[L"key"]->String();
	string handler = object[L"handler"]->String();

	EventHandler *target = NULL;

	for (multimap<string, EventHandler *>::iterator iter = eventDb->getHandlers().begin(); iter != eventDb->getHandlers().end(); iter++) {
		string ikey = iter->first;
		EventHandler *ihandler = iter->second;
		string ihandlerkey = ihandler->key();

		if (ikey.compare(key) == 0 && ihandlerkey.compare(handler) == 0) {
			target = ihandler;
		}
	}

	if (target == NULL) {
		if (handler.compare("handler.log") == 0) {
			target = new LogEventHandler();
		}

		if (handler.compare("handler.firewall.block") == 0) {
			target = new FirewallBlockSourceAddressHandler();
		}
		
		if (target != NULL) {
			eventDb->addHandler(key, target);
		}
		else {
			throw DelegateGeneralException("could not match handler");
		}
	}

	eventDb->save();
	return JSONObject();
}

JSONObject EventDelegate::config_del(JSONObject object) {
	string handler = object[L"handler"]->String();
	string key = object[L"key"]->String();

	EventHandler *target = NULL;

	for (multimap<string, EventHandler *>::iterator iter = eventDb->getHandlers().begin(); iter != eventDb->getHandlers().end(); iter++) {
		string ikey = iter->first;
		EventHandler *ihandler = iter->second;

		if (ikey.compare(key) == 0 && ihandler->key().compare(handler) == 0) {
			target = iter->second;
			eventDb->removeHandler(key, target);
			break;
		}
	}

	return JSONObject();
}

JSONObject EventDelegate::config_list(JSONObject object) {
	JSONObject ret;
	JSONArray array;

	for (multimap<string, EventHandler *>::iterator iter = eventDb->getHandlers().begin(); iter != eventDb->getHandlers().end(); iter++) {
		string ikey = iter->first;
		EventHandler *ihandler = iter->second;

		JSONObject o;
		o.put(L"handler", new JSONValue((string) ihandler->key()));
		o.put(L"key", new JSONValue((string) ikey));
		o.put(L"name", new JSONValue((string) ihandler->name()));

		array.push_back(new JSONValue(o));
	}

	ret.put(L"handlers", new JSONValue(array));
	return ret;
}
