/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include "Api/JsonManagementService.h"
#include "test.h"

using namespace std;

#include "Core/Event.h"
#include "Core/ConfigurationManager.h"
/*
class Handler1 : public virtual EventHandler {
public:

	void handle(Event* event) {
	};

	string name() {
		return "handler1";
	};

	int getId() {
		return 1;
	};
};

class Handler2 : public virtual EventHandler {
public:

	void handle(Event* event) {
	};

	string name() {
		return "handler2";
	};

	int getId() {
		return 2;
	};
};

TEST(EventDelegate, list) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	db->add(Event("event1", IDS_EVENT));
	db->add(Event("event2", IDS_PORTSCAN));

	EventDelegate* del = new EventDelegate(db);
	JSONObject ret = del->process("events/list", JSONObject());

	JSONArray events = ret[L"events"]->AsArray();
	EXPECT_TRUE(events.size() == 2);

	JSONObject e1 = events[1]->AsObject();
	JSONObject e2 = events[0]->AsObject();

	EXPECT_TRUE(e1[L"message"]->String().compare("event1") == 0);
	EXPECT_TRUE(e2[L"message"]->String().compare("event2") == 0);

}

TEST(EventDelegate, listAvailableEventHandlers) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);
	JSONObject ret = del->process("events/config/handlers/available", JSONObject());
	JSONArray events = ret[L"handlers"]->AsArray();
	EXPECT_TRUE(events.size() == 7);

	JSONObject e1 = events[0]->AsObject();
	JSONObject e2 = events[1]->AsObject();

	EXPECT_TRUE(e1[L"name"]->String().compare("Log Event Handler") == 0);
	EXPECT_TRUE(e1[L"id"]->AsNumber() == 1);
	EXPECT_TRUE(e2[L"name"]->String().compare("Stdout Event Handler") == 0);
	EXPECT_TRUE(e2[L"id"]->AsNumber() == 2);
}

TEST(EventDelegate, listAvailableEventTypes) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);
	JSONObject ret = del->process("events/config/types/available", JSONObject());
	JSONArray events = ret[L"types"]->AsArray();
	EXPECT_TRUE(events.size() == 18);
}

TEST(EventDelegate, addEventHandler) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);

	JSONObject pass;
	pass.put(L"handler", new JSONValue((double) 2));
	pass.put(L"type", new JSONValue((double) 2));

	JSONObject r = del->process("events/config/add", pass);

	EXPECT_TRUE(db->listHandlers(2).size() == 1);
}

TEST(EventDelegate, addEventHandler_InvalidHandlerCode) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);

	JSONObject pass;
	pass.put(L"handler", new JSONValue((double) 212312));
	pass.put(L"type", new JSONValue((double) 2));

	expectException(del, "events/config/add", pass);
}

TEST(EventDelegate, deleteEventHandler) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);

	EventHandler* realHandler = db->getHandler(2);
	if (realHandler) {
		db->addHandler(2, realHandler);
	}

	EXPECT_TRUE(db->listHandlers(2).size() == 1);
	JSONObject pass;
	pass.put(L"handler", new JSONValue((double) 2));
	pass.put(L"type", new JSONValue((double) 2));

	JSONObject r2 = del->process("events/config/del", pass);

	EXPECT_TRUE(db->listHandlers(2).size() == 0);
}

TEST(EventDelegate, deleteEventHandler_InvalidHandlerCode){
        EventDb* db = new EventDb(NULL, new ConfigurationManager());
        EventDelegate* del = new EventDelegate(db);

        JSONObject pass;
        pass.put(L"handler", new JSONValue((double) 212312));
        pass.put(L"type", new JSONValue((double) 2));

        expectException(del, "events/config/del", pass);
}

TEST(EventDelegate, listHandlers) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);

	EventHandler* realHandler = db->getHandler(2);
	if (realHandler) {
		db->addHandler(2, realHandler);
	}

	JSONObject pass;
	pass.put(L"type", new JSONValue((double) 2));
	JSONObject r2 = del->process("events/config/list", pass);
	JSONArray handlers = r2[L"handlers"]->AsArray();
	EXPECT_TRUE(handlers.size() == 1);
}

TEST(EventDelegate, handlerConfigSet) {
	EventDb* db = new EventDb(NULL, new ConfigurationManager());
	EventDelegate* del = new EventDelegate(db);

	EventHandler* realHandler = db->getHandler(5);
	if (realHandler) {
		db->addHandler(0, realHandler);
	}

	JSONObject pass;
	pass.put(L"type", new JSONValue((double) 0));
	pass.put(L"handler", new JSONValue((double) 5));
	pass.put(L"key", new JSONValue((string) "EMAIL_ADDRESSES"));
	pass.put(L"value", new JSONValue((string) "michael@sphinix.com"));

	JSONObject r2 = del->process("events/config/handlers/config", pass);

	EventHandler* handlerWithConfig = db->getHandler(5, 0);
	map<std::string, EventHandlerConfigItem*> items;
	handlerWithConfig->getConfiguration(items);
	EXPECT_TRUE(items.size() == 1);

	EventHandlerConfigItem* configItem = items["EMAIL_ADDRESSES"];
	EXPECT_TRUE(configItem != NULL);
	EXPECT_TRUE(configItem->item.compare("michael@sphinix.com") == 0);
}
*/
