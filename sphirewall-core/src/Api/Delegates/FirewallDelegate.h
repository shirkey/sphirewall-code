#ifndef FirewallDelegate_h
#define FirewallDelegate_h

#include "Api/Delegate.h"

class FirewallDelegate : public virtual Delegate {
	public:
		FirewallDelegate(SFwallCore::Firewall *firewall, IntMgr *interfaceManager, GroupDb *groupDb);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();
		void setApplicationFilter(SFwallCore::ApplicationFilter *filter);
		SFwallCore::ApplicationFilter *getApplicationFilter();
		void setEventDb(EventDb *eventDb);
	private:
		SFwallCore::Firewall *firewall;
		IntMgr* interfaceManager;
		GroupDb *groupDb;
		SFwallCore::ApplicationFilter *httpApplicationFilter;
		EventDb *eventDb;
		void serializeRule(JSONObject &rule, SFwallCore::Rule *target);

		_BEGIN_SPHIREWALL_DELEGATES(FirewallDelegate)
		DelegateHandlerImpl aliases_list;
		DelegateHandlerImpl aliases_add;
		DelegateHandlerImpl aliases_del;
		DelegateHandlerImpl aliases_get;
		DelegateHandlerImpl aliases_load;
		DelegateHandlerImpl aliases_del_bulk;
		DelegateHandlerImpl aliases_del_all;
		DelegateHandlerImpl aliases_alias_add;
		DelegateHandlerImpl aliases_alias_list;
		DelegateHandlerImpl aliases_alias_search;
		DelegateHandlerImpl aliases_alias_del;
		DelegateHandlerImpl aliases_add_bulk;
		DelegateHandlerImpl tracker_list;
		DelegateHandlerImpl tracker_terminate;
		DelegateHandlerImpl tracker_size;
		DelegateHandlerImpl blocklist_list;
		DelegateHandlerImpl blocklist_del;
		DelegateHandlerImpl blocklist_add;
		DelegateHandlerImpl blocklist_addList;
		DelegateHandlerImpl blocklist_getLists;
		DelegateHandlerImpl blocklist_delList;
		DelegateHandlerImpl blocklistList_addList;
		DelegateHandlerImpl blocklistList_delList;
		DelegateHandlerImpl acls_add;
		DelegateHandlerImpl acls_list;
		DelegateHandlerImpl acls_enable;
		DelegateHandlerImpl acls_disable;
		DelegateHandlerImpl acls_status;
		DelegateHandlerImpl acls_filter_delete;
		DelegateHandlerImpl acls_filter_delete_pos;
		DelegateHandlerImpl acls_filter_down;
		DelegateHandlerImpl acls_filter_down_pos;
		DelegateHandlerImpl acls_filter_up;
		DelegateHandlerImpl acls_filter_up_pos;
		DelegateHandlerImpl acls_nat_delete;
		DelegateHandlerImpl acls_add_trafficshaper;
		DelegateHandlerImpl acls_list_del_trafficshaper;
		DelegateHandlerImpl acls_list_trafficshaper;
		DelegateHandlerImpl acls_webfilter_rules_list;
		DelegateHandlerImpl acls_webfilter_lists_list;
		DelegateHandlerImpl acls_webfilter_lists_add;
		DelegateHandlerImpl acls_webfilter_rules_add;
		DelegateHandlerImpl acls_webfilter_lists_remove;
		DelegateHandlerImpl acls_webfilter_rules_remove;
		DelegateHandlerImpl acls_webfilter_lists_addurl;
		DelegateHandlerImpl acls_webfilter_lists_removeurl;
		DelegateHandlerImpl acls_webfilter_rules_movedown;
		DelegateHandlerImpl acls_webfilter_rules_moveup;
		DelegateHandlerImpl acls_webfilter_rules_deleteall;
		DelegateHandlerImpl acls_webfilter_rules_add_bulk;
		DelegateHandlerImpl acls_webfilter_rules_enable;
		DelegateHandlerImpl acls_webfilter_rules_disable;
		DelegateHandlerImpl acls_rewrite_forceauthranges_set;
		DelegateHandlerImpl acls_rewrite_forceauthranges;
		DelegateHandlerImpl periods_list;
		DelegateHandlerImpl periods_add;
		DelegateHandlerImpl periods_delete;
		DelegateHandlerImpl deny_list;
		DelegateHandlerImpl deny_del;
		DelegateHandlerImpl rewrite_remove;

		DelegateHandlerImpl balancer;
		DelegateHandlerImpl balancer_set;

		DelegateHandlerImpl acls_forwarding;
		DelegateHandlerImpl acls_forwarding_add;
		DelegateHandlerImpl acls_forwarding_enable;
		DelegateHandlerImpl acls_forwarding_disable;
		DelegateHandlerImpl acls_forwarding_delete;
		DelegateHandlerImpl acls_masquerading;
		DelegateHandlerImpl acls_masquerading_add;
		DelegateHandlerImpl acls_masquerading_delete;

		DelegateHandlerImpl acls_masquerading_disable;
		DelegateHandlerImpl acls_masquerading_enable;
		DelegateHandlerImpl autowan;
		DelegateHandlerImpl autowan_set;
		DelegateHandlerImpl signatures;
		DelegateHandlerImpl signatures_add;
		DelegateHandlerImpl signatures_del;

		_END_SPHIREWALL_DELEGATES

};

#endif
