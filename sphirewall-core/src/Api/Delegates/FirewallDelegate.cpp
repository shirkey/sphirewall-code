/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <arpa/inet.h>

using namespace std;

#include "Core/Event.h"
#include "Core/System.h"
#include "Json/JSON.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/State.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/ConnTracker.h"
#include "Api/Delegates/FirewallDelegate.h"
#include "Api/Exceptions.h"
#include "SFwallCore/NatAcls.h"
#include "SFwallCore/Criteria.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"
#include "SFwallCore/ApplicationLevel/CapturePortal.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &FirewallDelegate::y))

using namespace SFwallCore;


FirewallDelegate::FirewallDelegate(Firewall *firewall, IntMgr* interfaceManager, GroupDb *groupDb)
	: firewall(firewall), interfaceManager(interfaceManager), groupDb(groupDb), httpApplicationFilter(NULL), eventDb(NULL) {
	installHandler("firewall/aliases/list", aliases_list);
	installHandler("firewall/aliases/add", aliases_add);
	installHandler("firewall/aliases/add/bulk", aliases_add_bulk);
	installHandler("firewall/aliases/del", aliases_del);
	installHandler("firewall/aliases/del/all", aliases_del_all);
	installHandler("firewall/aliases/del/bulk", aliases_del_bulk);
	installHandler("firewall/aliases/alias/search", aliases_alias_search);
	installHandler("firewall/aliases/alias/list", aliases_alias_list);
	installHandler("firewall/aliases/alias/add", aliases_alias_add);
	installHandler("firewall/aliases/alias/del", aliases_alias_del);
	installHandler("firewall/aliases/get", aliases_get);
	installHandler("firewall/aliases/load", aliases_load);
	installHandler("firewall/tracker/list", tracker_list);
	installHandler("firewall/tracker/terminate", tracker_terminate);
	installHandler("firewall/acls/filter/up", acls_filter_up);
	installHandler("firewall/acls/filter/down", acls_filter_down);
	installHandler("firewall/acls/filter/delete", acls_filter_delete);
	installHandler("firewall/acls/nat/delete", acls_nat_delete);
	installHandler("firewall/acls/list", acls_list);
	installHandler("firewall/acls/add", acls_add);
	installHandler("firewall/acls/list/trafficshaper", acls_list_trafficshaper);
	installHandler("firewall/acls/add/trafficshaper", acls_add_trafficshaper);
	installHandler("firewall/acls/del/trafficshaper", acls_list_del_trafficshaper);
	installHandler("firewall/acls/enable", acls_enable);
	installHandler("firewall/acls/disable", acls_disable);
	installHandler("firewall/acls/status", acls_status);
	installHandler("firewall/webfilter/rules/list", acls_webfilter_rules_list);
	installHandler("firewall/webfilter/rules/deleteall", acls_webfilter_rules_deleteall);
	installHandler("firewall/webfilter/rules/add", acls_webfilter_rules_add);
	installHandler("firewall/webfilter/rules/add/bulk", acls_webfilter_rules_add_bulk);
	installHandler("firewall/webfilter/rules/remove", acls_webfilter_rules_remove);
	installHandler("firewall/tracker/size", tracker_size);
	installHandler("firewall/webfilter/rules/moveup", acls_webfilter_rules_moveup);
	installHandler("firewall/webfilter/rules/movedown", acls_webfilter_rules_movedown);
	installHandler("firewall/webfilter/rules/enable", acls_webfilter_rules_enable);
	installHandler("firewall/webfilter/rules/disable", acls_webfilter_rules_disable);
	installHandler("firewall/rewrite/forceauthranges", acls_rewrite_forceauthranges);
	installHandler("firewall/rewrite/forceauthranges/set", acls_rewrite_forceauthranges_set);
	installHandler("firewall/periods/list", periods_list);
	installHandler("firewall/periods/add", periods_add);
	installHandler("firewall/periods/delete", periods_delete);

	installHandler("firewall/acls/forwarding", acls_forwarding);
	installHandler("firewall/acls/forwarding/enable", acls_forwarding_enable);
	installHandler("firewall/acls/forwarding/disable", acls_forwarding_disable);
	installHandler("firewall/acls/forwarding/add", acls_forwarding_add);
	installHandler("firewall/acls/forwarding/delete", acls_forwarding_delete);

	installHandler("firewall/acls/masquerading", acls_masquerading);
	installHandler("firewall/acls/masquerading/add", acls_masquerading_add);
	installHandler("firewall/acls/masquerading/delete", acls_masquerading_delete);
	installHandler("firewall/acls/masquerading/enable", acls_masquerading_enable);
	installHandler("firewall/acls/masquerading/disable", acls_masquerading_disable);
	
	installHandler("firewall/autowan", autowan);
	installHandler("firewall/autowan/set", autowan_set);
	installHandler("firewall/signatures", signatures);
	installHandler("firewall/signatures/add", signatures_add);
	installHandler("firewall/signatures/del", signatures_del);
}

string FirewallDelegate::rexpression() {
	return "firewall/(.*)";
}

void FirewallDelegate::setApplicationFilter(ApplicationFilter *filter) {
	this->httpApplicationFilter = filter;
}

ApplicationFilter *FirewallDelegate::getApplicationFilter() {
	if (this->httpApplicationFilter == NULL) {
		this->httpApplicationFilter = firewall->httpFilter;
	}

	return this->httpApplicationFilter;
}

void FirewallDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

void FirewallDelegate::serializeRule(JSONObject &rule, SFwallCore::Rule *target) {
	rule.put(L"enabled", new JSONValue((bool) target->enabled));

	if (target->comment.size() > 0) {
		rule.put(L"comment", new JSONValue((string) target->comment));
	}

	ObjectContainer* criterias = new ObjectContainer(CARRAY);
	for(SFwallCore::Criteria* c: target->criteria){
		ObjectContainer* singleCriteria = new ObjectContainer(CREL);
		singleCriteria->put("type", new ObjectWrapper(c->key()));
		singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(c)));
		criterias->put(new ObjectWrapper(singleCriteria));
	}
	ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
	rule.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));
	delete criteriaWrapper;
}

JSONObject FirewallDelegate::signatures(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::SignaturePtr signature: firewall->signatureStore->available()) {
		JSONObject target;

		target.put(L"type", new JSONValue((string) signature->type()));
		target.put(L"name", new JSONValue((string) signature->name()));
		target.put(L"description", new JSONValue((string) signature->description()));
		if(signature->type().compare("signatures.regex") == 0){
			RegexSignature* rsig = (RegexSignature*) signature.get();
			target.put(L"expression", new JSONValue((string) rsig->expression));
		}

		target.put(L"id", new JSONValue((string) signature->id()));

		arr.push_back(new JSONValue(target));
	}

	ret.put(L"signatures", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::signatures_add(JSONObject args) {
	string id;
	if(args.has(L"id")){
		id = args[L"id"]->String();
	}else{
		id = StringUtils::genRandom();
	}

	SignaturePtr signature = firewall->signatureStore->get(id);
	if(!signature){
		signature = SignaturePtr(new RegexSignature());
		firewall->signatureStore->add(signature);
	}

	RegexSignature* rsig = (RegexSignature*) signature.get();

	rsig->expression = args[L"expression"]->String();
	rsig->uniqueId = id;
	rsig->userDefinedName= args[L"name"]->String();
	rsig->userDefinedDescription= args[L"description"]->String();
	rsig->refresh();
	firewall->signatureStore->save();	
	
	return JSONObject();
}

JSONObject FirewallDelegate::signatures_del(JSONObject args) {
	string id = args[L"id"]->String();
	SignaturePtr signature = firewall->signatureStore->get(id);
	if(signature){
		firewall->signatureStore->remove(signature);
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::PortForwardingRulePtr rule : firewall->natAcls->listPortForwardingRules()) {
		JSONObject target;

		ObjectContainer* criterias = new ObjectContainer(CARRAY);
		for(Criteria* criteria: rule->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(criteria->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(criteria)));
			criterias->put(new ObjectWrapper(singleCriteria));
		}
		ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
		target.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));
		delete criteriaWrapper;

		target.put(L"forwardingDestination", new JSONValue((string) IP4Addr::ip4AddrToString(rule->forwardingDestination)));
		target.put(L"id", new JSONValue((string) rule->id));
		target.put(L"enabled", new JSONValue((bool) rule->enabled));

		if (rule->forwardingDestinationPort != UNSET) {
			target.put(L"forwardingDestinationPort", new JSONValue((double) rule->forwardingDestinationPort));
		}

		arr.push_back(new JSONValue(target));
	}

	ret.put(L"rules", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_forwarding_add(JSONObject args) {
	SFwallCore::PortForwardingRulePtr rule;

	if (args.has(L"id")) {
		rule = firewall->natAcls->getPortForwardingRule(args[L"id"]->String());
	}

	if (!rule) {
		rule = SFwallCore::PortForwardingRulePtr(new SFwallCore::PortForwardingRule());
	}

	rule->criteria.clear();
	JSONArray criteria = args[L"criteria"]->AsArray();
	for(int x= 0; x < criteria.size(); x++){
		JSONObject individualCriteria = criteria[x]->AsObject();
		rule->criteria.push_back(CriteriaBuilder::parse(individualCriteria[L"type"]->String(), ObjectWrapper::parseRecurse(individualCriteria[L"conditions"])->container()));
	}

	rule->forwardingDestination = IP4Addr::stringToIP4Addr(args[L"forwardingDestination"]->String());

	if (args.has(L"forwardingDestinationPort")) {
		rule->forwardingDestinationPort = args[L"forwardingDestinationPort"]->AsNumber();
	}

	if (!args.has(L"id")) {
		firewall->natAcls->addForwardingRule(rule);
	}

	firewall->natAcls->initRules();
	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding_delete(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::PortForwardingRulePtr rule = firewall->natAcls->getPortForwardingRule(targetid);

	if (rule) {
		firewall->natAcls->delPortForwardingRule(rule);
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding_enable(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::PortForwardingRulePtr rule = firewall->natAcls->getPortForwardingRule(targetid);

	if (rule) {
		rule->enabled = true;
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_forwarding_disable(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::PortForwardingRulePtr rule = firewall->natAcls->getPortForwardingRule(targetid);

	if (rule) {
		rule->enabled = false;
	}

	firewall->natAcls->save();
	return JSONObject();
}
//MASQUERADING Routes
JSONObject FirewallDelegate::acls_masquerading(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (SFwallCore::MasqueradeRulePtr rule : firewall->natAcls->listMasqueradeRules()) {
		JSONObject target;

		ObjectContainer* criterias = new ObjectContainer(CARRAY);
		for(Criteria* criteria: rule->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(criteria->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(criteria)));
			criterias->put(new ObjectWrapper(singleCriteria));
		}
		ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
		target.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));
		delete criteriaWrapper;

		target.put(L"natTargetDevice", new JSONValue((string) rule->natTargetDevice));
		if (rule->natTargetIp != UNSET) {
			target.put(L"natTargetIp", new JSONValue((string) IP4Addr::ip4AddrToString(rule->natTargetIp)));
		}

		target.put(L"id", new JSONValue((string) rule->id));
		target.put(L"enabled", new JSONValue((bool) rule->enabled));

		arr.push_back(new JSONValue(target));
	}

	ret.put(L"rules", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_masquerading_add(JSONObject args) {
	SFwallCore::MasqueradeRulePtr rule;

	if (args.has(L"id")) {
		rule = firewall->natAcls->getMasqueradeRule(args[L"id"]->String());
	}

	if (!rule) {
		rule = SFwallCore::MasqueradeRulePtr(new SFwallCore::MasqueradeRule());
	}

	rule->criteria.clear();

	JSONArray criteria = args[L"criteria"]->AsArray();
	for(int x= 0; x < criteria.size(); x++){
		JSONObject individualCriteria = criteria[x]->AsObject();
		rule->criteria.push_back(CriteriaBuilder::parse(individualCriteria[L"type"]->String(), ObjectWrapper::parseRecurse(individualCriteria[L"conditions"])->container()));
	}

	if (args.has(L"natTargetDevice")) {
		rule->natTargetDevice = args[L"natTargetDevice"]->String();
	}
	else {
		rule->natTargetDevice = "";
	}

	if (args.has(L"natTargetIp")) {
		rule->natTargetIp = IP4Addr::stringToIP4Addr(args[L"natTargetIp"]->String());
	}
	else {
		rule->natTargetIp = -1;
	}

	if (!args.has(L"id")) {
		firewall->natAcls->addMasqueradeRule(rule);
	}

	firewall->natAcls->initRules();
	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_masquerading_delete(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::MasqueradeRulePtr rule = firewall->natAcls->getMasqueradeRule(targetid);

	if (rule) {
		firewall->natAcls->delMasqueradeRule(rule);
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_masquerading_enable(JSONObject args) {
	std::string targetid = args[L"id"]->String();

	SFwallCore::MasqueradeRulePtr rule = firewall->natAcls->getMasqueradeRule(targetid);

	if (rule) {
		rule->enabled = true;
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_masquerading_disable(JSONObject args) {
	std::string targetid = args[L"id"]->String();
	SFwallCore::MasqueradeRulePtr rule = firewall->natAcls->getMasqueradeRule(targetid);

	if (rule) {
		rule->enabled = false;
	}

	firewall->natAcls->save();
	return JSONObject();
}

JSONObject FirewallDelegate::process(std::string uri, JSONObject obj) {
	try {
		return invokeHandler(uri, obj);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}
}

JSONObject FirewallDelegate::periods_list(JSONObject args) {
	TimePeriodStore *store = System::getInstance()->periods;
	JSONObject ret;
	JSONArray rarr;

	for (TimePeriodPtr target : store->list()) {
		JSONObject obj;
		obj.put(L"startTime", new JSONValue((double) target->startTime));
		obj.put(L"endTime", new JSONValue((double) target->endTime));
		obj.put(L"startDate", new JSONValue((double) target->startDate));
		obj.put(L"endDate", new JSONValue((double) target->endDate));
		//days:
		obj.put(L"any", new JSONValue((bool) target->any));
		obj.put(L"mon", new JSONValue((bool) target->mon));
		obj.put(L"tue", new JSONValue((bool) target->tue));
		obj.put(L"wed", new JSONValue((bool) target->wed));
		obj.put(L"thu", new JSONValue((bool) target->thu));
		obj.put(L"fri", new JSONValue((bool) target->fri));
		obj.put(L"sat", new JSONValue((bool) target->sat));
		obj.put(L"sun", new JSONValue((bool) target->sun));
		obj.put(L"id", new JSONValue((string) target->uuid));
		obj.put(L"name", new JSONValue((string) target->name));

		rarr.push_back(new JSONValue(obj));
	}

	ret.put(L"periods", new JSONValue(rarr));
	return ret;
}

JSONObject FirewallDelegate::periods_add(JSONObject obj) {
	TimePeriodStore *store = System::getInstance()->periods;
	TimePeriodPtr period;

	if(obj.has(L"id")){
		period = store->getById(obj[L"id"]->String());
		period->startTime = obj[L"startTime"]->AsNumber();
		period->endTime = obj[L"endTime"]->AsNumber();
		period->startDate = obj[L"startDate"]->AsNumber();
		period->endDate = obj[L"endDate"]->AsNumber();
		period->any = obj[L"any"]->AsBool();
		period->mon = obj[L"mon"]->AsBool();
		period->tue = obj[L"tue"]->AsBool();
		period->wed = obj[L"wed"]->AsBool();
		period->thu = obj[L"thu"]->AsBool();
		period->fri = obj[L"fri"]->AsBool();
		period->sat = obj[L"sat"]->AsBool();
		period->sun = obj[L"sun"]->AsBool();
	}else{
		period = TimePeriodPtr(new TimePeriod());
		period->name = obj[L"name"]->String();
		store->add(period);
	}

	store->save();

	JSONObject ret;
	ret.put(L"id", new JSONValue((string) period->uuid));
	return ret;
}

JSONObject FirewallDelegate::periods_delete(JSONObject obj) {
	TimePeriodStore *store = System::getInstance()->periods;
	JSONObject ret;

	try {
		TimePeriodPtr period = store->getById(obj[L"id"]->String());
		if (period) {
			store->remove(period);
			store->save();
			ret.put(L"status", new JSONValue((double) 0));
		}
		else {
			throw DelegateGeneralException("That period does not exist.");
		}
	}
	catch (const exception &e) {
		ret.put(L"status", new JSONValue((double) - 1));
	}

	return ret;
}

JSONObject FirewallDelegate::autowan(JSONObject obj) {
	JSONObject ret;
	NatAclStore *store = firewall->natAcls;
	ret.put(L"mode", new JSONValue((double) store->get_autowan_mode()));

	JSONArray devices;
	for (AutowanInterfacePtr interface: store->get_autowan_rules()) {
		JSONObject o;
		o.put(L"failover_index", new JSONValue((double) interface->failover_index));
		o.put(L"interface", new JSONValue((string) interface->interface));

                ObjectContainer* criterias = new ObjectContainer(CARRAY);
                for(SFwallCore::Criteria* c: interface->criteria){
                        ObjectContainer* singleCriteria = new ObjectContainer(CREL);
                        singleCriteria->put("type", new ObjectWrapper(c->key()));
                        singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(c)));
                        criterias->put(new ObjectWrapper(singleCriteria));
                }
                ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
                o.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));

		devices.push_back(new JSONValue(o));
	}

	ret.put(L"interfaces", new JSONValue(devices));
	return ret;
}

JSONObject FirewallDelegate::autowan_set(JSONObject obj) {
	NatAclStore *store = firewall->natAcls;

	store->get_autowan_rules().clear();	
	JSONArray devices = obj[L"interfaces"]->AsArray();

	for (uint x = 0; x < devices.size(); x++) {
		JSONObject o = devices[x]->AsObject();	

		AutowanInterface* interface = new AutowanInterface();
		interface->interface = o[L"interface"]->String();
		interface->failover_index = o[L"failover_index"]->AsNumber();

		if(obj.has(L"criteria")){
			interface->criteria.clear();
			JSONArray criteria = obj[L"criteria"]->AsArray();
			for(int x= 0; x < criteria.size(); x++){
				JSONObject individualCriteria = criteria[x]->AsObject();
				Criteria* criteria = CriteriaBuilder::parse(individualCriteria[L"type"]->String(), ObjectWrapper::parseRecurse(individualCriteria[L"conditions"])->container());
				if(criteria){
					interface->criteria.push_back(criteria);
				}
			}
		}

		store->get_autowan_rules().push_back(AutowanInterfacePtr(interface));
	}

	store->set_autowan_mode(obj[L"mode"]->AsNumber());
	store->initRules();
	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_rewrite_forceauthranges(JSONObject args) {
	CapturePortalEngine *engine = firewall->capturePortalEngine;
	JSONObject ret;
	JSONArray ranges;
	JSONArray websites;
	JSONArray destinationExceptions;

	for (AliasPtr target :  engine->getForceAuthRanges()) {
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) target->id));
		o.put(L"aliasName", new JSONValue((string) target->name));

		ranges.push_back(new JSONValue(o));
	}

	for (AliasPtr target : engine->getForceAuthWebsiteExceptions()) {
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) target->id));
		o.put(L"aliasName", new JSONValue((string) target->name));

		websites.push_back(new JSONValue(o));
	}

	for (AliasPtr target : engine->getForceAuthDestIpExceptions()) {
		JSONObject o;
		o.put(L"aliasId", new JSONValue((string) target->id));
		o.put(L"aliasName", new JSONValue((string) target->name));

		destinationExceptions.push_back(new JSONValue(o));
	}



	ret.put(L"ranges", new JSONValue(ranges));
	ret.put(L"websites", new JSONValue(websites));
	ret.put(L"destExceptions", new JSONValue(destinationExceptions));
	return ret;
}

JSONObject FirewallDelegate::acls_rewrite_forceauthranges_set(JSONObject obj) {
	CapturePortalEngine *engine = firewall->capturePortalEngine;
	AliasDb *aliases = firewall->aliases;
	engine->getForceAuthRanges().clear();
	engine->getForceAuthWebsiteExceptions().clear();

	//Ranges
	JSONArray ranges = obj[L"ranges"]->AsArray();

	for (unsigned x = 0; x < ranges.size(); x++) {
		string range = ranges[x]->String();
		AliasPtr alias = aliases->get(range);

		if (alias) {
			engine->getForceAuthRanges().push_back(alias);
		}
	}

	//Websites
	JSONArray websites = obj[L"websites"]->AsArray();

	for (unsigned x = 0; x < websites.size(); x++) {
		string range = websites[x]->String();
		AliasPtr alias = aliases->get(range);

		if (alias) {
			engine->getForceAuthWebsiteExceptions().push_back(alias);
		}
	}

	JSONArray destinationExceptions = obj[L"destExceptions"]->AsArray();

	for (unsigned x = 0; x < destinationExceptions.size(); x++) {
		string range = destinationExceptions[x]->String();
		AliasPtr alias = aliases->get(range);

		if (alias) {
			engine->getForceAuthDestIpExceptions().push_back(alias);
		}
	}

	engine->save();
	return JSONObject();
}

JSONObject FirewallDelegate::aliases_list(JSONObject) {
	JSONObject ret;
	JSONArray array;

	for (std::pair<string, AliasPtr> aliasMapping : firewall->aliases->aliases) {
		JSONObject o;

		string a = aliasMapping.second->description() + ((!aliasMapping.second->source) ? "." : (" from " + aliasMapping.second->source->description()));

		o.put(L"id", new JSONValue(aliasMapping.first));
		o.put(L"name", new JSONValue(aliasMapping.second->name));
		o.put(L"type", new JSONValue((double) aliasMapping.second->type()));
		o.put(L"description", new JSONValue(a));
		array.push_back(new JSONValue(o));
	}

	ret.put(L"aliases", new JSONValue(array));
	return ret;
}

JSONObject FirewallDelegate::aliases_get(JSONObject object) {
	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		JSONObject o;
		o.put(L"id", new JSONValue(alias->id));
		o.put(L"name", new JSONValue(alias->name));
		o.put(L"description", new JSONValue(alias->description()));

		return o;
	}

	throw DelegateGeneralException("Could not find alias");
}

JSONObject FirewallDelegate::aliases_load(JSONObject object) {
	JSONObject ret;
	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());
	ret.put(L"loadstate", new JSONValue((double) ((alias) ? alias->load() : -1)));
	return ret;
}

JSONObject FirewallDelegate::aliases_alias_search(JSONObject object) {
	JSONObject ret;
	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	/*Depending on the alias type we must search for different things*/
	std::string searchExpression = object[L"search"]->String();	
	if(alias->type() == IP_RANGE || alias->type() == IP_SUBNET){
		ret.put(L"matched", new JSONValue((bool) alias->searchForNetworkMatch(IP4Addr::stringToIP4Addr(searchExpression))));
	}

	if(alias->type() == WEBSITE_LIST || alias->type() == STRING_WILDCARD_LIST){
		ret.put(L"matched", new JSONValue((bool) alias->search(searchExpression)));
	}

	return ret;
}

JSONObject FirewallDelegate::aliases_add_bulk(JSONObject object) {
	JSONArray items = object[L"items"]->AsArray();

	for (unsigned x = 0; x < items.size(); x++) {
		aliases_add(items[x]->AsObject());
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_add(JSONObject object) {
	if (firewall->aliases->getByName(object[L"name"]->String())) {
		return JSONObject();
	}

	SFwallCore::AliasType type = (SFwallCore::AliasType) object[L"type"]->AsNumber();
	int ret = 0;
	AliasPtr alias;

	switch (type) {
		case IP_RANGE: {
				       alias = AliasPtr(new IpRangeAlias());
				       alias->name = object[L"name"]->String();
				       break;
			       }

		case IP_SUBNET: {
					alias = AliasPtr(new IpSubnetAlias());
					alias->name = object[L"name"]->String();
					break;
				}

		case WEBSITE_LIST : {
					    alias = AliasPtr(new WebsiteListAlias());
					    alias->name = object[L"name"]->String();
					    break;
				    }

		case STRING_WILDCARD_LIST: {
						   alias = AliasPtr(new StringWildcardListAlias());
						   alias->name = object[L"name"]->String();
						   break;
					   }

		default:
					   throw DelegateGeneralException("Alias type could not be resolved");

	}

	if (object.has(L"source") && object[L"source"]->AsNumber() != -1) {
		AliasListSourceType source = (AliasListSourceType) object[L"source"]->AsNumber();

		switch (source) {
			case DNS: {
					  alias->source = new DnsListSource();
					  alias->source->detail = object[L"detail"]->String();
					  break;
				  }

			case HTTP_FILE: {
						alias->source = new HttpFileSource();
						alias->source->detail = object[L"detail"]->String();
						break;
					}

			default:
					throw DelegateGeneralException("Alias source type could not be resolved");

		};

	}
	else {
		alias->source = NULL;

		//Can I already add entries:
		if (object.has(L"items")) {
			JSONArray items = object[L"items"]->AsArray();

			for (unsigned x = 0; x < items.size(); x++) {
				alias->addEntry(items[x]->String());
			}
		}
	}

	ret = firewall->aliases->create(alias);

	JSONObject jret;
	jret.put(L"loadstate", new JSONValue((double) ret));
	return jret;
}

JSONObject FirewallDelegate::aliases_del(JSONObject object) {
	AliasPtr target = firewall->aliases->get(object[L"id"]->String());

	if (!target) {
		throw DelegateGeneralException("Could not find alias");
	}

	firewall->aliases->del(target);
	return JSONObject();
}

JSONObject FirewallDelegate::aliases_del_all(JSONObject object) {
	map<string, AliasPtr>::iterator iter;
	list<AliasPtr> toDelete;

	for (iter = firewall->aliases->aliases.begin(); iter != firewall->aliases->aliases.end(); iter++) {
		AliasPtr alias = iter->second;
		toDelete.push_back(alias);
	}

	for (list<AliasPtr>::iterator iter = toDelete.begin(); iter != toDelete.end(); iter++) {
		firewall->aliases->del((*iter));
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_del_bulk(JSONObject object) {
	JSONArray ids = object[L"ids"]->AsArray();

	for (unsigned x = 0; x < ids.size(); x++) {
		AliasPtr target = firewall->aliases->get(ids[x]->String());

		if (target) {
			firewall->aliases->del(target);
		}
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_list(JSONObject object) {
	JSONObject ret;
	JSONArray items;

	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		std::list<std::string> entries = alias->listEntries();

		for (std::list<std::string>::iterator iter = entries.begin();
				iter != entries.end();
				iter++) {

			items.push_back(new JSONValue((string)(*iter)));
		}

		ret.put(L"items", new JSONValue(items));
		ret.put(L"type", new JSONValue((double) alias->type()));
		return ret;
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_add(JSONObject object) {
	JSONObject ret;

	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		alias->addEntry(object[L"value"]->String());
		firewall->aliases->save();
	}

	return JSONObject();
}

JSONObject FirewallDelegate::aliases_alias_del(JSONObject object) {
	JSONObject ret;

	AliasPtr alias = firewall->aliases->get(object[L"id"]->String());

	if (alias) {
		alias->removeEntry(object[L"value"]->String());
		firewall->aliases->save();
	}

	return JSONObject();
}

JSONObject FirewallDelegate::tracker_size(JSONObject object) {
	JSONObject ret;
	ret.put(L"size", new JSONValue((double) firewall->connectionTracker->size()));
	return ret;
}

JSONObject FirewallDelegate::tracker_list(JSONObject object) {
	JSONObject ret;
	JSONArray conns;

	firewall->connectionTracker->holdLock();
	vector<SFwallCore::Connection *> connections = firewall->connectionTracker->listConnections();

	for (unsigned x = 0; x < connections.size(); x++) {
		SFwallCore::Connection *connection = connections[x];

		if (!connection->hasExpired()) {
			if (object.has(L"filterTime")) {
				int filterTime = object[L"filterTime"]->AsNumber();

				if (connection->getTime() < filterTime) {
					continue;
				}
			}

			JSONObject o;

			if (connection->getIp()->type() == IPV4) {
				ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) connection->getIp();
				o.put(L"sourceIp", new JSONValue((string) IP4Addr::ip4AddrToString(ipv4->getSrcIp())));
				o.put(L"destIp", new JSONValue((string) IP4Addr::ip4AddrToString(ipv4->getDstIp())));
			}
			else if (connection->getIp()->type() == IPV6) {
				ConnectionIpV6 *ipv6 = (ConnectionIpV6 *) connection->getIp();

				char dst[INET6_ADDRSTRLEN];
				char src[INET6_ADDRSTRLEN];

				inet_ntop(AF_INET6, ipv6->getDstIp(), (char *) &dst, INET6_ADDRSTRLEN);
				inet_ntop(AF_INET6, ipv6->getSrcIp(), (char *) &src, INET6_ADDRSTRLEN);

				o.put(L"sourceIp", new JSONValue((string) src));
				o.put(L"destIp", new JSONValue((string) dst));
			}

			o.put(L"bytes", new JSONValue((double) connection->getUpload() + connection->getDownload()));
			o.put(L"protocol", new JSONValue((double) connection->getProtocol()));

			SessionPtr session = connection->getSession();

			if (session) {
				o.put(L"user", new JSONValue((string) session->getUser()->getUserName()));
			}

			if (connection->getProtocol() == SFwallCore::TCP) {
				SFwallCore::TcpConnection *tcp = dynamic_cast<SFwallCore::TcpConnection *>(connection);
				o.put(L"sourcePort", new JSONValue((double) tcp->getSourcePort()));
				o.put(L"destPort", new JSONValue((double) tcp->getDestinationPort()));

				o.put(L"state", new JSONValue((string) tcp->getState()->echo()));
			}

			if (connection->getProtocol() == SFwallCore::UDP) {
				SFwallCore::UdpConnection *udp = dynamic_cast<SFwallCore::UdpConnection *>(connection);
				o.put(L"sourcePort", new JSONValue((double) udp->getSourcePort()));
				o.put(L"destPort", new JSONValue((double) udp->getDestinationPort()));
			}

			if (connection->idle() > (60 * 2)) {
				o.put(L"isIdle", new JSONValue(true));
			}

			conns.push_back(new JSONValue(o));
		}
	}

	firewall->connectionTracker->releaseLock();
	ret.put(L"connections", new JSONValue(conns));
	return ret;
}

JSONObject FirewallDelegate::tracker_terminate(JSONObject obj) {
	SFwallCore::PlainConnTracker *connTracker = firewall->connectionTracker;

	SFwallCore::ConnectionCriteria targetCriteria;
	targetCriteria.setSource(IP4Addr::stringToIP4Addr(obj[L"source"]->String()));
	targetCriteria.setDest(IP4Addr::stringToIP4Addr(obj[L"dest"]->String()));
	targetCriteria.setSourcePort(obj[L"sourcePort"]->AsNumber());
	targetCriteria.setDestPort(obj[L"destPort"]->AsNumber());

	if (eventDb) {
		EventParams params;
		params["source"] = obj[L"source"]->String();
		params["sourcePort"] = obj[L"sourcePort"]->AsNumber();
		params["dest"] = obj[L"dest"]->String();
		params["destPort"] = obj[L"destPort"]->AsNumber();
		eventDb->add(new Event(AUDIT_FIREWALL_CONNECTIONS_TERMINATE, params));
	}

	list<SFwallCore::Connection *> connections = connTracker->listConnections(targetCriteria);

	for (list<SFwallCore::Connection *>::iterator iter = connections.begin(); iter != connections.end(); iter++) {
		SFwallCore::Connection *conn = (*iter);
		firewall->connectionTracker->terminate(conn);
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_up(JSONObject args) {
	SFwallCore::ACLStore *store = firewall->acls;
	std::string id = args[L"id"]->String();
	Rule *rule = store->getRuleById(id);

	if (rule) {
		store->moveup(rule);
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_down(JSONObject args) {
	SFwallCore::ACLStore *store = firewall->acls;
	std::string id = args[L"id"]->String();
	Rule *rule = store->getRuleById(id);

	if (rule) {
		store->movedown(rule);
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	return JSONObject();
}

JSONObject FirewallDelegate::acls_filter_delete(JSONObject args) {
	std::string id = args[L"id"]->String();

	SFwallCore::ACLStore *store = firewall->acls;
	SFwallCore::Rule *target = firewall->acls->getRuleById(id);

	if (target) {
		store->deleteAclEntry(target);
	}
	else {
		throw DelegateGeneralException("rule position was not valid");
	}

	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_nat_delete(JSONObject args) {
	return acls_filter_delete(args);
}

JSONObject FirewallDelegate::acls_list(JSONObject) {
	JSONObject ret;
	JSONArray filter;
	JSONArray nat;

	SFwallCore::ACLStore *store = firewall->acls;
	vector<SFwallCore::FilterRule *> rules = store->listFilterRules();

	for (uint x = 0; x < rules.size(); x++) {
		JSONObject rule;

		SFwallCore::FilterRule *target = rules[x];

		if (target->valid) {
			serializeRule(rule, target);

			rule.put(L"action", new JSONValue((double) target->action));
			rule.put(L"ignoreconntrack", new JSONValue((bool) target->ignoreconntrack));

			rule.put(L"hits", new JSONValue((double) target->count));
			rule.put(L"pos", new JSONValue((double) x));
			rule.put(L"id", new JSONValue((string) target->id));
			rule.put(L"log", new JSONValue((bool) target->log));

			filter.push_back(new JSONValue(rule));
		}
	}

	JSONArray priority;
	vector<SFwallCore::PriorityRule *> priorityRules = store->listPriorityRules();

	for (unsigned x = 0; x < priorityRules.size(); x++) {
		JSONObject rule;

		SFwallCore::PriorityRule *target = priorityRules[x];

		if (target->valid) {
			serializeRule(rule, target);
			rule.put(L"nice", new JSONValue((double) target->nice));
			rule.put(L"hits", new JSONValue((double) target->count));
			rule.put(L"pos", new JSONValue((double) x));
			rule.put(L"id", new JSONValue((string) target->id));

			priority.push_back(new JSONValue(rule));
		}

	}

	ret.put(L"priority", new JSONValue(priority));
	ret.put(L"nat", new JSONValue(nat));
	ret.put(L"normal", new JSONValue(filter));
	return ret;
}

JSONObject FirewallDelegate::acls_add(JSONObject args) {
	//Work out what kind of rule we are dealing with first:
	int action = args[L"action"]->AsNumber();
	SFwallCore::Rule *rule = NULL;

	if (action == 0 || action == 1) {
		rule = new SFwallCore::FilterRule();
		rule->enabled = false;
	}
	else if (action == 4) {
		rule = new SFwallCore::PriorityRule();
		rule->enabled = false;
	}
	else {
		throw DelegateGeneralException("Undefined Action; Rule determination failed.");
	}

	if (args.has(L"id")) {
		rule->id = args[L"id"]->String();
	}

	if (args.has(L"comment")) {
		rule->comment = args[L"comment"]->String();
	}

	if (args.has(L"log")) {
		if (action == 0 || action == 1) {
			SFwallCore::FilterRule *crule = (SFwallCore::FilterRule *) rule;
			crule->log = args[L"log"]->AsBool();
		}
	}

	rule->criteria.clear();

	JSONArray criteria = args[L"criteria"]->AsArray();
	for(int x= 0; x < criteria.size(); x++){
		JSONObject individualCriteria = criteria[x]->AsObject();
		Criteria* criteria = CriteriaBuilder::parse(individualCriteria[L"type"]->String(), ObjectWrapper::parseRecurse(individualCriteria[L"conditions"])->container());
		if(criteria){
			rule->criteria.push_back(criteria);
		}
	}

	if (args.has(L"ignoreconntrack")) {
		rule->ignoreconntrack = args[L"ignoreconntrack"]->AsBool();
	}

	if (action == 0 || action == 1) {
		rule->action = action;
	}
	else if (action == 4) {
		PriorityRule *pr = (PriorityRule *) rule;
		pr->nice = args[L"nice"]->AsNumber();
	}

	firewall->acls->createAclEntry(rule);

	JSONObject ret;
	ret.put(L"id", new JSONValue((string) rule->id));
	return ret;
}

JSONObject FirewallDelegate::acls_add_trafficshaper(JSONObject args) {
	TsRulePtr rule; 
	bool modify = false;
	if(args.has(L"id")){
		modify = true;
		rule = firewall->trafficShaper->get(args[L"id"]->String());
	}

	if(!rule){
		rule = TsRulePtr(new TsRule(StringUtils::genRandom()));
	}

	rule->filter->criteria.clear();
	JSONArray criteria = args[L"criteria"]->AsArray();
	for(int x= 0; x < criteria.size(); x++){
		JSONObject individualCriteria = criteria[x]->AsObject();
		Criteria* criteria = CriteriaBuilder::parse(individualCriteria[L"type"]->String(), ObjectWrapper::parseRecurse(individualCriteria[L"conditions"])->container());
		if(criteria) rule->filter->criteria.push_back(CriteriaPtr(criteria));
	}


	int proposedUploadRate = args[L"upload"]->AsNumber();
	int proposedDownloadRate = args[L"download"]->AsNumber();

	//Check thats this rule isnt a complete wildcard with tiny values:
	if (proposedUploadRate < 1024 || proposedDownloadRate < 1024) {
		//Dont create a rule, the values are way to small - this is madness
		throw DelegateGeneralException("values are to small, this is madness");
	}else{
		rule->uploadRate = proposedUploadRate; 
		rule->downloadRate = proposedDownloadRate; 
	}

	rule->cumulative = args[L"cumulative"]->AsBool();
	rule->name = args[L"name"]->String();
	if(!modify){
		firewall->trafficShaper->add(rule);
	}

	firewall->trafficShaper->invalidate();
	firewall->trafficShaper->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_list_trafficshaper(JSONObject args) {
	JSONObject ret;
	JSONArray arr;

	for (TsRulePtr rule : firewall->trafficShaper->list()) {
		TokenFilterPtr target = rule->filter;
		JSONObject o;

		ObjectContainer* criterias = new ObjectContainer(CARRAY);
		for(SFwallCore::CriteriaPtr c: target->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(c->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(c.get())));
			criterias->put(new ObjectWrapper(singleCriteria));
		}
		ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
		o.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));		

		o.put(L"upload", new JSONValue((double) rule->uploadRate));
		o.put(L"download", new JSONValue((double) rule->downloadRate));
		o.put(L"id", new JSONValue((string) rule->id));
		o.put(L"cumulative", new JSONValue((bool) rule->cumulative));
		o.put(L"name", new JSONValue((string) rule->name));

		o.put(L"hits", new JSONValue((double) rule->getHits()));

		arr.push_back(new JSONValue(o));
	}

	ret.put(L"items", new JSONValue(arr));
	return ret;
}

JSONObject FirewallDelegate::acls_list_del_trafficshaper(JSONObject args) {
	TsRulePtr rule = firewall->trafficShaper->get(args[L"id"]->String());

	if (rule) {
		firewall->trafficShaper->del(rule);
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	firewall->trafficShaper->invalidate();
	firewall->trafficShaper->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_enable(JSONObject obj) {
	SFwallCore::ACLStore *store = firewall->acls;
	std::string id = obj[L"id"]->String();
	Rule *target = store->getRuleById(id);

	if (target) {
		target->enabled = true;
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_disable(JSONObject obj) {
	SFwallCore::ACLStore *store = firewall->acls;
	std::string id = obj[L"id"]->String();
	Rule *target = store->getRuleById(id);

	if (target) {
		target->enabled = false;
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	store->save();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_status(JSONObject obj) {
	JSONObject ret;
	SFwallCore::ACLStore *store = firewall->acls;
	std::string id = obj[L"id"]->String();
	Rule *target = store->getRuleById(id);

	if (target) {
		ret.put(L"status", new JSONValue((bool) target->enabled));
	}
	else {
		throw DelegateGeneralException("could not find rule");
	}

	return ret;
}

JSONObject FirewallDelegate::acls_webfilter_rules_list(JSONObject obj) {
	getApplicationFilter()->holdLock();
	JSONObject ret;
	JSONArray array;

	list<SFwallCore::FilterCriteria *> rules = getApplicationFilter()->getRules();

	for (SFwallCore::FilterCriteria *target : rules) {
		JSONObject rule;
		rule.put(L"id", new JSONValue((string) target->id));

		ObjectContainer* criterias = new ObjectContainer(CARRAY);
		for(Criteria* criteria: target->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(criteria->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(criteria)));
			criterias->put(new ObjectWrapper(singleCriteria));
		}
		ObjectWrapper* criteriaWrapper = new ObjectWrapper(criterias);
		rule.put(L"criteria", ObjectWrapper::convertRecurse(criteriaWrapper));	
		delete criteriaWrapper;

		rule.put(L"action", new JSONValue((double) target->action));
		rule.put(L"fireEvent", new JSONValue((bool) target->fireEvent));
		rule.put(L"redirect", new JSONValue((bool) target->redirect));
		rule.put(L"redirectUrl", new JSONValue((string) target->redirectUrl));
		rule.put(L"name", new JSONValue((string) target->name));
		rule.put(L"enabled", new JSONValue((bool) target->enabled));
		rule.put(L"metadata", new JSONValue((string) target->metadata));

		rule.put(L"temp_rule", new JSONValue((bool) target->temp_rule));
		rule.put(L"expiry_timestamp", new JSONValue((double) target->expiry_timestamp));

		array.push_back(new JSONValue(rule));
	}

	ret.put(L"rules", new JSONValue(array));
	getApplicationFilter()->releaseLock();
	return ret;
}

JSONObject FirewallDelegate::acls_webfilter_rules_add_bulk(JSONObject obj) {
	for (auto item : obj[L"items"]->AsArray()) {
		acls_webfilter_rules_add(item->AsObject());
	}
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_add(JSONObject obj) {
	bool newRule = true;
	FilterCriteria *rule = NULL;
	std::string id;

	if (obj.has(L"id")) {
		id = obj[L"id"]->String();
		rule = getApplicationFilter()->get(id);
	}
	else {
		id =  StringUtils::genRandom();
	}

	if (!rule) {
		rule = new FilterCriteria();
		rule->id = id;
	}
	else {
		newRule = false;
	}

	rule->criteria.clear();

	JSONArray criteria = obj[L"criteria"]->AsArray();
	for(int x= 0; x < criteria.size(); x++){
		JSONObject individualCriteria = criteria[x]->AsObject();
		Criteria* criteria = CriteriaBuilder::parse(individualCriteria[L"type"]->String(), ObjectWrapper::parseRecurse(individualCriteria[L"conditions"])->container());
		criteria->refresh();
		rule->criteria.push_back(criteria);
	}

	rule->action = (ApplicationFilterAction) obj[L"action"]->AsNumber();
	rule->fireEvent = obj[L"fireEvent"]->AsBool();
	rule->redirect = obj[L"redirect"]->AsBool();
	rule->redirectUrl = obj[L"redirectUrl"]->String();

	if (obj.has(L"enabled")) {
		rule->enabled = obj[L"enabled"]->AsBool();
	}

	if (obj.has(L"metadata")) {
		rule->metadata = obj[L"metadata"]->String();
	}
	if (obj.has(L"name")) {
		rule->name= obj[L"name"]->String();
	}
	if(obj.has(L"temp_rule")){
		rule->temp_rule = obj[L"temp_rule"]->AsBool();
		rule->expiry_timestamp = obj[L"expiry_timestamp"]->AsNumber();
	}	

	getApplicationFilter()->holdLock();

	if (newRule) {
		getApplicationFilter()->addRule(rule);
	}

	getApplicationFilter()->refresh();
	getApplicationFilter()->save();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_deleteall(JSONObject obj) {
	getApplicationFilter()->holdLock();

	for (FilterCriteria * list : getApplicationFilter()->getRules()) {
		delete list;
	}

	getApplicationFilter()->getRules().clear();
	getApplicationFilter()->refresh();
	getApplicationFilter()->save();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_remove(JSONObject obj) {
	getApplicationFilter()->holdLock();

	for (FilterCriteria * f : getApplicationFilter()->getRules()) {
		if (f->id.compare(obj[L"id"]->String()) == 0) {
			getApplicationFilter()->removeRule(f);
			break;
		}
	}

	getApplicationFilter()->save();
	getApplicationFilter()->refresh();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_moveup(JSONObject obj) {
	getApplicationFilter()->holdLock();
	FilterCriteria *criteria = getApplicationFilter()->get(obj[L"id"]->String());
	getApplicationFilter()->moveup(criteria);

	getApplicationFilter()->save();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_movedown(JSONObject obj) {
	getApplicationFilter()->holdLock();
	FilterCriteria *criteria = getApplicationFilter()->get(obj[L"id"]->String());
	getApplicationFilter()->movedown(criteria);

	getApplicationFilter()->save();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_enable(JSONObject obj) {
	getApplicationFilter()->holdLock();
	FilterCriteria *criteria = getApplicationFilter()->get(obj[L"id"]->String());

	if (criteria) {
		criteria->enabled = true;
		getApplicationFilter()->save();
	}


	getApplicationFilter()->refresh();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

JSONObject FirewallDelegate::acls_webfilter_rules_disable(JSONObject obj) {
	getApplicationFilter()->holdLock();
	FilterCriteria *criteria = getApplicationFilter()->get(obj[L"id"]->String());

	if (criteria) {
		criteria->enabled = false;
		getApplicationFilter()->save();
	}

	getApplicationFilter()->refresh();
	getApplicationFilter()->releaseLock();
	return JSONObject();
}

