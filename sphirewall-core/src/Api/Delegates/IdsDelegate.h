#ifndef IdsDelegate_h
#define IdsDelegate_h

#include "Api/Delegate.h"

class IdsDelegate : public virtual Delegate {
	public:
		IdsDelegate(IDS *ids);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();

		void setEventDb(EventDb *eventDb);
	private:
		IDS *ids;
		EventDb *eventDb;

		_BEGIN_SPHIREWALL_DELEGATES(IdsDelegate)
		DelegateHandlerImpl exceptions_list;
		DelegateHandlerImpl exceptions_add;
		DelegateHandlerImpl exceptions_del;
		_END_SPHIREWALL_DELEGATES
};

#endif
