/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Api/Exceptions.h"
#include "Json/JSON.h"
#include "Utils/StringUtils.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Auth/AuthenticationHandler.h"
#include "Auth/UserDb.h"
#include "Auth/SessionDb.h"
#include "Auth/User.h"
#include "Utils/Hash.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "Utils/WindowsWmiAuthenticator.h"
#include "Api/Delegates/AuthDelegate.h"
#include "SFwallCore/Firewall.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &AuthDelegate::y))

AuthDelegate::AuthDelegate(GroupDb *groupDb, UserDb *userDb, SFwallCore::Firewall *firewall, SessionDb *sessionDb, HostDiscoveryService *arp, AuthenticationManager *authManager)
	: groupDb(groupDb), userDb(userDb), firewall(firewall), sessionDb(sessionDb), arp(arp), authManager(authManager), eventDb(NULL) {
	installHandler("auth/groups/list", groups_list);
	installHandler("auth/groups/create", groups_create);
	installHandler("auth/groups/create/bulk", groups_create_bulk);
	installHandler("auth/groups/get", groups_get);
	installHandler("auth/groups/del", groups_del);
	installHandler("auth/groups/mergeoveruser", groups_mergeoveruser);
	installHandler("auth/groups/save", groups_save);
	installHandler("auth/users/list", users_list);
	installHandler("auth/users/add", users_add);
	installHandler("auth/users/del", users_del);
	installHandler("auth/users/get", users_get);
	installHandler("auth/users/save", users_save);
	installHandler("auth/users/setpassword", user_setpassword);
	installHandler("auth/users/groups/add", users_groups_add);
	installHandler("auth/users/groups/del", users_groups_del);
	installHandler("auth/users/disable", users_disable);
	installHandler("auth/users/enable", users_enable);
	installHandler("auth/sessions/list", sessions_list);
	installHandler("auth/sessions/persist", sessions_persist);
	installHandler("auth/sessions/persist/remove", sessions_persist_remove);
	installHandler("auth/login", login);
	installHandler("auth/logout", logout);
	installHandler("auth/users/defaultpasswordset" , defaultpasswordset);
	installHandler("auth/createsession", sessions_create);
	installHandler("auth/ldap", ldap);
	installHandler("auth/ldap/sync", ldap_sync);
	installHandler("auth/wmic", wmic);
	installHandler("auth/user/merge", user_merge);

	installHandler("auth/sessions/networktimeouts", sessions_networktimeouts);
	installHandler("auth/sessions/networktimeouts/remove", sessions_networktimeouts_remove);
	installHandler("auth/sessions/networktimeouts/manage", sessions_networktimeouts_manage);
}

string AuthDelegate::rexpression() {
	return "auth/(.*)";
}

void AuthDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

JSONObject AuthDelegate::process(string uri, JSONObject args) {
	try {
		return invokeHandler(uri, args);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}

	return JSONObject();
}

JSONObject AuthDelegate::wmic(JSONObject args) {
	JSONObject ret;
	ret.put(L"online", new JSONValue((bool) System::getInstance()->wmic->check()));
	return ret;
}

JSONObject AuthDelegate::ldap_sync(JSONObject args) {
	AuthenticationMethod *method = authManager->getMethod(LDAP_DB);
	JSONObject ret;

        try {
		method->canConnect();
		ret.put(L"count", new JSONValue((double) method->sync(userDb, groupDb)));
        }catch(AuthenticationMethodException& exception){
                ret.put(L"err", new JSONValue((string) exception.what()));
        }

	return ret;
}

JSONObject AuthDelegate::ldap(JSONObject args) {
	AuthenticationMethod *method = authManager->getMethod(LDAP_DB);
	JSONObject ret;
	try {
		ret.put(L"online", new JSONValue((bool) method->canConnect()));
	}catch(AuthenticationMethodException& exception){
		ret.put(L"online", new JSONValue((bool) false));
		ret.put(L"err", new JSONValue((string) exception.what()));
	}

	return ret;
}

JSONObject AuthDelegate::groups_create(JSONObject args) {
	string name = args[L"name"]->String();
	int timeout = (args.has(L"timeout")) ? args[L"timeout"]->AsNumber() : -1;

	if (!groupDb->getGroup(name)) {
		groupDb->createGroup(name, timeout);

		if (eventDb) {
			eventDb->add(
				new Event(
					AUDIT_CONFIGURATION_GROUPDB_ADD,
					EventParams(
			list<pair<string, Param>> {
				pair<string, Param>{"name", Param(name)},
				pair<string, Param>{"timeout", Param(timeout)}
			}
					)
				)
			);
		}

		groupDb->save();
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_create_bulk(JSONObject args) {
	for (JSONValue * jvp : args[L"values"]->AsArray()) {
		JSONObject &o = jvp->AsObject();
		string name = o[L"name"]->String();
		int timeout = (o.has(L"timeout")) ? o[L"timeout"]->AsNumber() : -1;

		if (!groupDb->getGroup(name)) {
			groupDb->createGroup(name, timeout);

			if (eventDb) {
				eventDb->add(
					new Event(
						AUDIT_CONFIGURATION_GROUPDB_ADD,
						EventParams(
				list<pair<string, Param>> {
					pair<string, Param>{"name", Param(name)},
					pair<string, Param>{"timeout", Param(timeout)}
				}
						)
					)
				);
			}
		}
		else {
			groupDb->getGroup(name)->setTimeout(timeout);
		}
	}

	groupDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::groups_mergeoveruser(JSONObject args) {
	//First remove the user from all groups:
	UserPtr target = userDb->getUser(args[L"username"]->String());

	if (!target) {
		target = userDb->createUser(args[L"username"]->String());
	}

	vector<GroupPtr> groups = target->getGroups();

	for (GroupPtr toDelete : groups) {
		target->removeGroup(toDelete);
	}

	JSONArray &groupNames = args[L"groups"]->AsArray();

	for (JSONValue * value : groupNames) {
		GroupPtr targetGroup = groupDb->getGroup(value->String());

		if (!targetGroup) {
			targetGroup = groupDb->createGroup(value->String());
		}

		target->addGroup(targetGroup);
	}

	groupDb->save();
	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::defaultpasswordset(JSONObject args) {
	JSONObject ret;

	UserPtr targetUser = userDb->getUser("admin");
	if (targetUser) {
		Hash hash;

		if (targetUser->getPassword().compare(hash.create_hash("admin", 5)) != -1) {
			ret.put(L"value", new JSONValue((bool) true));
		}
		else {
			ret.put(L"value", new JSONValue((bool) false));
		}
	}
	else {
		ret.put(L"value", new JSONValue((bool) false));
	}

	return ret;
}

JSONObject AuthDelegate::groups_list(JSONObject args) {
	JSONObject ret;
	JSONArray groups;
	for (GroupPtr target : groupDb->list()) {
		JSONObject group;
		group.put(L"id", new JSONValue((double) target->getId()));
		group.put(L"name", new JSONValue((string) target->getName()));
		group.put(L"desc", new JSONValue((string) target->getDesc()));
		group.put(L"manager", new JSONValue((string) target->getManager()));
		group.put(L"allowMui", new JSONValue((bool) target->isAllowMui()));
		group.put(L"timeout", new JSONValue((double) target->getTimeout()));
		group.put(L"metadata", new JSONValue((string) target->getMetadata()));

		//Quota information:
		QuotaInfo &quotas = target->getQuota();
		group.put(L"dailyQuota", new JSONValue((bool) quotas.dailyQuota));
		group.put(L"dailyQuotaLimit", new JSONValue((double) quotas.dailyQuotaLimit));

		group.put(L"weeklyQuota", new JSONValue((bool) quotas.weeklyQuota));
		group.put(L"weeklyQuotaLimit", new JSONValue((double) quotas.weeklyQuotaLimit));

		group.put(L"monthQuota", new JSONValue((bool) quotas.monthQuota));
		group.put(L"monthQuotaLimit", new JSONValue((double) quotas.monthQuotaLimit));

		group.put(L"totalQuota", new JSONValue((bool) quotas.totalQuota));
		group.put(L"totalQuotaLimit", new JSONValue((double) quotas.totalQuotaLimit));

		group.put(L"timeQuota", new JSONValue((bool) quotas.timeQuota));
		group.put(L"timeQuotaLimit", new JSONValue((double) quotas.timeQuotaLimit));

		groups.push_back(new JSONValue(group));
	}

	ret.put(L"groups", new JSONValue(groups));
	return ret;
}

JSONObject AuthDelegate::groups_get(JSONObject args) {
	GroupPtr target = groupDb->getGroup(args[L"id"]->AsNumber());

	if (target) {
		JSONObject group;
		group.put(L"id", new JSONValue((double) target->getId()));
		group.put(L"name", new JSONValue((string) target->getName()));
		group.put(L"desc", new JSONValue((string) target->getDesc()));
		group.put(L"manager", new JSONValue((string) target->getManager()));
		group.put(L"allowMui", new JSONValue((bool) target->isAllowMui()));
		group.put(L"timeout", new JSONValue((double) target->getTimeout()));
		group.put(L"metadata", new JSONValue((string) target->getMetadata()));

		QuotaInfo &quotas = target->getQuota();
		group.put(L"dailyQuota", new JSONValue((bool) quotas.dailyQuota));
		group.put(L"dailyQuotaLimit", new JSONValue((double) quotas.dailyQuotaLimit));

		group.put(L"weeklyQuota", new JSONValue((bool) quotas.weeklyQuota));
		group.put(L"weeklyQuotaLimit", new JSONValue((double) quotas.weeklyQuotaLimit));

		group.put(L"monthQuota", new JSONValue((bool) quotas.monthQuota));
		group.put(L"monthQuotaLimit", new JSONValue((double) quotas.monthQuotaLimit));

		group.put(L"totalQuota", new JSONValue((bool) quotas.totalQuota));
		group.put(L"totalQuotaLimit", new JSONValue((double) quotas.totalQuotaLimit));

		group.put(L"timeQuota", new JSONValue((bool) quotas.timeQuota));
		group.put(L"timeQuotaLimit", new JSONValue((double) quotas.timeQuotaLimit));

		return group;
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_del(JSONObject args) {
	GroupPtr target;

	if (args.has(L"id")) {
		target = groupDb->getGroup(args[L"id"]->AsNumber());
	}
	else if (args.has(L"name")) {
		target = groupDb->getGroup(args[L"name"]->String());
	}

	if (target) {
		if (eventDb) {
			EventParams params;
			params["group"] = target->getName();
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_DEL, params));
		}

		groupDb->delGroup(target);
	}

	return JSONObject();
}

JSONObject AuthDelegate::groups_save(JSONObject args) {
	GroupPtr target = groupDb->getGroup(args[L"id"]->AsNumber());

	if (target) {
		if (args.has(L"desc")) {
			target->setDesc(args[L"desc"]->String());
		}

		if (args.has(L"manager")) {
			target->setManager(args[L"manager"]->String());
		}

		if (args.has(L"mui")) {
			target->setAllowMui(args[L"mui"]->AsBool());
		}

		if (args.has(L"timeout")) {
			target->setTimeout(args[L"timeout"]->AsNumber());
		}

		if (args.has(L"metadata")) {
			target->setMetadata(args[L"metadata"]->String());
		}

		QuotaInfo &quota = target->getQuota();
		if (args.has(L"dailyQuota")) {
			quota.dailyQuota = args[L"dailyQuota"]->AsBool();
			quota.dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
		}

		if (args.has(L"weeklyQuota")) {
			quota.weeklyQuota = args[L"weeklyQuota"]->AsBool();
			quota.weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
		}

		if (args.has(L"monthQuota")) {
			quota.monthQuota = args[L"monthQuota"]->AsBool();
			quota.monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();
		}

		if (args.has(L"totalQuota")) {
			quota.totalQuota = args[L"totalQuota"]->AsBool();
			quota.totalQuotaLimit = args[L"totalQuotaLimit"]->AsNumber();
		}

		if (args.has(L"timeQuota")) {
			quota.timeQuota = args[L"timeQuota"]->AsBool();
			quota.timeQuotaLimit = args[L"timeQuotaLimit"]->AsNumber();
		}

		if (eventDb) {
			EventParams params;
			params["id"] = args[L"id"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_GROUPDB_MODIFIED, params));
		}
	}

	groupDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_list(JSONObject args) {
	JSONObject ret;
	JSONArray users;

	vector<string> userList = userDb->list();

	for (string userListItem : userList) {
		UserPtr source = userDb->getUser(userListItem);
		if (source) {
			JSONObject user;
			user.put(L"username", new JSONValue((string) source->getUserName()));
			user.put(L"fname", new JSONValue((string) source->getFname()));
			user.put(L"lname", new JSONValue((string) source->getLname()));
			user.put(L"email", new JSONValue((string) source->getEmail()));
			user.put(L"lastLogin", new JSONValue((double) source->getLastLogin()));
			user.put(L"enabled", new JSONValue((bool) source->getIsEnabled()));

			//Quota information:
			QuotaInfo *quotas = source->getQuota();
			user.put(L"dailyQuota", new JSONValue((bool) quotas->dailyQuota));
			user.put(L"dailyQuotaLimit", new JSONValue((double) quotas->dailyQuotaLimit));

			user.put(L"weeklyQuota", new JSONValue((bool) quotas->weeklyQuota));
			user.put(L"weeklyQuotaLimit", new JSONValue((double) quotas->weeklyQuotaLimit));

			user.put(L"monthQuota", new JSONValue((bool) quotas->monthQuota));
			user.put(L"monthQuotaLimit", new JSONValue((double) quotas->monthQuotaLimit));

			user.put(L"totalQuota", new JSONValue((bool) quotas->totalQuota));
			user.put(L"totalQuotaLimit", new JSONValue((double) quotas->totalQuotaLimit));

			user.put(L"timeQuota", new JSONValue((bool) quotas->timeQuota));
			user.put(L"timeQuotaLimit", new JSONValue((double) quotas->timeQuotaLimit));

			JSONArray groupsArray;
			for (GroupPtr group : source->getGroups()) {
				JSONObject jsonGroup;
				jsonGroup.put(L"id", new JSONValue((double) group->getId()));
				jsonGroup.put(L"name", new JSONValue((string) group->getName()));
				jsonGroup.put(L"desc", new JSONValue((string) group->getDesc()));

				groupsArray.push_back(new JSONValue(jsonGroup));
			}

			user.put(L"groups", new JSONValue(groupsArray));
			users.push_back(new JSONValue(user));
		}
	}

	ret.put(L"users", new JSONValue(users));
	return ret;
}

JSONObject AuthDelegate::user_merge(JSONObject args) {
	std::string username = args[L"username"]->String();
	UserPtr user = userDb->getUser(username);
	if (!user) {
		user = userDb->createUser(username);
	}

	user->setFname(args[L"fname"]->String());
	user->setLname(args[L"lname"]->String());
	user->setPassword(args[L"password"]->String());

	JSONArray groups = args[L"groups"]->AsArray();

	for (uint x = 0; x < groups.size(); x++) {
		GroupPtr target = groupDb->getGroup(groups[x]->String());

		if (target) {
			user->addGroup(target);
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_add(JSONObject args) {

	if (StringUtils::trim(args[L"username"]->String()) != "") {
		userDb->createUser(args[L"username"]->String());

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_ADD, params));
		}
	}

	return JSONObject();
}

JSONObject AuthDelegate::users_del(JSONObject args) {
	UserPtr user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->delUser(user);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_DEL, params));
		}
	}

	userDb->save();

	return JSONObject();
}

JSONObject AuthDelegate::users_get(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	if (target) {
		JSONObject user;
		user.put(L"username", new JSONValue((string) target->getUserName()));
		user.put(L"fname", new JSONValue((string) target->getFname()));
		user.put(L"lname", new JSONValue((string) target->getLname()));
		user.put(L"email", new JSONValue((string) target->getEmail()));
		user.put(L"lastLogin", new JSONValue((double) target->getLastLogin()));
		user.put(L"enabled", new JSONValue((bool) target->getIsEnabled()));
		user.put(L"authenticationType", new JSONValue((double) target->getAuthenticationType()));

		user.put(L"temp_user", new JSONValue((bool) target->temp_user));
		user.put(L"expiry_timestamp", new JSONValue((double) target->expiry_timestamp));

		QuotaInfo *quotas = target->getQuota();
		user.put(L"dailyQuota", new JSONValue((bool) quotas->dailyQuota));
		user.put(L"dailyQuotaLimit", new JSONValue((double) quotas->dailyQuotaLimit));

		user.put(L"weeklyQuota", new JSONValue((bool) quotas->weeklyQuota));
		user.put(L"weeklyQuotaLimit", new JSONValue((double) quotas->weeklyQuotaLimit));

		user.put(L"monthQuota", new JSONValue((bool) quotas->monthQuota));
		user.put(L"monthQuotaLimit", new JSONValue((double) quotas->monthQuotaLimit));

		user.put(L"totalQuota", new JSONValue((bool) quotas->totalQuota));
		user.put(L"totalQuotaLimit", new JSONValue((double) quotas->totalQuotaLimit));

		user.put(L"timeQuota", new JSONValue((bool) quotas->timeQuota));
		user.put(L"timeQuotaLimit", new JSONValue((double) quotas->timeQuotaLimit));


		JSONArray groupsArray;
		for (GroupPtr group : target->getGroups()) {
			JSONObject jsonGroup;
			jsonGroup.put(L"id", new JSONValue((double) group->getId()));
			jsonGroup.put(L"name", new JSONValue((string) group->getName()));
			jsonGroup.put(L"desc", new JSONValue((string) group->getDesc()));
			jsonGroup.put(L"metadata", new JSONValue((string) group->getMetadata()));

			groupsArray.push_back(new JSONValue(jsonGroup));
		}

		user.put(L"groups", new JSONValue(groupsArray));

		return user;
	}
	else {
		throw DelegateGeneralException("Could not find user");
	}
}

JSONObject AuthDelegate::users_save(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	if (target) {
		if (args.has(L"fname")) {
			target->setFname(args[L"fname"]->String());
		}

		if (args.has(L"lname")) {
			target->setLname(args[L"lname"]->String());
		}

		if (args.has(L"email")) {
			target->setEmail(args[L"email"]->String());
		}

		if(args.has(L"temp_user")){
			target->temp_user = args[L"temp_user"]->AsBool();
			target->expiry_timestamp = args[L"expiry_timestamp"]->AsNumber();
		}

		QuotaInfo *quota = target->getQuota();

		if (args.has(L"dailyQuota")) {
			quota->dailyQuota = args[L"dailyQuota"]->AsBool();
			quota->dailyQuotaLimit = args[L"dailyQuotaLimit"]->AsNumber();
		}

		if (args.has(L"weeklyQuota")) {
			quota->weeklyQuota = args[L"weeklyQuota"]->AsBool();
			quota->weeklyQuotaLimit = args[L"weeklyQuotaLimit"]->AsNumber();
		}

		if (args.has(L"monthQuota")) {
			quota->monthQuota = args[L"monthQuota"]->AsBool();
			quota->monthQuotaLimit = args[L"monthQuotaLimit"]->AsNumber();
		} // End if(args.has(L"monthQuota")).

		if (args.has(L"totalQuota")) {
			quota->totalQuota = args[L"totalQuota"]->AsBool();
			quota->totalQuotaLimit = args[L"totalQuotaLimit"]->AsNumber();
		}

		if (args.has(L"timeQuota")) {
			quota->timeQuota = args[L"timeQuota"]->AsBool();
			quota->timeQuotaLimit = args[L"timeQuotaLimit"]->AsNumber();
		}

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_MODIFIED, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::user_setpassword(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	if (target) {
		target->setPassword(args[L"password"]->String());

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_SETPASSWORD, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_groups_add(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	GroupPtr group = groupDb->getGroup(args[L"group"]->AsNumber());

	if (target && group) {
		target->addGroup(group);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			params["group"] = args[L"group"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_GROUPS_ADD, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_groups_del(JSONObject args) {
	UserPtr target = userDb->getUser(args[L"username"]->String());
	GroupPtr group = groupDb->getGroup(args[L"group"]->AsNumber());

	if (target && group) {
		target->removeGroup(group);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			params["group"] = args[L"group"]->AsNumber();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_GROUPS_DEL, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_disable(JSONObject args) {
	UserPtr user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->disableUser(user);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_DISABLE, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::users_enable(JSONObject args) {
	UserPtr user = userDb->getUser(args[L"username"]->String());
	if (user) {
		userDb->enableUser(user);

		if (eventDb) {
			EventParams params;
			params["username"] = args[L"username"]->String();
			eventDb->add(new Event(AUDIT_CONFIGURATION_USERDB_ENABLE, params));
		}
	}

	userDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_networktimeouts(JSONObject args) {
	JSONObject ret;
	JSONArray timeouts;

	sessionDb->holdLock();
	for(NetworkBasedTimeoutPtr timeout : sessionDb->listNetworkRangeTimeouts()){
		JSONObject target;
		target.put(L"id", new JSONValue((string) timeout->id));
		target.put(L"timeout", new JSONValue((double) timeout->timeout));

		JSONArray networks;	
		for(SFwallCore::AliasPtr network : timeout->networks){
			JSONObject obj;
			obj.put(L"id", new JSONValue((string) network->id));
			obj.put(L"name", new JSONValue((string) network->name));
			networks.push_back(new JSONValue(obj));
		}
		target.put(L"networks", new JSONValue(networks));
		timeouts.push_back(new JSONValue(target));
	}	
	sessionDb->releaseLock();
	ret.put(L"timeouts", new JSONValue(timeouts));
	return ret;
}

JSONObject AuthDelegate::sessions_networktimeouts_remove(JSONObject args) { 
	sessionDb->holdLock();
	/*Find out entity by id*/
	NetworkBasedTimeoutPtr target;
	for(NetworkBasedTimeoutPtr timeout : sessionDb->listNetworkRangeTimeouts()){
		if(timeout->id.compare(args[L"id"]->String()) == 0){
			target = timeout;	
			break;
		}
	}

	if(target){
		sessionDb->removeNetworkRangeTimeout(target);	
	}
	sessionDb->releaseLock();
	sessionDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_networktimeouts_manage(JSONObject args) {
	sessionDb->holdLock();

	NetworkBasedTimeoutPtr target;
	/*Find out entity by id*/
	if(args.has(L"id")){
		for(NetworkBasedTimeoutPtr timeout : sessionDb->listNetworkRangeTimeouts()){
			if(timeout->id.compare(args[L"id"]->String()) == 0){
				target = timeout;
				break;
			}
		}
	}else{
		target = NetworkBasedTimeoutPtr(new NetworkBasedTimeout());
		target->id = StringUtils::genRandom();
		sessionDb->saveNetworkRangeTimeout(target);
	}

	/*Now modify it*/
	if(target){
		target->networks.clear();
		target->timeout = args[L"timeout"]->AsNumber();		

		JSONArray networkIds = args[L"networks"]->AsArray();
		for(int x= 0; x < networkIds.size(); x++){
			SFwallCore::AliasPtr alias = firewall->aliases->get(networkIds[x]->String());
			if(alias){
				target->networks.push_back(alias);
			}
		}

	}

	sessionDb->releaseLock();
	sessionDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_list(JSONObject args) {
	JSONObject ret;
	JSONArray sessions;
	JSONArray persistedSessions;

	sessionDb->holdLock();
	vector<SessionPtr> sessions_orig = sessionDb->list();

	for (SessionPtr session : sessionDb->list()) {
		JSONObject s;
		s.put(L"user", new JSONValue((string) session->getUserName()));
		s.put(L"hw", new JSONValue((string) session->getMac()));
		s.put(L"loginTime", new JSONValue((double) session->getStartTime()));
		s.put(L"host", new JSONValue((string) session->getIp()));

		sessions.push_back(new JSONValue(s));
	}

	for (PersistedSession session : sessionDb->listPersistedSessions()) {
		JSONObject s;
		s.put(L"user", new JSONValue((string) session.username));
		s.put(L"hw", new JSONValue((string) session.mac));
		persistedSessions.push_back(new JSONValue(s));
	}

	ret.put(L"sessions", new JSONValue(sessions));
	ret.put(L"persisted", new JSONValue(persistedSessions));

	sessionDb->releaseLock();
	return ret;
}

JSONObject AuthDelegate::sessions_persist(JSONObject args) {
	string username = args[L"username"]->String();
	string hw = args[L"hw"]->String();

	sessionDb->persistSession(hw, username);
	sessionDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_persist_remove(JSONObject args) {
	string username = args[L"username"]->String();
	string hw = args[L"hw"]->String();

	sessionDb->removePersistedSession(hw, username);
	sessionDb->save();
	return JSONObject();
}

JSONObject AuthDelegate::sessions_create(JSONObject input) {
	string ipAddress = input[L"ipaddress"]->String();
	string username = input[L"username"]->String();

	string macAddress = "";

	if (input.has(L"mac")) {
		macAddress = input[L"mac"]->String();
	}
	else {
		macAddress = arp->get(ipAddress);
	}

	if (macAddress.size() == 0) {
		throw DelegateGeneralException("Could not find mac address for given ip address");
	}

	UserPtr user = authManager->resolveUser(username);
	if (!user) {
		if(input.has(L"create_user") && input[L"create_user"]->AsBool()){
			user = userDb->createUser(username);
		}else{
			throw DelegateGeneralException("Could not find user for the given username");
		}
	}

	if (eventDb) {
		EventParams params;
		params["user"] = username;
		params["ip"] = ipAddress;
		params["hw"] = macAddress;
		eventDb->add(new Event(USERDB_LOGIN_SUCCESS, params));
	}

	sessionDb->holdLock();
	SessionPtr session = sessionDb->get(macAddress, IP4Addr::stringToIP4Addr(ipAddress));

	if (session) {
		if (session->getUser() == user) {
			sessionDb->releaseLock();
			return JSONObject();
			//Do nothing,
		}
		else {
			sessionDb->deleteSession(session);
		}
	}

	arp->update(IP4Addr::stringToIP4Addr(ipAddress), macAddress);
	if(input.has(L"absoluteTimeout") && input.has(L"timeout")){
		sessionDb->create(user, macAddress, IP4Addr::stringToIP4Addr(ipAddress), input[L"timeout"]->AsNumber(), input[L"absoluteTimeout"]->AsBool());
	}else{
		sessionDb->create(user, macAddress, IP4Addr::stringToIP4Addr(ipAddress));
	}
	sessionDb->releaseLock();
	return JSONObject();
}

JSONObject AuthDelegate::login(JSONObject input) {
	string ipAddress = input[L"ipaddress"]->String();
	string username = input[L"username"]->String();
	string password;

	if (input.has(L"password")) {
		password = input[L"password"]->String();
	}

	JSONObject ret;
	string macAddress = arp->get(ipAddress);
	sessionDb->holdLock();

	if (sessionDb->get(macAddress, IP4Addr::stringToIP4Addr(ipAddress))) {
		ret.put(L"response", new JSONValue((double) - 5));
		ret.put(L"message", new JSONValue((string) "Session already exists for mac address"));
		sessionDb->releaseLock();
		return ret;
	}

	sessionDb->releaseLock();
	bool authenticated = authManager->authenticate(username, password);

	UserPtr user = userDb->getUser(username);
	if (authenticated && macAddress.size() > 0 && user) {
		sessionDb->holdLock();
		if(input.has(L"absoluteTimeout") && input.has(L"timeout")){
			sessionDb->create(user, macAddress, IP4Addr::stringToIP4Addr(ipAddress), input[L"timeout"]->AsNumber(), input[L"absoluteTimeout"]->AsBool());
		}else{
			sessionDb->create(user, macAddress, IP4Addr::stringToIP4Addr(ipAddress));
		}

		if (input.has(L"persist") && input[L"persist"]->AsBool()) {
			sessionDb->persistSession(macAddress, username);
		}

		sessionDb->releaseLock();

		ret.put(L"response", new JSONValue((double) 0));

		if (eventDb != NULL) {
			EventParams params;
			params["user"] = username;
			params["ip"] = ipAddress;
			params["hw"] = macAddress;
			eventDb->add(new Event(USERDB_LOGIN_SUCCESS, params));
		}
	}
	else {
		if (eventDb != NULL) {
			EventParams params;
			params["user"] = username;
			params["ip"] = ipAddress;
			params["hw"] = macAddress;
			eventDb->add(new Event(USERDB_LOGIN_FAILED, params));
		}

		if (!authenticated) {
			ret.put(L"response", new JSONValue((double) - 1));
			ret.put(L"message", new JSONValue((string) "Invalid Username or password"));
		}
		else if (macAddress.size() == 0) {
			ret.put(L"response", new JSONValue((double) - 1));
			ret.put(L"message", new JSONValue((string) "Could not find mac address for ip"));
		}else if(!user){
			ret.put(L"response", new JSONValue((double) - 1));
			ret.put(L"message", new JSONValue((string) "User could not be found"));
			Logger::instance()->log("api.auth", ERROR, "We could not resolve a user %s", username.c_str());
		}
	}

	return ret;
}

JSONObject AuthDelegate::logout(JSONObject input) {
	JSONObject ret;
	string ipAddress = input[L"ipaddress"]->String();

	string mac = arp->get(ipAddress);
	SessionPtr session = sessionDb->get(mac, IP4Addr::stringToIP4Addr(ipAddress));

	if (session) {
		ret.put(L"response", new JSONValue((double) 0));

		if (eventDb) {
			EventParams params;
			params["user"] = session->getUserName();
			params["ipaddress"] = ipAddress;
			eventDb->add(new Event(USERDB_LOGOUT, params));
		}

		sessionDb->holdLock();
		sessionDb->deleteSession(session);
		sessionDb->releaseLock();
	}
	else {
		ret.put(L"response", new JSONValue((double) - 1));
		ret.put(L"message", new JSONValue((string) "Could not find session"));
	}

	return ret;
}
