#ifndef EventDelegate_h
#define EventDelegate_h

#include "Api/Delegate.h"

class EventDelegate : public virtual Delegate {
	public:
		EventDelegate(EventDb *eventDb);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();

	private:
		EventDb *eventDb;

		_BEGIN_SPHIREWALL_DELEGATES(EventDelegate)
		DelegateHandlerImpl list;
		DelegateHandlerImpl size;
		DelegateHandlerImpl config_handlers_available;
		DelegateHandlerImpl config_type_available;
		DelegateHandlerImpl config_add;
		DelegateHandlerImpl config_del;
		DelegateHandlerImpl config_list;
		DelegateHandlerImpl config_handlers_config;
		DelegateHandlerImpl config_handlers_config_list;
		DelegateHandlerImpl purge;
		_END_SPHIREWALL_DELEGATES
};

#endif
