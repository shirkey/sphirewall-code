/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <gtest/gtest.h>
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Api/Delegates/AuthDelegate.h"
#include "Auth/User.h"
#include "Auth/Group.h"
#include "Auth/GroupDb.h"
#include "Core/ConfigurationManager.h"
#include "Core/HostDiscoveryService.h"
#include "Auth/AuthenticationHandler.h"
#include "test.h"

using namespace std;

TEST(AuthDelegate, base) {
	EXPECT_TRUE(true);
}

TEST(AuthDelegate, auth_groups_list) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();
	AuthDelegate *del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	GroupPtr g1 = db->createGroup("group1", 200);
	g1->setDesc("desc");
	g1->setManager("manager");
	g1->setAllowMui(true);

	GroupPtr g2 = db->createGroup("anothergroup");
	g2->setDesc("desc");
	g2->setManager("manager");
	g2->setAllowMui(true);

	JSONObject ret = del->process("auth/groups/list", JSONObject());
	JSONArray arr = ret[L"groups"]->AsArray();

	EXPECT_TRUE(arr.size() == 2);
	JSONObject group1Json = arr[0]->AsObject();

	EXPECT_STREQ("group1", group1Json[L"name"]->String().c_str());
	EXPECT_STREQ("desc", group1Json[L"desc"]->String().c_str());
	EXPECT_STREQ("manager", group1Json[L"manager"]->String().c_str());
	EXPECT_EQ(true, group1Json[L"allowMui"]->AsBool());
	EXPECT_EQ(200, group1Json[L"timeout"]->AsNumber());
}

TEST(AuthDelegate, auth_groups_create) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();

	AuthDelegate *del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	JSONObject g;
	g.put(L"timeout", new JSONValue((double) 200));
	g.put(L"name", new JSONValue((string) "group1"));
	JSONObject ret = del->process("auth/groups/create", g);

	EXPECT_TRUE(db->list().size() == 1);
	GroupPtr target = db->list()[0];
	EXPECT_TRUE(target != NULL);
	EXPECT_TRUE(target->getName().compare("group1") == 0);
	EXPECT_EQ(200, target->getTimeout());
}

TEST(AuthDelegate, auth_groups_create_bulk) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();

	AuthDelegate *del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	JSONObject ret = del->process("auth/groups/create/bulk",
								  JSON::Parse("{\"values\": [{\"name\": \"group1\", \"timeout\": 200.0}, {\"name\": \"group3\", \"timeout\": 200.0} ] }")->AsObject()
								 );

	EXPECT_TRUE(2 == db->list().size());
	GroupPtr target = db->list()[0];
	EXPECT_TRUE(target != NULL);
	EXPECT_STREQ("group1", target->getName().c_str());
	EXPECT_TRUE(200 == target->getTimeout());
}

TEST(AuthDelegate, auth_groups_get) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();

	AuthDelegate *del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	GroupPtr g1 = db->createGroup("group1");
	g1->setDesc("desc");
	g1->setManager("manager");
	g1->setAllowMui(true);
	g1->setTimeout(200);

	int id = db->getGroup("group1")->getId();
	JSONObject g;
	g.put(L"id", new JSONValue((double) id));
	JSONObject ret = del->process("auth/groups/get", g);

	EXPECT_STREQ("group1", ret[L"name"]->String().c_str());
	EXPECT_STREQ("desc", ret[L"desc"]->String().c_str());
	EXPECT_STREQ("manager", ret[L"manager"]->String().c_str());
	EXPECT_TRUE(true == ret[L"allowMui"]->AsBool());
	EXPECT_TRUE(200 == ret[L"timeout"]->AsNumber());
}

TEST(AuthDelegate, auth_groups_del) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();

	AuthDelegate *del = new AuthDelegate(db, NULL, NULL, NULL, NULL, NULL);

	db->createGroup("group1");
	EXPECT_EQ(1, db->list().size());

	int id = db->getGroup("group1")->getId();
	JSONObject g;
	g.put(L"id", new JSONValue((double) id));
	JSONObject ret = del->process("auth/groups/del", g);

	EXPECT_TRUE(db->list().size() == 0);
}

TEST(AuthDelegate, auth_users_list) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();

	UserDb *userDb = new UserDb(NULL, db, NULL);
	AuthDelegate *del = new AuthDelegate(db, userDb, NULL, NULL, NULL, NULL);

	userDb->createUser("user1");
	userDb->createUser("user2");

	JSONObject ret = del->process("auth/users/list", JSONObject());
	JSONArray users = ret[L"users"]->AsArray();
	EXPECT_TRUE(users.size() == 2);
}

TEST(AuthDelegate, auth_user_add) {
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();
	UserDb *userDb = new UserDb(NULL, db, NULL);
	AuthDelegate *del = new AuthDelegate(db, userDb, NULL, NULL, NULL, NULL);

	EXPECT_TRUE(userDb->list().size() == 0);

	JSONObject u;
	u.put(L"username", new JSONValue((string) "user1"));
	JSONObject ret = del->process("auth/users/add", u);

	EXPECT_TRUE(userDb->list().size() == 1);
}

TEST(AuthDelegate, auth_user_del) {
	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	MockFactory *tester = new MockFactory();
	GroupDb *db = tester->givenGroupDb();
	UserDb *userDb = new UserDb(NULL, db, NULL);
	userDb->setSessionDb(sessionDb);
	AuthDelegate *del = new AuthDelegate(db, userDb, NULL, NULL, NULL, NULL);

	userDb->createUser("user1");
	EXPECT_TRUE(userDb->list().size() == 1);

	JSONObject u;
	u.put(L"username", new JSONValue((string) "user1"));
	JSONObject ret = del->process("auth/users/del", u);

	EXPECT_TRUE(userDb->list().size() == 0);
}

TEST(AuthDelegate, auth_login) {
	MockFactory *tester = new MockFactory();

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthenticationManager *authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod *method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("1234");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == 0);

	if (ret[L"response"]->AsNumber() != 0) cerr << "Reason: " << ret[L"message"]->String() << endl;

	EXPECT_TRUE(sessionDb->get("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1")) != NULL);
}

TEST(AuthDelegate, auth_login_withoutpassword) {
	MockFactory *tester = new MockFactory();

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();
	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthenticationManager *authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod *method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);
	userDb->createUser("michael");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() < 0);
}

TEST(AuthDelegate, auth_login_invalid_user) {
	MockFactory *tester = new MockFactory();
	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();

	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthenticationManager *authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod *method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->get("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1")) == NULL);
}

TEST(AuthDelegate, auth_login_invalid_password) {
	MockFactory *tester = new MockFactory();

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();

	arp->update(IP4Addr::stringToIP4Addr("10.1.1.1"), "mac_address");

	AuthenticationManager *authManager = new AuthenticationManager(NULL);
	LocalDbAuthenticationMethod *method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);

	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("proper_password");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "10.1.1.1"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->get("mac_address", IP4Addr::stringToIP4Addr("10.1.1.1")) == NULL);
}

TEST(AuthDelegate, auth_login_invalid_macaddress) {
	MockFactory *tester = new MockFactory();
	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	GroupDb *groupDb = new GroupDb();
	UserDb *userDb = new UserDb(NULL, groupDb, sessionDb);
	HostDiscoveryService *arp = new HostDiscoveryService();

	AuthenticationManager *authManager = new AuthenticationManager(NULL);

	LocalDbAuthenticationMethod *method = new LocalDbAuthenticationMethod();
	method->setUserDb(userDb);
	authManager->addMethod(method);
	authManager->setLocalUserDb(userDb);
	AuthDelegate *del = new AuthDelegate(groupDb, userDb, NULL, sessionDb, arp, authManager);

	UserPtr user = userDb->createUser("michael");
	user->setPassword("1234");

	JSONObject args;
	args.put(L"username", new JSONValue((string) "michael"));
	args.put(L"password", new JSONValue((string) "1234"));
	args.put(L"ipaddress", new JSONValue((string) "1.1.1.2"));

	JSONObject ret = del->process("auth/login", args);
	EXPECT_TRUE(ret[L"response"]->AsNumber() == -1);
	EXPECT_TRUE(sessionDb->list().size() == 0);
}


