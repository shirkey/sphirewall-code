/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>

using namespace std;

#include "Api/Exceptions.h"
#include "Api/Delegates/IdsDelegate.h"
#include "Ids/Ids.h"
#include "Utils/IP4Addr.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/Event.h"

#define installHandler(x,y) delegateMap.insert(DelegatePath(x, &IdsDelegate::y))

IdsDelegate::IdsDelegate(IDS *ids)
	: ids(ids), eventDb(NULL) {
	installHandler("ids/exceptions/list", exceptions_list);
	installHandler("ids/exceptions/add", exceptions_add);
	installHandler("ids/exceptions/del", exceptions_del);
}

string IdsDelegate::rexpression() {
	return "ids/(.*)";
}

void IdsDelegate::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}


JSONObject IdsDelegate::process(string uri, JSONObject object) {
	try {
		return invokeHandler(uri, object);
	}
	catch (std::out_of_range e) {
		throw DelegateNotFoundException(uri);
	}
}

JSONObject IdsDelegate::exceptions_list(JSONObject object) {
	vector<AddressPair> l;
	ids->listExceptions(l);

	JSONObject ret;
	JSONArray pairs;

	for (AddressPair p : l) {
		JSONObject o;
		o.put(L"ip", new JSONValue((string) IP4Addr::ip4AddrToString(p.ip)));
		o.put(L"mask", new JSONValue((string) IP4Addr::ip4AddrToString(p.mask)));

		pairs.push_back(new JSONValue(o));
	}

	ret.put(L"exceptions", new JSONValue(pairs));
	return ret;
}

JSONObject IdsDelegate::exceptions_add(JSONObject object) {
	AddressPair pair;
	pair.ip = IP4Addr::stringToIP4Addr(object[L"ip"]->String());
	pair.mask = IP4Addr::stringToIP4Addr(object[L"mask"]->String());

	ids->addException(pair);

	if (eventDb) {
		EventParams params;
		params["ip"] = object[L"ip"]->String();
		params["mask"] = object[L"mask"]->String();
		eventDb->add(new Event(AUDIT_CONFIGURATION_IDS_EXCEPTION_ADDED, params));
	}

	return JSONObject();
}

JSONObject IdsDelegate::exceptions_del(JSONObject object) {
	AddressPair pair;
	pair.ip = IP4Addr::stringToIP4Addr(object[L"ip"]->String());
	pair.mask = IP4Addr::stringToIP4Addr(object[L"mask"]->String());

	ids->delException(pair);

	if (eventDb) {
		EventParams params;
		params["ip"] = object[L"ip"]->String();
		params["mask"] = object[L"mask"]->String();
		eventDb->add(new Event(AUDIT_CONFIGURATION_IDS_EXCEPTION_REMOVED, params));
	}

	return JSONObject();
}
