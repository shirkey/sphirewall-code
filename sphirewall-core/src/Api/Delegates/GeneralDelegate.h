#ifndef GeneralDelegate_H
#define GeneralDelegate_H

#include "Api/Delegate.h"

class GeneralDelegate : public virtual Delegate {
	public:

		GeneralDelegate(Config *config, SysMonitor *sysMonitor, LoggingConfiguration *loggingConfiguration, AuthenticationManager *authenticationManager);
		GeneralDelegate(Config *config);

		JSONObject process(std::string uri, JSONObject object);

		std::string rexpression();

		void setEventDb(EventDb *eventDb);
	private:
		Config *config;
		SysMonitor *sysMonitor;
		LoggingConfiguration *loggingConfiguration;
		EventDb *eventDb;
		AuthenticationManager *authenticationManager;

		void installHandlers();

		_BEGIN_SPHIREWALL_DELEGATES(GeneralDelegate)
		DelegateHandlerImpl config_dump;
		DelegateHandlerImpl config_get_version;
		DelegateHandlerImpl config_load;
		DelegateHandlerImpl config_get;
		DelegateHandlerImpl config_set;
		DelegateHandlerImpl config_set_bulk;
		DelegateHandlerImpl runtime_list;
		DelegateHandlerImpl runtime_get;
		DelegateHandlerImpl runtime_set;
		DelegateHandlerImpl metrics;
		DelegateHandlerImpl version;
		DelegateHandlerImpl logging_list;
		DelegateHandlerImpl logging_set;
		DelegateHandlerImpl logging_remove;
		DelegateHandlerImpl logging_critical;

		DelegateHandlerImpl ldap_testconnection;
		DelegateHandlerImpl smtp_testconnection;
		DelegateHandlerImpl smtp_publish;
		DelegateHandlerImpl cloud_connected;
		DelegateHandlerImpl rpc;
		DelegateHandlerImpl quotas;
		DelegateHandlerImpl quotas_set;
		DelegateHandlerImpl logs;
		DelegateHandlerImpl info;
		DelegateHandlerImpl upgrade;
		
		_END_SPHIREWALL_DELEGATES
};

#endif
