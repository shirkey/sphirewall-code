#ifndef AuthDelegate_h
#define AuthDelegate_h

#include "Api/Delegate.h"

class AuthDelegate : public virtual Delegate {
	public:
		AuthDelegate(GroupDb *groupDb, UserDb *userDb, SFwallCore::Firewall *firewall, SessionDb *sessionDb, HostDiscoveryService *arp, AuthenticationManager *authManager);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();
		void setEventDb(EventDb *eventDb);

	private:
		GroupDb *groupDb;
		UserDb *userDb;
		SFwallCore::Firewall *firewall;
		SessionDb *sessionDb;
		HostDiscoveryService *arp;
		AuthenticationManager *authManager;
		EventDb *eventDb;

		_BEGIN_SPHIREWALL_DELEGATES(AuthDelegate)
		DelegateHandlerImpl groups_list;
		DelegateHandlerImpl groups_create;
		DelegateHandlerImpl groups_create_bulk;
		DelegateHandlerImpl groups_get;
		DelegateHandlerImpl groups_del;
		DelegateHandlerImpl groups_save;
		DelegateHandlerImpl groups_mergeoveruser;
		DelegateHandlerImpl users_list;
		DelegateHandlerImpl users_get;
		DelegateHandlerImpl users_add;
		DelegateHandlerImpl users_del;
		DelegateHandlerImpl users_save;
		DelegateHandlerImpl users_groups_add;
		DelegateHandlerImpl users_groups_del;
		DelegateHandlerImpl users_enable;
		DelegateHandlerImpl users_disable;
		DelegateHandlerImpl user_merge;
		DelegateHandlerImpl user_setpassword;
		DelegateHandlerImpl sessions_create;
		DelegateHandlerImpl sessions_list;
		DelegateHandlerImpl sessions_persist;
		DelegateHandlerImpl login;
		DelegateHandlerImpl logout;
		DelegateHandlerImpl defaultpasswordset;
		DelegateHandlerImpl ldap;
		DelegateHandlerImpl ldap_sync;
		DelegateHandlerImpl wmic;
		DelegateHandlerImpl sessions_persist_remove;

		DelegateHandlerImpl sessions_networktimeouts;
		DelegateHandlerImpl sessions_networktimeouts_remove;
		DelegateHandlerImpl sessions_networktimeouts_manage;

		_END_SPHIREWALL_DELEGATES
};

#endif
