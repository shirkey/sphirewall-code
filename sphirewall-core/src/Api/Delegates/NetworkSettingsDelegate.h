#ifndef NetworkSettingsDelegate_H
#define NetworkSettingsDelegate_H

#include "Api/Delegate.h"
#include "Utils/Interfaces.h"
#include "Core/IPSec.h"
#include "Core/Vpn.h"
#include "Utils/Dnsmasq.h"

class NetworkSettingsDelegate : public virtual Delegate {
	public:
		NetworkSettingsDelegate(IntMgr *interfaceManager, DNSConfig *dnsConfig, HostDiscoveryService *arpTable, Dhcp3ConfigurationManager *dhcp, DnsmasqManager *);
		JSONObject process(std::string uri, JSONObject object);
		std::string rexpression();
		void setConnectionManager(ConnectionManager *connectionManager) ;
		void setEventDb(EventDb *eventDb);
		void setOpenvpnManager(OpenvpnManager *o);
		void setVpnManager(VpnManager *o){
			this->vpnManager = o;
		}
		void setIPSecManager(IPSecManager* o){
			this->ipsecManager = o;
		}
	private:
		IntMgr* interfaceManager;
		DNSConfig *dnsConfig;
		HostDiscoveryService *arp;
		Dhcp3ConfigurationManager *dhcpConfiguration;
		ConnectionManager *connectionManager;
		EventDb *eventDb;
		OpenvpnManager *openvpn;
		IPSecManager *ipsecManager;
		VpnManager *vpnManager;
		DnsmasqManager *dnsmasq;

		_BEGIN_SPHIREWALL_DELEGATES(NetworkSettingsDelegate)
		DelegateHandlerImpl devices_publish;
		DelegateHandlerImpl devices_list;
                DelegateHandlerImpl devices_up;
                DelegateHandlerImpl devices_save;
                DelegateHandlerImpl devices_down;

		DelegateHandlerImpl devices_set;
		DelegateHandlerImpl devices_dynamicdns_set;
		DelegateHandlerImpl devices_synced;
		DelegateHandlerImpl dns_get;
		DelegateHandlerImpl dns_set;
		DelegateHandlerImpl routes_list;
		DelegateHandlerImpl routes_add;
		DelegateHandlerImpl routes_del;
		DelegateHandlerImpl arp_list;
		DelegateHandlerImpl arp_size;
		DelegateHandlerImpl dnsmasq_publish;
		DelegateHandlerImpl dnsmasq_start;
		DelegateHandlerImpl dnsmasq_stop;
		DelegateHandlerImpl dnsmasq_running;
		DelegateHandlerImpl devices_delete;
		DelegateHandlerImpl devices_toggle;
		DelegateHandlerImpl devices_toggledhcp;
		DelegateHandlerImpl devices_alias_list;
		DelegateHandlerImpl devices_alias_delete;
		DelegateHandlerImpl devices_alias_add;
		DelegateHandlerImpl connections_list;
		DelegateHandlerImpl connectons_add;
		DelegateHandlerImpl connections_del;
		DelegateHandlerImpl connections_save;
		DelegateHandlerImpl connections_connect;
		DelegateHandlerImpl connections_disconnect;

		DelegateHandlerImpl vpn_start;
		DelegateHandlerImpl vpn_stop;
		DelegateHandlerImpl vpn_publish;
		DelegateHandlerImpl vpn_status;
		DelegateHandlerImpl vpn_options;
                DelegateHandlerImpl vpn_instances_list;
                DelegateHandlerImpl vpn_instance_create;
                DelegateHandlerImpl vpn_instance_remove;

		DelegateHandlerImpl openvpn_log;
		DelegateHandlerImpl openvpn_install;
		DelegateHandlerImpl openvpn_uninstall;
		DelegateHandlerImpl openvpn_checkinstall;
		DelegateHandlerImpl openvpn_daemonexists;
		DelegateHandlerImpl openvpn_client_static;
		DelegateHandlerImpl openvpn_clients_list;
		DelegateHandlerImpl openvpn_clients_add;
		DelegateHandlerImpl openvpn_clients_delete;

		DelegateHandlerImpl wireless_start;
		DelegateHandlerImpl wireless_stop;
		DelegateHandlerImpl wireless_online;
		DelegateHandlerImpl dyndns;
		DelegateHandlerImpl dyndns_set;

		DelegateHandlerImpl devices_leases_add;
		DelegateHandlerImpl devices_leases_del;
		DelegateHandlerImpl ping;
		DelegateHandlerImpl set_quarantined;

		_END_SPHIREWALL_DELEGATES
};

#endif
