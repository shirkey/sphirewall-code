/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "Api/JsonManagementService.h"
#include "Api/ManagementSocketServer.h"
#include "Core/System.h"
#include "Core/ConfigurationManager.h"

using boost::asio::ip::tcp;
using namespace std;

ManagementSocketServer::SSLSession::SSLSession(boost::asio::io_service &io_service, boost::asio::ssl::context &context)
	: sslSocket(io_service, context) {
}

SSL_SOCKET::lowest_layer_type &ManagementSocketServer::SSLSession::socket() {
	return sslSocket.lowest_layer();
}

void ManagementSocketServer::SSLSession::start() {
	sslSocket.async_handshake(boost::asio::ssl::stream_base::server,
							  boost::bind(&SSLSession::handle_handshake, this,
										  boost::asio::placeholders::error));
}

void ManagementSocketServer::SSLSession::handle_handshake(
	const boost::system::error_code &error) {
	if (!error) {
		sslSocket.async_read_some(boost::asio::buffer(data_,
								  MAX_SOCKET_DATA_LENGTH),
								  boost::bind(&SSLSession::handle_read, this,
											  boost::asio::placeholders::error,
											  boost::asio::placeholders::bytes_transferred));
	}
	else {
		delete this;
	}
}

void ManagementSocketServer::SSLSession::handle_read(const boost::system::error_code &error, size_t bytes_transferred) {
	if (!error) {
		data_[bytes_transferred] = 0; // Make sure input data is terminated
		strncpy(data_, (mss->jsonManagementService->process(data_, "").first + "\n").c_str(), MAX_SOCKET_DATA_LENGTH - 3);
		boost::asio::async_write(sslSocket,
								 boost::asio::buffer(data_, strlen(data_)),
								 boost::bind(&SSLSession::handle_write, this,
											 boost::asio::placeholders::error));
	}
	else {
		delete this;
	}
}

void ManagementSocketServer::SSLSession::handle_write(
	const boost::system::error_code &error) {
	if (!error) {
		sslSocket.async_read_some(boost::asio::buffer(data_,
								  MAX_SOCKET_DATA_LENGTH),
								  boost::bind(&SSLSession::handle_read, this,
											  boost::asio::placeholders::error,
											  boost::asio::placeholders::bytes_transferred));
	}
	else {
		delete this;
	}
}

std::string ManagementSocketServer::SSLServer::getPassword() {
	JSONObject root;
	string     password;


	password = System::getInstance()->configurationManager.get("general:ssl_password")->string();
	return password;
}

void ManagementSocketServer::SSLServer::handle_accept(SSLSession *newSession,
		const boost::system::error_code &errCode) {
	if (!errCode) {
		newSession->start();
		newSession = new SSLSession(io_service, context);
		newSession->setManagementSocketServer(mss);
		acceptor.async_accept(newSession->socket(),
							  boost::bind(&SSLServer::handle_accept, this, newSession,
										  boost::asio::placeholders::error));
	}
	else {
		delete newSession;
	}
}

ManagementSocketServer::SSLServer::SSLServer(
	boost::asio::io_service &io_service, unsigned short port,ManagementSocketServer *mss)
	: io_service(io_service), acceptor(io_service,
									   boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
	context(io_service, boost::asio::ssl::context::sslv23) {
	try {
		const string sslDir = "./ssl/";


		this->mss = mss; // Save container instance.
		context.set_options(boost::asio::ssl::context::default_workarounds |
							boost::asio::ssl::context::no_sslv2 |
							boost::asio::ssl::context::single_dh_use);

		context.set_password_callback(boost::bind(
										  &ManagementSocketServer::SSLServer::getPassword, this));
		context.use_certificate_chain_file(sslDir + "servercert.pem");
		context.use_private_key_file(sslDir + "serverkey.pem",
									 boost::asio::ssl::context::pem);
		context.use_tmp_dh_file(sslDir + "dh1024.pem");
		ManagementSocketServer::SSLSession *session =
			new ManagementSocketServer::SSLSession(io_service, context);
		session->setManagementSocketServer(mss);
		acceptor.async_accept(session->socket(),
							  boost::bind(&SSLServer::handle_accept, this, session,
										  boost::asio::placeholders::error));
	}
	catch (boost::system::system_error except) {
		std::string msg = "SSL Exception: ";

		msg.append(except.what());
		Logger::instance()->log("sphirewalld.api.ssl", ERROR, msg);
	}
}

void ManagementSocketServer::runSSL() {
	Logger::instance()->log("sphirewalld.api.ssl", INFO, "API::ManagementSocketServer::starting SSL on port: " + intToString(port));

	try {
		boost::asio::io_service io_service;
		sslServer = new SSLServer(io_service, ManagementSocketServer::port, this);
		io_service.run(); // Start the SSL session.
	}
	catch (std::exception &except) {
		std::cerr << "Exception: " << except.what() << "\n";
	}

	return;
}

void ManagementSocketServer::run() {

	if (ssl == true) {
		runSSL();
		return;
	}

	boost::asio::io_service io_service;
	tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port));

	Logger::instance()->log("sphirewalld.api.normal", INFO, "API::ManagementSocketServer::starting non-SSL  on port: " + intToString(port));

	for (;;) {
		socket_ptr sock(new tcp::socket(io_service));
		a.accept(*sock);

		Logger::instance()->log("sphirewalld.api.normal", DEBUG, "accepted new connection from the management service");
		boost::thread(boost::bind(&ManagementSocketServer::handleConnection, this, sock));
		Logger::instance()->log("sphirewalld.api.normal", DEBUG, "connection closed from the management service");
	}
}

void ManagementSocketServer::handleConnection(socket_ptr sock) {
	try {
		boost::asio::streambuf response;
		boost::asio::read_until(*sock, response, "\n");
		string buffer = string((istreambuf_iterator<char>(&response)),
							   istreambuf_iterator<char>());

		string ipaddress = (*sock).remote_endpoint().address().to_string();

		if (config) {
			if (config->hasByPath("general:disable_remote_management") && config->get("general:disable_remote_management")->boolean()) {
				if (ipaddress.compare("127.0.0.1") != 0) {
					Logger::instance()->log("sphirewalld.api.normal.handle", INFO, "denied management api request from " + ipaddress);
					return;
				}
			}
		}

		Logger::instance()->log("sphirewalld.api.normal.handle", DEBUG, "management api request from client: " + buffer);
		std::pair<string, int> res = jsonManagementService->process(buffer, ipaddress);
		Logger::instance()->log("sphirewalld.api.normal.handle", DEBUG, "management api response to client:" + res.first);

		boost::asio::streambuf request(MAX_SOCKET_DATA_LENGTH);
		std::ostream request_stream(&request);

		request_stream << res.first << "\n";
		boost::asio::write(*sock, request);

		(*sock).close();

	}
	catch (boost::exception_detail::clone_impl<boost::exception_detail::error_info_injector<boost::system::system_error> > e) {
		char message[256];
		sprintf(message,
				"management connection was terminated unexpectantly, this can be caused by an invalid terminating charater. Error: %s",
				e.what());
		Logger::instance()->log("sphirewalld.api.normal", ERROR, message);

	}
}

