#ifndef Exceptions_H
#define Exceptions_H
#include <exception>

namespace std {
	typedef basic_string<char> string;
};

class JsonManagementServiceException : public std::exception {
	public:
		JsonManagementServiceException(std::string message,  int statuscode = 500, int internalCode = -1) noexcept;
		virtual ~JsonManagementServiceException() noexcept;
		virtual const char *what() const noexcept;
		std::string message() const;
		int statusCode() const;
		int internalCode() const;
	protected:
		std::string w;
		int statuscode;
		int internalcode;
};

class ParsingException : public JsonManagementServiceException {
	public:
		ParsingException(std::string message = "A field is missing or of the wrong type", int statusCode = 500, int internalCode = -1) noexcept;
};

class ServiceNotAvailableException : public JsonManagementServiceException {
	public:
		ServiceNotAvailableException(std::string message = "This service is currently unavailable.",  int statuscode = 503, int internalCode = -3) noexcept;
};

class DelegateGeneralException : public JsonManagementServiceException {
	public:
		DelegateGeneralException(std::string message, int statuscode = 500, int internalCode = -1) noexcept;
};

class DelegateNotFoundException : public JsonManagementServiceException {
	public:
		DelegateNotFoundException(std::string message = "Could not find the requested delegate.", int statuscode = 404,  int internalCode = -1) noexcept;
};

class AuthenticationException: public JsonManagementServiceException {
	public:
		AuthenticationException(std::string message = "Authentication Failed at this point in time", int statuscode = 403, int internalCode = -2) noexcept;
};

#endif
