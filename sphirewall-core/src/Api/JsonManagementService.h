/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSON_MANAGEMENT_SERVICE
#define JSON_MANAGEMENT_SERVICE

#include "Auth/UserDb.h"

class AnalyticsClient;
class IntMgr;
class Config;
class SysMonitor;
class EventDb;
class IDS;
class DNSConfig;
class GroupDb;
class SessionDb;
class LoggingConfiguration;
class Delegate;
class HostDiscoveryService;
class AuthManager;
class Dhcp3ConfigurationManager;
class ConnectionManager;
class OpenvpnManager;
class AuthenticationManager;
class JSONValue;
class IPSecManager;
class VpnManager;

namespace std {
	template<class _Key, class _Compare, class _Alloc> class set;
	template<typename _Tp, typename _Alloc > class list;
	typedef basic_string<char> string;
};

namespace SFwallCore {
	class ApplicationFilter;
	class Firewall;
};

class ApiSession {
	public:
		ApiSession(UserPtr user, std::string uuid);

		UserPtr user;
		long int lastSeenTime;
		std::string uuid;
};

class JsonManagementService : public DeleteUserListener {
	public:
		JsonManagementService();
		void initDelegate();
		std::pair<std::string, int> process(std::string input, std::string sender);
		void setConfig(Config *config);
		void setSysMonitor(SysMonitor *sysMonitor);
		void setEventDb(EventDb *eventDb);
		void setIds(IDS *ids);
		void setInterfaceManager(IntMgr *i);
		void setDnsConfig(DNSConfig *dnsConfig);
		void setFirewall(SFwallCore::Firewall *firewall);
		void setGroupDb(GroupDb *groupDb);
		void setUserDb(UserDb *userDb);
		void setSessionDb(SessionDb *sessionDb);
		void setHostDiscoveryService(HostDiscoveryService *arpTable);
		void setLoggingConfiguration(LoggingConfiguration *loggingConfiguration);
		void setDhcp(Dhcp3ConfigurationManager *dhcpConfigurationManager);
		void setOpenvpn(OpenvpnManager *openvpn);
		void setVpnManager(VpnManager *vpnManager){
			this->vpnManager = vpnManager;
		}
		void setConnectionManager(ConnectionManager *connectionManager);
		void setAuthenticationManager(AuthenticationManager *authenticationManager);
		void setIPSecManager(IPSecManager* ipsecManager){
			this->ipsecManager = ipsecManager;
		}
		void handleDeleteUser(UserPtr user);
		void registerDelegate(Delegate *d);
		void ignoreAuthentication(bool value);
	private:
		std::list<Delegate *> registeredDelegates;
		std::list<ApiSession *> sessions;

		Config *config;
		SysMonitor *sysMonitor;
		EventDb *eventDb;
		IDS *ids;
		IntMgr* interfaceManager;
		DNSConfig *dnsConfig;
		SFwallCore::Firewall *firewall;
		GroupDb *groupDb;
		UserDb *userDb;
		SessionDb *sessionDb;
		HostDiscoveryService *arpTable;
		LoggingConfiguration *loggingConfiguration;
		Dhcp3ConfigurationManager *dhcpConfigurationManager;
		OpenvpnManager *openvpn;
		bool ignoreAuth;
		ConnectionManager *connectionManager;
		AuthenticationManager *authenticationManager;
		IPSecManager* ipsecManager;
		VpnManager* vpnManager;

		ApiSession *getSession(std::string token);
		bool isValidSession(std::string token);
		std::string createSession(UserPtr user);
		std::set<std::string> requestsWithoutAuth;

		//Internal request handlers:
		std::string handleAuthentication(JSONObject &obj);
		std::string handleRequest(std::string, JSONObject &obj);
		Lock *lock;

		bool noSessionAuth(std::string requesturi, std::string token);
		bool noAuthRequired(std::string requestUrl);
		std::string generateError(JSONObject object, std::string message, int code);
		std::string generateError(std::string message, int code);
};

#endif
