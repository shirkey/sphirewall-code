#include "Api/Delegate.h"
#include <boost/regex.hpp>

bool Delegate::matches(std::string uri) {
	boost::regex r(rexpression());
	boost::smatch what;
	return boost::regex_match(uri, what, r, boost::match_extra);
}
