/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/regex.hpp>
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <string>
#include <ctime>

#include "Api/JsonManagementService.h"
#include "Api/Delegates/AuthDelegate.h"
#include "Api/Delegates/EventDelegate.h"
#include "Api/Delegates/GeneralDelegate.h"
#include "Api/Delegates/FirewallDelegate.h"
#include "Api/Delegates/IdsDelegate.h"
#include "Api/Delegates/NetworkSettingsDelegate.h"
#include "Api/Exceptions.h"
#include "Json/JSONValue.h"
#include "Json/JSON.h"
#include "Core/Config.h"
#include "Core/SysMonitor.h"
#include "Core/Logger.h"
#include "Utils/StringUtils.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Auth/User.h"
#include "Auth/AuthenticationHandler.h"
#include "Core/Vpn.h"

using namespace std;

ApiSession::ApiSession(UserPtr user, string uuid)
	: user(user), uuid(uuid) {
	lastSeenTime = time(NULL);
}

JsonManagementService::JsonManagementService() {
	ignoreAuth = false;
	lock = new Lock();
	config = NULL;
}

void JsonManagementService::initDelegate() {
	GeneralDelegate *generalDelegate = new GeneralDelegate(config, sysMonitor, loggingConfiguration, authenticationManager);
	generalDelegate->setEventDb(eventDb);
	registeredDelegates.push_back(generalDelegate);
	registeredDelegates.push_back(new EventDelegate(eventDb));

	IdsDelegate *idsDel = new IdsDelegate(ids);
	idsDel->setEventDb(eventDb);
	registeredDelegates.push_back(idsDel);

	NetworkSettingsDelegate *net = new NetworkSettingsDelegate(interfaceManager, dnsConfig, arpTable, dhcpConfigurationManager, System::getInstance()->getDnsmasq());
	net->setConnectionManager(connectionManager);
	net->setEventDb(eventDb);
	net->setOpenvpnManager(openvpn);
	net->setIPSecManager(ipsecManager);
	net->setVpnManager(vpnManager);
	registeredDelegates.push_back(net);

	FirewallDelegate *fwall = new FirewallDelegate(firewall, interfaceManager, groupDb);
	fwall->setEventDb(eventDb);
	registeredDelegates.push_back(fwall);

	AuthDelegate *authDelegate = new AuthDelegate(groupDb, userDb, firewall, sessionDb, arpTable, authenticationManager);
	authDelegate->setEventDb(eventDb);
	registeredDelegates.push_back(authDelegate);

	//Register safe urls
	requestsWithoutAuth.insert("auth/login");
	requestsWithoutAuth.insert("auth/logout");
	requestsWithoutAuth.insert("general/version");
	requestsWithoutAuth.insert("auth/users/list");
	requestsWithoutAuth.insert("general/cloud/connected");
	requestsWithoutAuth.insert("firewall/rewrite/remove");
}

std::string JsonManagementService::handleAuthentication(JSONObject &root) {
	try {
		if (authenticationManager->authenticate(root[L"username"]->String(), root[L"password"]->String())) {
			UserPtr user = userDb->getUser(root[L"username"]->String());

			if (user && user->isAllowedMuiAccess()) {
				EventParams params;
				params["user"] = root[L"username"]->String();
				eventDb->add(new Event(USERDB_LOGIN_SUCCESS, params));

				string token = createSession(user);
				return "{\"code\":0,\"message\":\"\",\"token\":\"" + token + "\"}";
			}
			else {
				EventParams params;
				params["user"] = root[L"username"]->String();
				eventDb->add(new Event(USERDB_LOGIN_FAILED, params));

				throw AuthenticationException("user has not been granted access to the management api");
			}
		}
		else {
			EventParams params;
			params["user"] = root[L"username"]->String();
			eventDb->add(new Event(USERDB_LOGIN_FAILED, params));
			throw AuthenticationException("invalid username or password");
		}
	}
	catch (const JSONFieldNotFoundException &e) {
		throw ParsingException(e.what());
	}
}

bool JsonManagementService::noAuthRequired(std::string requestUri) {
	return requestsWithoutAuth.find(requestUri) != requestsWithoutAuth.end();
}

std::string JsonManagementService::handleRequest(std::string requestUri, JSONObject &root) {
	try {
		std::string token;

		if (root.has(L"token")) {
			token = root[L"token"]->String();
		}

		lock->lock();
		ApiSession *session = getSession(token);
		lock->unlock();

		if (session || noAuthRequired(requestUri) || noSessionAuth(requestUri, token) || ignoreAuth) {
			list<Delegate *>::iterator iter;

			for (iter = registeredDelegates.begin();
					iter != registeredDelegates.end();
					iter++) {



				Delegate *delegate = (*iter);

				if (delegate->matches(requestUri)) {
					JSONObject responseRoot;
					responseRoot.put(L"code", new JSONValue((double) 0));

					if (root.has(L"seq")) {
						responseRoot.put(L"ack", new JSONValue((double) root[L"seq"]->AsNumber()));
					}

					JSONObject args = root[L"args"]->AsObject();
					JSONObject data;
			
					try {
						data = delegate->process(requestUri, args);
					}catch(VpnException e){
						throw DelegateGeneralException(e.what());
					}
	
					responseRoot.put(L"response", new JSONValue(data));
					JSONValue *responseValue = new JSONValue(responseRoot);

					std::string sMsg = WStringToString(responseValue->Stringify());
					delete responseValue;
					return sMsg;
				}
			}

			throw DelegateNotFoundException();
		}
		else {
			throw AuthenticationException();
		}
	}
	catch (const JSONFieldNotFoundException &e) {
		throw ParsingException(e.what());
	}
}

std::string JsonManagementService::generateError(JSONObject object, std::string message, int code) {
	stringstream ss;
	ss << std::setprecision(9);

	ss << "{\"code\":" << code << ",\"message\":\"" << message << "\"";

	if (object.has(L"seq")) {
		ss << ",\"ack\":" << object[L"seq"]->AsNumber();
	}

	ss << "}\n";
	return ss.str();
}

std::string JsonManagementService::generateError(std::string message, int code) {
	return ("{\"code\":" + intToString(code) + ",\"message\":\"" + message + "\"}\n");
}

std::pair<std::string,  int> JsonManagementService::process(std::string input, std::string sender = "unknown") {
	std::unique_ptr<JSONValue> value(JSON::Parse(input.c_str()));
	std::string requestUri;
	std::pair<std::string, int> response;

	Logger::instance()->log("sphirewalld.api.json", DEBUG, "data is '%s'", input.c_str());

	try {
		if (!value) {
			throw ParsingException(input);
		}

		JSONObject root = value->AsObject();
		requestUri = WStringToString(root[L"request"]->AsString());

		if (requestUri.compare("auth") == 0) {
			lock->lock();
			response = std::pair<std::string, int>(handleAuthentication(root) + "\n", 200);
			lock->unlock();
		}
		else {
			response = std::pair<std::string, int>(handleRequest(requestUri, root) + "\n", 200);
		}

		Logger::instance()->log("sphirewalld.api.json", INFO, "Processed management request '%s' from '%s'", requestUri.c_str(), sender.c_str());
	}
	catch (const JsonManagementServiceException &e) {
		lock->unlock();
		Logger::instance()->log(
			"sphirewalld.api.json",
			ERROR,
			"Management Request Exception: '%s' -> '%s' - '%d'\n'%s'",
			requestUri.c_str(),
			sender.c_str(),
			e.statusCode(),
			e.what()
		);

		if (!value) {
			response = std::pair<std::string, int>(generateError(e.message(), e.internalCode()), e.statusCode());
		}
		else {
			response = std::pair<std::string, int>(generateError(value->AsObject(), e.message(), e.internalCode()), e.statusCode());
		}
	}

	return response;
}

void JsonManagementService::handleDeleteUser(UserPtr user) {
	list<ApiSession *>::iterator iter;

	for (iter = sessions.begin(); iter != sessions.end(); iter++) {
		ApiSession *target = (*iter);

		if (target->user == user) {
			delete target;
			iter = sessions.erase(iter);
		}
	}

}

ApiSession *JsonManagementService::getSession(std::string token) {
	for (list<ApiSession *>::iterator iter = sessions.begin();
			iter != sessions.end();
			iter++) {

		//First check the session is valid:
		ApiSession *target = (*iter);
		
		int timeout = System::getInstance()->configurationManager.has("authentication:session_timeout") ? System::getInstance()->configurationManager.get("authentication:session_timeout")->number() : 600;
		if ((time(NULL) - target->lastSeenTime) > 600) {
			EventParams params;
			params["user"] = target->user->getUserName();
			eventDb->add(new Event(USERDB_SESSION_TIMEOUT, params));
			delete target;
			iter = sessions.erase(iter);
			continue;
		}
		else {
			if (target->uuid.compare(token) == 0) {
				target->lastSeenTime = time(NULL);
				return target;
			}
		}
	}

	return NULL;
}

std::string JsonManagementService::createSession(UserPtr user) {
	string uuid = StringUtils::genRandom();
	ApiSession *session = new ApiSession(user, uuid);
	sessions.push_back(session);
	return uuid;
}

bool JsonManagementService::noSessionAuth(string requesturi, string token) {
	if (!config || !config->getConfigurationManager()) {
		return false;
	}

	if (config->getConfigurationManager()->has("noSessionAuth")) {
		ObjectContainer *tokens = config->getConfigurationManager()->getElement("noSessionAuth")->get("tokens")->container();

		for (int x = 0; x < tokens->size(); x++) {
			ObjectContainer *target = tokens->get(x)->container();

			if (target->get("token")->string().compare(token) == 0) {
				ObjectContainer *allowedUris = target->get("allowedUris")->container();

				for (int y = 0; y < allowedUris->size(); y++) {
					if (allowedUris->get(y)->string().compare(requesturi) == 0) {
						return true;
					}
				}
			}
		}
	}

	return false;
}

void JsonManagementService::setAuthenticationManager(AuthenticationManager *authenticationManager) {
	this->authenticationManager = authenticationManager;
}

void JsonManagementService::setConfig(Config *config) {
	this->config = config;
}

void JsonManagementService::setConnectionManager(ConnectionManager *connectionManager) {
	this->connectionManager = connectionManager;
}

void JsonManagementService::setDhcp(Dhcp3ConfigurationManager *dhcpConfigurationManager) {
	this->dhcpConfigurationManager = dhcpConfigurationManager;
}

void JsonManagementService::setDnsConfig(DNSConfig *dnsConfig) {
	this->dnsConfig = dnsConfig;
}

void JsonManagementService::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

void JsonManagementService::setFirewall(SFwallCore::Firewall *firewall) {
	this->firewall = firewall;
}

void JsonManagementService::setGroupDb(GroupDb *groupDb) {
	this->groupDb = groupDb;
}

void JsonManagementService::setHostDiscoveryService(HostDiscoveryService *arpTable) {
	this->arpTable = arpTable;
}

void JsonManagementService::setIds(IDS *ids) {
	this->ids = ids;
}

void JsonManagementService::setInterfaceManager(IntMgr* i) {
	this->interfaceManager = i;
}

void JsonManagementService::setLoggingConfiguration(LoggingConfiguration *loggingConfiguration) {
	this->loggingConfiguration = loggingConfiguration;
}

void JsonManagementService::setOpenvpn(OpenvpnManager *openvpn) {
	this->openvpn = openvpn;
}

void JsonManagementService::setSessionDb(SessionDb *sessionDb) {
	this->sessionDb = sessionDb;
}

void JsonManagementService::setSysMonitor(SysMonitor *sysMonitor) {
	this->sysMonitor = sysMonitor;
}

bool JsonManagementService::isValidSession(string token) {
	if (this->getSession(token) != NULL) {
		return true;
	}

	return false;
}

void JsonManagementService::registerDelegate(Delegate *d) {
	registeredDelegates.push_back(d);
}

void JsonManagementService::ignoreAuthentication(bool value) {
	this->ignoreAuth = value;
}

void JsonManagementService::setUserDb(UserDb *userDb) {
	this->userDb = userDb;
	this->userDb->registerDeleteListener(this);
}
