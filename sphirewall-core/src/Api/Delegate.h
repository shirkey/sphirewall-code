#ifndef Delegate_h
#define Delegate_h
#include <string>
#include <boost/unordered/unordered_map.hpp>

#define useDelegateHandlerImpl typedef JSONObject (DelegateHandlerImpl)(JSONObject)
#define useDelegatehandler(class) typedef JSONObject (class::*DelegateHandler)(JSONObject)
#define useDelegateMap(class) typedef boost::unordered_map<std::string, JSONObject (class::*)(JSONObject)> DelegateMap
#define useDelegatePath typedef std::pair<std::string, DelegateHandler> DelegatePath
#define invokeHandler(uri, argument) (this->*(delegateMap.at(uri)))(argument)

#define _BEGIN_SPHIREWALL_DELEGATES(class) useDelegateHandlerImpl;\
	useDelegatehandler(class);\
	useDelegateMap(class);\
	useDelegatePath;\
	DelegateMap delegateMap;
#define _END_SPHIREWALL_DELEGATES

class IDS;
class EventDb;
class Event;
class JSONObject;
class GroupDb;
class UserDb;
class SessionDb;
class HostDiscoveryService;
class AuthenticationManager;
class AnalyticsClient;
class InterfaceManager;
class Config;
class SysMonitor;
class LoggingConfiguration;
class AuthenticationManager;
class Openvpn;
class NetDeviceConfiguration;
class ConnectionManager;
class OpenvpnManager;
class Dhcp3ConfigurationManager;
class DNSConfig;
class InterfaceManager;


namespace SFwallCore {
	class Firewall;
	class ApplicationFilter;
	class Rule;
}

class Delegate {
	public:
		virtual std::string rexpression() = 0;
		virtual JSONObject process(std::string uri, JSONObject input) = 0;
		bool matches(std::string uri);
};

#endif
