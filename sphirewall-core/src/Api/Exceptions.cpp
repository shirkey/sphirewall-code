#include <string>
#include "Api/Exceptions.h"

JsonManagementServiceException::JsonManagementServiceException(std::string message, int statuscode, int internalcode) noexcept
:
w(message),
  statuscode(statuscode),
  internalcode(internalcode)
{}

JsonManagementServiceException::~JsonManagementServiceException() noexcept {

}

std::string JsonManagementServiceException::message() const {
	return w;
}

const char *JsonManagementServiceException::what() const noexcept {
	return w.c_str();
}

int JsonManagementServiceException::statusCode() const {
	return statuscode;
}

int JsonManagementServiceException::internalCode() const {
	return internalcode;
}


ParsingException::ParsingException(std::string message, int statusCode, int internalcode) noexcept
:
JsonManagementServiceException(message, statusCode, internalcode) {
	if (message.compare("A field is missing or of the wrong type")) {
		w = "The field <" + message + "> could not be found or parsed correctly.";
	}
}


ServiceNotAvailableException::ServiceNotAvailableException(std::string message, int statuscode, int internalcode) noexcept
:
JsonManagementServiceException(message,  statuscode, internalcode) {

}

DelegateGeneralException::DelegateGeneralException(std::string message,  int statuscode, int internalcode) noexcept
:
JsonManagementServiceException(message, statuscode, internalcode) {

}

DelegateNotFoundException::DelegateNotFoundException(std::string message, int statuscode, int internalcode) noexcept
:
JsonManagementServiceException(message, statuscode, internalcode) {

}

AuthenticationException::AuthenticationException(std::string message, int statuscode, int internalcode) noexcept
:
JsonManagementServiceException(message, statuscode, internalcode) {

}
