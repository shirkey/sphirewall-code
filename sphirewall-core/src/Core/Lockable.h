#ifndef LOCKABLE_H
#define LOCKABLE_H

class Lock;
class Lockable {
	public:
		Lockable();
		virtual ~Lockable();
		bool tryLock();
		void holdLock();
		void releaseLock();
	private:
		Lock *lock;

};

#endif
