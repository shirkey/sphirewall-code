/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;

#include "Core/HostDiscoveryService.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Utils/Lock.h"
#include "Core/Lockable.h"
#include "SFwallCore/Packet.h"
#include "Core/Host.h"

int HostDiscoveryService::HOST_DISCOVERY_SERVICE_TIMEOUT = 60 * 10;

long HostDiscoveryService::updates(){
	return _updates;
}

HostPtr HostDiscoveryService::update(unsigned int ip, std::string mac) {
	if (tryLock()) {
		HostPtr arpEntry = getByMac(mac, ip);
		_updates++;

		if (arpEntry) {
			arpEntry->lastSeen = time(NULL);
		}
		else {
			if (eventDb) {
				EventParams params;
				params["ip"] = IP4Addr::ip4AddrToString(ip);
				params["hw"] = mac;

				if (eventDb) {
					eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
				}
			}

			arpEntry = add(mac, ip);
		}

		releaseLock();
		return arpEntry;
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::update(struct in6_addr *ip, std::string mac) {
	if (tryLock()) {
		HostPtr arpEntry = getByMac(mac, ip);
		_updates++;
		if (arpEntry) {
			arpEntry->lastSeen = time(NULL);
		}
		else {
			if (eventDb) {
				EventParams params;
				params["ip"] = IP6Addr::toString(ip);
				params["hw"] = mac;

				if (eventDb) {
					eventDb->add(new Event(NETWORK_ARP_DISCOVERED, params));
				}
			}

			arpEntry = add(mac, ip);
		}

		releaseLock();
		return arpEntry;
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::update(SFwallCore::Packet *pk) {
	if (pk->type() == IPV4) {
		SFwallCore::PacketV4 *v4 = (SFwallCore::PacketV4 *) pk;

		if (IP4Addr::isLocal(v4->getSrcIp()) && v4->getHw().size() != 0) {
			return update(v4->getSrcIp(), pk->getHw());
		}
	}
	else if (pk->type() == IPV6) {
		SFwallCore::PacketV6 *v4 = (SFwallCore::PacketV6 *) pk;

		if (v4->getSrcIp() && v4->getHw().size() != 0) {
			return update(v4->getSrcIp(), pk->getHw());
		}
	}

	return HostPtr();
}

string HostDiscoveryService::get(string ip) {
	holdLock();

	if (IP6Addr::valid(ip)) {
		struct in6_addr intIp;
		IP6Addr::fromString(&intIp, ip);
		HostPtr entry = get(&intIp);

		if (entry) {
			releaseLock();
			return entry->mac;
		}
	}
	else {
		in_addr_t intIp = IP4Addr::stringToIP4Addr(ip);
		HostPtr entry = get(intIp);

		if (entry) {
			releaseLock();
			return entry->mac;
		}
	}

	releaseLock();
	return "";
}

HostPtr HostDiscoveryService::get(in_addr_t ip) {
	for (std::pair<std::string, std::list<HostPtr> > entries : entryTable) {
		for (HostPtr entry : entries.second) {
			if (entry->type == 0 && entry->ip == ip) {
				return entry;
			}
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::getByMac(string mac, in_addr_t ip) {
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);

	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 0 && entry->ip == ip) {
				return entry;
			}
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::get(struct in6_addr *ip) {
	for (std::pair<std::string, std::list<HostPtr> > entries : entryTable) {
		for (HostPtr entry : entries.second) {
			if (entry->type == 1) {
				if (memcmp(ip, &entry->ipV6, sizeof(struct in6_addr)) == 0) {
					return entry;
				}
			}
		}
	}

	return HostPtr();
}

HostPtr HostDiscoveryService::getByMac(string mac, struct in6_addr *ip) {
	boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.find(mac);

	if (iter != entryTable.end()) {
		std::list<HostPtr> entries = iter->second;

		for (HostPtr entry : entries) {
			if (entry->type == 1) {
				if (memcmp(ip, &entry->ipV6, sizeof(struct in6_addr)) == 0) {
					return entry;
				}
			}
		}
	}

	return HostPtr();
}

void HostDiscoveryService::list(std::list<HostPtr> &entries) {
	for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = entryTable.begin(); iter != entryTable.end(); iter++) {
		for (HostPtr entry : iter->second) {
			entries.push_back(entry);
		}
	}
}

void HostDiscoveryService::CleanUpCron::run() {
	int t = time(NULL) - HOST_DISCOVERY_SERVICE_TIMEOUT;
	root->holdLock();
	vector<string> rowsToDelete;

	for (boost::unordered_map<string, std::list<HostPtr> >::iterator iter = root->entryTable.begin(); iter != root->entryTable.end(); iter++) {
		for (std::list<HostPtr>::iterator eiter = iter->second.begin(); eiter != iter->second.end(); eiter++) {

			HostPtr target = (*eiter);

			if (target->lastSeen < t) {
				EventParams params;
				params["ip"] = target->getIp();
				params["hw"] = target->mac;

				if (root->eventDb) {
					root->eventDb->add(new Event(NETWORK_ARP_TIMEDOUT, params));
				}

				eiter = iter->second.erase(eiter);
				root->_size -= 1;
			}
		}

		if (iter->second.size() == 0) {
			rowsToDelete.push_back(iter->first);
		}
	}

	for (unsigned x = 0; x < rowsToDelete.size(); x++) {
		root->entryTable.erase(rowsToDelete[x]);
	}

	root->releaseLock();
}

HostPtr HostDiscoveryService::add(string mac, in_addr_t ip) {
	if (mac.size() > 0) {
		HostPtr entry(new Host());
		entry->mac = mac;
		entry->ip = ip;
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);
		entry->type = 0;

		dnsHostnameWorker->push(entry);
		entryTable[mac].push_back(entry);
		_size += 1;

		return entry;
	}

	return HostPtr();
}


HostPtr HostDiscoveryService::add(string mac, struct in6_addr *ip) {
	if (mac.size() > 0) {
		HostPtr entry(new Host());
		entry->mac = mac;
		memcpy(&entry->ipV6, ip, sizeof(struct in6_addr));
		entry->firstSeen = time(NULL);
		entry->lastSeen = time(NULL);
		entry->type = 1;
		dnsHostnameWorker->push(entry);
		entryTable[mac].push_back(entry);
		_size += 1;

		return entry;
	}

	return HostPtr();
}


int HostDiscoveryService::size() {
	return _size;
}

HostDiscoveryService::HostDiscoveryService(CronManager *cron, Config *config) :  _size(0), _updates(0) {
	cron->registerJob(new CleanUpCron(this));
	config->getRuntime()->loadOrPut("HOST_DISCOVERY_SERVICE_TIMEOUT", &HOST_DISCOVERY_SERVICE_TIMEOUT);
	eventDb = NULL;
	dnsHostnameWorker = new HostDnsHostnameResolverWorker();
}


HostDiscoveryService::HostDiscoveryService(): _size(0), _updates(0) {
	eventDb = NULL;
	dnsHostnameWorker = new HostDnsHostnameResolverWorker();
}

void HostDiscoveryService::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

HostDnsHostnameResolverWorker::HostDnsHostnameResolverWorker()
	: running(false) {}

void HostDnsHostnameResolverWorker::push(HostPtr hostPtr) {
	holdLock();
	workQueue.push(hostPtr);

	if (!running) {
		running = true;
		releaseLock();
		boost::thread(boost::bind(&HostDnsHostnameResolverWorker::process, this));
	}
	else {
		releaseLock();
	}
}

void HostDnsHostnameResolverWorker::process() {
	while (true) {
		holdLock();

		if (workQueue.empty()) {
			releaseLock();
			break;
		}

		HostPtr host = workQueue.front();
		workQueue.pop();
		releaseLock();

		if (host) {
			host->fetchHostname();
		}
	}

	running = false;
}

