#include <iostream>
#include <sstream>
#include <iomanip>


#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <list>
#include <map>

using namespace std;

#include "Core/Logger.h"
#include "Core/Vpn.h"
#include "vici.h"

enum { 
	START = 0, 
	SECTION_START = 1, 
	SECTION_END = 2, 
	KEY_VALUE = 3, 
	LIST_START = 4, 
	LIST_ITEM = 5, 
	LIST_END = 6, 
	END = 7	
};

char ViciClient::read_char(int sock){
	char c;    
	recv(sock, &c, sizeof(char), 0);
	return c;
}


u_int8_t ViciClient::read_int8(int s){
        u_int8_t c;
        recv(s, &c, sizeof(u_int8_t), 0);
        return c;
}

u_int16_t ViciClient::read_int16(int s){
        u_int16_t c;
        recv(s, &c, sizeof(u_int16_t), 0);
	return c;
}

std::string ViciClient::read_string_block(int s, int size){
	char* value = (char*) malloc(size);
	recv(s, value, size, 0);
	string ret(value, size);
	free(value);
	return ret;
}

std::string ViciClient::parse_string_field_len8(int s){
	u_int8_t key_length = read_int8(s);
	return read_string_block(s, key_length);
}

std::string ViciClient::parse_string_field_len16(int s){
        u_int16_t key_length = read_int16(s);
        return read_string_block(s, ntohs(key_length));
}

int ViciClient::process_key_value(int s, ViciMessage* section){
	std::string key = parse_string_field_len8(s);

	std::string value = parse_string_field_len16(s); 
	section->key_values[key] = value;
	return key.size() + value.size() + 3;
}

int ViciClient::process_list(int s, ViciMessage* section){
	int cost = 0;
	string key = parse_string_field_len8(s);
	cost += (1 + key.size());
	
	//Now work on each list item:
	list<std::string> temp_list;
	char c;
	while(c = read_char(s)){
		switch(c){
			case LIST_ITEM: {
						string list_item_field = parse_string_field_len16(s);
						temp_list.push_back(list_item_field);
						cost += list_item_field.size() + 2;
						break;
					}

			case LIST_END:
					section->list_items[key] = temp_list;
					return ++cost;	
					break;
		};

		cost++;
	}

	return cost;	
}

ViciMessage* ViciClient::handle_response(int s, int response_data_len){
	ViciMessage* root;
	ViciMessage* cursor;

	root = new ViciMessage(); 
	root->root = NULL;
	cursor = root;

	for(int x = 0; x < response_data_len; x++){
		char c= read_char(s);
		if(c < 32){
			//Must be something we should take note of
			switch(c){
				case SECTION_START: {

							    //First get the name of the section we are creating:
							    string section_name = parse_string_field_len8(s);

							    ViciMessage* new_section = new ViciMessage(); 
							    new_section->root = cursor;

							    cursor->sections[section_name] = new_section;
							    cursor = new_section;
							    x+= section_name.size() + 1;			
							    break;
						    }

				case SECTION_END:
						    cursor = cursor->root;	
						    break;
				case KEY_VALUE:
						    x += process_key_value(s, cursor);
						    break;
				case LIST_START:
						    x += process_list(s, cursor);
						    break;
				case END:
					break;
				case START:
					break;
				default:{
						break;
					}
			}
		}
	}

	return root;
}

void ViciMessage::dump(){
	//Print key-values:
	for(map<string, string>::iterator iter = key_values.begin(); iter != key_values.end(); iter++){
		printf("%s=%s\n", iter->first.c_str(), iter->second.c_str());		
	}

	for(map<string, list<string> >::iterator iter = list_items.begin(); iter != list_items.end(); iter++){
		printf("%s= [", iter->first.c_str());		

		for(list<string>::iterator list_iter = iter->second.begin(); list_iter != iter->second.end(); list_iter++){
			printf("%s, ", (*list_iter).c_str());
		}
		printf("]\n");
	}

	for(map<string, ViciMessage*>::iterator iter = sections.begin(); iter != sections.end(); iter++){
		printf("%s = {\n", iter->first.c_str());
		iter->second->dump();
		printf("}\n");
	}
}

class Buffer {
	public:
		Buffer(){
			current_size = 0;
			internal_buffer = (char*) malloc(512);
		}

		~Buffer(){
			free(internal_buffer);
		}

		int current_size;
		char* internal_buffer;

		void push_int8(u_int8_t in){
			memcpy(internal_buffer + current_size, &in, sizeof(u_int8_t));
			current_size += sizeof(u_int8_t);
		}

		void push_int16(u_int16_t in){
			memcpy(internal_buffer + current_size, &in, sizeof(u_int16_t));
			current_size += sizeof(u_int16_t);
		}

		void push_string(const char* in, int len){
			memcpy(internal_buffer + current_size, in, len);
			current_size += len;
		}

};

void ViciClient::serialize_message(Buffer* buffer, ViciMessage* message){
	//First go around the key values
	for(map<string, string>::iterator iter = message->key_values.begin(); iter != message->key_values.end(); iter++){
		buffer->push_int8(KEY_VALUE);
		buffer->push_int8((u_int8_t) iter->first.size());
		buffer->push_string(iter->first.c_str(), iter->first.size());
		buffer->push_int16(htons(iter->second.size()));
		buffer->push_string(iter->second.c_str(), iter->second.size());
	}

	for(std::map<std::string, list<std::string> >::iterator iter = message->list_items.begin(); iter != message->list_items.end(); iter++){
		string key = iter->first;
		list<string> items = iter->second;

		buffer->push_int8(LIST_START);
		buffer->push_int8(key.size());
		buffer->push_string(key.c_str(), key.size());

		for(list<string>::iterator list_iter = items.begin(); list_iter != items.end(); list_iter++){
			string list_item_string = (*list_iter);	

			buffer->push_int8(LIST_ITEM);	
			buffer->push_int16(htons(list_item_string.size()));	
			buffer->push_string(list_item_string.c_str(), list_item_string.size());	
		}	

		buffer->push_int8(LIST_END);
	}

	for(map<std::string, ViciMessage*>::iterator iter = message->sections.begin(); iter != message->sections.end(); iter++){
		string key = iter->first;
		ViciMessage* inner_message = iter->second;

		buffer->push_int8(SECTION_START);
		buffer->push_int8(key.size());
		buffer->push_string(key.c_str(), key.size());
		serialize_message(buffer, inner_message);	

		buffer->push_int8(SECTION_END);
	}		
}

ViciResponse* ViciClient::response(){
	ViciResponse* response = new ViciResponse();
	u_int32_t response_len;
	recv(s, &response_len, sizeof(response_len), 0);
	recv(s, &response->type, sizeof(response->type), 0);
	response->len = ntohl(response_len) -1;

	if(response->len == 0){
		return NULL;
	}	

	switch(response->type){
		case VICI_CMD_RESPONSE: {
						response->message = handle_response(s, response->len);
						break;
					}
		case VICI_EVENT: {
					 u_int8_t event_name_size = read_int8(s);
					 string event_name = read_string_block(s, event_name_size);
					 response->len -= 1 + event_name.size();

					 response->message = handle_response(s, response->len);	
				 }
				 break;

                case VICI_EVENT_CONFIRM:
		case VICI_EVENT_UNKNOWN:
		default:
				 return NULL;
	};

	return response;
}

void ViciClient::request(ViciRequest* request){
	u_int8_t op = request->type;
	const char* name = request->command.c_str();
	u_int8_t namelen = strlen(name);

	Buffer buffer;
	serialize_message(&buffer, request->message); 
	u_int32_t total_len = htonl(sizeof(op) + sizeof(namelen) + strlen(name) + buffer.current_size);

	send(s, &total_len, sizeof(total_len), 0);
	send(s, &op, sizeof(op), 0);
	send(s, &namelen, sizeof(namelen), 0);
	send(s, name, strlen(name), 0);
	send(s, buffer.internal_buffer, buffer.current_size, 0);
}

string ViciMessage::get_value(std::string key){
	int section_cursor = string::npos;
	if((section_cursor = key.find(".")) != string::npos){
		std::string cursor_key = key.substr(0, key.find("."));
		std::string remaining_key = key.substr(key.find(".") + 1, key.size());
		return get_section(cursor_key)->get_value(remaining_key);
	}

	if(key_values.find(key) != key_values.end()){
		return key_values[key];
	}

	throw ViciElementNotFound(key);
}

bool ViciMessage::has_value(std::string key){
	int section_cursor = string::npos;
	if((section_cursor = key.find(".")) != string::npos){
		std::string cursor_key = key.substr(0, key.find("."));
		std::string remaining_key = key.substr(key.find(".") + 1, key.size());
		return get_section(cursor_key)->has_value(remaining_key);
	}

	if(key_values.find(key) != key_values.end()){
		return true;
	}

	return false;
}


list<std::string> ViciMessage::get_list(std::string key){
	int section_cursor = string::npos;
	if((section_cursor = key.find(".")) != string::npos){
		std::string cursor_key = key.substr(0, key.find("."));
		std::string remaining_key = key.substr(key.find(".") + 1, key.size());
		return get_section(cursor_key)->get_list(remaining_key);
	}

	if(list_items.find(key) != list_items.end()){
		return list_items[key];
	}

	throw ViciElementNotFound(key);
}

ViciMessage* ViciMessage::get_section(std::string key){
	int section_cursor = string::npos;
	if((section_cursor = key.find(".")) != string::npos){
		std::string cursor_key = key.substr(0, key.find("."));
		std::string remaining_key = key.substr(key.find(".") + 1, key.size());
		return get_section(cursor_key)->get_section(remaining_key);
	}

	if(sections.find(key) != sections.end()){
		return sections[key];
	}

	throw ViciElementNotFound(key);
}

bool ViciMessage::has_section(std::string key){
	int section_cursor = string::npos;
	if((section_cursor = key.find(".")) != string::npos){
		std::string cursor_key = key.substr(0, key.find("."));
		std::string remaining_key = key.substr(key.find(".") + 1, key.size());
		return true;
	}

	if(sections.find(key) != sections.end()){
		return true;
	}

	return false;
}

void ViciMessage::set_value(std::string key, std::string value){
	key_values[key] = value;
}

void ViciMessage::set_list(std::string key, list<string> values){
	list_items[key] = values;
}

void ViciMessage::set_section(string key, ViciMessage* section){
	sections[key] = section;
}

ViciMessage::ViciMessage(){
	this->root = NULL;
}

ViciMessage::~ViciMessage(){
	for(map<std::string, ViciMessage*>::iterator iter = sections.begin(); iter != sections.end(); iter++){
		delete iter->second;
	}	
}

int ViciClient::init(){
	int t, len;
	struct sockaddr_un remote;
	char str[100];

	if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		Logger::instance()->log("sphirewalld.vici", ERROR, "Could not open a unix socket");
		throw VpnException("Could not open a unix socket");
	}

	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, "/var/run/charon.vici");
	len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(s, (struct sockaddr *)&remote, len) == -1) {
		Logger::instance()->log("sphirewalld.vici", ERROR,  "Could not open a connection to the vici unix socket interface on '/var/run/charon.vici'");
		throw VpnException("Could not open a connection to the vici unix socket interface on '/var/run/charon.vici'");
	}

	return 0;
}

void ViciClient::destroy(){
	close(s);
}

void ViciResponse::dump(){
	printf("message len: %d\n", len);
	printf("message type: %d\n", type);
	printf("message content below\n");
	message->dump();
}
