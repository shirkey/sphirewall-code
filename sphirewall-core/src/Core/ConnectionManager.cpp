/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include "System.h"
#include "Utils/FileUtils.h"
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

#include "Core/ConnectionManager.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Utils/LinuxUtils.h"

bool ConnectionManager::load() {
	if (configurationManager->has("connections")) {
		connections.clear();

		ObjectContainer *root = configurationManager->getElement("connections");
		ObjectContainer *arr = root->get("pppConnections")->container();

		for (int x = 0; x < arr->size(); x++) {
			ObjectContainer *o = arr->get(x)->container();

			if (o->get("type")->number() == PPPoE) {
				PPPoE_Connection *target = new PPPoE_Connection();
				target->setName(o->get("name")->string());
				target->setDevice(o->get("device")->string());
				target->getAuthenticationCredentials().authenticationType = (ConnectionAuthenticationType) o->get("authenticationType")->number();
				target->getAuthenticationCredentials().username = o->get("username")->string();
				target->getAuthenticationCredentials().password = o->get("password")->string();
				publishAuthenticationCredentials();
				target->publishSettings();
				target->connect();

				connections.push_back(target);
			}
			else if (o->get("type")->number() == OPENVPN) {
				Openvpn_Connection *target = new Openvpn_Connection();
				target->setName(o->get("name")->string());
				target->getAuthenticationCredentials().keyfile = o->get("keyfile")->string();
				connections.push_back(target);
				target->publishSettings();
				target->connect();
			}

		}
	}

	configurationManager->save();
	return true;
}

void ConnectionManager::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	list<Connection *> connections = getConnections();
	list<Connection *>::iterator iter;

	ObjectContainer *arr = new ObjectContainer(CARRAY);

	for (iter = connections.begin();
			iter != connections.end();
			iter++) {
		Connection *target = (*iter);

		ObjectContainer *o = new ObjectContainer(CREL);
		o->put("name", new ObjectWrapper((string) target->getName()));
		o->put("type", new ObjectWrapper((double) target->type()));

		if (target->type() == PPPoE) {
			o->put("device", new ObjectWrapper((string) target->getDevice()));
			o->put("username", new ObjectWrapper((string) target->getAuthenticationCredentials().username));
			o->put("password", new ObjectWrapper((string) target->getAuthenticationCredentials().password));
			o->put("authenticationType", new ObjectWrapper((double) target->getAuthenticationCredentials().authenticationType));
		}
		else if (target->type() == OPENVPN) {
			o->put("keyfile", new ObjectWrapper((string) target->getAuthenticationCredentials().keyfile));
		}

		arr->put(new ObjectWrapper(o));
	}

	root->put("pppConnections", new ObjectWrapper(arr));

	configurationManager->setElement("connections", root);
	configurationManager->save();
}

std::list<Connection *> ConnectionManager::getConnections() {
	return this->connections;
}

void ConnectionManager::deleteConnection(Connection *target) {
	target->disconnect();
	connections.remove(target);

	delete target;

	save();
}

void ConnectionManager::createConnection(std::string name, ConnectionType type) {
	//check that this connection exists alread ?
	if (getConnection(name) == NULL) {
		if (type == PPPoE) {
			Connection *connection = new PPPoE_Connection();
			connection->setName(name);
			connections.push_back(connection);
		}
		else {
			Connection *connection = new Openvpn_Connection();
			connection->setName(name);
			connections.push_back(connection);

		}

		save();
	}
}

Connection *ConnectionManager::getConnection(std::string name) {
	list<Connection *>::iterator iter;

	for (iter = connections.begin();
			iter != connections.end();
			iter++) {
		Connection *connection = (*iter);

		if (connection->getName().compare(name) == 0) {
			return connection;
		}
	}

	return NULL;
}

void ConnectionManager::publishAuthenticationCredentials() {
	//Iterate over all the connection files:
	list<Connection *> connections = getConnections();
	list<Connection *>::iterator iter;

	ofstream chapFileStream;
	chapFileStream.open("/etc/ppp/chap-secrets");
	chapFileStream << "#This file has been modified by sphirewall\n";

	ofstream papFileStream;
	papFileStream.open("/etc/ppp/pap-secrets");
	papFileStream << "#This file has been modified by sphirewall\n";

	for (iter = connections.begin();
			iter != connections.end();
			iter++) {

		Connection *target = (*iter);

		if (target->getAuthenticationCredentials().authenticationType == CHAP) {
			chapFileStream << target->getAuthenticationCredentials().username << " * " << target->getAuthenticationCredentials().password << "\n";
		}
		else if (target->getAuthenticationCredentials().authenticationType == PAP) {
			papFileStream << target->getAuthenticationCredentials().username << " * " << target->getAuthenticationCredentials().password << "\n";
		}
	}

	chapFileStream.close();
	papFileStream.close();
}

void PPPoE_Connection::publishSettings() {
	ofstream pppFile;
	string file = "/etc/ppp/peers/" + getName();
	pppFile.open(file.c_str());

	pppFile << "#This file has been generated by sphirewall\n";
	pppFile << "noipdefault\n";
	pppFile << "defaultroute\n";
	pppFile << "replacedefaultroute\n";
	pppFile << "hide-password\n";
	pppFile << "noauth\n";
	pppFile << "persist\n";
	pppFile << "plugin rp-pppoe.so " << getDevice() << "\n";
	pppFile << "user \"" << getAuthenticationCredentials().username << "\"\n";
	pppFile << "usepeerdns\n";
	pppFile << "maxfail 2\n";

	pppFile.close();
}

bool PPPoE_Connection::isConnected() {
	string command = "pgrep -fx \"/usr/sbin/pppd call " + getName() + "\"";
	int ret = system(command.c_str());
	return ret == 0;
}

void PPPoE_Connection::connect() {
	LinuxUtils::execWithNoFds("pon " + getName());
}

void PPPoE_Connection::disconnect() {
	LinuxUtils::execWithNoFds("poff " + getName());
}

bool Openvpn_Connection::isConnected() {
	return FileUtils::checkExists(getPidFile());
}

std::string Openvpn_Connection::getConfigFile() {
	stringstream ss;
	ss << "/etc/openvpn_client_" << getName() << ".conf";
	return ss.str();
}

std::string Openvpn_Connection::getPidFile() {
	stringstream ss;
	ss << "/var/lock/openvpn_client_" << getName() << ".pid";
	return ss.str();
}

std::string Openvpn_Connection::getLogFilename() {
	stringstream ss;
	ss << "/var/log/openvpn_client_" << getName() << ".log";
	return ss.str();
}


void Openvpn_Connection::connect() {
	publishSettings();
	stringstream ss;
	ss << "/bin/bash -c 'start-stop-daemon --start --quiet --oknodo --pidfile " << getPidFile() << " --exec /usr/sbin/openvpn";
	ss << " -- --writepid " << getPidFile() << " --daemon ovpn-openvpn --config " << getConfigFile() << " --log " << getLogFilename() << "'";

	LinuxUtils::execWithNoFds(ss.str());
	sleep(3);
}

void Openvpn_Connection::disconnect() {
	string pid = FileUtils::read(getPidFile().c_str());

	if (pid.size() > 0) {
		int ipid = stoi(pid);

		if (ipid > 1) {
			kill(ipid, SIGTERM);
			remove(getPidFile().c_str());
		}

		sleep(3);
	}
}

void Openvpn_Connection::publishSettings() {
	ofstream pppFile;
	pppFile.open(getConfigFile().c_str());
	pppFile << getAuthenticationCredentials().keyfile;
	pppFile.close();
}

std::string PPPoE_Connection::log() {
	string ret;
	LinuxUtils::exec("tail -n 500 /var/log/syslog | grep pppd | grep -v DEFAULT::", ret);
	return ret;
}

std::string Openvpn_Connection::log() {
	string file = FileUtils::read(getLogFilename().c_str());
	return file;
}
