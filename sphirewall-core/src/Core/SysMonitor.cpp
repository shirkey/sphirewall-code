/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <sstream>
#include <proc/readproc.h>

using namespace std;
using std::string;

#include "Core/SysMonitor.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/TimeWrapper.h"
#include "Core/HostDiscoveryService.h"
#include "Core/Event.h"
#include "Utils/NetworkUtils.h"

class GeneralMetricSampler: public MetricSampler {
	public:
		void sample(map<string, double> &items) {
			struct proc_t usage;
			look_up_our_self(&usage);
			items["system.vsize"] = usage.vsize;
			items["system.utime"] = usage.utime;
			items["system.stime"] = usage.stime;
			items["system.priority"] = usage.priority;
			items["system.nice"] = usage.nice;
			items["system.size"] = usage.size;
			
			items["sessiondb.size"] = System::getInstance()->getSessionDb()->dbSize();
			items["firewall.arp.size"] = System::getInstance()->getArp()->size();
			items["firewall.arp.updates"] = System::getInstance()->getArp()->updates();
		}
};

void LoggingMetricHandler::operate(map<string, double> input){
	Logger::instance()->log("sysmonitor.metrics", INFO, "---- Start Metrics Dump ----");
	for(map<string, double>::iterator iter = input.begin(); iter != input.end(); iter++){
		Logger::instance()->log("sysmonitor.metrics", INFO, "metric: %s current value: %f", iter->first.c_str(), iter->second);
	}

	Logger::instance()->log("sysmonitor.metrics", INFO, "---- End Metrics Dump ----");
}

void AnaEngineMetricHandler::operate(map<string, double> input){
	AnalyticsClient client;
	client.addMetricSnapshot(input);
}

void SysMonitor::pollall() {
	for (Watch* watch : registeredWatches) {
		watch->poll();
	}

	map<string, double> input;
	for (MetricSampler* sampler : registeredSamplers){
		sampler->sample(input);
	}

	for(MetricHandler* handler : metricHandlers){
		try {
			handler->operate(input);
		}catch(exception &e){
			Logger::instance()->log("sysmonitor.metrics", ERROR, "A metric hander failed to execute for reason %s", e.what());
		}
	}
}

void SysMonitor::registerMetric(MetricSampler *sampler) {
	registeredSamplers.push_back(sampler);
}

void SysMonitor::registerMetricHandler(MetricHandler* handler){
	metricHandlers.push_back(handler);
}

SysMonitor::SysMonitor(EventDb *eventDb, CronManager *cronManager, SFwallCore::Firewall *firewall, BandwidthDbPrimer *bandwidthDb) {
	this->eventDb = eventDb;
	this->firewall = firewall;

	InternetConnectionWatch *icw = new InternetConnectionWatch(&System::getInstance()->configurationManager);
	icw->setEventDb(eventDb);

	registeredWatches.push_back(icw);
	cronManager->registerJob(new SysMonitorCron(this));

	registerMetric(new GeneralMetricSampler());
}

void InternetConnectionWatch::poll() {
	if (config->hasByPath("watchdog:monitorGoogle") && config->get("watchdog:monitorGoogle")->boolean()) {
		int res = NetworkUtils::ping("google.com");

		if (res == -1 && state) {
			eventDb->add(new Event(WATCHDOG_INTERNET_CONNECTION_DOWN, EventParams()));
			state = false;
		}
		else if (res != -1 && !state) {
			eventDb->add(new Event(WATCHDOG_INTERNET_CONNECTION_UP, EventParams()));
			state = true;
		}
	}
}

