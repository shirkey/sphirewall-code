#include <boost/shared_ptr.hpp>
#include <map>
#include <list>
#include <sstream>

#include "Core/Vpn.h"
#include "Core/vici.h"
#include "Core/ConfigurationManager.h"

class IPSecInstance : public virtual VpnInstance {
	public:
		IPSecInstance(){
			auto_start = false;
		}

		virtual int type() = 0;
		std::string meta;

		bool auto_start;
		int auth_mode;
		std::string secret_key;

                virtual void initiate() = 0;
                virtual void terminate() = 0;
                virtual int status() = 0;

};

class IPSecGatewayToGatewayInstance : public virtual IPSecInstance {
	public:
		std::string local_host;
		std::string local_subnet;
		std::string local_id;
		
		std::string remote_host;
		std::string remote_subnet;
		std::string remote_id;

		int type() {
			return VPN_IPSEC_GATEWAY_TO_GATEWAY;
		}

                void initiate();
                void terminate();
                int status();

        private:
                void __add_keys();
                void __add_connection();
                void __initiate_connection();
                void __terminate_connection();
                void __unload_connection();

		void __configure_routes();
		void __remove_routes();
};

class IPSecL2TPGatewayInstance {

};

class IPSecL2TPGatewayClient {

};

typedef boost::shared_ptr<IPSecInstance> IPSecInstancePtr;
class IPSecManager {
	public:
		IPSecManager(){
			this->nat_traversal_enabled = false;
			this->logging_running = false;
		}

		void add(IPSecInstancePtr instance);
		void remove(IPSecInstancePtr instance);
		IPSecInstancePtr get(std::string key);
		std::list<IPSecInstancePtr> list_instances();

		void do_connect(IPSecInstancePtr target);
		void do_disconnect(IPSecInstancePtr target);
		int status(IPSecInstancePtr target);
		std::string log(IPSecInstancePtr target);

		bool nat_traversal_enabled;
		void init_service();

		void __logging_thread();
		void start_logging();

		bool load(ConfigurationManager* configuration);
		void save(ConfigurationManager* configuration);
	
		bool available(int type){
			
		}
	private:
		bool logging_running;
		std::map<string, IPSecInstancePtr> instances;
};
