/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <time.h>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <list>

using namespace std;

#include "Core/Event.h"
#include "Utils/Utils.h"
#include "Core/System.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Core/Logger.h"
#include "Core/EventHandler.h"
#include "SFwallCore/Rewrite.h"

const char *IDS_EVENT = "event.ids";
const char *IDS_PORTSCAN = "event.ids.portscan";
const char *IDS_SSHD_BRUTEFORCE = "event.ids.sshbruteforce";
const char *SYSTEM_MONITOR = "event.sysmonitor.alert";
const char *IDS_FLOOD_ATTACK = "event.ids.flood";
const char *IDS_SNORT_ALERT = "event.ids.snort";
const char *IDS_SNORT_ALERT_1 = "event.ids.snort.1";
const char *IDS_SNORT_ALERT_2 = "event.ids.snort.2";
const char *IDS_SNORT_ALERT_3 = "event.ids.snort.3";
const char *IDS_SNORT_ALERT_4 = "event.ids.snort.4";
const char *IDS_SNORT_ALERT_5 = "event.ids.snort.5";
const char *USERDB_LOGIN_FAILED = "event.userdb.auth.failed";
const char *USERDB_LOGIN_SUCCESS = "event.userdb.auth.success";
const char *USERDB_QUOTA_EXCEEDED = "event.userdb.quota.exceeded";
const char *USERDB_QUOTA_OK = "event.userdb.quota.ok";
const char *USERDB_SESSION_CREATE = "event.userdb.session.create";
const char *USERDB_SESSION_TIMEOUT = "event.userdb.session.timeout";
const char *USERDB_LOGOUT = "event.userdb.logout";
const char *AUDIT_EVENT = "event.audit";
const char *EVENT_PURGE = "event.purge";
const char *AUDIT_CONFIGURATION_CHANGED = "event.audit.configuration.general.set";
const char *AUDIT_RPC_CALL = "event.audit.rpc";
const char *AUDIT_CONFIGURATION_IDS_EXCEPTION_ADDED = "event.audit.configuration.ids.exceptions.add";
const char *AUDIT_CONFIGURATION_IDS_EXCEPTION_REMOVED = "event.audit.configuration.ids.exceptions.removed";
const char *AUDIT_CONFIGURATION_NETWORK_DEVICES_TOGGLE = "event.audit.configuration.network.devices.toggle";
const char *AUDIT_CONFIGURATION_NETWORK_DEVICES_SETIP = "event.audit.configuration.network.devices.setip";
const char *AUDIT_CONFIGURATION_NETWORK_DNS_SET = "event.audit.configuration.network.dns.set";
const char *AUDIT_CONFIGURATION_NETWORK_ROUTES_ADD = "event.audit.configuration.network.routes.add";
const char *AUDIT_CONFIGURATION_NETWORK_ROUTES_DEL = "event.audit.configuration.network.routes.del";
const char *AUDIT_CONFIGURATION_NETWORK_DHCP_SET = "event.audit.configuration.network.dhcp.set";
const char *AUDIT_CONFIGURATION_NETWORK_DHCP_PUBLISH = "event.audit.configuration.network.dhcp.publish";
const char *AUDIT_CONFIGURATION_NETWORK_DHCP_START = "event.audit.configuration.network.dhcp.start";
const char *AUDIT_CONFIGURATION_NETWORK_DHCP_STOP = "event.audit.configuration.network.dhcp.stop";
const char *AUDIT_CONFIGURATION_NETWORK_CONNECTION_START = "event.audit.configuration.network.connection.start";
const char *AUDIT_CONFIGURATION_NETWORK_CONNECTION_STOP = "event.audit.configuration.network.connection.stop";
const char *AUDIT_CONFIGURATION_GROUPDB_ADD = "event.audit.configuration.groupdb.add";
const char *AUDIT_CONFIGURATION_GROUPDB_DEL = "event.audit.configuration.groupdb.del";
const char *AUDIT_CONFIGURATION_GROUPDB_MODIFIED = "event.audit.configuration.groupdb.modified";
const char *AUDIT_CONFIGURATION_USERDB_ADD = "event.audit.configuration.userdb.add";
const char *AUDIT_CONFIGURATION_USERDB_DEL = "event.audit.configuration.userdb.del";
const char *AUDIT_CONFIGURATION_USERDB_MODIFIED = "event.audit.configuration.userdb.modified";
const char *AUDIT_CONFIGURATION_USERDB_SETPASSWORD = "event.audit.configuration.userdb.setpassword";
const char *AUDIT_CONFIGURATION_USERDB_GROUPS_ADD = "event.audit.configuration.userdb.groups.add";
const char *AUDIT_CONFIGURATION_USERDB_GROUPS_DEL = "event.audit.configuration.userdb.groups.del";
const char *AUDIT_CONFIGURATION_USERDB_DISABLE = "event.audit.configuration.userdb.disable";
const char *AUDIT_CONFIGURATION_USERDB_ENABLE = "event.audit.configuration.userdb.enable";
const char *AUDIT_FIREWALL_CONNECTIONS_TERMINATE = "event.audit.firewall.connections.terminate";
const char *NETWORK_DEVICES_ADDED = "event.network.devices.added";
const char *NETWORK_DEVICES_CHANGED = "event.network.devices.change";
const char *NETWORK_DEVICES_REMOVED = "event.network.devices.removed";
const char *NETWORK_ARP_DISCOVERED = "event.network.arp.discovered";
const char *NETWORK_ARP_TIMEDOUT = "event.network.arp.timedout";
const char *RUNTIME_METRIC = "event.metric";
const char *START = "event.start";

const char *FIREWALL_WEBFILTER_HIT = "event.firewall.webfilter.hit";
const char *FIREWALL_FILTERING_HIT= "event.firewall.filtering.hit";
const char *GLOBAL_QUOTA_EXCEEDED = "event.global.quota.exceeded";
const char *GLOBAL_QUOTA_OK = "event.global.quota.ok";

const char *WATCHDOG_INTERNET_CONNECTION_UP = "event.watchdog.connectivity.google.up";
const char *WATCHDOG_INTERNET_CONNECTION_DOWN = "event.watchdog.connectivity.google.down";

int EventDb::EVENT_DB_PURGE_THRESHOLD = 60 * 10;


Event::Event(string key, EventParams params)
	: t(time(NULL)), key(key), params(params)
{}

EventParams::EventParams(list< pair< string, Param > > events)
	: params() {
	for (pair<string, Param> p : events) {
		params[p.first] = p.second;
	}
}

EventParams::EventParams()
	: params()
{}

string Event::toString() {
	stringstream ss;
	ss << "Event " << key << " with details " << params.toString();
	return ss.str();
}

void EventDb::add(Event *e) {
	EventPtr ptr(e);

	if (async) {
		handlerWorker->push(ptr);
	}
	else {
		this->runhandlers(ptr);
	}

	holdLock();
	events.insert(events.begin(), ptr);
	releaseLock();
}

void EventDb::runhandlers(EventPtr e) {
	multimap<std::string, EventHandler *>::iterator iter;

	for (iter = handlers.begin(); iter != handlers.end(); ++iter) {
		EventHandler *handler = iter->second;

		if (!handler->enabled()) {
			continue;
		}

		std::string hkey = iter->first;

		if (e->key.find(hkey) != std::string::npos) {
			handler->handle(e);
		}
	}
}

void EventDb::purgeEvents() {
	holdLock();
	events.clear();
	releaseLock();
	add(new Event(EVENT_PURGE, EventParams()));
}

void EventDb::purgeOldEvents() {
	if (tryLock()) {
		int minTime = time(NULL) - EVENT_DB_PURGE_THRESHOLD;
		std::list<EventPtr>::iterator iter = events.begin();

		while (iter != events.end()) {
			EventPtr event = (*iter);

			if (event->t < minTime) {
				iter = events.erase(iter);
			}
			else {
				iter++;
			}
		}
	}

	releaseLock();
}

int EventDb::size() {
	return events.size();
}

EventDb::EventDb(Config *config)
	: config(config) {
	this->async = true;

	if (config) {
		config->getRuntime()->loadOrPut("EVENT_DB_PURGE_THRESHOLD", &EVENT_DB_PURGE_THRESHOLD);
	}

	this->handlerWorker = new EventHandlerWorker(this);
}

EventDb::EventDb() : EventDb(NULL){}

bool EventDb::load() {
	
	//Some event handlers are inserted by the system and should not be removed
        multimap<std::string, EventHandler *>::iterator iter;
        for (iter = handlers.begin(); iter != handlers.end();) {
		EventHandler* handler = iter->second;
		if(!handler->dontremove()){
			handlers.erase(iter++);
		}else{
			iter++;
		}
	}

	if (config->getConfigurationManager()->has("eventDb")) {
		ObjectContainer *root = config->getConfigurationManager()->getElement("eventDb");
		ObjectContainer *arr1 = root->get("handlers")->container();

		for (int x = 0; x < arr1->size(); x++) {
			ObjectContainer *o = arr1->get(x)->container();

			string handlerKey = o->get("handler")->string();

			if (handlerKey.compare("handler.log") == 0) {
				addHandler(o->get("key")->string(), new LogEventHandler());
			}
			else if (handlerKey.compare("handler.firewall.block") == 0) {
				addHandler(o->get("key")->string(), new FirewallBlockSourceAddressHandler());
			}
			else {
				Logger::instance()->log("sphirewalld.eventdb.load", ERROR, "Could not find a handler that was stored in configuration");
			}
		}
	}
	return true;
}

void EventDb::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *a = new ObjectContainer(CARRAY);

	for (multimap<string, EventHandler *>::iterator iter = getHandlers().begin(); iter != getHandlers().end(); iter++) {
		string ikey = iter->first;
		EventHandler *ihandler = iter->second;
		string ihandlerkey = ihandler->key();

		ObjectContainer *o = new ObjectContainer(CREL);
		o->put("key", new ObjectWrapper((string) ikey));
		o->put("handler", new ObjectWrapper((string) ihandlerkey));

		a->put(new ObjectWrapper(o));
	}

	root->put("handlers", new ObjectWrapper(a));
	config->getConfigurationManager()->setElement("eventDb", root);
	config->getConfigurationManager()->save();
}

std::list<EventPtr> EventDb::list() {
	return events;
}

void EventDb::addHandler(std::string key, EventHandler *handler) {
	handlers.insert(pair<string, EventHandler *>(key, handler));
}

void EventDb::removeHandler(std::string key, EventHandler *handler) {
	multimap<std::string, EventHandler *>::iterator iter;

	for (iter = handlers.find(key); iter != handlers.end(); ++iter) {
		if ((*iter).second == handler) {
			handlers.erase(iter);
			break;
		}
	}
}

void EventDb::clearHandlers() {
	handlers.clear();
}

Param *EventParams::get(const string input) {
	if (params.find(input) == params.end()) {
		return NULL;
	}

	return &params[input];
}

map< string, Param > &EventParams::getParams() {
	return params;
}

void EventParams::addParam(string key, Param param) {
	params[key] = param;
}

Param &EventParams::operator[](const string &input) {
	return params[input];
}

string EventParams::toString() {
	stringstream ss;

	for (pair<string, Param> p : params) {
		ss << "{'" << p.first << "':";

		if (p.second.isString()) {
			ss << "'" << p.second.string() << "'";
		}
		else {
			ss << p.second.number();
		}

		ss << "},";
	}

	return ss.str();
}

Param::Param()
	: s(false), n(false)
{}

Param::Param(std::string value)
	: s(true), n(false), stringValue(value)
{}

Param::Param(double value)
	: s(false), n(true), numberValue(value)
{}

bool Param::isNumber() const {
	return this->n;
}

bool Param::isString() const {
	return this->s;
}

string Param::string() const {
	return stringValue;
}

double Param::number() const {
	return this->numberValue;
}

Param &Param::operator=(std::string stringValue) {
	s = true;
	n = false;
	this->stringValue = stringValue;
	return *this;
}

Param &Param::operator=(double numberValue) {
	s = false;
	n = true;
	this->numberValue = numberValue;
	return *this;
}


EventDbPurgeCron::EventDbPurgeCron(EventDb *eventDb)
	: eventDb(eventDb), CronJob(60 * 5, "EVENT_DB_PURGE_CRON", true) {}

void EventDbPurgeCron::run() {
	eventDb->purgeOldEvents();
}

EventHandlerWorker::EventHandlerWorker(EventDb *eventDb)
	: Lockable(), eventDb(eventDb), running(false) {}

void EventHandlerWorker::process() {
	while (true) {
		holdLock();

		if (workQueue.empty()) {
			releaseLock();
			break;
		}

		EventPtr event = workQueue.front();
		workQueue.pop();
		releaseLock();

		eventDb->runhandlers(event);
	}

	running = false;
}

void EventHandlerWorker::push(EventPtr event) {
	holdLock();
	workQueue.push(event);

	if (!running) {
		running = true;
		releaseLock();
		boost::thread(boost::bind(&EventHandlerWorker::process, this));
	}
	else {
		releaseLock();
	}
}


