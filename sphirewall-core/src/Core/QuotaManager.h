#ifndef QUOTA_MANAGER_H
#define QUOTA_MANAGER_H

#include "Core/ConfigurationManager.h"
#include "Core/Cron.h"
#include "SFwallCore/ConnTrackerDeleteHook.h"

class UserDb;
class GroupDb;
class User;
class Group;
class EventDb;
typedef boost::shared_ptr<Group> GroupPtr;
typedef boost::shared_ptr<User> UserPtr;

class QuotaCounter {
        public:
		QuotaCounter(){
			dayTotal = 0;
			weekTotal = 0;
			monthTotal = 0;
			cumulativeTotal = 0;

			lastUpdateDayOfYear = 0;
			lastUpdateWeekStart = 0;
			lastUpdateMonth = 0;
		}
		void increment(double size);

		//Intelligent get methods
		double getTotalTransfer();
		double getDayTotalTransfer();
		double getWeekTotalTransfer();
		double getMonthTotalTransfer();

	private:
		double dayTotal;
		double weekTotal;
		double monthTotal;
		double cumulativeTotal;

		int lastUpdateDayOfYear;
		int lastUpdateWeekStart;
		int lastUpdateMonth;
};

class QuotaInfo {
	public:
		QuotaInfo() {
			dailyQuota = false;
			dailyQuotaLimit = 0;
			weeklyQuota = false;
			weeklyQuotaLimit = 0;
			monthQuota = false;
			monthQuotaLimit = 0;
			totalQuota = false;
			totalQuotaLimit = 0;
			timeQuota = false;
			timeQuotaLimit = 0;
			quotaExceeded = false;
		}

		bool dailyQuota;
		long dailyQuotaLimit;

		bool weeklyQuota;
		long weeklyQuotaLimit;

		bool monthQuota;
		long monthQuotaLimit;

		bool totalQuota;
		long totalQuotaLimit;

		bool timeQuota;
		long timeQuotaLimit;
		bool quotaExceeded;
};

class QuotaManager : public SFwallCore::DeleteConnectionHook, public Configurable, public CronJob {
	public:

		QuotaManager(UserDb *userDb, GroupDb *groupD, EventDb* eventDb);

		//DeleteConnectionHook callback method 
		void hook(SFwallCore::Connection *conn);

		//CronJob callback method
		void run();

		bool evaluateQuotaForUser(UserPtr user);
		bool evaluateQuotaForUser(int baseTime, UserPtr user);
		bool evaluateGlobalQuotas();

		bool load();
		void save();

		QuotaInfo *getQuota() {
			return globalQuota;
		}

		const char* getConfigurationSystemName(){
			return "Quota monitor";
		}

		bool isGlobalQuotaExceeded(){
			return globalQuota->quotaExceeded;
		}

	protected:
		UserDb *userDb;
		GroupDb *groupDb;
		QuotaInfo *globalQuota;
		QuotaCounter *globalCounter;
		EventDb* eventDb;

		void alertOnQuota(UserPtr user, bool exceeded);
		void alertOnQuota(bool exceeded);
};

#endif
