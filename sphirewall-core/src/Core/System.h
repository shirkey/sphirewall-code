/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SYSTEM_H_INCLUDED
#define SPHIREWALL_SYSTEM_H_INCLUDED

#include "Cron.h"
#include "Config.h"
#include "Core/ConfigurationManager.h"
#include "Core/Logger.h"
#include "Utils/TimeManager.h"

class AuthenticationManager;
class Dhcp3ConfigurationManager;
class ConnectionManager;
class OpenvpnManager;
class WirelessConfiguration;
class CloudManagementConnection;
class HostDiscoveryService;
class GlobalDynDns;
class SessionDb;
class GroupDb;
class UserDb;
class IDS;
class SysMonitor;
class BandwidthDbPrimer;

class EventDb;
class QuotaManager;
class EventDb;
class InterfaceManager;
class DNSConfig;
class WinWmiAuth;
class TimePeriodStore;
class DnsmasqManager;
class IPSecManager;
class VpnManager;
class DynamicRouteManager;
class IntMgr;
class NRouteManager;

namespace SFwallCore {
	class Firewall;
};

class System {
		friend class CloudManagementConnection;
	public:
		std::string version;
		std::string versionName;

		/*!Return an instance of System*/
		static System *getInstance();
		static void sysRelease();

		void init();
		void shutdown();

		pid_t getProcessId();
		bool ALIVE;
		string lockFile;

		ConfigurationManager configurationManager;
		Config config;

		TimePeriodStore *periods;
		SFwallCore::Firewall *getFirewall() {
			return sFirewall;
		}

		BandwidthDbPrimer *getBandwidthDbPrimer() {
			return bandwidthDb;
		}

		IDS *getIds() {
			return ids;
		}

		GroupDb *getGroupDb() {
			return groupDb;
		}

		UserDb *getUserDb() {
			return userDb;
		}

		SessionDb *getSessionDb() {
			return sessionDb;
		}

		HostDiscoveryService *getArp() {
			return arp;
		}

		CronManager *getCronManager() {
			return cronManager;
		}

		EventDb *getEventDb() {
			return events;
		}

		WirelessConfiguration *getWirelessConfiguration() {
			return wirelessConfiguration;
		}

		CloudManagementConnection *getCloudConnection() {
			return cloud;
		}

		void setCloudConnection(CloudManagementConnection *cloud) {
			this->cloud = cloud;
		}

		GlobalDynDns *getGlobalDynDns() {
			return globalDynDns;
		}

		QuotaManager *getQuotaManager() {
			return quotaManager;
		}

		ConfigurationManager *getConfigurationManager() {
			return &configurationManager;
		}

		void setSessionDb(SessionDb *sessionDb) {
			this->sessionDb = sessionDb;
		}

		TimeManager &getTimeManager() {
			return timeManager;
		}

		DnsmasqManager *getDnsmasq() {
			return dnsmasq;
		}

		IPSecManager* getIPSecManager(){
			return ipsecManager;
		}

		VpnManager* getVpnManager(){
			return vpnManager;
		}

		DynamicRouteManager* getDynamicRouteManager(){
			return dynamicRouteManager;
		}

		IntMgr* get_interface_manager(){
			return interface_manager;
		}

		NRouteManager* get_route_manager(){
			return route_manager;
		}	

		DNSConfig* get_dns_config(){
			return dnsConfig;
		}
		WinWmiAuth *wmic;
	private:
		System();
		System(const System &);
		System &operator=(const System &);
		virtual ~System();
		static System *m_instance;

		void createLock(std::string lockfile);
		void removeLock(std::string lockfile);
		bool verifyLock(std::string lockfile);
		pid_t retLockPid(string lockFile);

		//Processes
		SysMonitor *sysMonitor;
		SFwallCore::Firewall *sFirewall;
		Dhcp3ConfigurationManager *dhcpConfigurationManager;
		OpenvpnManager *openvpn;
		BandwidthDbPrimer *bandwidthDb;
		IDS *ids;

		GroupDb *groupDb;
		UserDb *userDb;
		EventDb *events;
		SessionDb *sessionDb;
		HostDiscoveryService *arp;
		DNSConfig* dnsConfig;
		ConnectionManager *connectionManager;

		CronManager *cronManager;
		AuthenticationManager *authenticationManager;
		LoggingConfiguration *loggingConfig;

		WirelessConfiguration *wirelessConfiguration;
		CloudManagementConnection *cloud;
		GlobalDynDns *globalDynDns;
		QuotaManager *quotaManager;
		TimeManager timeManager;
		DnsmasqManager *dnsmasq;
		IPSecManager* ipsecManager;
		VpnManager* vpnManager;
		DynamicRouteManager* dynamicRouteManager;
		IntMgr* interface_manager;
		NRouteManager* route_manager;
};

#endif
