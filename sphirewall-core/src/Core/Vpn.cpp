#include <iostream>

using namespace std;

#include "Core/Vpn.h"
#include "Core/IPSec.h"
#include "Core/Openvpn.h"

list<VpnInstancePtr> VpnManager::list_instances(){
	list<VpnInstancePtr> instances;
	for(VpnInstancePtr instance : ipsec->list_instances()){
		instances.push_back(instance);
	}

	for(VpnInstancePtr instance : openvpn->listInstances()){
		instances.push_back(instance);
	}

	return instances;
}

void VpnManager::add_instance(std::string name, int type){
	if(type == VPN_IPSEC_GATEWAY_TO_GATEWAY){
		IPSecGatewayToGatewayInstance* instance = new IPSecGatewayToGatewayInstance();
		instance->name = name;
		ipsec->add(IPSecInstancePtr(instance));
	}else if(type == VPN_OPENVPN_SERVER_STATIC_KEYS){
		OpenvpnStaticKeys* instance = new OpenvpnStaticKeys();
		instance->name = name;
		openvpn->addInstance(OpenvpnPtr(instance));
	}else if(type == VPN_OPENVPN_SERVER_TLS){
		OpenvpnTlsKeys* instance = new OpenvpnTlsKeys();
		instance->name = name;
		openvpn->addInstance(OpenvpnPtr(instance));
	}
}

void VpnManager::delete_instance(VpnInstancePtr instance){
	if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
		IPSecInstancePtr i = boost::dynamic_pointer_cast<IPSecInstance>(instance);
		ipsec->remove(i);
        }else if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
		OpenvpnPtr i = boost::dynamic_pointer_cast<Openvpn>(instance);
                openvpn->removeInstance(i);
        }
}

void VpnManager::do_connect(VpnInstancePtr instance){
	if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
		ipsec->do_connect(boost::dynamic_pointer_cast<IPSecInstance>(instance));
	}else if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
		openvpn->do_connect(boost::dynamic_pointer_cast<Openvpn>(instance));
	}
}

void VpnManager::do_disconnect(VpnInstancePtr instance){
	if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
		ipsec->do_disconnect(boost::dynamic_pointer_cast<IPSecInstance>(instance));
	}else if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
		openvpn->do_disconnect(boost::dynamic_pointer_cast<Openvpn>(instance));
	}
}

int VpnManager::status(VpnInstancePtr instance){
        if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
                return ipsec->status(boost::dynamic_pointer_cast<IPSecInstance>(instance));
        }else if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
                return openvpn->status(boost::dynamic_pointer_cast<Openvpn>(instance));
        }
	return -1;
}

VpnInstancePtr VpnManager::get_instance(std::string name){
	for(VpnInstancePtr instance : list_instances()){
		if(instance->name.compare(name) == 0){
			return instance;
		}
	}

	return VpnInstancePtr();
}

std::string VpnManager::log(VpnInstancePtr instance){
        if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
                return ipsec->log(boost::dynamic_pointer_cast<IPSecInstance>(instance));
        }else if(instance->type() == VPN_OPENVPN_SERVER_STATIC_KEYS || instance->type() == VPN_OPENVPN_SERVER_TLS){
                return openvpn->log(boost::dynamic_pointer_cast<Openvpn>(instance));
        }
        return "";
}

void VpnManager::save(){
	ipsec->save(configurationManager);
	openvpn->save(configurationManager);
}

bool VpnManager::load(){
	ipsec->load(configurationManager);
	openvpn->load(configurationManager);
}

