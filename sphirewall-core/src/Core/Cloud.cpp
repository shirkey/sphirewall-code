/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/asio.hpp>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/asio/ssl.hpp>

using namespace boost::algorithm;

#include "Core/Cloud.h"
#include "Api/JsonManagementService.h"
#include "Core/System.h"
#include "Core/ConfigurationManager.h"
#include "Utils/FileUtils.h"
#include "Utils/StringUtils.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Criteria.h"

using namespace std;

int CloudManagementConnection::CLOUD_CONNECTION_POOL_SIZE = 1;
CloudManagementConnection::CloudManagementConnection()
	: connectionCount(0), instanceid(StringUtils::genRandom()){ 

		System::getInstance()->config.getRuntime()->loadOrPut("CLOUD_CONNECTION_POOL_SIZE", &CLOUD_CONNECTION_POOL_SIZE);	
	}

std::string CloudManagementConnection::getHostname() {
	if (configurationManager->hasByPath("cloud:hostname")) {
		return configurationManager->get("cloud:hostname")->string();
	}

	//deprecated properties
	return "api.linewize.net";
}

std::string CloudManagementConnection::getKey() {
	if (configurationManager->hasByPath("cloud:key")) {
		return configurationManager->get("cloud:key")->string();
	}

	return "";
}

std::string CloudManagementConnection::deviceid = "";
std::string CloudManagementConnection::getDeviceId() {
	if (deviceid.size() == 0) {
		if (configurationManager->hasByPath("cloud:deviceid")) {
			deviceid = configurationManager->get("cloud:deviceid")->string();
			if(deviceid.size() > 0){
				return deviceid;
			}
		}

		deviceid = FileUtils::read("/sys/class/net/eth0/address");
		trim(deviceid);
	}

	return deviceid;
}

std::string CloudManagementConnection::getWatchdogId(){
	string ret = FileUtils::read("/sys/class/net/eth0/address");
	trim(ret);
	return ret;
}

int CloudManagementConnection::getPort() {
	if (configurationManager->hasByPath("cloud:port")) {
		return configurationManager->get("cloud:port")->number();
	}

	return 8084;
}

bool CloudManagementConnection::enabled() {
	if (configurationManager->hasByPath("cloud:enabled")) {
		return configurationManager->get("cloud:enabled")->boolean();
	}

	return false;
}

bool CloudManagementConnection::verify() {
	if (configurationManager->hasByPath("cloud:verify")) {
		return configurationManager->get("cloud:verify")->boolean();
	}

	return true;
}


bool CloudManagementConnection::connected() {
	return this->connectionCount > 0;
}

typedef boost::shared_ptr<boost::asio::io_service> io_service_ptr;

void CloudManagementConnection::single_connector(JsonManagementService *service){
	connectionCount++;
	try {
		boost::asio::io_service io;
		stringstream ss; ss << getPort();
		ssl::context ctx(ssl::context::sslv23);

		if(verify()){
			ctx.set_verify_mode(boost::asio::ssl::verify_peer | boost::asio::ssl::verify_fail_if_no_peer_cert);
			ctx.set_default_verify_paths();
		}

		ssl_socket sock(io, ctx);
		if(verify()){
			sock.set_verify_callback(ssl::rfc2818_verification(getHostname()));
		}

		tcp::resolver resolver(io);
		tcp::resolver::query query(tcp::v4(), getHostname(), ss.str());
		boost::asio::connect(sock.lowest_layer(), resolver.resolve(query));

		sock.lowest_layer().set_option(boost::asio::socket_base::keep_alive(true));
		sock.lowest_layer().set_option(tcp::no_delay(true));
		sock.handshake(ssl_socket::client);

		Logger::instance()->log("sphirewalld.cloud.ssl", INFO, "Connecting to a sphirewall cloud provider at %s:%d", getHostname().c_str(), getPort());
		stringstream auth; 
		auth << "{\"key\":\"" << getKey() << "\", \"deviceid\":\"" << getDeviceId() << "\",\"sphirewallid\":\""<< instanceid << "\", \"watchdogid\":\"" << getWatchdogId()<< "\"}\n";
		boost::asio::write(sock, boost::asio::buffer(auth.str(), auth.str().size()));

		Logger::instance()->log("sphirewalld.cloud.ssl",  INFO, "Connected to cloud provider, ready to accept rpc calls");
		while (1) {
			boost::asio::streambuf response;
			boost::asio::read_until(sock, response, "\n");
			string buffer = string((istreambuf_iterator<char>(&response)), istreambuf_iterator<char>());
			pair<string, int> output = service->process(buffer, getHostname());
			output.first += "\n";

			Logger::instance()->log("sphirewalld.cloud.ssl",  DEBUG, "returned %s", output.first.c_str());
			boost::asio::write(sock, boost::asio::buffer(output.first, output.first.size()));
		}

		delete service;
		io.stop();
	}
	catch (exception &e) {
		Logger::instance()->log("sphirewalld.cloud", ERROR, "Connection to the cloud service could not be established or was dropped due to %s", e.what());
		
	}

	connectionCount--;
}

void CloudManagementConnection::connect() {
	JsonManagementService *service = new JsonManagementService();
	service->setConfig(&System::getInstance()->config);
	service->setIds(System::getInstance()->ids);
	service->setSysMonitor(System::getInstance()->sysMonitor);
	service->setEventDb(System::getInstance()->events);
	service->setInterfaceManager(System::getInstance()->get_interface_manager());
	service->setDnsConfig(System::getInstance()->dnsConfig);
	service->setFirewall(System::getInstance()->sFirewall);
	service->setGroupDb(System::getInstance()->groupDb);
	service->setUserDb(System::getInstance()->userDb);
	service->setSessionDb(System::getInstance()->sessionDb);
	service->setHostDiscoveryService(System::getInstance()->arp);
	service->setLoggingConfiguration(System::getInstance()->loggingConfig);
	service->setOpenvpn(System::getInstance()->openvpn);
	service->setDhcp(System::getInstance()->dhcpConfigurationManager);
	service->setAuthenticationManager(System::getInstance()->authenticationManager);
	service->setConnectionManager(System::getInstance()->connectionManager);
	service->setIPSecManager(System::getInstance()->ipsecManager);
	service->setVpnManager(System::getInstance()->vpnManager);
	service->ignoreAuthentication(true);
	service->initDelegate();	

	while (System::getInstance()->ALIVE) {
		if(enabled() && connectionCount < CLOUD_CONNECTION_POOL_SIZE){
			Logger::instance()->log("sphirewalld.cloud", INFO, "Cloud connection pool is less than CLOUD_CONNECTION_POOL_SIZE, creating connection");
			boost::thread(boost::bind(&CloudManagementConnection::single_connector, this, service));
		}			

		sleep(2);
	}

}

void CloudManagementConnection::add_rules(SFwallCore::ACLStore* store){
	SFwallCore::ProvidedFilterRule* rule = new SFwallCore::ProvidedFilterRule(this);
	SFwallCore::DestinationIpCriteria* dest = new SFwallCore::DestinationIpCriteria();
	dest->ip = IP4Addr::stringToIP4Addr("54.206.2.66");	
	dest->mask = IP4Addr::stringToIP4Addr("255.255.255.255");	
	rule->criteria.push_back(dest);

	SFwallCore::DestinationPortCriteria* ports = new SFwallCore::DestinationPortCriteria();
	ports->ports.insert(8084);
	ports->ports.insert(8184);
	ports->ports.insert(80);
	ports->ports.insert(443);
	rule->criteria.push_back(ports);
	rule->action = SQ_ACCEPT;
	rule->ignore_application_layer_filters = true;
	store->add_provided_rule(rule);

        SFwallCore::ProvidedFilterRule* dns_rule = new SFwallCore::ProvidedFilterRule(this);
        SFwallCore::DestinationPortCriteria* dns_ports = new SFwallCore::DestinationPortCriteria();
        dns_ports->ports.insert(53);
        dns_rule->criteria.push_back(dns_ports);
        dns_rule->action = SQ_ACCEPT;
	dns_rule->ignore_application_layer_filters = true;
        store->add_provided_rule(dns_rule);
}

bool CloudManagementConnection::load(){
	System::getInstance()->getFirewall()->acls->register_filtering_rule_provider(this);
}
