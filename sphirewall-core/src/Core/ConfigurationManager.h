/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONFIGURATION_MANAGER_H
#define CONFIGURATION_MANAGER_H

#include "Json/JSON.h"
#include "Json/JSONValue.h"

#include <vector>
#include <map>
#include <list>
#include <sstream>
#include <unordered_map>
#include <memory>

class Lock;
class ObjectWrapper;
class ObjectContainer;
namespace SFwallCore {
	class ApplicationLayerTracker;
};

enum ObjectContainerType {
	CREL, CARRAY
};

class Serialiser {
	public:
		virtual ~Serialiser() {}
		virtual std::string serialize(ObjectWrapper *input) = 0;
		virtual ObjectWrapper *parse(std::string input) = 0;
};

class JsonSerialiser : public virtual Serialiser {
	public:
		~JsonSerialiser();
		JsonSerialiser();

		static ObjectWrapper *parseJsonString(std::string input);
		static std::string serializeToJsonString(ObjectWrapper *input, bool format = false);

		std::string serialize(ObjectWrapper *input);
		ObjectWrapper *parse(std::string input);

		void setFormat(bool format);
	private:
		void recurseObject(std::stringstream &root, ObjectContainer *input);
		ObjectWrapper *parseRecurse(JSONValue *value);

		bool format;
		int c;
};

class ObjectNotFoundException: public std::exception {
        public:
                ObjectNotFoundException(std::string message){
			this->w = message;
		}
                ~ObjectNotFoundException() throw (){}

                virtual const char *what() const throw (){
			return w.c_str(); 
		}

                std::string message(){
			return w;
		}
        private:
                std::string w;
};

class ObjectContainer {
	public:
		ObjectContainer(ObjectContainerType type);
		~ObjectContainer();

		void put(std::string key, ObjectWrapper *input);
		void put(ObjectWrapper *input);

		ObjectWrapper *get(std::string key);
		ObjectWrapper *get(int index);

		bool has(std::string key);
		int size();

		void foreach(std::function< void (ObjectWrapper *) > perform);

		ObjectContainerType getType() const;

		std::map<std::string, ObjectWrapper *> &getKeyObjects();
		std::vector<ObjectWrapper*> &getListObjects(){
			return listObjects;
		}
	private:
		ObjectContainerType type;

		std::map<std::string, ObjectWrapper *> keyObjects;
		std::vector<ObjectWrapper *>  listObjects;

};

enum ObjectWrapperType {
	OBJECT, STRING, DOUBLE, BOOL
};

class ObjectWrapper {
	public:
		~ObjectWrapper();
		ObjectWrapper(ObjectContainer *container);
		ObjectWrapper(std::string value);
		ObjectWrapper(double value);
		ObjectWrapper(bool value);

		std::string string() const;
		ObjectContainer *container() const;
		double number() const;
		bool boolean() const;
		bool isString() const;
		bool isNumber() const;
		bool isBoolean() const;
		bool isContainer() const;
		std::string toString() const;

		static ObjectWrapper* parseRecurse(JSONValue *value);
		static JSONValue* convertRecurse(ObjectWrapper* input);	
	private:
		std::string stringValue;
		bool boolValue;
		double numberValue;
		ObjectWrapperType type;
		ObjectContainer *containerValue;
};

class ConfigurationException : public std::exception {
	public:
		ConfigurationException(std::string message);
		~ConfigurationException() throw ();

		virtual const char *what() const throw ();
		std::string message();
	private:
		std::string w;
};

class ConfigurationManager;

class ConfigurationCallback {
	public:
		virtual ~ConfigurationCallback() {};
		virtual void run() = 0;
};

class Configurable {
	public:
		Configurable(){
			this->configurationManager = NULL;
		}

		virtual bool load() = 0;
		virtual const char* getConfigurationSystemName() = 0;
		void setConfigurationManager(ConfigurationManager* configurationManager){
			this->configurationManager = configurationManager;
		}

	protected:
		ConfigurationManager* configurationManager;
};

class ConfigurationManager {
	public:
		static int READ_ONLY_CONFIGURATION;

		ConfigurationManager();
		~ConfigurationManager();

		int put(std::string path, std::string value);
		int put(std::string path, bool value);
		int put(std::string path, double value);

		int defaultIfUnset(std::string path, std::string value);
		int defaultIfUnset(std::string path, bool value);
		int defaultIfUnset(std::string path, double value);

		ObjectWrapper *get(std::string path);
		std::string getAsString(std::string p, std::string defaultVal = "");
		bool hasByPath(std::string path);

		std::string dump();
		void save();
		bool load(std::string filename);
		bool load(std::string persistantFilename, std::string templateFilename);
		bool loadFromString(std::string input);

		bool has(std::string);
		ObjectContainer *getElement(std::string);
		void setElement(std::string key, ObjectContainer *object);

		void registerConfigurable(Configurable* entity);
		void registerChangeHandler(std::string, std::shared_ptr<ConfigurationCallback> callback);
		void triggerChangeHandlers();

		bool backup();
		void initDefaults();

		int configuration_version;
	private:
		int put(std::string path, ObjectWrapper *value);
		ObjectWrapper *rootValue;
		std::string filename;
		std::unordered_map<std::string, std::shared_ptr<ConfigurationCallback>> configurationCallbacks;
		
		std::list<Configurable*> configurables;
		void loadConfigurables();
};

#endif
