/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Host.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"

string Host::getIp() {
	if (type == 0) {
		return IP4Addr::ip4AddrToString(ip);
	}
	else if (type == 1) {
		return IP6Addr::toString(&ipV6);
	}

	return "invalid";
}

void Host::fetchHostname() {
	hostnameCache = IP4Addr::hostname(ip);
}

std::string Host::hostname() {
	return hostnameCache;
}
