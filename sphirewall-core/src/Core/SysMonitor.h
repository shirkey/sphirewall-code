/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SYSMONITOR_H_INCLUDED
#define SPHIREWALL_SYSMONITOR_H_INCLUDED

#include <vector>
#include <set>
#include <map>
#include <string>

#include "Core/Cron.h"
#include "Core/ConfigurationManager.h"

class UserDb;
class GroupDb;
class User;
typedef boost::shared_ptr<User> UserPtr;
class BandwidthDbPrimer;
class Sampler;
class EventDb;
class QuotaInfo;
class ConfigurationManager;
namespace SFwallCore {
	class Firewall;
};

class Watch {
	public:
		virtual void poll() = 0;

		void setEventDb(EventDb *eventDb) {
			this->eventDb = eventDb;
		}

	protected:
		EventDb *eventDb;
};

class MetricsWatch : public Watch {
	public:
		MetricsWatch(SFwallCore::Firewall *firewall, BandwidthDbPrimer *bandwidthDb) :
			firewall(firewall), bandwidthDb(bandwidthDb) {}

		void poll();
		std::map<std::string, double> getSnapshot();

	private:
		SFwallCore::Firewall *firewall;
		BandwidthDbPrimer *bandwidthDb;
		std::map<std::string, Sampler *> registeredSamplers;
};

class InternetConnectionWatch : public Watch {
	public:
		InternetConnectionWatch(ConfigurationManager *config) : Watch() {
			this->config = config;
			this->state = false;
		}

		void poll();
	private:
		ConfigurationManager *config;
		bool state;
};

class MetricSampler {
	public:
		virtual void sample(std::map<std::string, double> &input) = 0;
};

class MetricHandler {
	public:
		virtual void operate(std::map<std::string, double> input) = 0;
		virtual bool enabled(){
			return true;
		}
};

class LoggingMetricHandler : public MetricHandler {
	public:
		void operate(std::map<std::string, double> input);	
};	

class AnaEngineMetricHandler : public MetricHandler {
        public:
                void operate(std::map<std::string, double> input);
};

class SysMonitor {
	public:
		SysMonitor() {}
		SysMonitor(EventDb *eventDb, CronManager *cronManager, SFwallCore::Firewall *firewall, BandwidthDbPrimer *bandwidthDb);

		void registerMetric(MetricSampler *sampler);
		void registerMetricHandler(MetricHandler* handler);
	private:
		EventDb *eventDb;
		std::list<Watch *> registeredWatches;
		MetricsWatch *metrics;
		SFwallCore::Firewall *firewall;

		std::list<MetricSampler *> registeredSamplers;
		class SysMonitorCron : public CronJob {
			public:

				SysMonitorCron(SysMonitor *monitor) : CronJob(60, "SYSMONITOR_CRON", true), monitor(monitor) {
				}

				void run() {
					monitor->pollall();
				}

			private:
				SysMonitor *monitor;
		};

		void pollall();
		std::list<MetricHandler*> metricHandlers;
};

#endif
