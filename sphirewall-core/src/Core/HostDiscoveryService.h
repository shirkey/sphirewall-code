/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALLD_ARP_H
#define SPHIREWALLD_ARP_H

#include <map>
#include <vector>
#include <list>
#include <netinet/in.h>
#include <string>
#include <boost/unordered_map.hpp>

#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Core/Lockable.h"
#include "Core/Host.h"

class EventDb;
class Lock;
namespace SFwallCore {
	class Packet;
};

class HostDnsHostnameResolverWorker : public Lockable {
	public:
		HostDnsHostnameResolverWorker();
		void push(HostPtr);
	private:
		void process();
		std::queue<HostPtr> workQueue;
		bool running;
};

class HostDiscoveryService : public Lockable {
	public:

		class CleanUpCron : public CronJob {
			public:

				CleanUpCron(HostDiscoveryService *root) : root(root), CronJob(60 * 30, "HOST_DISCOVERY_CLEANUP_CRON", true) {}
				void run();

			private:
				HostDiscoveryService *root;
		};

		HostDiscoveryService(CronManager *cron, Config *config);
		HostDiscoveryService();
		std::string get(std::string ip);
		HostPtr get(in_addr_t ip);

		HostPtr update(SFwallCore::Packet *packet);
		HostPtr update(unsigned int ip, std::string mac);
		HostPtr update(struct in6_addr *ip, std::string mac);

		void list(std::list<HostPtr> &entries);

		int size();
		long updates();

		void setEventDb(EventDb *eventDb);

		HostPtr getByMac(std::string mac, in_addr_t ip);
		HostPtr getByMac(std::string mac, struct in6_addr *ip);
	private:
		boost::unordered_map<std::string, std::list<HostPtr> > entryTable;
		EventDb *eventDb;

		HostPtr add(std::string mac, in_addr_t ip);

		HostPtr add(std::string mac, struct in6_addr *ip);
		HostPtr get(struct in6_addr *ip);

		static int HOST_DISCOVERY_SERVICE_TIMEOUT;
		int _size;
		long _updates;

		HostDnsHostnameResolverWorker *dnsHostnameWorker;
};

#endif
