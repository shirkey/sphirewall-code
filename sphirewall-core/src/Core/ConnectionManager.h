/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONNECTIONMANAGER_H
#define	CONNECTIONMANAGER_H

#include <list>
#include <string>
#include "Core/ConfigurationManager.h" 

class ConfigurationManager;
enum ConnectionType {
	PPPoE = 0, OPENVPN = 1
};

enum ConnectionAuthenticationType {
	PAP = 0, CHAP = 1, KEYFILE = 2
};

class AuthenticationCredentials {
	public:
		std::string username;
		std::string password;
		ConnectionAuthenticationType authenticationType;
		std::string keyfile;
};

class Connection {
	public:
		virtual ConnectionType type() = 0;

		virtual ~Connection() {
		}

		AuthenticationCredentials &getAuthenticationCredentials() {
			return credentials;
		}

		void setAuthenticationCredentials(AuthenticationCredentials cred) {
			this->credentials = cred;
		}

		virtual bool isConnected() = 0;
		virtual void connect() = 0;
		virtual void disconnect() = 0;

		std::string getName() {
			return name;
		}

		void setName(std::string name) {
			this->name = name;
		}

		std::string getDevice() {
			return device;
		}

		void setDevice(std::string dev) {
			this->device = dev;
		}

		virtual void publishSettings() = 0;
		virtual std::string log() {
			return "";
		}
	private:
		AuthenticationCredentials credentials;
		std::string name;
		std::string device;
};

class PPPoE_Connection : public virtual Connection {
	public:

		~PPPoE_Connection() {
		}

		bool isConnected();
		void connect();
		void disconnect();
		void publishSettings();

		ConnectionType type() {
			return PPPoE;
		}

		std::string log();
};

class Openvpn_Connection : public virtual Connection {
	public:
		~Openvpn_Connection() {}

		bool isConnected();
		void connect();
		void disconnect();
		void publishSettings();

		ConnectionType type() {
			return OPENVPN;
		}

		std::string log();
	private:
		std::string getConfigFile();
		std::string getPidFile();
		std::string getLogFilename();
};

class ConnectionManager : public Configurable {
	public:
		std::list<Connection *> getConnections();

		void deleteConnection(Connection *target);
		void createConnection(std::string name, ConnectionType type);
		Connection *getConnection(std::string name);

		bool load();
		void save();

		void publishAuthenticationCredentials();

                const char* getConfigurationSystemName(){
                        return "Connection manager";
                }

	private:
		std::list<Connection *> connections;
};

#endif

