#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "Core/QuotaManager.h"
#include "Core/System.h"
#include "Utils/TimeWrapper.h"

TEST(QuotaCounter, increment_basic){
	QuotaCounter* counter = new QuotaCounter();
	counter->increment(100);

	EXPECT_TRUE(counter->getTotalTransfer() == 100);
	EXPECT_TRUE(counter->getDayTotalTransfer() == 100);
	EXPECT_TRUE(counter->getWeekTotalTransfer() == 100);
	EXPECT_TRUE(counter->getMonthTotalTransfer() == 100);

	counter->increment(1);

	EXPECT_TRUE(counter->getTotalTransfer() == 101);
	EXPECT_TRUE(counter->getDayTotalTransfer() == 101);
	EXPECT_TRUE(counter->getWeekTotalTransfer() == 101);
	EXPECT_TRUE(counter->getMonthTotalTransfer() == 101);
}

TEST(QuotaCounter, increment_test_day_logic){
        QuotaCounter* counter = new QuotaCounter();
        counter->increment(100);

        //Go forward a day:
        System::getInstance()->getTimeManager().useThisTime(time(NULL) + 60*60*24);

	counter->increment(1);
	counter->increment(1);
	EXPECT_TRUE(counter->getDayTotalTransfer() == 2);	

	System::getInstance()->getTimeManager().useThisTime(time(NULL) + 60*60*24 * 2);
	counter->increment(1);
	EXPECT_TRUE(counter->getDayTotalTransfer() == 1);
}

TEST(QuotaCounter, increment_test_week_logic){
        QuotaCounter* counter = new QuotaCounter();
        counter->increment(100);

        //Go forward a day:
	Time* thisWeek = Time::startOfWeek(time(NULL));

        System::getInstance()->getTimeManager().useThisTime(thisWeek->timestamp() + 60 * 60 * 24);
        EXPECT_TRUE(counter->getWeekTotalTransfer() == 100);

	counter->increment(1);
	counter->increment(1);
	EXPECT_TRUE(counter->getWeekTotalTransfer() == 102);	

	//Go forward two days
	System::getInstance()->getTimeManager().useThisTime(thisWeek->timestamp() + 60 * 60 * 24 * 2);
        counter->increment(1);
        EXPECT_TRUE(counter->getWeekTotalTransfer() == 103);

	//Next week
	System::getInstance()->getTimeManager().useThisTime(thisWeek->timestamp() + 60 * 60 * 24 * 8);
        counter->increment(1);

        EXPECT_TRUE(counter->getWeekTotalTransfer() == 1);
}

TEST(QuotaCounter, increment_test_month_logic){
        QuotaCounter* counter = new QuotaCounter();
        counter->increment(100);

        EXPECT_TRUE(counter->getMonthTotalTransfer() == 100);

	counter->increment(1);
	counter->increment(1);
	EXPECT_TRUE(counter->getMonthTotalTransfer() == 102);	

	//Go forward two days
	System::getInstance()->getTimeManager().useThisTime(time(NULL) + 60 * 60 * 24 * 2);
        counter->increment(1);
        EXPECT_TRUE(counter->getMonthTotalTransfer() == 103);

	//Go forward 40 days
	System::getInstance()->getTimeManager().useThisTime(time(NULL) + 60 * 60 * 24 * 40);
        counter->increment(1);

        EXPECT_TRUE(counter->getMonthTotalTransfer() == 1);
}


