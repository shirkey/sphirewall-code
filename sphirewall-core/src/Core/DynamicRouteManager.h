#ifndef DYNAMIC_ROUTE_MANAGER_H
#define DYNAMIC_ROUTE_MANAGER_H

class DynamicRouteDescription {
	public:
		std::string gw;
		std::string destination;
		int route_table_id;
};

class DynamicRouteManager {
	public:
		void flush_route_table(int route_table_id);
		void add_route_table(int route_table_id);
		void remove_route_table(int route_table_id);

		void add_route(DynamicRouteDescription route);
		void remove_route(DynamicRouteDescription route);
};

#endif
