/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OPENVPN_H
#define OPENVPN_H

#include <list>
#include <string>
#include "Core/Process.h"
#include "Core/Vpn.h"

class ConfigurationManager;
class OpenvpnClientConf {
	public:
		std::string file;
		std::string name;
};

class Openvpn : public virtual VpnInstance {
        public:
                Openvpn() {
			auto_start = false;
                        remoteport = 1194;
                }

                virtual void installbase() = 0;
                void uninstallbase();
                virtual void publish() = 0;

                void connect();
                void disconnect();
                int status();

                std::string log();

                int remoteport;
		std::string remoteserverip;
                bool compression;
		string customopts;
                std::string serverip;
                std::string clientip;
                std::string servermask;
		bool auto_start;
        protected:
                std::string getPath();
                std::string getPidFile();
                std::string getLogFile();

};

class OpenvpnStaticKeys : public virtual Openvpn {
	public:
		int type() {
			return VPN_OPENVPN_SERVER_STATIC_KEYS;
		}

		void publish();
		void installbase();
		std::string generate_client_configuration();
};

class OpenvpnTlsKeys : public virtual Openvpn {
	public:
		int type() {
			return VPN_OPENVPN_SERVER_TLS;
		}

                OpenvpnClientConf *createClient(std::string name);
                void deleteClient(OpenvpnClientConf *);
                std::list<OpenvpnClientConf *> &getClients();
		std::string generateUserFile(std::string name);

		void publish();
		void installbase();

		//Certification Details
                std::string certcountry;
                std::string certprovince;
                std::string certcity;
                std::string certorg;
                std::string certemail;

	private:
                std::list<OpenvpnClientConf *> clients;
};

typedef boost::shared_ptr<Openvpn> OpenvpnPtr;
class OpenvpnManager {
	public:
		void save(ConfigurationManager* configurationManager);
		bool load(ConfigurationManager* configurationManager);

		void start();
		void stop();

		OpenvpnPtr getInstance(std::string name);
		void addInstance(OpenvpnPtr instance);
		void removeInstance(OpenvpnPtr instance);

		std::list<OpenvpnPtr> &listInstances() {
			return instances;
		}

		bool daemonExists();

		void do_connect(OpenvpnPtr target);
		void do_disconnect(OpenvpnPtr target);
		int status(OpenvpnPtr target);
		std::string log(OpenvpnPtr target){
			return target->log();
		}
	
	private:
		std::list<OpenvpnPtr> instances;
};

#endif
