/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dirent.h"
#include <iostream>
#include <sstream>
#include <unistd.h>

using namespace std;

#include "Core/System.h"
#include "Utils/Utils.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "Auth/UserDb.h"
#include "Core/SignalHandler.h"
#include "SFwallCore/Firewall.h"
#include "Auth/AuthenticationHandler.h"
#include "Api/JsonManagementService.h"
#include "Api/ManagementSocketServer.h"
#include "ConnectionManager.h"
#include "Core/Openvpn.h"
#include "Core/Cloud.h"
#include "Core/Wireless.h"
#include "Core/HostDiscoveryService.h"
#include "Utils/DynDns.h"
#include "autoconf.h"
#include "Core/Event.h"
#include "Ids/Ids.h"
#include "Core/Logger.h"
#include "Utils/WindowsWmiAuthenticator.h"
#include "SFwallCore/TimePeriods.h"
#include "BandwidthDb/CloudAnalyticsListener.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "Utils/TimeManager.h"
#include "Utils/Dnsmasq.h"
#include "Core/IPSec.h"
#include "Core/Vpn.h"
#include "Core/DynamicRouteManager.h"
#include "Utils/Interfaces.h"
#include "Utils/NRoute.h"

System *System::m_instance = 0; // Pointer to THE System instance

System *System::getInstance() {
	if (0 == m_instance)
		m_instance = new System();

	return m_instance;
}

void System::sysRelease() {
	if (m_instance)
		delete m_instance;

	m_instance = 0;
}

System::System()
	: timeManager() {
	version = PACKAGE_VERSION;
	std::stringstream vn;
	vn << GIT_BRANCH << ":" << GIT_VERSION << "@" << BUILD_TIME;
	versionName = vn.str();

	sessionDb = NULL;
	events= NULL;
}

System::~System() {
	delete sFirewall;
	delete arp;
	delete sessionDb;
	delete groupDb;
	delete userDb;
	delete sysMonitor;
	delete bandwidthDb;
	delete ids;
	delete events;
	delete cronManager;
}

pid_t System::getProcessId() {
	lockFile = "/var/lock/sphirewall.lock";
	return retLockPid(lockFile);
}

void System::init() {
	System::getInstance()->ALIVE = true;
	bool loaded = false;
	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "sphirewalld: version %s -- %s pid: %d", version.c_str(), versionName.c_str(), getpid());
	config.setConfigurationManager(&configurationManager);
	lockFile = "/var/lock/sphirewall.lock";

	if (!verifyLock(lockFile)) {
		Logger::instance()->log("sphirewalld.system.init", CONSOLE, "A lock file exists, cannot start the core daemon");
		exit(-1);
	}
	else {
		Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Creating lock file");
		createLock(lockFile);
	}

	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Starting Sphirewall");
	Logger::instance()->setLevel(INFO);
	loggingConfig = new LoggingConfiguration(Logger::instance());
	events = new EventDb(&config);
	periods = new TimePeriodStore();
		
	dnsConfig = new DNSConfig();
	route_manager = new NRouteManager();

	interface_manager = new IntMgr();
	interface_manager->register_change_listener(route_manager);
	dynamicRouteManager = new DynamicRouteManager();

	cronManager = new CronManager(&config);
	cronManager->registerJob(new EventDbPurgeCron(events));

	arp = new HostDiscoveryService(cronManager, &config);
	arp->setEventDb(events);

	ids = new IDS(events);
	cronManager->registerJob(ids);

	groupDb = new GroupDb();
	userDb = new UserDb(events, groupDb, sessionDb);
	groupDb->registerRemovedListener(userDb);

	CloudAnalyticsListener *cloudAnaListener = new CloudAnalyticsListener();
	events->addHandler("", cloudAnaListener);
	cronManager->registerJob(new CloudAnalyticsListenerEventFlushCron(cloudAnaListener));

	bandwidthDb = new BandwidthDbPrimer(userDb, &config);
	bandwidthDb->registerListener(cloudAnaListener);
	bandwidthDb->registerListener(new AnalyticsClient());

	cronManager->registerJob(bandwidthDb);

	sFirewall = new SFwallCore::Firewall(&config);
	sFirewall->setInterfaceManager(interface_manager);

	quotaManager = new QuotaManager(userDb, groupDb, events);
	cronManager->registerJob(quotaManager);

	sysMonitor = new SysMonitor(events, cronManager, sFirewall, bandwidthDb);
	sessionDb = new SessionDb(cronManager, sFirewall->getPlainConnTracker());
	sessionDb->setUserDb(userDb);
	userDb->setSessionDb(sessionDb);

	this->authenticationManager = new AuthenticationManager(cronManager);
	this->authenticationManager->setLocalUserDb(userDb);
	this->authenticationManager->setLocalGroupDb(groupDb);
	cronManager->registerJob(new AuthenticationManager::SyncCron(this->authenticationManager));

	LocalDbAuthenticationMethod *localMethod = new LocalDbAuthenticationMethod();
	localMethod->setUserDb(userDb);
	this->authenticationManager->addMethod(localMethod);

	LdapAuthenticationMethod *ldap = new LdapAuthenticationMethod();
	ldap->setConfigurationManager(&configurationManager);
	this->authenticationManager->addMethod(ldap);

	PamAuthenticationMethod *pam = new PamAuthenticationMethod();
	pam->setConfigurationManager(&configurationManager);
	this->authenticationManager->addMethod(pam);

	BasicHttpAuthenticationHandler *bha = new BasicHttpAuthenticationHandler();
	bha->setConfigurationManager(&configurationManager);
	this->authenticationManager->addMethod(bha);

	connectionManager = new ConnectionManager();
	openvpn = new OpenvpnManager();

	wirelessConfiguration = new WirelessConfiguration(&configurationManager);
	dnsmasq = new DnsmasqManager(interface_manager, &configurationManager);

	globalDynDns = new GlobalDynDns();
	cronManager->registerJob(globalDynDns);

	ipsecManager = new IPSecManager();
	vpnManager = new VpnManager(openvpn, ipsecManager);


	JsonManagementService *json = new JsonManagementService();
	json->setConfig(&config);
	json->setIds(System::getInstance()->ids);
	json->setSysMonitor(System::getInstance()->sysMonitor);
	json->setEventDb(System::getInstance()->events);
	json->setInterfaceManager(interface_manager);
	json->setDnsConfig(System::getInstance()->dnsConfig);
	json->setFirewall(System::getInstance()->sFirewall);
	json->setGroupDb(System::getInstance()->groupDb);
	json->setUserDb(System::getInstance()->userDb);
	json->setSessionDb(System::getInstance()->sessionDb);
	json->setHostDiscoveryService(System::getInstance()->arp);
	json->setLoggingConfiguration(System::getInstance()->loggingConfig);
	json->setOpenvpn(openvpn);
	json->setDhcp(dhcpConfigurationManager);
	json->setAuthenticationManager(authenticationManager);
	json->setConnectionManager(connectionManager);
	json->initDelegate();

	ManagementSocketServer *webSvc = new ManagementSocketServer(8001, json, false); // Web/non-SSL CLI interface server.
	cloud = new CloudManagementConnection();
	configurationManager.registerChangeHandler("cloud:deviceid", std::shared_ptr<ConfigurationCallback>(new CloudConfigurationChangeHandler(cloud)));
	wmic = new WinWmiAuth(sessionDb, events, this->authenticationManager, arp, &configurationManager);

	//Dummy constructors:
        sysMonitor->registerMetric(sFirewall);
	sysMonitor->registerMetricHandler(new LoggingMetricHandler());
	sysMonitor->registerMetricHandler(new AnaEngineMetricHandler());
	sysMonitor->registerMetricHandler(cloudAnaListener);

	configurationManager.registerConfigurable(interface_manager);
	configurationManager.registerConfigurable(route_manager);
	configurationManager.registerConfigurable(periods);
	configurationManager.registerConfigurable(groupDb);
	configurationManager.registerConfigurable(userDb);
	configurationManager.registerConfigurable(quotaManager);
	configurationManager.registerConfigurable(events);
	configurationManager.registerConfigurable(connectionManager);
	configurationManager.registerConfigurable(globalDynDns);
	configurationManager.registerConfigurable(loggingConfig);
	configurationManager.registerConfigurable(config.getRuntime());
	sFirewall->registerConfigurables(&configurationManager);
	configurationManager.registerConfigurable(sFirewall);
	configurationManager.registerConfigurable(sessionDb);
	configurationManager.registerConfigurable(vpnManager);
        configurationManager.registerConfigurable(cloud);

	//Lets load everything here:
        if(configurationManager.load("/etc/sphirewall.conf")){
               Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading configuration from file '/etc/sphirewall.conf' succeeded");
        }else if(configurationManager.load("/etc/sphirewall.conf", "/etc/sphirewall.conf.default")){
                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading default configuration from file '/etc/sphirewall.conf.default' succeeded");
        }else{

                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Could not load configuration, restoring defaults");
		if(configurationManager.backup()){
			Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Backed up old configuration file to '/etc'");
		}

                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Loading default configuration");
		configurationManager.initDefaults();
		configurationManager.save();
                Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Saved default configuration to '/etc/sphirewall.conf'");
        }

	//Lets start all the subsystems here:
	Logger::instance()->start();
        ids->start();
        if (wirelessConfiguration->enabled()) {
                wirelessConfiguration->start();
        }
        dnsmasq->initalize();
	interface_manager->register_change_listener(dnsmasq);

	sFirewall->start();

        new boost::thread(boost::bind(&CloudManagementConnection::connect, cloud));
	new boost::thread(boost::bind(&ManagementSocketServer::run, webSvc));
	new boost::thread(boost::bind(&WinWmiAuth::run, wmic));

	events->add(new Event(START, EventParams()));
	cronManager->start();
	while (ALIVE) {
		sleep(10);
	}
}

void System::shutdown(){
	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "sphirewalld is exiting");
	Logger::instance()->flush();
	removeLock(lockFile);
	exit(-1);
}

void System::createLock(string lockfile) {
	ofstream templock(lockfile);
	templock << intToString(getpid());
}

void System::removeLock(string lockfile) {
	unlink(lockfile.c_str());
}

bool System::verifyLock(string lockfile) {
	DIR *dir = NULL;
	pid_t pid;
	struct dirent * ent;
	if (!(dir = opendir("/proc/"))) {return false;}
	if((pid = retLockPid(lockfile)) == -1) {return true;}

	while ((ent = readdir(dir)) != NULL) {
		try {
			if (ent->d_type == DT_DIR && stoi(ent->d_name) == pid) {
				return false;
			}
		} catch (const std::invalid_argument &e) {
			// Do nothing; this is simply not a process dir
		}
	}
	closedir(dir);

	Logger::instance()->log("sphirewalld.system.init", CONSOLE, "Removing stale lock file.");
	removeLock(lockfile);
	return true;
}

pid_t System::retLockPid(string lockFile) {
	ifstream fp(lockFile.c_str());

	if (fp.is_open()) {
		string str_pid;
		getline(fp, str_pid);
		return stoi(str_pid);
	}

	return -1;
}
