#include <iostream>
#include <fstream>
using namespace std;

#include "Utils/LinuxUtils.h"
#include "Utils/FileUtils.h"
#include "Core/IPSec.h"
#include "Core/Vpn.h"
#include "Core/Logger.h"
#include "Core/DynamicRouteManager.h"
#include "Core/System.h"
#include "Utils/NRoute.h"

#define TAB_CHAR '\t'

void IPSecManager::add(IPSecInstancePtr instance){
	instances[instance->name] = instance;
}

IPSecInstancePtr IPSecManager::get(std::string key){
	if(instances.find(key) != instances.end()){
		return instances[key];
	}

	return IPSecInstancePtr();
}

void IPSecManager::remove(IPSecInstancePtr instance){
	instances.erase(instance->name);
}

list<IPSecInstancePtr> IPSecManager::list_instances(){
	list<IPSecInstancePtr> ret;
	for(auto instance : instances){
		ret.push_back(instance.second);
	}
	return ret;
}

void IPSecManager::init_service(){
	if(!FileUtils::checkExists("/var/run/charon.pid")){
		Logger::instance()->log("sphirewalld.vpn.ipsec.init_service", INFO, "Starting ipsec services");

		string error_string;
		LinuxUtils::exec("ipsec start", error_string);
		Logger::instance()->log("sphirewalld.vpn.ipsec.init_service", INFO, error_string.c_str());
	}

	start_logging();
}

void IPSecManager::do_connect(IPSecInstancePtr target){
	init_service();
	target->initiate();
}

void IPSecManager::do_disconnect(IPSecInstancePtr target){
	target->terminate();
}

int IPSecManager::status(IPSecInstancePtr target){
	return target->status();
}

std::string IPSecManager::log(IPSecInstancePtr target){
	string ret;
	stringstream read_command;
	read_command << "cat /var/log/syslog | grep sphirewalld.vpn.ipsec | grep " << target->name;
	LinuxUtils::exec(read_command.str().c_str(), ret);
	return ret;
}

void IPSecGatewayToGatewayInstance::initiate(){
	Logger::instance()->log("sphirewalld.vpn.ipsec.initiate", INFO, "initiating '%s' gateway-gateway ipsec tunnel", name.c_str());

	__add_keys();
	__add_connection();
	sleep(2);
	__initiate_connection();
	__configure_routes();
}

void IPSecGatewayToGatewayInstance::terminate(){
	Logger::instance()->log("sphirewalld.vpn.ipsec.initiate", INFO, "terminating '%s' gateway-gateway ipsec tunnel", name.c_str());

	__terminate_connection();
	__unload_connection();
	__remove_routes();
}

int IPSecGatewayToGatewayInstance::status(){
	list<ViciResponse*> states;

	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = VICI_EVENT_REGISTER;
	request->command = "list-sa";
	request->message = new ViciMessage();
	client->request(request);

	ViciRequest* request2 = new ViciRequest();
	request2->type = 0;
	request2->command = "list-sas";
	request2->message = new ViciMessage();

	client->request(request2);
	client->response();

	ViciResponse* response = NULL;
	while(response = client->response()){
		states.push_back(response);
	}

	int return_state = VPN_STATE_DEAD;
	for(ViciResponse* target: states){
		if(target->message->has_section(name)){
			//We found something to work with:
			ViciMessage* root_section = target->message->get_section(name);
			std::string state = root_section->get_value("state");
			if(state.compare("CONNECTING") == 0){
				return_state = VPN_STATE_ESTABLISHING;
			}

			if(state.compare("ESTABLISHED") == 0){
				return_state = VPN_STATE_ESTABLISHED;
			}
		}
	}

	for(ViciResponse* target: states){
		delete target;
	}

	client->destroy();
	delete request;
	delete request2;
	delete client;

	return  return_state;
}

void IPSecGatewayToGatewayInstance::__add_keys(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "load-shared";
	request->message = new ViciMessage();
	request->message->set_value("type", "ike");
	request->message->set_value("data", secret_key);
	list<string> owners;
	owners.push_back(local_id);
	owners.push_back(remote_id);
	request->message->set_list("owners", owners);

	client->request(request);
	ViciResponse* response = client->response();

	client->destroy();
	delete client;
	delete request;
	delete response;
}

void IPSecGatewayToGatewayInstance::__add_connection(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "load-conn";
	request->message = new ViciMessage();
	request->message->set_section(name, new ViciMessage());

	list<std::string> local_addrs; local_addrs.push_back(local_host);
	list<std::string> remote_addrs; remote_addrs.push_back(remote_host);

	request->message->get_section(name)->set_value("version", "0");
	request->message->get_section(name)->set_list("local_addrs", local_addrs);
	request->message->get_section(name)->set_list("remote_addrs", remote_addrs);

	request->message->get_section(name)->set_section("local-auth1", new ViciMessage());
	request->message->get_section(name + ".local-auth1")->set_value("auth", "psk");
	request->message->get_section(name + ".local-auth1")->set_value("id", local_id);

	request->message->get_section(name)->set_section("remote-auth1", new ViciMessage());
	request->message->get_section(name + ".remote-auth1")->set_value("auth", "psk");
	request->message->get_section(name + ".remote-auth1")->set_value("id", remote_id);

	//CHILD-SA
	list<std::string> local_ts; local_ts.push_back(local_subnet);
	list<std::string> remote_ts; remote_ts.push_back(remote_subnet);

	request->message->get_section(name)->set_section("children", new ViciMessage());
	request->message->get_section(name)->get_section("children")->set_section(name, new ViciMessage());
	request->message->get_section(name)->get_section("children")->get_section(name)->set_list("local_ts", local_ts);
	request->message->get_section(name)->get_section("children")->get_section(name)->set_list("remote_ts", remote_ts);
	request->message->get_section(name)->get_section("children")->get_section(name)->set_value("mode", "tunnel");

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__initiate_connection(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "initiate";
	request->message = new ViciMessage();
	request->message->set_value("child", name);

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__terminate_connection(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "terminate";
	request->message = new ViciMessage();
	request->message->set_value("ike", name);

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__unload_connection(){
	ViciClient* client = new ViciClient();
	client->init();

	ViciRequest* request = new ViciRequest();
	request->type = 0;
	request->command = "unload-conn";
	request->message = new ViciMessage();
	request->message->set_value("name", name);

	client->request(request);

	client->destroy();
	delete client;
	delete request;
}

void IPSecGatewayToGatewayInstance::__configure_routes(){
	NRoutePtr route(new NRoute());	
	route->set_destination_from_string(remote_subnet);
	route->set_route_nexthop_from_string(remote_host);

	System::getInstance()->get_route_manager()->add_route_entry(route);
}

void IPSecGatewayToGatewayInstance::__remove_routes(){
        NRoutePtr route(new NRoute());
        route->set_destination_from_string(remote_subnet);
        route->set_route_nexthop_from_string(remote_host);

        System::getInstance()->get_route_manager()->del_route_entry(route);
}

void IPSecManager::__logging_thread(){
	ViciClient* client = new ViciClient();
	
	try {
		client->init();
	}catch(VpnException e){
		return;
	}

	ViciRequest* request = new ViciRequest();
	request->type = VICI_EVENT_REGISTER;
	request->command = "log";
	request->message = new ViciMessage();
	client->request(request);
	client->response();

	logging_running = true;
	while(true){
		ViciResponse* response = client->response();
		if(response->message->has_value("ikesa-name")){
			string name = response->message->get_value("ikesa-name");
			string message = response->message->get_value("msg");

			Logger::instance()->log("sphirewalld.vpn.ipsec", INFO, 
					"ipsec connection '%s' reported '%s'", name.c_str(), message.c_str());
		}else{
			string message = response->message->get_value("msg");
			Logger::instance()->log("sphirewalld.vpn.ipsec", INFO, 
					"ipsec manager reported '%s'", message.c_str());
		}

		delete response;
	}

	logging_running = false;
	client->destroy();
	delete client;
	delete request;
}

void IPSecManager::start_logging(){
	//Launch thread
	if(!logging_running){
		new boost::thread(boost::bind(&IPSecManager::__logging_thread, this));
	}
}

bool IPSecManager::load(ConfigurationManager* configuration){
	try{
		init_service();
	}catch(VpnException& e){
		Logger::instance()->log("sphirewalld.vpn.ipsec", ERROR,
				"could not init service, reason '%s'", e.what().c_str());		
	}

	if (configuration->has("vpn:ipsec")) {
		ObjectContainer *root = configuration->getElement("vpn:ipsec");
		ObjectContainer *arr1 = root->get("connections")->container();

		for (int x = 0; x < arr1->size(); x++) {
			ObjectContainer *o = arr1->get(x)->container();
			//Determine Type
			int type = o->get("type")->number();
			if(type == VPN_IPSEC_GATEWAY_TO_GATEWAY){
				IPSecGatewayToGatewayInstance* instance = new IPSecGatewayToGatewayInstance();
				instance->local_host = o->get("local_host")->string();
				instance->local_subnet= o->get("local_subnet")->string();
				instance->local_id= o->get("local_id")->string();

				instance->remote_host= o->get("remote_host")->string();
				instance->remote_subnet= o->get("remote_subnet")->string();
				instance->remote_id= o->get("remote_id")->string();

				instance->auto_start= o->get("auto_start")->boolean();
				instance->auth_mode= o->get("auth_mode")->number();
				instance->secret_key= o->get("secret_key")->string();
				instance->meta= o->get("meta")->string();
				instance->name = o->get("name")->string();

				IPSecInstancePtr instance_ptr = IPSecInstancePtr(instance);	
				add(instance_ptr);

				if(instance_ptr->auto_start){
					try{
						do_connect(instance_ptr);					
					}catch(VpnException& e){
						Logger::instance()->log("sphirewalld.vpn.ipsec", ERROR, 
								"could not start a connection, reason '%s'", e.what().c_str());
					}
				}

			}
		}
	}
}
void IPSecManager::save(ConfigurationManager* configuration){
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *a = new ObjectContainer(CARRAY);

	for(IPSecInstancePtr instance :  list_instances()){
		ObjectContainer* o = new ObjectContainer(CREL);

		o->put("name", new ObjectWrapper((string) instance->name));	
		o->put("auto_start", new ObjectWrapper((bool) instance->auto_start));	
		o->put("meta", new ObjectWrapper((string) instance->meta));	
		o->put("auth_mode", new ObjectWrapper((double) instance->auth_mode));	
		o->put("type", new ObjectWrapper((double) instance->type()));	
		o->put("secret_key", new ObjectWrapper((string) instance->secret_key));	

		if(instance->type() == VPN_IPSEC_GATEWAY_TO_GATEWAY){
			IPSecGatewayToGatewayInstance* target = dynamic_cast<IPSecGatewayToGatewayInstance*>(instance.get());

			o->put("remote_host", new ObjectWrapper((string) target->remote_host));	
			o->put("remote_subnet", new ObjectWrapper((string) target->remote_subnet));	
			o->put("remote_id", new ObjectWrapper((string) target->remote_id));	

			o->put("local_host", new ObjectWrapper((string) target->local_host));	
			o->put("local_subnet", new ObjectWrapper((string) target->local_subnet));	
			o->put("local_id", new ObjectWrapper((string) target->local_id));	
		}

		a->put(new ObjectWrapper(o));
	}

	root->put("connections", new ObjectWrapper(a));
	configuration->setElement("vpn:ipsec", root);
	configuration->save();
}

