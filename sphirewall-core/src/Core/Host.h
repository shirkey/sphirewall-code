/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HOST_H
#define HOST_H

#include <string>
#include <netinet/in.h>
#include <boost/shared_ptr.hpp>

class Host {
	public:
		Host(){
			quarantined = false;
			lastSeen = 0;
			firstSeen = 0;
		}		

		std::string getIp();
		std::string hostname();
		void fetchHostname();

		std::string mac;
		in_addr_t ip;
		struct in6_addr ipV6;

		int lastSeen;
		int firstSeen;
		int type;
		bool quarantined;
	private:
		std::string hostnameCache;
};


typedef boost::shared_ptr<Host> HostPtr;

#endif // HOST_H
