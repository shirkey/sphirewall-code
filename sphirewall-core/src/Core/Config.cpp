/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

using namespace std;
using std::string;

#include "Core/Config.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Core/Logger.h"

Config::Config() {
	runtime = new RuntimeConfiguration();
}

int RuntimeConfiguration::get(std::string key) {
	if (contains(key))
		return *configMap[key];

	return -1;
}

int RuntimeConfiguration::set(std::string key, int newvalue) {
	if (configMap.find(key) != configMap.end()) {
		int *v = configMap[key];
		*v = newvalue;

		if (configurationManager != NULL) {
			configurationManager->put("runtime:" + key, (double) *v);
		}

		return 0;
	}

	return -1;
}

void RuntimeConfiguration::put(std::string key, int *value) {
	configMap[key] = value;
}

void RuntimeConfiguration::loadOrPut(std::string key, int *ptr) {
	if (configurationManager != NULL && configurationManager->hasByPath("runtime:" + key)) {
		put(key, ptr);
		set(key, configurationManager->get("runtime:" + key)->number());
	}
	else {
		put(key, ptr);
		set(key, *ptr);
	}
}

bool RuntimeConfiguration::load(){
	for (map<string, int *>::iterator iter = configMap.begin();
			iter != configMap.end();
			++iter) {
		loadOrPut(iter->first, iter->second);
	}
}

void RuntimeConfiguration::listAll(std::list<string> &l) {
	for (map<string, int *>::iterator iter = configMap.begin();
			iter != configMap.end();
			++iter) {
		l.push_back(iter->first);
	}
}

bool RuntimeConfiguration::contains(std::string key) {
	return configMap.find(key) != configMap.end();
}

int LoggingConfiguration::getLevel(std::string context) {
	map<string, Priority> priorities = rootLogger->getRegionLevel();

	if (priorities.find(context) != priorities.end()) {
		return priorities[context];
	}

	return rootLogger->getLevel();
}

int LoggingConfiguration::setLevel(std::string context, int level) {
	if (level >= 0 &&  level < 5) {
		rootLogger->setRegionLevel(context, (Priority) level);
		save();
		return 0;
	}
	else {
		return -1;
	}
}

int LoggingConfiguration::rmLevel(std::string context) {
	rootLogger->delRegionLevel(context);
	save();
	return 0;
}


void LoggingConfiguration::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *items = new ObjectContainer(CARRAY);

	list<string> names = getContexts();

	for (list<string>::iterator iter = names.begin();
			iter != names.end();
			iter++) {

		string cName = (*iter);
		ObjectContainer *o = new ObjectContainer(CREL);
		o->put("context", new ObjectWrapper((string) cName));
		o->put("level", new ObjectWrapper((double) rootLogger->getRegionLevel()[cName]));

		items->put(new ObjectWrapper(o));
	}

	root->put("critical_path_enabled", new ObjectWrapper((bool) rootLogger->criticalPathEnabled));
	root->put("levels", new ObjectWrapper(items));
	configurationManager->setElement("logging", root);
	configurationManager->save();
}

bool LoggingConfiguration::load() {
	if (configurationManager->has("logging")) {
		ObjectContainer *root = configurationManager->getElement("logging");
		ObjectContainer *v = root->get("levels")->container();

		for (int x = 0; x < v->size(); x++) {
			ObjectContainer *p = v->get(x)->container();
			rootLogger->setRegionLevel(p->get("context")->string(), (Priority) p->get("level")->number());
		}

		rootLogger->criticalPathEnabled = root->get("critical_path_enabled")->boolean();
	}
	return true;
}

std::list<std::string> LoggingConfiguration::getContexts() {
	list<string> ret;
	map<string, Priority> priorities = rootLogger->getRegionLevel();

	for (map<string, Priority>::iterator iter = priorities.begin();
			iter != priorities.end();
			iter++) {
		ret.push_back(iter->first);
	}

	return ret;
}

ConfigurationManager *Config::getConfigurationManager() {
	return configurationManager;
}

RuntimeConfiguration *Config::getRuntime() {
	return runtime;
}

void Config::setConfigurationManager(ConfigurationManager *configurationManager)  {
	this->configurationManager = configurationManager;
	this->runtime->setConfigurationManager(configurationManager);
}

RuntimeConfiguration::RuntimeConfiguration()
  : configMap()
{}

LoggingConfiguration::LoggingConfiguration(Logger *rootLogger)
  :rootLogger(rootLogger)
{
}
