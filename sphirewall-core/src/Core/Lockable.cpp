#include <iostream>

using namespace std;

#include "Core/Lockable.h"
#include "Utils/Lock.h"

Lockable::Lockable() {
	lock = new Lock();
}

bool Lockable::tryLock() {
	return lock->tryLock();
}

void Lockable::holdLock() {
	lock->lock();
}

void Lockable::releaseLock() {
	lock->unlock();
}

Lockable::~Lockable() {
	delete lock;
}

