#ifndef VPN_H
#define VPN_H

#include <boost/shared_ptr.hpp>
#include <list>

#define VPN_OPENVPN_SERVER_STATIC_KEYS 1
#define VPN_IPSEC_GATEWAY_TO_GATEWAY 2
#define VPN_OPENVPN_SERVER_TLS 3

#define VPN_STATE_ESTABLISHING 1
#define VPN_STATE_ESTABLISHED 0
#define VPN_STATE_DEAD -1

#include "Core/ConfigurationManager.h"

class OpenvpnManager;
class IPSecManager;

class VpnInstance {
	public:
		virtual int type() = 0;
		string name;
};

typedef boost::shared_ptr<VpnInstance> VpnInstancePtr;

class VpnException: std::exception {
        public:
                VpnException(std::string what) : exception_value(what){}
                ~VpnException() throw(){}
                std::string what() {
                        return exception_value;
                }

        private:
                std::string exception_value;
};

class VpnManager : public Configurable{
	public:
		VpnManager(OpenvpnManager* openvpn, IPSecManager* ipsec){
			this->openvpn = openvpn;
			this->ipsec = ipsec;
		}

		std::list<VpnInstancePtr> list_instances();	
		void add_instance(std::string name, int type);
		void delete_instance(VpnInstancePtr);
		VpnInstancePtr get_instance(std::string name);

		void do_connect(VpnInstancePtr instance);
		void do_disconnect(VpnInstancePtr instance);
		int status(VpnInstancePtr instance);

		std::string log(VpnInstancePtr instance);		

		void save();
		bool load();
	        const char* getConfigurationSystemName(){
			return "Vpn Manager";
		}

	private:
		OpenvpnManager* openvpn;
		IPSecManager* ipsec;
};

#endif
