/*
ldapSettings:
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <cstdio>
#include <Utils/StringUtils.h>

using namespace std;

#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Auth/AuthenticationHandler.h"
#include "Core/System.h"
#include "Utils/Lock.h"
#include "Utils/FileUtils.h"

int ConfigurationManager::READ_ONLY_CONFIGURATION = 0;

JsonSerialiser::JsonSerialiser()
	: c(-1), format(false)
{}

JsonSerialiser::~JsonSerialiser()
{}

void JsonSerialiser::setFormat(bool format) {
	this->format = format;
}

ObjectWrapper::~ObjectWrapper() {
	if (isContainer()) {
		delete containerValue;
	}
}

ObjectContainer::ObjectContainer(ObjectContainerType type)
	: type(type)
{}

ObjectContainer::~ObjectContainer() {
	for (map<string, ObjectWrapper *>::iterator iter = keyObjects.begin();
			iter != keyObjects.end();
			iter++) {
		delete iter->second;
	}

	for (vector<ObjectWrapper *>::iterator iter = listObjects.begin();
			iter != listObjects.end();
			iter++) {
		delete(*iter);
	}
}

ObjectContainerType ObjectContainer::getType() const {
	return this->type;
}

ObjectWrapper::ObjectWrapper(ObjectContainer *container)
	: type(OBJECT), containerValue(container)
{}

ObjectWrapper::ObjectWrapper(std::string value)
	: type(STRING), stringValue(value), containerValue(NULL)
{}

ObjectWrapper::ObjectWrapper(double value)
	: type(DOUBLE), numberValue(value), containerValue(NULL)
{}

ObjectWrapper::ObjectWrapper(bool value)
	: type(BOOL), boolValue(value), containerValue(NULL)
{}

string ObjectWrapper::string() const {
	return stringValue;
}

ObjectContainer *ObjectWrapper::container() const {
	return containerValue;
}

double ObjectWrapper::number() const {
	return numberValue;
}

bool ObjectWrapper::boolean() const {
	return boolValue;
}

bool ObjectWrapper::isBoolean() const {
	return type == BOOL;
}

bool ObjectWrapper::isContainer() const {
	return type == OBJECT;
}

bool ObjectWrapper::isNumber() const {
	return type == DOUBLE;
}

bool ObjectWrapper::isString() const {
	return type == STRING;
}

string ObjectWrapper::toString() const {
	std::stringstream ss;
	ss << " ObjectWrapper [";

	if (isContainer()) {
		ss << " container ";
	}
	else if (isNumber()) {
		ss << " " << numberValue << " ";
	}
	else if (isBoolean()) {
		ss << " " << boolValue << " ";
	}
	else if (isString()) {
		ss << " " << stringValue << " ";
	}

	ss << " ]\n";

	return ss.str();
}

std::map< string, ObjectWrapper * > &ObjectContainer::getKeyObjects() {
	return keyObjects;
}

void ObjectContainer::foreach(std::function<void (ObjectWrapper*)> perform) {
	for (ObjectWrapper * x : listObjects) {
		perform(x);
	}
}


ObjectWrapper *ObjectContainer::get(std::string key) {
	if (type == CREL) {
		if(has(key)){
			return keyObjects[key];
		}
	}

	throw ObjectNotFoundException(key);
}


void ObjectContainer::put(std::string key, ObjectWrapper *input) {
	if (type == CREL) {
		if (has(key)) {
			delete keyObjects[key];
		}

		keyObjects[key] = input;
	}
}

void ObjectContainer::put(ObjectWrapper *input) {
	if (type == CARRAY) {
		listObjects.push_back(input);
	}
}

bool ObjectContainer::has(std::string key) {
	if (type == CREL) {
		return keyObjects.find(key) != keyObjects.end();
	}

	return false;
}

int ObjectContainer::size() {
	if (type == CARRAY) {
		return listObjects.size();
	}

	return 0;
}

ObjectWrapper *ObjectContainer::get(int index) {
	if (index < long(listObjects.size())) {
		return listObjects[index];
	}

	return NULL;
}

bool ConfigurationManager::backup(){
	if(FileUtils::checkExists("/etc/sphirewall.conf")){
		string oldf = FileUtils::read("/etc/sphirewall.conf");
		
		stringstream fn; fn << "/etc/sphirewall.conf.backup." <<  StringUtils::genRandom();
		FileUtils::write(fn.str().c_str(), oldf, true);
		return true;		
	}
	return false;
}

ConfigurationManager::ConfigurationManager() {
	configuration_version = 0;
	rootValue = new ObjectWrapper(new ObjectContainer(CREL));
	initDefaults();
}

ConfigurationManager::~ConfigurationManager() {
	delete rootValue;
}

std::string ConfigurationManager::dump() {
	if (rootValue == NULL) {
		throw ConfigurationException("rootValue is null");
	}

	return JsonSerialiser::serializeToJsonString(rootValue, true); 
}

#define MIN_CONF_SIZE 1
void ConfigurationManager::save() {
	//First increment version number
	if(rootValue->container()->has("configuration_version")){
		configuration_version = rootValue->container()->get("configuration_version")->number();
	}

	//Persist configuration_version and sphirewall_version
	rootValue->container()->put("configuration_version", new ObjectWrapper((double) ++configuration_version));
	rootValue->container()->put("sphirewall_version", new ObjectWrapper((string) System::getInstance()->version));

	if(READ_ONLY_CONFIGURATION == 0){
		string configuration_dump = dump();
		if(configuration_dump.size() > MIN_CONF_SIZE){	
			ofstream fp;
			fp.open(filename);
			fp << "# sphirewall.conf -- Generated by Sphirewall for the Core Sphirewall Daemon\n";
			fp << "# For support, see http://sphirewall.net\n";

			fp << configuration_dump; 
			fp.close();
		}
	}
}

void ConfigurationManager::initDefaults(){
	defaultIfUnset("general:admin_group", 0.0f);
	defaultIfUnset("general:gid", 6001.0f);
	defaultIfUnset("authentication:session_timeout", 600.0f);
	defaultIfUnset("general:bandwidth_hostname", (string) "localhost");
	defaultIfUnset("general:bandwidth_port", 8002.0f);
	defaultIfUnset("general:snort_enabled", false);
	defaultIfUnset("general:snort_file", (string) "/var/log/snort/alert");
	defaultIfUnset("general:bandwidth_threshold", 1500.0f);
	defaultIfUnset("general:bandwidth_retension", 432000.0f);
	defaultIfUnset("general:bandwidth_day_switch_period", 0.0f);
	defaultIfUnset("general:hashpasswords", true);

	defaultIfUnset("general:rpc", false);
	defaultIfUnset("general:eventPurgeThreshold", 86400.0f);
	defaultIfUnset("general:httpApplicationTracker", true);
	defaultIfUnset("general:proxyApplicationTracker", true);
	defaultIfUnset("general:disableGoogleSearchSSL", false);

	loadConfigurables();
}

bool ConfigurationManager::load(std::string persistantFilename){
	return this->load(persistantFilename, persistantFilename);
}

bool ConfigurationManager::loadFromString(std::string input){
	try {
		rootValue = JsonSerialiser::parseJsonString(input);
		loadConfigurables();
	}catch(const ObjectNotFoundException& e){
		Logger::instance()->log("sphirewalld.configurationmanager.load", ERROR, "Object in configuration was not found, key '%s'", e.what());
		return false;
	}catch(const ConfigurationException& e){
		Logger::instance()->log("sphirewalld.configurationmanager.load", ERROR, "ConfigurationException was thrown, reason '%s'", e.what());
		return false;
	}

	System::getInstance()->config.getRuntime()->loadOrPut("READ_ONLY_CONFIGURATION", &READ_ONLY_CONFIGURATION);
	save();
	return true;
}

bool ConfigurationManager::load(std::string persistantFilename, std::string templateFilename){
	this->filename = persistantFilename;
	ifstream fp(templateFilename);

	if (fp.is_open()) {
		stringstream inputBuffer;

		while (fp.good()) {
			string line;
			getline(fp, line);

			if (line[0] != '#') {
				inputBuffer << line;
			}
		}

		return loadFromString(inputBuffer.str());
	}
	return false;
}

void ConfigurationManager::loadConfigurables(){
	for(Configurable* entity : configurables){
		Logger::instance()->log("sphirewalld.configurationmanager.load", INFO, "Loading configuration for system '%s'", entity->getConfigurationSystemName());
		entity->setConfigurationManager(this);
		entity->load();
	}
}

ObjectContainer *ConfigurationManager::getElement(std::string key) {
	if (rootValue == NULL) {
		throw ConfigurationException("rootValue is null");
	}

	return get(key)->container(); 
}

void ConfigurationManager::setElement(std::string key, ObjectContainer *container) {
	if (rootValue == NULL) {
		throw ConfigurationException("rootValue is null");
	}

	put(key, new ObjectWrapper(container));
}

void ConfigurationManager::registerChangeHandler(std::string key, std::shared_ptr<ConfigurationCallback> handler) {
	configurationCallbacks[key] = handler;
}

void ConfigurationManager::triggerChangeHandlers() {
	for (std::pair<std::string, std::shared_ptr<ConfigurationCallback>> handler : configurationCallbacks) {
		handler.second->run();
	}
}

bool ConfigurationManager::has(std::string key) {
	return hasByPath(key);
}

int ConfigurationManager::put(std::string path, ObjectWrapper *value) {
	if (!rootValue) {
		throw ConfigurationException("rootValue is null");
	}

	vector<string> pathElements;
	split(path, ':', pathElements);
	ObjectContainer *curser = rootValue->container();

	for (uint x = 0; x < pathElements.size(); x++) {
		if (x == pathElements.size() - 1) { // last element
			curser->put(pathElements[x], value);
			return 0;
		}
		else {
			if (curser->has(pathElements[x])) {
				if(curser->get(pathElements[x])){
					curser = curser->get(pathElements[x])->container();
				}			}
			else {
				ObjectContainer *o = new ObjectContainer(CREL);
				curser->put((pathElements[x]), new ObjectWrapper(o));
				curser = o;
			}
		}
	}

	return 0;
}

int ConfigurationManager::put(std::string path, std::string value) {
	int res = put(path, new ObjectWrapper(value));
	if (configurationCallbacks.find(path) != configurationCallbacks.end()) {
		configurationCallbacks[path]->run();
	}
	return res;
}

int ConfigurationManager::put(std::string path, bool value) {
	int res = put(path, new ObjectWrapper(value));
	if (configurationCallbacks.find(path) != configurationCallbacks.end()) {
		configurationCallbacks[path]->run();
	}
	return res;
}

int ConfigurationManager::put(std::string path, double value) {
	int res = put(path, new ObjectWrapper(value));
	if (configurationCallbacks.find(path) != configurationCallbacks.end()) {
		configurationCallbacks[path]->run();
	}
	return res;
}

int ConfigurationManager::defaultIfUnset(string path, string value) {
	if (!hasByPath(path)) {
		return put(path, value);
	}
	return 0;
}

int ConfigurationManager::defaultIfUnset(string path, bool value) {
	if (!hasByPath(path)) {
		return put(path, value);
	}
	return 0;
}

int ConfigurationManager::defaultIfUnset(string path, double value) {
	if (!hasByPath(path)) {
		return put(path, value);
	}
	return 0;
}

ObjectWrapper *ConfigurationManager::get(std::string path) {
	if (!hasByPath(path)) {
		throw ObjectNotFoundException(path);
	}

	if (!rootValue) {
		throw ConfigurationException("rootValue is null");
	}

	vector<string> pathElements;
	split(path, ':', pathElements);

	ObjectContainer *base = rootValue->container();
	ObjectContainer *curser = base;

	for (uint x = 0; x < pathElements.size(); x++) {
		if (x == pathElements.size() - 1) {
			if (curser) {
				return curser->get(pathElements[x]);
			}

		}
		else {
			if (curser == NULL) {
				curser = base->get(pathElements[x])->container();
			}
			else {
				curser = curser->get(pathElements[x])->container();
			}
		}
	}

	return 0;
}

string ConfigurationManager::getAsString(string p, string defaultVal) {
	if(hasByPath(p)){
		return get(p)->string();
	}

	return defaultVal;
}


bool ConfigurationManager::hasByPath(std::string path) {
	if (!rootValue) {
		throw ConfigurationException("rootValue is null");
	}

	vector<string> pathElements;
	split(path, ':', pathElements);

	ObjectContainer *base = rootValue->container();
	ObjectContainer *curser = NULL;

	for (uint x = 0; x < pathElements.size(); x++) {
		if (x == pathElements.size() - 1) {
			if(curser == NULL){
				curser = base;
			}

			return curser->has(pathElements[x]);
		}
		else {
			if (curser == NULL) {
				if (!base->has(pathElements[x])) {
					return false;
				}

				curser = base->get(pathElements[x])->container();
			}
			else {
				if (!curser->has(pathElements[x])) {
					return false;
				}

				curser = curser->get(pathElements[x])->container();
			}
		}
	}

	return false;
}

ConfigurationException::ConfigurationException(std::string message)
	: w(message) {
	}

ConfigurationException::~ConfigurationException() throw() {}

const char *ConfigurationException::what() const throw() {
	return w.c_str();
}

string ConfigurationException::message() {
	return w;
}


std::string StringifyString(std::string str) {
	std::string str_out = "";

	std::string::iterator iter = str.begin();

	while (iter != str.end()) {
		wchar_t chr = *iter;

		if (chr == '"' || chr == '\\' || chr == '/') {
			str_out += '\\';
			str_out += chr;
		}
		else if (chr == '\b') {
			str_out += "\\b";
		}
		else if (chr == '\f') {
			str_out += "\\f";
		}
		else if (chr == '\n') {
			str_out += "\\n";
		}
		else if (chr == '\r') {
			str_out += "\\r";
		}
		else if (chr == '\t') {
			str_out += "\\t";
		}
		else if (chr < ' ') {
			str_out += "\\u";

			for (int i = 0; i < 4; i++) {
				int value = (chr >> 12) & 0xf;

				if (value >= 0 && value <= 9)
					str_out += (wchar_t)('0' + value);
				else if (value >= 10 && value <= 15)
					str_out += (wchar_t)('A' + (value - 10));

				chr <<= 4;
			}
		}
		else {
			str_out += chr;
		}

		iter++;
	}

	return str_out;
}

void JsonSerialiser::recurseObject(std::stringstream &root, ObjectContainer *input) {
	c++;

	if (input->getType() == CREL) {
		if (format) {
			root << "\n";

			for (int xx = 0; xx < c; xx++) {
				root << "\t";
			}

			root << "{\n";
		}
		else {
			root << "{";
		}

		map<string, ObjectWrapper *> &crelObjects = input->getKeyObjects();
		bool lastWasInvalid = false;
		for (map<string, ObjectWrapper *>::iterator iter = crelObjects.begin();
				iter != crelObjects.end();
				iter++) {

			std::string key = iter->first;
			ObjectWrapper *potentialValue = iter->second;
			if(key.size() == 0 || potentialValue == NULL){
				lastWasInvalid = true;
				continue;
			}

			if (!lastWasInvalid && iter != crelObjects.begin()) {
				root << ",";

				if (format) {
					root << "\n";
				}
			}

			lastWasInvalid = false;
			if (format) {
				for (int xx = 0; xx < c; xx++) {
					root << "\t";
				}
			}

			root << "\"" << key << "\":";

			if (potentialValue->isContainer()) {
				recurseObject(root, potentialValue->container());
			}
			else if (potentialValue->isBoolean()) {
				if (potentialValue->boolean()) {
					root << "true";
				}
				else {
					root << "false";
				}
			}
			else if (potentialValue->isNumber()) {
				root << potentialValue->number();
			}
			else if (potentialValue->isString()) {
				root << "\"" << StringifyString(potentialValue->string()) << "\"";
			}
		}

		if (format) {
			root << "\n";

			for (int xx = 0; xx < c; xx++) {
				root << "\t";
			}
		}

		root << "}";

	}
	else if (input->getType() == CARRAY) {
		if (format) {
			root << "\n";

			for (int xx = 0; xx < c; xx++) {
				root << "\t";
			}

			root << "[\n";
		}
		else {
			root << "[";
		}

		for (int x = 0; x < input->size(); x++) {
			if (format) {
				if (x != 0) {
					root << ",\n";
				}

				for (int xx = 0; xx < c; xx++) {
					root << "\t";
				}
			}
			else {
				if (x != 0) {
					root << ",";
				}
			}

			ObjectWrapper *potentialValue = input->get(x);

			if (potentialValue->isContainer()) {
				ObjectContainer *deeperContainer = potentialValue->container();

				if (deeperContainer->getType() == CREL) {
					recurseObject(root, deeperContainer);
				}
				else if (deeperContainer->getType() == CARRAY) {
					recurseObject(root, deeperContainer);
				}
			}
			else if (potentialValue->isBoolean()) {
				if (potentialValue->boolean()) {
					root << "true";
				}
				else {
					root << "false";
				}
			}
			else if (potentialValue->isNumber()) {
				root << potentialValue->number();
			}
			else if (potentialValue->isString()) {
				root << "\"" << StringifyString(potentialValue->string()) << "\"";
			}
		}

		if (format) {
			root << "\n";

			for (int xx = 0; xx < c; xx++) {
				root << "\t";
			}
		}

		root << "]";
	}

	c--;
}

std::string JsonSerialiser::serialize(ObjectWrapper *input) {
	stringstream stream;
	stream << std::setprecision(10);

	recurseObject(stream, input->container());
	return stream.str();
}

ObjectWrapper* ObjectWrapper::parseRecurse(JSONValue *value) {
	if (value->IsString()) {
		return new ObjectWrapper(value->String());
	}
	else if (value->IsBool()) {
		return new ObjectWrapper(value->AsBool());
	}
	else if (value->IsNumber()) {
		return new ObjectWrapper(value->AsNumber());
	}
	else if (value->IsObject()) {
		ObjectContainer *container = new ObjectContainer(CREL);

		JSONObject &obj = value->AsObject();

		for (std::pair<std::wstring, JSONValue *> pair : obj.values) {
			container->put(WStringToString(pair.first), parseRecurse(pair.second));
		}

		return new ObjectWrapper(container);
	}
	else if (value->IsArray()) {
		ObjectContainer *container = new ObjectContainer(CARRAY);

		for (JSONValue * jsonValue : value->AsArray()) {
			container->put(parseRecurse(jsonValue));
		}

		return new ObjectWrapper(container);
	}

	return NULL;
}

JSONValue* ObjectWrapper::convertRecurse(ObjectWrapper* input){
	if(input->isString()){
		return new JSONValue((std::string) input->string());
	}else if (input->isNumber()){
		return new JSONValue((double) input->number());
	}else if (input->isBoolean()){
		return new JSONValue((bool) input->boolean());
	}else if(input->isContainer()){
		ObjectContainer* container = input->container();
		if(container->getType() == CREL){
			JSONObject obj;			

			std::map<std::string, ObjectWrapper *>& kvs = container->getKeyObjects();			
			for(pair<std::string, ObjectWrapper*> kv : kvs){
				JSONValue* temp =  convertRecurse(kv.second);
				obj.put(StringToWString(kv.first), temp);	
			}

			return new JSONValue(obj);
		}else if(container->getType() == CARRAY){
			JSONArray arr;
			for(ObjectWrapper* target : container->getListObjects()){
				arr.push_back(convertRecurse(target));
			}			

			return new JSONValue(arr);
		}
	}

	return NULL;
}

ObjectWrapper *JsonSerialiser::parse(std::string input) {
	JSONValue *value = JSON::Parse(input.c_str());

	if (!value) {
		throw ConfigurationException("Could not parse json");
	}

	ObjectWrapper *ret = ObjectWrapper::parseRecurse(value);
	delete value;
	return ret;
}

ObjectWrapper *JsonSerialiser::parseJsonString(std::string input) {
	JsonSerialiser *s = new JsonSerialiser();
	ObjectWrapper *ret = s->parse(input);
	delete s;
	return ret;
}

std::string JsonSerialiser::serializeToJsonString(ObjectWrapper *input, bool format) {
	JsonSerialiser *s = new JsonSerialiser();
	s->setFormat(format);
	string ret = s->serialize(input);
	delete s;
	return ret;
}

void ConfigurationManager::registerConfigurable(Configurable* entity){
	configurables.push_back(entity);
}
