/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "Core/System.h"
#include "Core/Openvpn.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/FileUtils.h"
#include "Utils/StringUtils.h"
#include "Utils/LinuxUtils.h"
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

void OpenvpnManager::do_connect(OpenvpnPtr target){
	target->connect();
}

void OpenvpnManager::do_disconnect(OpenvpnPtr target){
	target->disconnect();
}

int OpenvpnManager::status(OpenvpnPtr target){
	return target->status();	
}

void Openvpn::connect() {
	publish();
	stringstream ss;
	ss << "/bin/bash -c 'start-stop-daemon --start --quiet --oknodo --pidfile " << getPidFile() << " --exec /usr/sbin/openvpn";
	ss << " -- --writepid " << getPidFile() << " --daemon ovpn-openvpn --cd " << getPath() << " --config " << getPath() << "/openvpn.conf'";

	LinuxUtils::execWithNoFds(ss.str());
	sleep(3);
}

void Openvpn::disconnect() {
	string pid = FileUtils::read(getPidFile().c_str());

	if (pid.size() > 0) {
		int ipid = stoi(pid);

		if (ipid > 1) {
			kill(ipid, SIGTERM);
			remove(getPidFile().c_str());
			remove("/var/run/openvpn.openvpn.status");
		}

		sleep(3);
	}
}

void OpenvpnTlsKeys::publish(){
	stringstream ss;

	ss << "proto tcp\n";
	ss << "server " << serverip << " " << servermask << "\n";
	ss << "ifconfig-pool-persist ipp.txt\n";
	ss << "ca " << getPath() << "/easy-rsa/keys/ca.crt\n";
	ss << "cert " << getPath() << "/easy-rsa/keys/server.crt\n";
	ss << "key " << getPath() << "/easy-rsa/keys/server.key\n";
	ss << "dh " << getPath() << "/easy-rsa/keys/dh1024.pem\n";
	ss << "push \"route " << serverip;
	ss << " " << servermask << "\"\n";	

	//Default options:
	ss << "dev tun\n";
	ss << "keepalive 10 60\n";
	ss << "ping-timer-rem\n";
	ss << "persist-tun\n";
	ss << "persist-key\n";
	ss << "group daemon\n";
	ss << "daemon\n";
	ss << "cd " << getPath() << "\n";
	ss << "log-append " << getLogFile() << "\n";

	//Customisable Options
	ss << "port " << remoteport << "\n";

	if (compression) {
		ss << "comp-lzo\n";
	}

	string config = ss.str();
	string filename = getPath() + "/openvpn.conf";
	ofstream configFile;
	configFile.open(filename.c_str());
	configFile << config;
}

void OpenvpnStaticKeys::publish(){
	stringstream ss;
	ss << "ifconfig " << serverip << " " << clientip << endl;
	ss << "secret " << getPath() << "/static.key\n";

	//Default options:
	ss << "dev tun\n";
	ss << "keepalive 10 60\n";
	ss << "ping-timer-rem\n";
	ss << "persist-tun\n";
	ss << "persist-key\n";
	ss << "group daemon\n";
	ss << "daemon\n";
	ss << "cd " << getPath() << "\n";
	ss << "log-append " << getLogFile() << "\n";

	//Customisable Options
	ss << "port " << remoteport << "\n";

	if (compression) {
		ss << "comp-lzo\n";
	}

	string config = ss.str();
	string filename = getPath() + "/openvpn.conf";
	ofstream configFile;
	configFile.open(filename.c_str());
	configFile << config;

}

int Openvpn::status() {
	return FileUtils::checkExists(getPidFile()) ? VPN_STATE_ESTABLISHED : VPN_STATE_DEAD;
}

std::string Openvpn::getPidFile() {
	stringstream ss;
	ss << "/var/run/openvpn." << name << ".pid";
	return ss.str();
}

OpenvpnClientConf *OpenvpnTlsKeys::createClient(std::string name) {
	OpenvpnClientConf *c = new OpenvpnClientConf();
	c->name = name;
	stringstream ss;
	ss << "/bin/bash -c 'cd " << getPath() << "/easy-rsa && source ./vars && export EASY_RSA=\'${EASY_RSA:-.}\' && KEY_CN=";
	ss << StringUtils::genRandom() << " $EASY_RSA/pkitool " << name << "'";

	if (system(ss.str().c_str()));

	clients.push_back(c);
	return c;
}

std::string OpenvpnTlsKeys::generateUserFile(std::string name) {
	stringstream ss;

	//Global variables:
	if (compression) {
		ss << "comp-lzo\n";
	}

	ss << "dev tun\n";
	ss << "resolv-retry infinite\n";
	ss << "nobind\n";
	ss << "persist-key\n";
	ss << "persist-tun\n";
	ss << "verb 3\n\n";

	ss << "remote " << remoteserverip << " " << remoteport << "\n";
	ss << "client\n";
	ss << "proto tcp\n";
	ss << "port " << remoteport << "\n";

	ss << "<ca>\n";
	stringstream ca;
	ca << getPath() << "/easy-rsa/keys/ca.crt";
	ss << FileUtils::read(ca.str().c_str());
	ss << "</ca>\n";
	ss << "<cert>\n";
	stringstream crtfile;
	crtfile << getPath() << "/easy-rsa/keys/" << name << ".crt";
	string caEntireFile = FileUtils::read(crtfile.str().c_str());
	int startPos = caEntireFile.find("-----BEGIN CERTIFICATE-----");
	int stopPos = caEntireFile.find("-----END CERTIFICATE-----");

	for (int x = startPos; x < stopPos; x++) {
		ss << caEntireFile[x];
	}

	ss << "-----END CERTIFICATE-----\n";
	ss << "</cert>\n";

	ss << "<key>\n";
	stringstream keyname;
	keyname << getPath() << "/easy-rsa/keys/" << name << ".key";
	ss << FileUtils::read(keyname.str().c_str());
	ss << "</key>\n";

	return ss.str();
}

std::string OpenvpnStaticKeys::generate_client_configuration() {
	stringstream ss;

	//Global variables:
	if (compression) {
		ss << "comp-lzo\n";
	}

	ss << "dev tun\n";
	ss << "resolv-retry infinite\n";
	ss << "nobind\n";
	ss << "persist-key\n";
	ss << "persist-tun\n";
	ss << "verb 3\n\n";

	ss << "remote " << remoteserverip << " " << remoteport << "\n";
	ss << "ifconfig " << clientip << " " << serverip << "\n";
	ss << "<secret>\n";
	string keyfile = getPath() + "/static.key";

	ss << FileUtils::read(keyfile.c_str());
	ss << "</secret>\n";

	return ss.str();
}

void OpenvpnTlsKeys::deleteClient(OpenvpnClientConf *c) {
	clients.remove(c);
	stringstream ss;
	ss << "/bin/bash -c 'cd " << getPath() << "/easy-rsa/keys/ && rm " << c->name << ".*'";

	if (system(ss.str().c_str()));
}

std::list<OpenvpnClientConf *> &OpenvpnTlsKeys::getClients() {
	return clients;
}

std::string Openvpn::log() {
	string ret;
	string c = "tail -n 500 " + getLogFile();
	LinuxUtils::exec(c.c_str(), ret);
	return ret;
}

void OpenvpnStaticKeys::installbase() {
	stringstream ss;
	ss << "/bin/bash -c 'mkdir " << getPath() << " && /usr/sbin/openvpn --genkey --secret " << getPath() << "/static.key'";

	string ret;
	LinuxUtils::exec(ss.str(), ret);
}

void OpenvpnTlsKeys::installbase() {
	stringstream ss;
	ss << "/bin/bash -c '/usr/bin/setup-openvpn.sh ";
	ss << certcountry << " ";
	ss << certprovince << " ";
	ss << certcity << " ";
	ss << certorg << " ";
	ss << certemail << " ";
	ss << getPath();
	ss << "'";
	string ret;
	LinuxUtils::exec(ss.str(), ret);
}

std::string Openvpn::getPath() {
	return "/etc/sphirewall-openvpn-" + name;
}

std::string Openvpn::getLogFile() {
	stringstream ss;
	ss << "/var/log/sphirewall-openvpn-" << name << ".log";
	return ss.str();
}

void Openvpn::uninstallbase() {
	disconnect();

	stringstream ss;
	ss << "/bin/bash -c 'rm -Rf " << getPath() << "'";
	string ret;
	LinuxUtils::exec(ss.str(), ret);
}

bool OpenvpnManager::load(ConfigurationManager* configurationManager) {
	instances.clear();

	if (configurationManager->has("vpn:openvpn")) {
		ObjectContainer *root = configurationManager->getElement("vpn:openvpn");
		ObjectContainer *servers = root->get("servers")->container();

		for (int x = 0; x < servers->size(); x++) {
			ObjectContainer *o = servers->get(x)->container();

			Openvpn* new_openvpn_instance = NULL;
			if(o->get("type")->number() == VPN_OPENVPN_SERVER_STATIC_KEYS){
				new_openvpn_instance = new OpenvpnStaticKeys();
			}else{
				new_openvpn_instance = new OpenvpnTlsKeys();
			}

			new_openvpn_instance->serverip = o->get("serverip")->string();
			new_openvpn_instance->clientip = o->get("clientip")->string();
			new_openvpn_instance->servermask = o->get("servermask")->string();
			new_openvpn_instance->remoteserverip = o->get("remoteserverip")->string();
			new_openvpn_instance->customopts = o->get("customopts")->string();
			new_openvpn_instance->remoteport = o->get("remoteport")->number();
			new_openvpn_instance->compression = o->get("compression")->boolean();
			new_openvpn_instance->auto_start= o->get("auto_start")->boolean();
			new_openvpn_instance->name = o->get("name")->string();

			if(new_openvpn_instance->type() == VPN_OPENVPN_SERVER_TLS){
				ObjectContainer *clientsArr = o->get("clients")->container();
				for (int x = 0; x < clientsArr->size(); x++) {
					ObjectContainer *o = clientsArr->get(x)->container();
					OpenvpnClientConf *nc = new OpenvpnClientConf();
					nc->name = o->get("name")->string();
					dynamic_cast<OpenvpnTlsKeys*>(new_openvpn_instance)->getClients().push_back(nc);
				}
			}
			instances.push_back(OpenvpnPtr(new_openvpn_instance));
		}
	}
}

void OpenvpnManager::save(ConfigurationManager* configurationManager) {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *sarr = new ObjectContainer(CARRAY);

	for (OpenvpnPtr openvpn: instances) {
		ObjectContainer *single = new ObjectContainer(CREL);
		single->put("name", new ObjectWrapper((string) openvpn->name));
		single->put("type", new ObjectWrapper((double) openvpn->type()));

		single->put("serverip", new ObjectWrapper((string) openvpn->serverip));
		single->put("clientip", new ObjectWrapper((string) openvpn->clientip));
		single->put("servermask", new ObjectWrapper((string) openvpn->servermask));
		single->put("remoteserverip", new ObjectWrapper((string) openvpn->remoteserverip));
		single->put("customopts", new ObjectWrapper((string) openvpn->customopts));
		single->put("remoteport", new ObjectWrapper((double) openvpn->remoteport));
		single->put("compression", new ObjectWrapper((bool) openvpn->compression));
		single->put("auto_start", new ObjectWrapper((bool) openvpn->auto_start));

		ObjectContainer *clientArray = new ObjectContainer(CARRAY);
		
		if(openvpn->type() == VPN_OPENVPN_SERVER_TLS){
			OpenvpnTlsKeys* otk = dynamic_cast<OpenvpnTlsKeys*>(openvpn.get());
	
			for (OpenvpnClientConf* client : otk->getClients()) {
				ObjectContainer *o = new ObjectContainer(CREL);
				o->put("name", new ObjectWrapper((string) client->name));
				clientArray->put(new ObjectWrapper(o));
			}
			single->put("clients", new ObjectWrapper(clientArray));
		}
		sarr->put(new ObjectWrapper(single));
	}

	root->put("servers",  new ObjectWrapper(sarr));
	configurationManager->setElement("vpn:openvpn", root);
	configurationManager->save();
}

OpenvpnPtr OpenvpnManager::getInstance(std::string name) {
	for(OpenvpnPtr instance : instances){
		if(instance->name.compare(name) == 0){
			return instance;
		}
	}

	return OpenvpnPtr();
}

void OpenvpnManager::addInstance(OpenvpnPtr target) {
	instances.push_back(target);
}

void OpenvpnManager::removeInstance(OpenvpnPtr target) {
	target->uninstallbase();

	for (list<OpenvpnPtr>::iterator iter = instances.begin(); iter != instances.end(); iter++) {
		if ((*iter) == target) {
			instances.erase(iter);
			break;
		}
	}
}

bool OpenvpnManager::daemonExists() {
	return FileUtils::checkExists("/usr/sbin/openvpn");
}

