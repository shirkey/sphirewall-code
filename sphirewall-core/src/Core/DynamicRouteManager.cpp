#include <iostream>
#include <sstream>

using namespace std;

#include "Core/DynamicRouteManager.h"
#include "Utils/LinuxUtils.h"
#include "Core/Logger.h"

void DynamicRouteManager::flush_route_table(int route_table_id){
	stringstream ss;
	ss << "ip route flush table " << route_table_id;

	Logger::instance()->log("sphirewalld.dynamicroutemanager.flush_route_table", INFO, "Flushing route table '%d'", route_table_id);
	string error_string;
	if(!LinuxUtils::exec(ss.str(), error_string)){
		Logger::instance()->log("sphirewalld.dynamicroutemanager.flush_route_table", ERROR, 
			"Could not flush route table '%d', route2 reported '%s'", route_table_id, error_string.c_str());
	}
}

void DynamicRouteManager::add_route_table(int route_table_id){

}

void DynamicRouteManager::remove_route_table(int route_table_id){

}

void DynamicRouteManager::add_route(DynamicRouteDescription route){
	stringstream ss;
	ss << "ip route add " << route.destination << " via " << route.gw << " table " << route.route_table_id;

	Logger::instance()->log("sphirewalld.dynamicroutemanager.add_route", INFO, "Adding route '%s'", ss.str().c_str());
	string error_string;
	if(!LinuxUtils::exec(ss.str(), error_string)){
		Logger::instance()->log("sphirewalld.dynamicroutemanager.add_route", ERROR, 
				"Could not add route,  route2 reported '%s'", error_string.c_str());
	}
}

void DynamicRouteManager::remove_route(DynamicRouteDescription route){
        stringstream ss;
        ss << "ip route del " << route.destination << " via " << route.gw << " table " << route.route_table_id;

        Logger::instance()->log("sphirewalld.dynamicroutemanager.remove_route", INFO, "Removing route '%s'", ss.str().c_str());
        string error_string;
        if(!LinuxUtils::exec(ss.str(), error_string)){
                Logger::instance()->log("sphirewalld.dynamicroutemanager.remove_route", ERROR,
                                "Could not remove route,  route2 reported '%s'", error_string.c_str());
        }
}

