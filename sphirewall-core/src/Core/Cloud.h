/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CLOUD_H
#define CLOUD_H
#include "Core/Logger.h"
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include "Core/ConfigurationManager.h" 
#include "SFwallCore/Acl.h" 

namespace SFwallCore {
	class Connection;
};

class ConfigurationManager;
class JsonManagementService;

using namespace std;
using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
typedef ssl::stream<tcp::socket> ssl_socket;
typedef boost::shared_ptr<ssl_socket> ssl_socket_ptr;
typedef boost::shared_ptr<boost::asio::io_service> io_service_ptr;

class CloudManagementConnection : public virtual SFwallCore::FilteringRuleProvider, public virtual Configurable {
	public:
		CloudManagementConnection();

		std::string getHostname();
		std::string getKey();
		std::string getDeviceId();
		std::string getWatchdogId();

		bool status();
		void connect();
		void disconect();

		int getPort();
		bool enabled();

		bool connected();
		bool sslEnabled();
		bool verify();

		static string deviceid;

		void add_rules(SFwallCore::ACLStore* store);

		bool load();
		void save(){}
		const char* getConfigurationSystemName(){
			return "CloudManagementConnection";
		}
	private:
		bool verify_certificate(bool preverified, boost::asio::ssl::verify_context &ctx);
		void single_connector(JsonManagementService*);

		std::atomic<int> connectionCount;
		static int CLOUD_CONNECTION_POOL_SIZE;
		std::string instanceid; 
};

class CloudConfigurationChangeHandler : public ConfigurationCallback {
	public:
		CloudConfigurationChangeHandler(CloudManagementConnection* cloud){
			this->cloud = cloud;
		}

		void run(){
			cloud->deviceid = "";
		}

	private:
		CloudManagementConnection* cloud;
};

#endif
