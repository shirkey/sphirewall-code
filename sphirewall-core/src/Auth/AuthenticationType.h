#ifndef AUTHENTICATION_TYPE_H
#define AUTHENTICATION_TYPE_H

enum AuthenticationType {
	LOCAL_DB = 0, LDAP_DB = 1, PAM_DB = 2, BASIC_HTTP = 3
};

#endif
