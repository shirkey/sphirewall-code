/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USERDB_H
#define USERDB_H

#include "Core/Logger.h"
#include "Core/Lockable.h"
#include "Auth/GroupDb.h"
#include "Auth/User.h"
#include "Core/ConfigurationManager.h"
#include <boost/unordered_map.hpp>

class User;
namespace std {
	typedef basic_string<char> string;
};

class SessionDb;
class GroupRemovedListener;
class User;
class Event;
class EventDb;

class DeleteUserListener {
	public:
		virtual ~DeleteUserListener() {}
		virtual void handleDeleteUser(UserPtr user) = 0;
};

class UserDb : public GroupRemovedListener, public Lockable, public Configurable {
	public:
		class CleanupTempUsers : public CronJob {
			public:
				CleanupTempUsers(UserDb* user_db) :
					CronJob(60 * 10, "CLEANUP_TEMP_USERS_CRONJOB", true),
					user_db(user_db) {

					}

				void run();
				UserDb* user_db;
		};

		UserDb(EventDb *e, GroupDb *g, SessionDb *s);
		~UserDb();

		bool load();
		void save(bool lockIt = true);
		UserPtr createUser(std::string username);
		bool delUser(UserPtr user);

		void disableUser(UserPtr user);
		void enableUser(UserPtr user);

		bool authenticateUser(std::string username, std::string password);

		UserPtr getUser(std::string username);
		std::vector<std::string> list();
		boost::unordered::unordered_map<std::string, UserPtr> userMap();
		long getNextTokenId();
		void persistUserFromExternalSource(UserPtr user);
		void setSessionDb(SessionDb *sessionDbPtr);
		void registerDeleteListener(DeleteUserListener *target);

		//Listeners:
		void groupRemoved(GroupPtr group);
		const char* getConfigurationSystemName(){
			return "User database";
		}

	private:
		EventDb *eventDb;
		GroupDb *groupDb;
		std::string filename;
		boost::unordered::unordered_map<std::string, UserPtr> users;
		int pamAuth(char *login, char *passwd);
		SessionDb *sessionDb;
		std::list<DeleteUserListener *> deleteListeners;
};

#endif
