/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_GROUPDB_H_INCLUDED
#define SPHIREWALL_GROUPDB_H_INCLUDED

#include "Core/Logger.h"
#include "Utils/Lock.h"
#include "Core/Lockable.h"
#include "Core/ConfigurationManager.h"
#include "Auth/Group.h"

#include <map>
#include <vector>
#include <list>

class ConfigurationManager;
class Group;

class GroupRemovedListener {
	public:
		virtual void groupRemoved(GroupPtr group) = 0;
};

class GroupDb : public Lockable, public Configurable {
	public:
		GroupDb() : Lockable(){
		}

		bool load();
		void save();

		GroupPtr createGroup(std::string name, int timeout = -1);
		bool delGroup(GroupPtr target);

		GroupPtr getGroup(int id);
		GroupPtr getGroup(std::string gName);
		std::vector<GroupPtr> list();

		void registerRemovedListener(GroupRemovedListener *listener);
                const char* getConfigurationSystemName(){
                        return "Group database";
                }

	private:
		int findUniqueId();
		std::map<int, GroupPtr> groups;

		std::list<GroupRemovedListener *> removeListeners;
};

#endif


