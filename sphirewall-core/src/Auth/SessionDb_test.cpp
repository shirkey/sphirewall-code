/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>

using namespace std;

#include "Core/ConfigurationManager.h"
#include <Core/System.h>
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Auth/SessionDb.h"
#include "Core/Logger.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/IP6Addr.h"

TEST(sessionDb, findSessionsForUser) {
	Config *config = new Config();
	CronManager *cronManager = new CronManager(config);
	SFwallCore::PlainConnTracker *plainConnTracker = new SFwallCore::PlainConnTracker(NULL);
	SessionDb *sessionDb = new SessionDb(cronManager, plainConnTracker);
	sessionDb->setConfigurationManager(new ConfigurationManager());

	string mac = "OO:AA:BB:CC:DD";
	string ip = "10.0.0.1";

	UserPtr user(new User());
	SessionPtr session = sessionDb->create(user, mac, IP4Addr::stringToIP4Addr(ip));

	EXPECT_TRUE(session.get());
	EXPECT_TRUE(session.get() == sessionDb->get(mac, IP4Addr::stringToIP4Addr(ip)).get()) << "Get session from mac address";
	EXPECT_TRUE(sessionDb->findSessionsForUser(user).size() == 1);

	std::list<SessionPtr> listOfSessions = sessionDb->findSessionsForUser(user);
	std::list<SessionPtr>::iterator iter;

	for (iter = listOfSessions.begin(); iter != listOfSessions.end(); iter++) {
		EXPECT_TRUE(session == (*iter));
		break;
	}
}

TEST(sessionDb, test_ignore_mac_address_set) {
	Config *config = new Config();
	CronManager *cronManager = new CronManager(config);
	SFwallCore::PlainConnTracker *plainConnTracker = new SFwallCore::PlainConnTracker(NULL);
	SessionDb *sessionDb = new SessionDb(cronManager, plainConnTracker);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	sessionDb->SESSION_DB_IGNORE_MAC_ADDRESS = 1;

	string mac = "OO:AA:BB:CC:DD";
	string ip = "10.0.0.1";

	UserPtr user(new User());
	SessionPtr session = sessionDb->create(user, mac, IP4Addr::stringToIP4Addr(ip));

	EXPECT_TRUE(session.get() == sessionDb->get("sdfsdfsdsfd", IP4Addr::stringToIP4Addr(ip)).get()) << "Get session from mac address";
	EXPECT_TRUE(session.get() == sessionDb->get("", IP4Addr::stringToIP4Addr(ip)).get()) << "Get session from mac address";
	EXPECT_TRUE(session.get() == sessionDb->get("OO:AA:BB:CC:DD", IP4Addr::stringToIP4Addr(ip)).get()) << "Get session from mac address";
	EXPECT_TRUE(sessionDb->findSessionsForUser(user).size() == 1);

	std::list<SessionPtr> listOfSessions = sessionDb->findSessionsForUser(user);
	std::list<SessionPtr>::iterator iter;

	for (iter = listOfSessions.begin(); iter != listOfSessions.end(); iter++) {
		EXPECT_TRUE(session == (*iter));
		break;
	}
}

TEST(sessionDb, create_and_get_ipv6) {
	Config *config = new Config();
	CronManager *cronManager = new CronManager(config);
	SFwallCore::PlainConnTracker *plainConnTracker = new SFwallCore::PlainConnTracker( NULL);
	SessionDb *sessionDb = new SessionDb( cronManager, plainConnTracker);
	sessionDb->setConfigurationManager(new ConfigurationManager());

	string mac = "OO:AA:BB:CC:DD";
	string ip = "2607:f0d0:2001:a::1";

        UserPtr user(new User());
	SessionPtr session = sessionDb->create(user, mac, IP6Addr::fromString(ip));

	EXPECT_TRUE(session.get() == sessionDb->get(mac, IP6Addr::fromString(ip)).get()) << "Get session from mac address";
	EXPECT_TRUE(sessionDb->get(mac, IP6Addr::fromString("2607:f0d0:2001:a::2")).get() == NULL);
	EXPECT_TRUE(sessionDb->list().size() == 1);

	sessionDb->deleteSession(session);
	EXPECT_TRUE(sessionDb->list().size() == 0);
}

TEST(session, timeout_times_out) {
	System::getInstance()->getTimeManager().useThisTime(0);
	Config *config = new Config();
	config->setConfigurationManager(System::getInstance()->getConfigurationManager());
	System::getInstance()->getConfigurationManager()->put("general:session_timeout", (double) 200);
	SessionDb *sessionDb = new SessionDb(NULL, NULL);

	GroupDb *groupDb = new GroupDb();
	GroupPtr group = groupDb->createGroup("group1", 5);
        UserPtr user(new User());
	user->addGroup(group);

	SessionPtr nsp = sessionDb->create(user, "OO:AA:BB:CC:DD", IP4Addr::stringToIP4Addr("10.0.0.1"));
	EXPECT_TRUE(nsp->getTimeoutLength() == 5);

}

TEST(session, network_based_timeout_times_out) {
	Config *config = new Config();
	config->setConfigurationManager(System::getInstance()->getConfigurationManager());
	System::getInstance()->getConfigurationManager()->put("general:session_timeout", (double) 900000000);

	SessionDb *sessionDb = new SessionDb(NULL, NULL);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	
	SFwallCore::AliasPtr alias = SFwallCore::AliasPtr(new SFwallCore::IpRangeAlias());
	alias->addEntry("10.0.0.0-10.0.0.255");	

	NetworkBasedTimeoutPtr nbto = NetworkBasedTimeoutPtr(new NetworkBasedTimeout());
	nbto->timeout = 2;
	nbto->networks.push_back(alias);
	sessionDb->saveNetworkRangeTimeout(nbto);
	
        UserPtr user(new User());
	SessionPtr nsp = sessionDb->create(user, "OO:AA:BB:CC:DD", IP4Addr::stringToIP4Addr("10.0.0.1"));
	EXPECT_TRUE(nsp->getTimeoutLength() == 2);
}


TEST(sessionDb, persisted_sessions) {
	Config *config = new Config();
	CronManager *cronManager = new CronManager(config);
	SFwallCore::PlainConnTracker *plainConnTracker = new SFwallCore::PlainConnTracker(NULL);
	SessionDb *sessionDb = new SessionDb(cronManager, plainConnTracker);
	sessionDb->setConfigurationManager(new ConfigurationManager());
	UserDb *userDb = new UserDb(NULL, NULL, sessionDb);
	sessionDb->setUserDb(userDb);

	string mac = "OO:AA:BB:CC:DD";
	string ip = "192.168.1.1";

	userDb->createUser("admin");

	EXPECT_TRUE(sessionDb->get(mac, IP4Addr::stringToIP4Addr(ip)).get() == NULL);
	EXPECT_TRUE(sessionDb->list().size() == 0);

	sessionDb->persistSession(mac, "admin");

	EXPECT_TRUE(sessionDb->get(mac, IP4Addr::stringToIP4Addr(ip)).get());
	EXPECT_TRUE(sessionDb->list().size() == 1);
}

TEST(session, absolute_timeout){
	Session* session = new Session(UserPtr(), "mac", 0, 10, true);
	System::getInstance()->getTimeManager().useThisTime(10);
	EXPECT_FALSE(session->isTimedOut());
	
	System::getInstance()->getTimeManager().useThisTime(11);
	EXPECT_TRUE(session->isTimedOut());
}
