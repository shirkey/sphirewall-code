/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/unordered_map.hpp>
#include <sstream>
#include <fstream>

#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Utils/Hash.h"
#include "Auth/SessionDb.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"

UserDb::UserDb(EventDb *e, GroupDb *g, SessionDb *s)
	: Lockable(), eventDb(e), groupDb(g), sessionDb(s)
{}


UserDb::~UserDb() {
}

bool UserDb::load() {
	users.clear();
	Logger::instance()->log("sphirewalld.userdb", EVENT, "Loading User Database");

	if (!configurationManager->has("userDb")) {
		Logger::instance()->log("sphirewalld.userdb", ERROR, "No users found in configuration, loading defaults");
		/*Lets load into the db a default admin user*/

		UserPtr defaultAdminUser(new User("admin"));
		defaultAdminUser->setCreationTime(time(NULL));
		defaultAdminUser->setPassword("admin");
		GroupPtr group = groupDb->getGroup(1);

		defaultAdminUser->addGroup(group);
		defaultAdminUser->enable();
		defaultAdminUser->setAuthenticationType(LOCAL_DB);
		users.insert(pair<string, UserPtr>(defaultAdminUser->getUserName(), defaultAdminUser));
		save();
	}
	else {
		ObjectContainer *root = configurationManager->getElement("userDb");
		ObjectContainer *users = root->get("users")->container();

		for (int x = 0; x < users->size(); x++) {
			ObjectContainer *o = users->get(x)->container();
			UserPtr temp;

			temp = UserPtr(new User());
			temp->user = o->get("username")->string();

			if (o->has("firstname")) {
				temp->fname = o->get("firstname")->string();
			}

			if (o->has("lastname")) {
				temp->lname = o->get("lastname")->string();
			}

			if (o->has("password")) {
				temp->password = o->get("password")->string();
			}

			temp->isEnabled = o->get("enabled")->boolean();
			temp->email = o->get("email")->string();

			if (o->has("dn")) {
				temp->dn = o->get("dn")->string();
			}

			temp->email = o->get("email")->string();
			temp->setAuthenticationType((AuthenticationType) o->get("authenticationType")->number());
			temp->setCreationTime(o->get("createdTime")->number());

			QuotaInfo *quota = temp->getQuota();

			if (o->has("dailyQuota")) {
				quota->dailyQuota = o->get("dailyQuota")->boolean();
				quota->dailyQuotaLimit = o->get("dailyQuotaLimit")->number();
			}

			if (o->has("weeklyQuota")) {
				quota->weeklyQuota = o->get("weeklyQuota")->boolean();
				quota->weeklyQuotaLimit = o->get("weeklyQuotaLimit")->number();
			}

			if (o->has("monthQuota")) {
				quota->monthQuota = o->get("monthQuota")->boolean();
				quota->monthQuotaLimit = o->get("monthQuotaLimit")->number();
			}

			if (o->has("totalQuota")) {
				quota->totalQuota = o->get("totalQuota")->boolean();
				quota->totalQuotaLimit = o->get("totalQuotaLimit")->number();
			}

			if (o->has("timeQuota")) {
				quota->timeQuota = o->get("timeQuota")->boolean();
				quota->timeQuotaLimit = o->get("timeQuotaLimit")->number();
			}

			if(o->has("temp_user")){
                                temp->temp_user = o->get("temp_user")->boolean();
                                temp->expiry_timestamp= o->get("expiry_timestamp")->number();
			}

			ObjectContainer *groups = o->get("groups")->container();

			for (int gid = 0; gid < groups->size(); gid++) {
				GroupPtr g;

				if (groups->get(gid)->isContainer()) {
					ObjectContainer *jsonGroup = groups->get(gid)->container();
					g = groupDb->getGroup(jsonGroup->get("id")->number());
				}
				else {
					g = groupDb->getGroup(groups->get(gid)->number());
				}

				if (g != NULL) {
					temp->groups.push_back(g);
				}
			}

			this->users[temp->user] = temp;
		}
	}

        System::getInstance()->getCronManager()->registerJob(new UserDb::CleanupTempUsers(this));
	return true;
}

/**
  Saves changes to the user database.

  @params lockIt - determines if this method should lock the user DB.
  Defaults to true (see UserDb.h).  Set to false if calling routine
  has user DB already locked.
 **/
void UserDb::save(bool lockIt) {
	if (lockIt) {
		holdLock();
	}

	if (configurationManager) {
		ObjectContainer *root = new ObjectContainer(CREL);
		ObjectContainer *array = new ObjectContainer(CARRAY);

		for (pair<string, UserPtr> p : users) {
			UserPtr source = p.second;
			if(!source){
				continue;
			}
			
			ObjectContainer *user = new ObjectContainer(CREL);
			user->put("username", new ObjectWrapper((string) source->user));

			if (source->fname.size() > 0) {
				user->put("firstname", new ObjectWrapper((string) source->fname));
			}

			if (source->lname.size() > 0) {
				user->put("lastname", new ObjectWrapper((string) source->lname));
			}

			user->put("enabled", new ObjectWrapper((bool) source->isEnabled));
			user->put("email", new ObjectWrapper((string) source->email));
			user->put("password", new ObjectWrapper((string) source->password));
			user->put("createdTime", new ObjectWrapper((double) source->getCreationTime()));
			user->put("dn", new ObjectWrapper((string) source->dn));

			user->put("temp_user", new ObjectWrapper((bool) source->temp_user));
			user->put("expiry_timestamp", new ObjectWrapper((double) source->expiry_timestamp));

			QuotaInfo *quotas = source->getQuota();

			if (quotas->dailyQuota) {
				user->put("dailyQuota", new ObjectWrapper((bool) quotas->dailyQuota));
				user->put("dailyQuotaLimit", new ObjectWrapper((double) quotas->dailyQuotaLimit));
			}

			if (quotas->weeklyQuota) {
				user->put("weeklyQuota", new ObjectWrapper((bool) quotas->weeklyQuota));
				user->put("weeklyQuotaLimit", new ObjectWrapper((double) quotas->weeklyQuotaLimit));
			}

			if (quotas->monthQuota) {
				user->put("monthQuota", new ObjectWrapper((bool) quotas->monthQuota));
				user->put("monthQuotaLimit", new ObjectWrapper((double) quotas->monthQuotaLimit));
			}

			if (quotas->totalQuota) {
				user->put("totalQuota", new ObjectWrapper((bool) quotas->totalQuota));
				user->put("totalQuotaLimit", new ObjectWrapper((double) quotas->totalQuotaLimit));
			}

			if (quotas->timeQuota) {
				user->put("timeQuota", new ObjectWrapper((bool) quotas->timeQuota));
				user->put("timeQuotaLimit", new ObjectWrapper((double) quotas->timeQuotaLimit));
			}

			ObjectContainer *groups = new ObjectContainer(CARRAY);

			for (int x = 0; x < (int) source->groups.size(); x++) {
				groups->put(new ObjectWrapper((double) source->groups[x]->getId()));
			}

			user->put("groups", new ObjectWrapper(groups));
			user->put("authenticationType", new ObjectWrapper((double) source->getAuthenticationType()));
			array->put(new ObjectWrapper(user));
		}

		root->put("users", new ObjectWrapper(array));
		configurationManager->setElement("userDb", root);
		configurationManager->save();
	}

	if (lockIt) {
		releaseLock();
	}
}

UserPtr UserDb::createUser(string username) {
	UserPtr user(new User(username));
	users[user->user] = user;
	user->setAuthenticationType(LOCAL_DB);
	user->setCreationTime(time(NULL));

	save();
	return user;
}

bool UserDb::delUser(UserPtr user) {
	boost::unordered_map<string, UserPtr>::iterator location;
	holdLock();

	std::for_each(deleteListeners.begin(), deleteListeners.end(), boost::bind(&DeleteUserListener::handleDeleteUser, _1, user));

	if ((location = users.find(user->user)) != users.end()) {
		users.erase(location);
		save(false);
	}

	releaseLock();
	return true;
}

UserPtr UserDb::getUser(string username) {
	return users[username];
}

bool UserDb::authenticateUser(string username, string password) {
	UserPtr u;

	if ((u = getUser(username)) && u->isEnabled) {
		if (Hash::check_hash(password, u->getPassword())) {
			if (Hash::shouldUpdate(u->getPassword())) {
				u->password = Hash::create_hash(password);
			}

			u->lastLogin = time(NULL);
			save();
			return true;
		}
		else {
			u->lastFail = time(NULL);
			save();
		}
	}

	return false;
}

vector<string> UserDb::list() {
	vector<string> ret;

	for (pair<string, UserPtr> p : users) {
		ret.push_back(p.first);
	}

	return ret;
}

void UserDb::disableUser(UserPtr user) {
	user->disable();
	sessionDb->deleteSessionsForUser(user);
}

void UserDb::enableUser(UserPtr user) {
	user->enable();
}

void UserDb::persistUserFromExternalSource(UserPtr user) {
	users[user->getUserName()] = user;
	save();
}

void UserDb::setSessionDb(SessionDb *sessionDbPtr) {
	sessionDb = sessionDbPtr;
	registerDeleteListener(sessionDbPtr);
}

void UserDb::registerDeleteListener(DeleteUserListener *target) {
	deleteListeners.push_back(target);
}

void UserDb::groupRemoved(GroupPtr group) {
	holdLock();

	for (std::pair<string, UserPtr> p : users) {
		UserPtr user = p.second;

		if (user->checkForGroup(group)) {
			user->removeGroup(group);
		}
	}

	releaseLock();
}

void UserDb::CleanupTempUsers::run(){
	bool user_to_delete = false;
	for(string username : user_db->list()){
		UserPtr user = user_db->getUser(username);
		if(user){
			if(user->temp_user && time(NULL) > user->expiry_timestamp){
				Logger::instance()->log("sphirewalld.userdb.cleanup", INFO, 
					"Deleting temporary user %s as they have expired", user->getUserName().c_str());

				user_db->delUser(user);
				user_to_delete = true;
			}
		}
	}

	if(user_to_delete){
		user_db->save();
	}
}
