/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Auth/AuthenticationHandler.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include <iostream>
#include <fcntl.h>
#include <sys/resource.h>
#include <security/pam_appl.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "Utils/HttpRequestWrapper.h"
#include <boost/regex.hpp>
#include <set>

#define STATUS_OK 	  0
#define STATUS_UNKNOWN	  1
#define STATUS_INVALID	  2
#define STATUS_BLOCKED    3
#define STATUS_EXPIRED    4
#define STATUS_PW_EXPIRED 5
#define STATUS_NOLOGIN    6
#define STATUS_MANYFAILS  7

#define STATUS_INT_USER  50
#define STATUS_INT_ARGS  51
#define STATUS_INT_ERR   52

int hisuid;
int haveuid = 0;

#define BFSZ 1024
#define msgi(i) (*(msg[i]))

struct ad_user {
	char *login;
	char *passwd;
};

#define SCOPE LDAP_SCOPE_SUBTREE
#define LDAP_DEPRECATED 1

#include <ldap.h>

using namespace std;

UserPtr AuthenticationManager::resolveUser(std::string username) {
	lock->lock(); //This is not such a great idea, but needed at the moment:
	Logger::instance()->log("sphirewalld.auth.manager", INFO, "resolving user " + username);

	UserPtr userCopyFromDatabase = localUserDb->getUser(username);

	if (userCopyFromDatabase) {
		Logger::instance()->log("sphirewalld.auth.manager", DEBUG, "found user in localdb ");
		//Check the user type:
		lock->unlock();
		return userCopyFromDatabase;
	}
	else {
		Logger::instance()->log("sphirewalld.auth.manager", DEBUG, "user was not found in sphirewalldb, searching external authentication handlers");

		//Need to get some information on this user, we dont know anything about him yet
		for (AuthenticationMethod * method : this->methods) {
			if (method->isEnabled()) {
				try {
					UserPtr newUserFound = method->findUser(username);
					if (newUserFound) {
						Logger::instance()->log("sphirewalld.auth.manager", DEBUG, "user was found in external authentication handler");
						method->updateUser(localGroupDb, newUserFound);
						localUserDb->persistUserFromExternalSource(newUserFound);

						//Add groups:
						if (System::getInstance()->configurationManager.hasByPath("authentication:remote_authentication_group")) {
							int groupId = System::getInstance()->configurationManager.get("authentication:remote_authentication_group")->number();

							if (groupId != -1) {
								GroupPtr group = localGroupDb->getGroup(groupId);
								if (group) {
									newUserFound->addGroup(group);
								}
							}
						}

						lock->unlock();
						return newUserFound;
					}
				}catch(AuthenticationMethodException& exception){		
					 Logger::instance()->log("sphirewalld.auth.manager", ERROR, "a critical error occurred while searching for user in an authentication handler '%s'", exception.what().c_str());
				}	
			}
		}
	}

	lock->unlock();
	Logger::instance()->log("sphirewalld.auth.manager", INFO, "user could not be found");
	return UserPtr();
}

bool AuthenticationManager::authenticate(std::string username, std::string password) {
	Logger::instance()->log("sphirewalld.auth.manager.authenticate", DEBUG, "checking password for user " + username);

	UserPtr userCopyFromDatabase = resolveUser(username);

	if (userCopyFromDatabase) {
		//Check the user type:
		AuthenticationMethod *method = getMethodForUser(userCopyFromDatabase->getAuthenticationType());

		if (method) {
			bool result = false;
			try {
				result = method->authenticate(userCopyFromDatabase, username, password);
			}catch(AuthenticationMethodException& exception){
			        Logger::instance()->log("sphirewalld.auth.manager.authenticate", ERROR, "error during authentication request, '%s'", exception.what().c_str());		
			}
			return result;
		}
	}

	return false;
}

AuthenticationMethod *AuthenticationManager::getMethodForUser(AuthenticationType methodType) {
	list<AuthenticationMethod *>::iterator iter;

	for(AuthenticationMethod* method : methods){
		if (method->canConnect() && method->isEnabled()) {
			if (method->getType() == methodType) {
				return method;
			}
		}
	}

	return NULL;
}

bool LocalDbAuthenticationMethod::authenticate(UserPtr userObject, std::string username, std::string password) {
	return userDb->authenticateUser(username, password);
}

UserPtr LocalDbAuthenticationMethod::findUser(std::string username) {
	return userDb->getUser(username);
}

void AuthenticationManager::SyncCron::run() {
	authenticationManager->lock->lock();
	for (AuthenticationMethod* method : authenticationManager->methods) {
		if (method->isEnabled()) {
			try {
				method->sync(authenticationManager->localUserDb, authenticationManager->localGroupDb);
			}catch(AuthenticationMethodException& exception){
				Logger::instance()->log("sphirewalld.auth.manager.sync", ERROR, "error during sync, '%s'", exception.what().c_str());
			}
		}
	}

	authenticationManager->localUserDb->save();
	authenticationManager->localGroupDb->save();
	authenticationManager->lock->unlock();
}

bool PamAuthenticationMethod::isEnabled() {
	if (configurationManager->hasByPath("authentication:pam:enabled")) {
		return configurationManager->get("authentication:pam:enabled")->boolean();
	}

	return false;
}

bool PamAuthenticationMethod::canConnect() {
	return true;
}

int PAM_conv(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr) {
	struct ad_user *user = (struct ad_user *) appdata_ptr;
	struct pam_response *response;
	int i;

	if (msg == NULL || resp == NULL || user == NULL)
		return PAM_CONV_ERR;

	response = (struct pam_response *)
		malloc(num_msg * sizeof(struct pam_response));

	for (i = 0; i < num_msg; i++) {
		response[i].resp_retcode = 0;
		response[i].resp = NULL;

		switch (msgi(i).msg_style) {
			case PAM_PROMPT_ECHO_ON:
				/* Store the login as the response */
				/* This likely never gets called, since login was on pam_start() */
				response[i].resp = appdata_ptr ? (char *) strdup(user->login) : NULL;
				break;

			case PAM_PROMPT_ECHO_OFF:
				/* Store the password as the response */
				response[i].resp = appdata_ptr ? (char *) strdup(user->passwd) : NULL;
				break;

			case PAM_TEXT_INFO:
			case PAM_ERROR_MSG:
				/* Shouldn't happen since we have PAM_SILENT set. If it happens
				 * anyway, ignore it. */
				break;

			default:

				/* Something strange... */
				if (response != NULL) free(response);

				return PAM_CONV_ERR;
		}
	}

	/* On success, return the response structure */
	*resp = response;
	return PAM_SUCCESS;
}

bool PamAuthenticationMethod::authenticate(UserPtr userObject, std::string username, std::string password) {

	char *usernameArray = (char *) malloc(username.size() * sizeof(char));
	char *passwordArray = (char *) malloc(password.size() * sizeof(char));
	memcpy((void *) usernameArray, (void *) username.c_str(), username.size());
	memcpy((void *) passwordArray, (void *) password.c_str(), password.size());

	char *strchr();
	struct rlimit rlim;

	rlim.rlim_cur = rlim.rlim_max = 0;
	(void) setrlimit(RLIMIT_CORE, &rlim);

	struct ad_user user_info = {usernameArray, passwordArray};
	struct pam_conv conv = {PAM_conv, (void *) &user_info};
	pam_handle_t *pamh = NULL;
	int retval;

	user_info.login = usernameArray;
	user_info.passwd = passwordArray;

	string pam_module;

	if (configurationManager->hasByPath("authentication:pam:module")) {
		pam_module = configurationManager->get("authentication:pam:module")->string();
	}
	else {
		pam_module = "common-password";
	}

	retval = pam_start((char *) pam_module.c_str(), usernameArray, &conv, &pamh);

	if (retval == PAM_SUCCESS)
		retval = pam_authenticate(pamh, PAM_SILENT);

	if (retval == PAM_SUCCESS)
		retval = pam_acct_mgmt(pamh, 0);

	if (pam_end(pamh, retval) != PAM_SUCCESS) {
		pamh = NULL;
		free(usernameArray);
		free(passwordArray);
		return false;
	}

	if (retval == 0) {
		free(usernameArray);
		free(passwordArray);
		return true;
	}

	return false;
}

UserPtr PamAuthenticationMethod::findUser(std::string username) {
	User *tempUser = new User();
	tempUser->setUserName(username);
	tempUser->setAuthenticationType(PAM_DB);
	return UserPtr(tempUser);
}

//Basic Auth Handler:
bool BasicHttpAuthenticationHandler::isEnabled() {
	if (configurationManager->hasByPath("authentication:basic:enabled")) {
		return configurationManager->get("authentication:basic:enabled")->boolean();
	}

	return false;
}

bool BasicHttpAuthenticationHandler::authenticate(UserPtr userObj, std::string username, std::string password) {
	string baseurl = configurationManager->get("authentication:basic:baseurl")->string();
	stringstream ss;
	ss << baseurl << "?username=" << username << "&password=" << password;

	HttpRequestWrapper *req = new HttpRequestWrapper(ss.str(), GET);

	try {
		string response = req->execute();

		if (response.compare("OK") == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	catch (const HttpRequestWrapperException &e) {
		return false;
	}

	delete req;
	return false;
}

UserPtr BasicHttpAuthenticationHandler::findUser(std::string username) {
	User *tempUser = new User();
	tempUser->setUserName(username);
	tempUser->setAuthenticationType(BASIC_HTTP);
	return UserPtr(tempUser);
}

AuthenticationManager::AuthenticationManager(CronManager *cronManager) {
	lock = new Lock();
}

void AuthenticationManager::addMethod(AuthenticationMethod *method) {
	methods.push_back(method);
}

AuthenticationMethod *AuthenticationManager::getMethod(AuthenticationType type) {
	std::list<AuthenticationMethod *>::iterator iter;

	for (iter = methods.begin(); iter != methods.end(); iter++) {
		AuthenticationMethod *m = (*iter);

		if (m->getType() == type) {
			return m;
		}
	}

	return NULL;
}


