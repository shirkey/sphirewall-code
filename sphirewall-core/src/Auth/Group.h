/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GROUPS_H
#define GROUPS_H

#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include "Core/QuotaManager.h"

class Group {
	public:
		Group();
		Group(int id, std::string name, std::string desc, std::string manager, bool allow_mui, int timeoutLength = -1);

		void setId(int id);
		void setName(std::string name);
		void setDesc(std::string desc);
		void setManager(std::string manager);
		void setAllowMui(bool a);
		void setQuota(QuotaInfo &quota);
		void setTimeout(int timeoutLength = -1);
		void setMetadata(std::string m);

		int getId() const;
		std::string getName() const;
		std::string getDesc() const;
		std::string getManager() const;
		std::string getMetadata() const;
		bool isAllowMui() const;
		QuotaInfo &getQuota();
		int getTimeout();

	private:
		int id;
		std::string name;
		std::string desc;
		std::string manager;
		bool allow_mui;
		QuotaInfo quota;
		int timeoutLength;
		std::string metadata;
};

typedef boost::shared_ptr<Group> GroupPtr;

#endif
