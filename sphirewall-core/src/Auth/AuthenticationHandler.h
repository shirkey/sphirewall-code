/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUTH
#define AUTH

#define LDAP_DEPRECATED 1

#include <ldap.h>
#include <list>
#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Auth/AuthenticationType.h"

class UserDb;
class User;
class ConfigurationManager;
class GroupDb;
class Lock;
typedef boost::shared_ptr<User> UserPtr;

class AuthenticationMethod {
	public:
		virtual bool authenticate(UserPtr user, std::string username, std::string password) = 0;
		virtual bool isEnabled() = 0;
		virtual bool canConnect() = 0;
		virtual AuthenticationType getType() = 0;
		virtual UserPtr findUser(std::string username) = 0;
		virtual int sync(UserDb *, GroupDb *) {
			return 0;
		}
		virtual void updateUser(GroupDb *, UserPtr user) {}
};

class AuthenticationMethodException: std::exception {
        public:
                AuthenticationMethodException(std::string what) : exception_value(what){}
                ~AuthenticationMethodException() throw(){}
                std::string what() {
			return exception_value;
		}	

	private:
                std::string exception_value;
};

class BasicHttpAuthenticationHandler : public virtual AuthenticationMethod {
	public:

		BasicHttpAuthenticationHandler() {
		}

		AuthenticationType getType() {
			return BASIC_HTTP;
		}

		bool isEnabled();
		bool canConnect() {
			return true;
		}

		bool authenticate(UserPtr user, std::string username, std::string password);
		UserPtr findUser(std::string username);

		void setConfigurationManager(ConfigurationManager *configurationManager) {
			this->configurationManager = configurationManager;
		}

	protected:
		ConfigurationManager *configurationManager;

};


class PamAuthenticationMethod : public virtual AuthenticationMethod {
	public:

		PamAuthenticationMethod() {
		}

		AuthenticationType getType() {
			return PAM_DB;
		}

		bool isEnabled();
		bool canConnect();
		bool authenticate(UserPtr user, std::string username, std::string password);
		UserPtr findUser(std::string username);

		void setConfigurationManager(ConfigurationManager *configurationManager) {
			this->configurationManager = configurationManager;
		}

	protected:
		ConfigurationManager *configurationManager;

};


#define AD_EDIRECTORY 0
#define OPENLDAP 1

class LdapAuthenticationMethod : public virtual AuthenticationMethod {
	public:

		LdapAuthenticationMethod() {
		}

		AuthenticationType getType() {
			return LDAP_DB;
		}

		bool isEnabled();
		bool canConnect();
		bool authenticate(UserPtr user, std::string username, std::string password);
		UserPtr findUser(std::string username);
		void setConfigurationManager(ConfigurationManager *configurationManager) {
			this->configurationManager = configurationManager;
		}

		int sync(UserDb *, GroupDb *);
		std::string getUserSearchPrefix();
		void updateUser(GroupDb *, UserPtr user);
	protected:
		ConfigurationManager *configurationManager;
		LDAP *connect(std::string hostname, int port, std::string username, std::string password);

		//Server properties:
		std::string getHostname() const;
		int getPort() const;
		std::string getLdapUsername() const;
		std::string getLdapPassword() const;
		std::string getBaseDn() const;
		std::string getUserNameAttribute();
		std::string getUserBase();
		std::string getUserGroupAttribute();
		std::string getGroupBase();
		std::string getGroupNameAttribute();
		std::string getGroupMemberNameAttribute();

		std::string getUserDnAttribute();
		bool authenticateUserUsingSuppliedDn();
		bool userOwnsGroupMembership();

		void refreshUserGroups(UserPtr user);
	private:
		std::string processGroupAttribute(std::string input);
};

class LocalDbAuthenticationMethod : public virtual AuthenticationMethod {
	public:

		bool isEnabled() {
			return true;
		}

		AuthenticationType getType() {
			return LOCAL_DB;
		}

		bool canConnect() {
			return true;
		}

		bool authenticate(UserPtr user, std::string username, std::string password);
		UserPtr findUser(std::string username);

		void setUserDb(UserDb *userDb) {
			this->userDb = userDb;
		}

	private:
		UserDb *userDb;
};

class AuthenticationManager {
	public:

		class SyncCron: public CronJob {
			public:
				SyncCron(AuthenticationManager *authenticationManager)
					: CronJob(60 * 60, "AUTHENTICATION_MANAGER_SYNC_CRON", true), authenticationManager(authenticationManager) {
				}
				void run();
			private:
				AuthenticationManager *authenticationManager;
		};


		AuthenticationManager(CronManager *cronManager);
		bool authenticate(std::string username, std::string password);
		AuthenticationMethod *getMethodForUser(AuthenticationType methodType);
		UserPtr resolveUser(std::string username);

		void setLocalUserDb(UserDb *uDb) {
			this->localUserDb = uDb;
		}

		void setLocalGroupDb(GroupDb *gdb) {
			this->localGroupDb = gdb;
		}

		void addMethod(AuthenticationMethod *method);
		AuthenticationMethod *getMethod(AuthenticationType type);
	private:
		std::list<AuthenticationMethod *> methods;

		UserDb *localUserDb;
		GroupDb *localGroupDb;
		Lock *lock;
};

#endif
