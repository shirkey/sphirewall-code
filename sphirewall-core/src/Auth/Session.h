/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SESSIONS_H_INCLUDED
#define SPHIREWALL_SESSIONS_H_INCLUDED

#include <vector>
#include <string>
#include <netinet/ip6.h>
#include <boost/shared_ptr.hpp>

class User;
typedef boost::shared_ptr<User> UserPtr;
class Session {
	public:
		Session();
		Session(UserPtr user, std::string mac, unsigned int ipaddress, int timeout = 600, bool absoluteTimeout=false);
		Session(UserPtr user, std::string mac, struct in6_addr ipaddress, int timeout = 600, bool absoluteTimeout=false);

		std::string getMac() const;
		std::string getUserName() const;
		std::string getIp() ;
		unsigned int getIp_Int() const;
		int getStartTime() const;
		int getSessionLast() const;
		int getTimeoutLength() const;
		int getType() const;

		void touch();
		UserPtr getUser() const;

		bool isTimedOut();
	private:
		std::string macAddr;
		int sessionStartTime;
		int lastPacketTime;
		int sessionTimeoutLength;
		bool absoluteTimeout;

		UserPtr user;
		unsigned int ip;
		struct in6_addr ipv6;
		int type;
};

typedef boost::shared_ptr<Session> SessionPtr;
#endif
