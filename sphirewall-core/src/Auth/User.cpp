/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <map>

using namespace std;
using std::string;

#include "Core/Logger.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Auth/User.h"
#include "Utils/Hash.h"

int User::getCreationTime() const {
	return this->creationTime;
}

User::User()
	: quotaExceeded(false), dn(""), isEnabled(true), fname(""), lname(""), password(""), lastLogin(0), lastFail(0),
	  creationTime(time(NULL)), isTemp(false), user(""), email(""), quota(new QuotaInfo), groupLock(new Lock()),
	  authenticationType(LOCAL_DB), quotaCounter(new QuotaCounter())
{
	temp_user = false;
	expiry_timestamp = -1;

}

User::User(string username)
	: quotaExceeded(false), dn(""), isEnabled(true), fname(""), lname(""), password(""), lastLogin(0), lastFail(0),
	creationTime(time(NULL)), isTemp(false), user(username), email(""), quota(new QuotaInfo), groupLock(new Lock()),
	authenticationType(LOCAL_DB), quotaCounter(new QuotaCounter()) {
		temp_user = false;
		expiry_timestamp = -1;

	}

QuotaInfo *User::getQuota() const {
	return this->quota;
}

bool User::isQuotaExceeded() const {
	return this->quotaExceeded;
}

void User::setCreationTime(int t) {
	this->creationTime = t;
}

void User::setQuotaExceeded(bool value) {
	this->quotaExceeded = value;
}

User::~User()
{}

bool User::enableDisable(bool enable) {
	isEnabled = enable;
	return true;
}

void User::setPassword(string pwd) {
	password = Hash::create_hash(pwd);
}

bool User::checkForGroup(GroupPtr group) {
	groupLock->lock();

	for (int x = 0; x < (int) groups.size(); x++) {
		if (group->getId() == groups[x]->getId()) {
			groupLock->unlock();
			return true;
		}
	}

	groupLock->unlock();
	return false;
}

void User::addGroup(GroupPtr group) {
	if (!checkForGroup(group)) {
		Logger::instance()->log("sphirewalld.userdb", DEBUG, "adding group to user");

		groupLock->lock();
		groups.push_back(group);
		groupLock->unlock();
	}
}

void User::removeGroup(GroupPtr group) {
	groupLock->lock();

	for (int x = 0; x < (int) groups.size(); x++) {
		if (groups[x]->getId() == group->getId()) {
			groups.erase(groups.begin() + x);
			break;
		}
	}

	groupLock->unlock();
}

bool User::enable() {
	isEnabled = true;
	return true;
}

bool User::disable() {
	isEnabled = false;
	return true;
}

bool User::isAllowedMuiAccess() {
	groupLock->lock();

	for (GroupPtr group : groups) {
		if (group->isAllowMui()) {
			groupLock->unlock();
			return true;
		}
	}

	groupLock->unlock();
	return false;
}

string User::getUserName() {
	return user;
}

string User::getFname() {
	return fname;
}

string User::getLname() {
	return lname;
}

string User::getPassword() {
	return password;
}

bool User::getIsEnabled() {
	return isEnabled;
}

int User::getLastLogin() {
	return lastLogin;
}

int User::getLastFail() {
	return lastFail;
}

std::string User::getEmail() const {
	return email;
}

void User::setFname(string str) {
	fname = str;
}

void User::setLname(string str) {
	lname = str;
}

void User::setGroups(vector<GroupPtr> groups) {
	groupLock->lock();
	this->groups = groups;
	groupLock->unlock();
}

void User::setEmail(std::string email) {
	this->email = email;
}

void User::setUserName(std::string n) {
	this->user = n;
}

AuthenticationType User::getAuthenticationType() {
	return this->authenticationType;
}

void User::setAuthenticationType(AuthenticationType authenticationType) {
	this->authenticationType = authenticationType;
}

std::vector<GroupPtr> User::getGroups() const {
	return groups;
}
