/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
using namespace std;

#include "Auth/AuthenticationHandler.h"
#include "Auth/UserDb.h"
#include "Auth/User.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include <iostream>
#include <fcntl.h>
#include <sys/resource.h>
#include <security/pam_appl.h>
#include <pwd.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include "Utils/HttpRequestWrapper.h"
#include <boost/regex.hpp>
#include <set>

#define SCOPE LDAP_SCOPE_SUBTREE
#define LDAP_DEPRECATED 1
#include <ldap.h>

using namespace std;

class LdapEntry {
	public:
		multimap<string, string> attributes;

		bool has(string key) {
			return attributes.find(key) != attributes.end();
		}

		std::string scalar(const std::string key) {
			for (multimap<string, string>::iterator iter = attributes.find(key); iter != attributes.end(); iter++) {
				return iter->second;
			}

			return "";
		}
};

class LdapSearchResult {
	public:
		int resultcode;
		std::list<LdapEntry> entries;
};

LdapSearchResult ldapSearch(LDAP *connection, const char *basedn, const char *search) {
	Logger::instance()->log("sphirewalld.auth.manager.ldapSearch", DEBUG, 
		"ldapSearch called basedn = '%s', search = '%s'", basedn, search);

	LdapSearchResult res;
	LDAPMessage *result;

	const char *attributes[] = {"*", "+", NULL};
	int rc = ldap_search_ext_s(connection, basedn, SCOPE, search, const_cast<char **>((const char **)attributes), 0, NULL, NULL, NULL, 0, &result);
	res.resultcode = rc;

	if (rc == LDAP_SUCCESS) {
		LDAPMessage *entry = NULL;

		for (entry = ldap_first_entry(connection, result); entry != NULL;
				entry = ldap_next_entry(connection, entry)) {

			LdapEntry target;
			BerElement *ber;

			for (char *attribute = ldap_first_attribute(connection, entry, &ber);
					attribute != NULL;  attribute = ldap_next_attribute(connection, entry, ber)) {

				char **values = ldap_get_values(connection, entry, attribute);

				if (values != NULL) {
					for (int i = 0; values[i] != NULL; i++) {
						Logger::instance()->log("sphirewalld.auth.manager.ldapSearch.attributes", INFO, 
							"ldapSearch pulled %s - %s", attribute, values[i]);
						target.attributes.insert(pair<std::string, std::string>(attribute, values[i]));
					}

					ldap_value_free(values);
				}

				ldap_memfree(attribute);
			}

			if (ber != NULL) {
				ber_free(ber, 0);
			}

			res.entries.push_back(target);
		}
	}

	Logger::instance()->log("sphirewalld.auth.manager.ldapSearch", DEBUG,
		"ldapSearch returned status code %d and no entries %d", res.resultcode, res.entries.size());

	ldap_msgfree(result);
	return res;
}


std::string LdapAuthenticationMethod::processGroupAttribute(std::string input) {
	string groupMembershipRaw = input;
	static boost::regex p("cn=(.*?),.+", boost::regex::icase);
	boost::smatch what;

	Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR, "processGroupAttribute called with '%s'", input.c_str());
	if (boost::regex_match(groupMembershipRaw, what, p, boost::match_extra)) {
		string groupName = what[1];

		Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR, "found group '%s'", groupName.c_str());
		return groupName;
	}

	return "";
}

int LdapAuthenticationMethod::sync(UserDb *userDb, GroupDb *groupDb) {
	Logger::instance()->log("sphirewalld.auth.manager.ldap", INFO, "starting ldap group sync");

	set<string> ret;
	LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

	if (connection) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap", INFO, "connected to ldap service");

		if (userOwnsGroupMembership()) {
			stringstream search;
			search << getUserNameAttribute() << "=*";
			Logger::instance()->log("sphirewalld.auth.manager.ldap.sync", DEBUG, "starting user/group sync, searching '%s", search.str().c_str());

			LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), search.str().c_str());

			if (result.resultcode == LDAP_SUCCESS) {
				for (LdapEntry entry : result.entries) {
					for (multimap<string, string>::iterator iter = entry.attributes.find(getUserGroupAttribute());
							iter != entry.attributes.end(); iter++) {

						ret.insert(processGroupAttribute(iter->second));
					}
				}
			}
			else {
				throw AuthenticationMethodException(ldap_err2string(result.resultcode));
				Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR,
							"could not query ldap service, reason was '%s'", ldap_err2string(result.resultcode));
			}

		}
		else {
			stringstream search;
			search << getGroupBase() << "," << getBaseDn();
			LdapSearchResult result = ldapSearch(connection, search.str().c_str(), NULL);

			if (result.resultcode == LDAP_SUCCESS) {
				for (LdapEntry entry : result.entries) {
					if (entry.has(getGroupNameAttribute())) {
						ret.insert(entry.scalar(getGroupNameAttribute()));
					}
				}
			}
			else {
				throw AuthenticationMethodException(ldap_err2string(result.resultcode));
				Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR,
							"could not query ldap service, reason was '%s'", ldap_err2string(result.resultcode));
			}
		}

		ldap_unbind(connection);
	}

	groupDb->holdLock();

	for (std::string name : ret) {
		if (!groupDb->getGroup(name)) {
			groupDb->createGroup(name);
			Logger::instance()->log("sphirewalld.auth.manager.ldap.sync", INFO, "added group '%s' to sphirewall groupdb", name.c_str());
		}
	}

	groupDb->releaseLock();

	Logger::instance()->log("sphirewalld.auth.manager.ldap.sync", INFO, "updating group information on users");

	for (std::string name : userDb->list()) {
		UserPtr user = userDb->getUser(name);

		if (user && user->getAuthenticationType() == LDAP_DB) {
			Logger::instance()->log("sphirewalld.auth.manager.ldap", INFO, "getting group information for user '%s'", name.c_str());

			updateUser(groupDb, user);
		}
	}

	userDb->save();
	Logger::instance()->log("sphirewalld.auth.manager.ldap.sync", INFO, "finished sync ldap service");
	return ret.size();
}

bool LdapAuthenticationMethod::canConnect() {
	LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

	if (connection) {
		ldap_unbind(connection);
		return true;
	}

	return false;
}

bool LdapAuthenticationMethod::authenticate(UserPtr userObject, std::string username, std::string password) {
	Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", INFO, "attempting to authenticate user '%s' with ldap service", username.c_str());

	if (password.size() == 0) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", INFO, "password is empty, returning false");
		return false;
	}

	LDAP *connection = NULL;
	stringstream search;

	if (!authenticateUserUsingSuppliedDn()) {
		search << getUserNameAttribute() << "=" << userObject->getUserName();

		if (getUserBase().size() > 0) {
			search << "," << getUserBase();
		}

		if (getBaseDn().size() > 0) {
			search << "," << getBaseDn();
		}
	}
	else {
		search << userObject->dn;
	}

	if (search.str().size() == 0) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", ERROR, "search string for authenticate request is empty, user = '%s'", username.c_str());
		return false;
	}

	Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", DEBUG, "search string for authenticate request is '%s'", search.str().c_str());

	connection = connect(getHostname(), getPort(), search.str(), password);

	if (connection) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap.authenticate", INFO, "authentication request for user '%s' was a success", username.c_str());
		ldap_unbind(connection);
		return true;
	}

	return false;
}

void LdapAuthenticationMethod::updateUser(GroupDb *groupDb, UserPtr user) {
	Logger::instance()->log("sphirewalld.auth.manager.ldap.updateUser", INFO,
				"attempting to find group membership for user '%s'", user->getUserName().c_str());

	vector<string> groups;
	LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

	if (connection) {
		if (userOwnsGroupMembership()) {
			stringstream search;
			search << getUserNameAttribute();
			search << "=" << user->getUserName();
			LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), search.str().c_str());

			if (result.resultcode == LDAP_SUCCESS) {
				Logger::instance()->log("sphirewalld.auth.manager.ldap.getGroupNames", DEBUG, "search matches, looking at ldap_first_entry");

				for (LdapEntry entry : result.entries) {
					for (multimap<string, string>::iterator iter = entry.attributes.find(getUserGroupAttribute()); iter != entry.attributes.end(); iter++) {
						groups.push_back(processGroupAttribute(iter->second));
					}

					for (multimap<string, string>::iterator iter = entry.attributes.begin(); iter != entry.attributes.end(); iter++) {
						Logger::instance()->log("sphirewalld.auth.manager.ldap", INFO,
									"attribute '%s' = '%s'", iter->first.c_str(), iter->second.c_str());
					}

					if (authenticateUserUsingSuppliedDn() && entry.has(getUserDnAttribute())) {
						if (entry.has(getUserDnAttribute())) {
							user->dn = entry.scalar(getUserDnAttribute());
							Logger::instance()->log("sphirewalld.auth.manager.ldap", INFO,
										"found dn for user '%s' as '%s'", user->getUserName().c_str(), user->dn.c_str());
						}
					}
				}
			}
		}
		else {
			LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), NULL);

			if (result.resultcode == LDAP_SUCCESS) {
				Logger::instance()->log("sphirewalld.auth.manager.ldap.getGroupNames", DEBUG, "search matches, looking at ldap_first_entry");

				for (LdapEntry entry : result.entries) {
					if (entry.has(getGroupNameAttribute())) {
						if (entry.has(getGroupMemberNameAttribute())) {
							//Find all the members, and then add the group if it does not already exist
							for (multimap<string, string>::iterator iter = entry.attributes.find(getGroupMemberNameAttribute()); iter != entry.attributes.end(); iter++) {
								if (iter->second.compare(user->getUserName()) == 0) {
									groups.push_back(entry.scalar(getGroupNameAttribute()));
								}
							}
						}
					}
				}
			}
		}

		vector<GroupPtr> newGroups;
		for (string groupname : groups) {
			if(groupname.size() <=0){
				continue;
			}

			GroupPtr group = groupDb->getGroup(groupname);
			if (!group) {
				group = groupDb->createGroup(groupname);
			}	

			if(group){
				newGroups.push_back(group);
			}
		}

		user->setGroups(newGroups);
		ldap_unbind(connection);
	}
}

UserPtr LdapAuthenticationMethod::findUser(std::string username) {
	LDAP *connection = connect(getHostname(), getPort(), getLdapUsername(), getLdapPassword());

	if (connection) {
		stringstream search;
		search << getUserNameAttribute() << "=" << username;

		Logger::instance()->log("sphirewalld.auth.manager.ldap", DEBUG, "findUser called for basedn: %s search: %s", getBaseDn().c_str(), search.str().c_str());
		LdapSearchResult result = ldapSearch(connection, getBaseDn().c_str(), search.str().c_str());

		if (result.resultcode == LDAP_SUCCESS) {
			if (result.entries.size() > 0) {
				for (LdapEntry entry : result.entries) {
					Logger::instance()->log("sphirewalld.auth.manager.ldap.findUser", INFO, "creating new user object in sphirewall db for user '%s'", username.c_str());

					User *newUser = new User();
					newUser->setAuthenticationType(LDAP_DB);
					newUser->setUserName(username);

					ldap_unbind(connection);
					return UserPtr(newUser);
				}
			}
			else {
				Logger::instance()->log("sphirewalld.auth.manager.ldap.findUser", ERROR,
						"could not find user '%s' in ldap database", username.c_str(), ldap_err2string(result.resultcode));
			}
		}
		else {
			Logger::instance()->log("sphirewalld.auth.manager.ldap.findUser", ERROR, "search for user '%s' failed for reason '%s'", username.c_str(), ldap_err2string(result.resultcode));
			throw AuthenticationMethodException(ldap_err2string(result.resultcode));
		}

		ldap_unbind(connection);
	}

	return UserPtr();
}

std::string LdapAuthenticationMethod::getHostname() const {
	if (configurationManager->hasByPath("authentication:ldap:hostname")) {
		return configurationManager->get("authentication:ldap:hostname")->string();
	}

	return "";
}

int LdapAuthenticationMethod::getPort() const {
	if (configurationManager->hasByPath("authentication:ldap:port")) {
		return configurationManager->get("authentication:ldap:port")->number();
	}

	return -1;
}

std::string LdapAuthenticationMethod::getLdapUsername() const {
	if (configurationManager->hasByPath("authentication:ldap:ldapusername")) {
		return configurationManager->get("authentication:ldap:ldapusername")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getLdapPassword() const {
	if (configurationManager->hasByPath("authentication:ldap:ldappassword")) {
		return configurationManager->get("authentication:ldap:ldappassword")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getBaseDn() const {
	if (configurationManager->hasByPath("authentication:ldap:basedn")) {
		return configurationManager->get("authentication:ldap:basedn")->string();
	}

	return "";
}

bool LdapAuthenticationMethod::isEnabled() {
	if (configurationManager->hasByPath("authentication:ldap:enabled")) {
		return configurationManager->get("authentication:ldap:enabled")->boolean();
	}

	return false;
}

bool LdapAuthenticationMethod::userOwnsGroupMembership() {
	if (configurationManager->hasByPath("authentication:ldap:userownedgroupmembership")) {
		return configurationManager->get("authentication:ldap:userownedgroupmembership")->boolean();
	}

	return true;
}

std::string LdapAuthenticationMethod::getUserNameAttribute() {
	//CN or uuid
	if (configurationManager->hasByPath("authentication:ldap:usernameattribute")) {
		return configurationManager->get("authentication:ldap:usernameattribute")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getUserBase() {
	//OU=Users
	if (configurationManager->hasByPath("authentication:ldap:userbase")) {
		return configurationManager->get("authentication:ldap:userbase")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getUserGroupAttribute() {
	//memberOf
	if (configurationManager->hasByPath("authentication:ldap:usergroupattribute")) {
		return configurationManager->get("authentication:ldap:usergroupattribute")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getUserDnAttribute() {
	if (configurationManager->hasByPath("authentication:ldap:userdnattribute")) {
		return configurationManager->get("authentication:ldap:userdnattribute")->string();
	}

	return "";
}

bool LdapAuthenticationMethod::authenticateUserUsingSuppliedDn() {
	if (configurationManager->hasByPath("authentication:ldap:authenticateuserusingsupplieddn")) {
		return configurationManager->get("authentication:ldap:authenticateuserusingsupplieddn")->boolean();
	}

	return false;
}

//Only used if users stored on groups
std::string LdapAuthenticationMethod::getGroupBase() {
	//OU=Groups
	if (configurationManager->hasByPath("authentication:ldap:groupbase")) {
		return configurationManager->get("authentication:ldap:groupbase")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getGroupNameAttribute() {
	//cn
	if (configurationManager->hasByPath("authentication:ldap:groupnameattribute")) {
		return configurationManager->get("authentication:ldap:groupnameattribute")->string();
	}

	return "";
}

std::string LdapAuthenticationMethod::getGroupMemberNameAttribute() {
	//userid
	if (configurationManager->hasByPath("authentication:ldap:groupmembernameattribute")) {
		return configurationManager->get("authentication:ldap:groupmembernameattribute")->string();
	}

	return "";
}

LDAP *LdapAuthenticationMethod::connect(std::string hostname, int port, std::string username, std::string password) {
	LDAP *ld;

	if ((ld = ldap_init(hostname.c_str(), port)) == NULL) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR, "ldap init failed");
		throw AuthenticationMethodException("ldap init failed");
	}

	int version = LDAP_VERSION3;
	ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);
        ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF);

	int rc = ldap_simple_bind_s(ld, username.c_str(), password.c_str());

	if (rc != LDAP_SUCCESS) {
		Logger::instance()->log("sphirewalld.auth.manager.ldap", ERROR, "could not connect to the ldap service, reason is '%s'", ldap_err2string(rc));
		ldap_unbind(ld);
		throw AuthenticationMethodException(ldap_err2string(rc));
	}

	return ld;
}

