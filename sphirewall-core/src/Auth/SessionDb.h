/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_SESSIONSDB_H_INCLUDED
#define SPHIREWALL_SESSIONSDB_H_INCLUDED

#include "Auth/Session.h"
#include "Auth/UserDb.h"
#include "Core/Logger.h"
#include "Core/Cron.h"
#include "Core/Lockable.h"
#include "Core/ConfigurationManager.h"
#include <boost/unordered_map.hpp>
#include <string>
#include <vector>
#include "SFwallCore/Alias.h"

namespace SFwallCore {
	class PlainConnTracker;
};

class User;
class ConfigurationManager;

class PersistedSession {
	public:
		PersistedSession() {}
		PersistedSession(std::string mac, std::string username) : mac(mac), username(username) {}

		std::string username;
		std::string mac;
};

class NetworkBasedTimeout {
	public:
		NetworkBasedTimeout() :
			timeout(0){}
		
		std::list<SFwallCore::AliasPtr> networks;
		int timeout;	
		std::string id;
};

typedef boost::shared_ptr<NetworkBasedTimeout> NetworkBasedTimeoutPtr;

class SessionDb : public DeleteUserListener, public Lockable, public Configurable {
	public:
		class CleanUpCron : public CronJob {
			public:
				CleanUpCron(SessionDb *sessionDb);
				void run();
			private:
				SessionDb *sessionDb;
		};

		SessionDb(CronManager *, SFwallCore::PlainConnTracker *);

		SessionPtr create(UserPtr user, std::string hwAddress, unsigned int ip, int timeout=-1, bool absoluteTimeout=false);
		SessionPtr create(UserPtr user, std::string hwAddress, struct in6_addr ip, int timeout=-1, bool absoluteTimeout=false);
		SessionPtr get(std::string mac, unsigned int ip);
		SessionPtr get(std::string mac, struct in6_addr ip);

		void deleteSession(SessionPtr session);
		std::list<SessionPtr> findSessionsForUser(UserPtr user);
		std::list<SessionPtr> findSessionsForUser(std::string username);
		std::string findUser(std::string user);
		std::vector<SessionPtr> list(bool only_expired = false);

		void handleDeleteUser(UserPtr user);

		void removeFromTracker(SessionPtr session);
		void removeFromStore(SessionPtr session);

		bool load();
		void save();

		static int SESSION_DB_IGNORE_MAC_ADDRESS;
		int dbSize();
		void deleteSessionsForUser(UserPtr user);
		void setUserDb(UserDb *userDb) {
			this->userDb = userDb;
		}

		std::multimap<std::string, int> failCount;

		void persistSession(std::string mac, std::string username);
		std::list<PersistedSession> listPersistedSessions();
		void removePersistedSession(std::string mac, std::string username);

                const char* getConfigurationSystemName(){
                        return "Session manager";
                }

		void saveNetworkRangeTimeout(NetworkBasedTimeoutPtr timeout);
		void removeNetworkRangeTimeout(NetworkBasedTimeoutPtr timeout);
		std::list<NetworkBasedTimeoutPtr> listNetworkRangeTimeouts();

	private:
		boost::unordered_map<std::string, boost::unordered_map<unsigned int, SessionPtr> > sessions;
		boost::unordered_map<std::string, boost::unordered_map<std::string, SessionPtr> > sessionsV6;

		boost::unordered_map<std::string, PersistedSession> persistedSessions;
		std::list<NetworkBasedTimeoutPtr> networkBasedTimeouts;

		SFwallCore::PlainConnTracker *plainConnTracker;
		int size;

		UserDb *userDb;
};

#endif
