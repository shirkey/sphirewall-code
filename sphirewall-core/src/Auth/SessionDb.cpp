/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

using namespace std;

#include "Auth/User.h"
#include "Auth/UserDb.h"
#include "Auth/Session.h"
#include "Auth/SessionDb.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/ConnTracker.h"
#include "Core/Event.h"

int SessionDb::SESSION_DB_IGNORE_MAC_ADDRESS = 0;

SessionDb::SessionDb(CronManager *cronManager, SFwallCore::PlainConnTracker *plainConnTracker)
	: Lockable(), plainConnTracker(plainConnTracker), size(0){

	CleanUpCron *cron = new CleanUpCron(this);

	if (cronManager != NULL) {
		cronManager->registerJob(cron);
	}

	System::getInstance()->config.getRuntime()->loadOrPut("SESSION_DB_IGNORE_MAC_ADDRESS", &SESSION_DB_IGNORE_MAC_ADDRESS);
};

SessionPtr SessionDb::get(string mac, unsigned int ip) {
	boost::unordered_map<string, boost::unordered_map<unsigned int, SessionPtr> >::iterator iter = sessions.find(SESSION_DB_IGNORE_MAC_ADDRESS == 0 ? mac : "");

	while (iter != sessions.end()) {
		boost::unordered_map<unsigned int, SessionPtr> &inner = iter->second;

		if (inner.find(ip) != inner.end()) {
			return inner[ip];
		}

		iter++;
	}

	//If the session does not exist, also check the persisted sessions:
	if (persistedSessions.find(mac) != persistedSessions.end()) {
		PersistedSession persistedSession = persistedSessions[mac];
		UserPtr user = userDb->getUser(persistedSession.username);
		if (user) {
			if(System::getInstance()->getEventDb()){
				EventParams params;
				params["user"] = persistedSession.username; 
				params["ip"] = IP4Addr::ip4AddrToString(ip); 
				params["hw"] = mac;
				System::getInstance()->getEventDb()->add(new Event(USERDB_LOGIN_SUCCESS, params));
			}
			return create(user, mac, ip);
		}
	}

	return SessionPtr();
}

SessionPtr SessionDb::get(string mac, struct in6_addr ip) {
	boost::unordered_map<string, boost::unordered_map<std::string, SessionPtr> >::iterator iter = sessionsV6.find(SESSION_DB_IGNORE_MAC_ADDRESS == 0 ? mac : "");

	while (iter != sessionsV6.end()) {
		boost::unordered_map<std::string, SessionPtr> &inner = iter->second;

		if (inner.find(IP6Addr::toString(&ip)) != inner.end()) {
			return inner[IP6Addr::toString(&ip)];
		}

		iter++;
	}

	//If the session does not exist, also check the persisted sessions:
	if (persistedSessions.find(mac) != persistedSessions.end()) {
		PersistedSession persistedSession = persistedSessions[mac];
		UserPtr user = userDb->getUser(persistedSession.username);

		if (user) {
                        if(System::getInstance()->getEventDb()){
                                EventParams params;
                                params["user"] = persistedSession.username; 
                                params["ip"] = IP6Addr::toString(&ip);
                                params["hw"] = mac;
                                System::getInstance()->getEventDb()->add(new Event(USERDB_LOGIN_SUCCESS, params));
                        }

			return create(user, mac, ip);
		}
	}

	return SessionPtr();
}

void SessionDb::deleteSession(SessionPtr session) {
	removeFromStore(session);
}

void SessionDb::removeFromStore(SessionPtr session) {
	for (boost::unordered_map<string, boost::unordered_map<unsigned int, SessionPtr> >::iterator iter = sessions.begin(); iter != sessions.end(); iter++) {
		boost::unordered_map<unsigned int, SessionPtr> &inner = iter->second;
		inner.erase(session->getIp_Int());
		size--;
	}

	for (boost::unordered_map<string, boost::unordered_map<std::string, SessionPtr> >::iterator iter = sessionsV6.begin(); iter != sessionsV6.end(); iter++) {
		boost::unordered_map<std::string, SessionPtr> &inner = iter->second;
		inner.erase(session->getIp());
		size--;
	}
}

std::list<SessionPtr> SessionDb::findSessionsForUser(UserPtr user) {
	std::list<SessionPtr> ret;

	for (std::pair<string, boost::unordered_map<unsigned int, SessionPtr> > p0 : sessions) {
		for (std::pair<unsigned int, SessionPtr> p1 : p0.second) {
			if (p1.second->getUser() == user) {
				ret.push_back(p1.second);
			}
		}
	}

	for (std::pair<string, boost::unordered_map<string, SessionPtr> > p0 : sessionsV6) {
		for (std::pair<string, SessionPtr> p1 : p0.second) {
			if (p1.second->getUser() == user) {
				ret.push_back(p1.second);
			}
		}
	}

	return ret;
}

void SessionDb::handleDeleteUser(UserPtr user) {
	std::list<SessionPtr> sessions = findSessionsForUser(user);

	for (std::list<SessionPtr>::iterator iter = sessions.begin();
			iter != sessions.end();
			iter++) {

		deleteSession((*iter));
	}
}

std::list<SessionPtr> SessionDb::findSessionsForUser(string user) {
	std::list<SessionPtr> ret;

	for (std::pair<string, boost::unordered_map<unsigned int, SessionPtr> > p0 : sessions) {
		for (std::pair<unsigned int, SessionPtr> p1 : p0.second) {
			if (p1.second->getUser()->getUserName() == user) {
				ret.push_back(p1.second);
			}
		}
	}

	for (std::pair<string, boost::unordered_map<string, SessionPtr> > p0 : sessionsV6) {
		for (std::pair<string, SessionPtr> p1 : p0.second) {
			if (p1.second->getUser()->getUserName() == user) {
				ret.push_back(p1.second);
			}
		}
	}

	return ret;
}

string SessionDb::findUser(string ip) {
	boost::unordered_map<string, boost::unordered_map<unsigned int, SessionPtr> >::iterator iter = sessions.begin();

	while (iter != sessions.end()) {
		boost::unordered_map<unsigned int, SessionPtr> &inner = iter->second;

		for (boost::unordered_map<unsigned int, SessionPtr>::iterator iiter = inner.begin();
				iiter != inner.end();
				iiter++) {

			SessionPtr target = iiter->second;

			if (target->getIp().compare(ip) == 0) {
				return target->getUserName();
			}
		}

		++iter;
	}

	return "unknown";
}

vector<SessionPtr> SessionDb::list(bool only_expired) {
	vector<SessionPtr> ret;

	for (std::pair<string, boost::unordered_map<unsigned int, SessionPtr>> p0 : sessions) {
		for (std::pair<unsigned int, SessionPtr> p1 : p0.second) {
			if ((only_expired && p1.second->isTimedOut()) || !only_expired) {
				ret.push_back(p1.second);
			}
		}
	}

	for (std::pair<string, boost::unordered_map<string, SessionPtr>> p0 : sessionsV6) {
		for (std::pair<string, SessionPtr> p1 : p0.second) {
			if ((only_expired && p1.second->isTimedOut()) || !only_expired) {
				ret.push_back(p1.second);
			}
		}
	}

	return ret;
}

SessionDb::CleanUpCron::CleanUpCron(SessionDb *sessionDb) : CronJob(60 * 5, "SESSIONDB_CLEANUP_CRON", true),
	sessionDb(sessionDb) {}

	int SessionDb::dbSize() {
		return size;
	}

void SessionDb::deleteSessionsForUser(UserPtr u) {
	holdLock();

	for (SessionPtr s : findSessionsForUser(u)) {
		deleteSession(s);
	}

	releaseLock();
}


void SessionDb::CleanUpCron::run() {
	Logger::instance()->log("sphirewalld.sessiondb.cleanup", INFO, "Removing stale user sessions");

	if (sessionDb->tryLock()) {
		string user;

		int timeout = System::getInstance()->configurationManager.hasByPath("authentication:session_timeout") ? System::getInstance()->configurationManager.get("authentication:session_timeout")->number() : 600;
		if (timeout == 0) {
			// Allow session timeouts to be overridden if the default is 0
			Logger::instance()->log("sphirewalld.sessiondb.cleanup", INFO, "Aborting, session timeout is set to 0");
			sessionDb->releaseLock();
			return;
		}

		for (SessionPtr session : sessionDb->list(true)) {
			Logger::instance()->log("sphirewalld.sessiondb", INFO,
					"Removing session user: %s mac: %s ip: %s", session->getUserName().c_str(), session->getMac().c_str(), session->getIp().c_str());
			sessionDb->removeFromStore(session);

			if(System::getInstance()->getEventDb()){
				EventParams params;
				params["user"] = session->getUser()->getUserName();
				System::getInstance()->getEventDb()->add(new Event(USERDB_SESSION_TIMEOUT, params));
			}
		}

		sessionDb->releaseLock();
	}
	else {
		Logger::instance()->log("sphirewalld.sessiondb.cleanup", INFO, "Could not run cleanup, sessionDb is currently locked");
	}
}

SessionPtr SessionDb::create(UserPtr user, string hwAddress, unsigned int ip, int timeout, bool absoluteTimeout) {
	SessionPtr session;
	if(timeout == -1){
		int maxTimeout = 0;

		/*Search the group based timeouts, use the max that we find*/
		for (GroupPtr  g : user->getGroups()) {
			if (g->getTimeout() != 0 && maxTimeout < g->getTimeout()) {
				maxTimeout = g->getTimeout();
			}
			else if (g->getTimeout() == 0) {
				// This happens when you want a group to disable timeout on it's users
				maxTimeout = 0;
				break;
			}
		}

		/*Search the network based timeouts, use the max that we find*/
		for(NetworkBasedTimeoutPtr nbt: networkBasedTimeouts){
			//Search each network to find a match
			for(SFwallCore::AliasPtr a : nbt->networks){
				if(a->searchForNetworkMatch(ip)){
					if(nbt->timeout != 0 && maxTimeout < nbt->timeout){
						maxTimeout = nbt->timeout;
					}
				}
			}
		}

		/*We didnt find a suitable timeout to use, lets just take the default then*/
		if(maxTimeout == 0){
			maxTimeout = configurationManager->hasByPath("authentication:session_timeout") ? configurationManager->get("authentication:session_timeout")->number() : -1;
		}
		session = SessionPtr(new Session(user, hwAddress, ip, maxTimeout));
	}else{
		session = SessionPtr(new Session(user, hwAddress, ip, timeout, absoluteTimeout));
	}

	sessions[SESSION_DB_IGNORE_MAC_ADDRESS == 0 ? hwAddress : ""][session->getIp_Int()] = session;
	size += 1;
	return session;
}

SessionPtr SessionDb::create(UserPtr user, string hwAddress, struct in6_addr ip, int timeout, bool absoluteTimeout) {
	SessionPtr session;
	if(timeout == -1){
		int maxTimeout = configurationManager->hasByPath("authentication:session_timeout") ? configurationManager->get("authentication:session_timeout")->number() : -1;

		for (GroupPtr g : user->getGroups()) {
			if (g->getTimeout() != 0 && maxTimeout < g->getTimeout()) {
				maxTimeout = g->getTimeout();
			}
			else {
				// This happens when you want a group to disable timeout on it's users
				maxTimeout = 0;
				break;
			}
		}

		session = SessionPtr(new Session(user, hwAddress, ip, maxTimeout));
	}else{
		session = SessionPtr(new Session(user, hwAddress, ip, timeout, absoluteTimeout));
	}

	sessionsV6[SESSION_DB_IGNORE_MAC_ADDRESS == 0 ? hwAddress : ""][IP6Addr::toString(&ip)] = session;
	return session;
}

bool SessionDb::load() {
	this->holdLock();
	persistedSessions.clear();

	Logger::instance()->log("sphirewalld.sessiondb", EVENT, "Loading Sessions");

	if (!configurationManager->has("sessionDb")) {
		Logger::instance()->log("sphirewalld.sessiondb", ERROR, "Loading session database failed, no sessions found. Ignoring");
	}
	else {
		ObjectContainer* root = configurationManager->getElement("sessionDb");
		ObjectContainer* sessions = root->get("sessions")->container();

		for (int x = 0; x < sessions->size(); x++) {
			ObjectContainer* o = sessions->get(x)->container();
			std::string mac = o->get("macAddress")->string();
			std::string username = o->get("username")->string();
			persistedSessions[mac] = PersistedSession(mac, username);
		}

		if(root->has("network_based_timeouts")){
			ObjectContainer* nbto = root->get("network_based_timeouts")->container();

			for (int x = 0; x < nbto->size(); x++) {
				ObjectContainer* o = nbto->get(x)->container();

				NetworkBasedTimeoutPtr nbtp = NetworkBasedTimeoutPtr(new NetworkBasedTimeout());
				nbtp->id = o->get("id")->string();
				nbtp->timeout = o->get("timeout")->number();

				SFwallCore::AliasDb* aliases = System::getInstance()->getFirewall()->aliases;
				ObjectContainer* networks = o->get("networks")->container();

				for(int y = 0; y < networks->size(); y++){
					SFwallCore::AliasPtr network = aliases->get(networks->get(y)->string());
					if(network){
						nbtp->networks.push_back(network);
					}
				}	

				networkBasedTimeouts.push_back(nbtp);
			}
		}

	}

	this->releaseLock();
	return true;
}

void SessionDb::save() {
	this->holdLock();

	Logger::instance()->log("sphirewalld.sessiondb", EVENT, "Saving Sessions");

	if (configurationManager) {
		ObjectContainer *root = new ObjectContainer(CREL);
		ObjectContainer *array = new ObjectContainer(CARRAY);

		for (pair<std::string, PersistedSession> target : persistedSessions) {
			ObjectContainer *session = new ObjectContainer(CREL);

			session->put("username", new ObjectWrapper(target.second.username));
			session->put("macAddress", new ObjectWrapper(target.second.mac));

			array->put(new ObjectWrapper(session));
		}

		root->put("sessions", new ObjectWrapper(array));

		ObjectContainer *timeoutArray = new ObjectContainer(CARRAY);
		for (NetworkBasedTimeoutPtr timeout : networkBasedTimeouts) {
			ObjectContainer *to= new ObjectContainer(CREL);

			to->put("id", new ObjectWrapper((string) timeout->id));
			to->put("timeout", new ObjectWrapper((double) timeout->timeout));
			ObjectContainer *networksArray= new ObjectContainer(CARRAY);
			for(SFwallCore::AliasPtr network : timeout->networks){
				networksArray->put(new ObjectWrapper(network->id));	
			}

			to->put("networks", new ObjectWrapper(networksArray));
			timeoutArray->put(new ObjectWrapper(to));
		}

		root->put("network_based_timeouts", new ObjectWrapper(timeoutArray));

		configurationManager->setElement("sessionDb", root);
		configurationManager->save();
	}

	this->releaseLock();
}

void SessionDb::persistSession(std::string mac, std::string username) {
	persistedSessions[mac] = PersistedSession(mac, username);
}

std::list<PersistedSession> SessionDb::listPersistedSessions() {
	std::list<PersistedSession> ret;

	for (pair<std::string, PersistedSession> target : persistedSessions) {
		ret.push_back(target.second);
	}

	return ret;
}

void SessionDb::removePersistedSession(std::string mac, std::string username) {
	persistedSessions.erase(mac);
}

void SessionDb::saveNetworkRangeTimeout(NetworkBasedTimeoutPtr timeout){
	networkBasedTimeouts.push_back(timeout);
}

void SessionDb::removeNetworkRangeTimeout(NetworkBasedTimeoutPtr timeout){
	networkBasedTimeouts.remove(timeout);
}

std::list<NetworkBasedTimeoutPtr> SessionDb::listNetworkRangeTimeouts(){
	return networkBasedTimeouts;
}


