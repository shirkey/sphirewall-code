/* This file is part of Sphirewall, it comes with a copy of the license in LICENSE.txt, which is also available at http://sphirewall.net/LICENSE.txt */

#ifndef TESTFACTORY_H
#define TESTFACTORY_H
#include <string>

namespace SFwallCore {
	class TcpPacket;
	class UdpPacket;
	class IcmpPacket;
	class Packet;
	class PacketBuilder {
		public:
			static Packet *createTcp(std::string sourceIp, std::string destIp, int sourcePort, int destPort, std::string data = "");
			static Packet *createUdp(std::string sourceIp, std::string destIp, int sourcePort, int destPort, char *data = NULL, size_t len = 0);
			static Packet *createIcmp(std::string sourceIp, std::string destIp, int icmpid);

			static Packet *createTcpV6(std::string sourceIp, std::string destIp, int sourcePort, int destPort);

			static void setAck(TcpPacket *tcpPacket);
			static void setSyn(TcpPacket *tcpPacket);

			static void setSourceDev(Packet *packet, int device);
			static void setDestDev(Packet *packet, int device);
	};
};
#endif
