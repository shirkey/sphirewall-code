/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_STATE_H
#define SPHIREWALL_STATE_H

#include "Packetfwd.h"
#include "Utils/TimeUtils.h"

namespace SFwallCore {

	typedef enum {
		SYNSYNACK,
		SYNACKACK,
		TCPUP,
		FIN_WAIT, //First fin seen
		CLOSE_WAIT, //Ack for the first fin seen
		LAST_ACK, //Second fin seen
		TIME_WAIT, //Ack for the second find seen
		FINFINACK,
		CLOSED, //Closed with RST, ACK
		RESET
	} tcpState;

	class TcpState {
		public:
			unsigned long finack_from_dst;
			unsigned long finack_from_src;

			unsigned long fin2_wait_seq;
			unsigned long fin_wait_seq;

			bool recvd_finack_from_src;
			bool recvd_finack_from_dst;

			std::string echo();

			tcpState state;
	};

	class TcpTrackable {
		public:
			TcpTrackable(const TcpPacket *packet);
			TcpTrackable() {}

			virtual ~TcpTrackable() {
			}

			void calcState(const TcpPacket *packet);
			bool hasExpired(int idleTime);
			TcpState state;

			unsigned int lastAckSeq;
			unsigned int lastSeq;
			//Used to gather more information on a connection: Last state
			bool lastFin;
			bool lastAck;
			bool lastSyn;
			bool lastRst;
	};
};
#endif
