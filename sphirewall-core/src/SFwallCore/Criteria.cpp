#include <iostream>
#include "SFwallCore/Criteria.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Firewall.h"
#include "Core/System.h"
#include "Auth/GroupDb.h"
#include "Utils/HttpRequestWrapper.h"
#include "Utils/StringUtils.h"

using namespace std;

std::string SFwallCore::SourceIpCriteria::key() const {
	return "source.ipv4";
}

bool SFwallCore::SourceIpCriteria::match(SFwallCore::Packet* packet){
	if (packet->type() != IPV4) {
		return false;
	}

	PacketV4 *ipv4 = (PacketV4 *) packet;
	if (IP4Addr::matchNetwork(ipv4->getSrcIp(), ip, mask) == -1) {
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationIpCriteria::key() const {
	return "destination.ipv4";
}

bool SFwallCore::DestinationIpCriteria::match(SFwallCore::Packet* packet){
	if (packet->type() != IPV4) {
		return false;
	}

	PacketV4 *ipv4 = (PacketV4 *) packet;
	if (IP4Addr::matchNetwork(ipv4->getDstIp(), ip, mask) == -1) {
		return false;
	}

	return true;
}

std::string SFwallCore::SourceIp6Criteria::key() const {
	return "source.ipv6";
}

bool SFwallCore::SourceIp6Criteria::match(SFwallCore::Packet* packet){
	if (packet->type() != IPV6) {
		return false;
	}

	PacketV6 *ip = (PacketV6 *) packet;
	if (!IP6Addr::matchNetwork(ip->getSrcIp(), addr, cidr)) {
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationIp6Criteria::key() const {
	return "destination.ipv6";
}

bool SFwallCore::DestinationIp6Criteria::match(SFwallCore::Packet* packet){
	if (packet->type() != IPV6) {
		return false;
	}

	PacketV6 *ip = (PacketV6*) packet;
	if (!IP6Addr::matchNetwork(ip->getDstIp(), addr, cidr)) {
		return false;
	}

	return true;
}

std::string SFwallCore::GroupCriteria::key() const {
	return "group";
}

bool SFwallCore::GroupCriteria::match(SFwallCore::Packet* packet){
	if (!packet->getConnection()->getSession()) {
		return false;
	}

	if(group){
		if (packet->getConnection()->getSession()->getUser()->checkForGroup(group)) {
			return true;	
		}
	}else if(groups.size() > 0){
		bool hasGroupMatch = false;
		for (GroupPtr group : groups) {
			if (packet->getConnection()->getSession()->getUser()->checkForGroup(group)) {
				return true;
			}       
		}       
	}	

	return false;
}

std::string SFwallCore::SourceIpAliasCriteria::key() const {
	return "source.ipv4.alias";
}

bool SFwallCore::SourceIpAliasCriteria::match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		bool hasSourceMatch = false;

		if (packet->type() != IPV4) {
			return false;
		}

		PacketV4 *ipv4 = (PacketV4 *) packet;

		for (AliasPtr alias : aliases) {
			if (alias->searchForNetworkMatch(ipv4->getSrcIp())) {
				return true;
			}
		}
	}

	return false;
}

std::string SFwallCore::DestinationIpAliasCriteria::key() const {
	return "destination.ipv4.alias";
}

bool SFwallCore::DestinationIpAliasCriteria::match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		bool hasSourceMatch = false;

		if (packet->type() != IPV4) {
			return false;
		}

		PacketV4 *ipv4 = (PacketV4 *) packet;

		for (AliasPtr alias : aliases) {
			if (alias->searchForNetworkMatch(ipv4->getDstIp())) {
				return true;
			}
		}
	}

	return false;
}

std::string SFwallCore::DestinationPortCriteria::key() const {
	return "destination.transport.port";
}

bool SFwallCore::DestinationPortCriteria::match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (ports.size() > 0 && ports.find(packet->getDstPort()) == ports.end()) {
		return false;
	}

	return true;
}

std::string SFwallCore::SourcePortCriteria::key() const {
	return "source.transport.port";
}

bool SFwallCore::SourcePortCriteria::match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (ports.size() > 0 && ports.find(packet->getSrcPort()) == ports.end()) {
		return false;
	}

	return true;
}

std::string SFwallCore::SourcePortRangeCriteria::key() const {
	return "source.transport.portrange";
}

bool SFwallCore::SourcePortRangeCriteria::match(SFwallCore::Packet* packet){
	if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
		return false;
	}

	if (!(packet->getSrcPort() >= startPort && packet->getSrcPort() <= endPort)) {
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationPortRangeCriteria::key() const {
        return "destination.transport.portrange";
}

bool SFwallCore::DestinationPortRangeCriteria::match(SFwallCore::Packet* packet){
        if (!(packet->getProtocol() == TCP || packet->getProtocol() == UDP)) {
                return false;
        }

        if (!(packet->getDstPort() >= startPort && packet->getDstPort() <= endPort)) {
                return false;
        }

        return true;
}

std::string SFwallCore::TimePeriodCriteria::key() const {
	return "timeperiod";
}

bool SFwallCore::TimePeriodCriteria::match(SFwallCore::Packet* packet){
	if (!TimePeriodStore::checkMatch(optimisedPeriods, time(NULL))) {
		return false;
	}

	return true;
}

void SFwallCore::TimePeriodCriteria::reduce(TimePeriodPtr newPeriod, list<TimePeriodPtr>& remaining){
        list<TimePeriodPtr>::iterator siter;
	for(siter = remaining.begin(); siter != remaining.end(); siter++){
		TimePeriodPtr target = (*siter);
		if(target == newPeriod){
			continue;
		}

		if(newPeriod->startTime == target->startTime && newPeriod->endTime == target->endTime 
			&& newPeriod->startDate== target->startDate && newPeriod->endDate == target->endDate){

			newPeriod->mon |= target->mon;
			newPeriod->tue |= target->tue;
			newPeriod->wed |= target->wed;
			newPeriod->thu |= target->thu;
			newPeriod->fri |= target->fri;
			newPeriod->sat |= target->sat;
			newPeriod->sun |= target->sun;
			newPeriod->any |= target->any;

			siter = remaining.erase(siter);
		}
	}
}

void SFwallCore::TimePeriodCriteria::reduce(list<TimePeriodPtr>& remaining){
	for(TimePeriodPtr toProcess : remaining){
		reduce(toProcess, remaining);
	}
}

void SFwallCore::TimePeriodCriteria::refresh(){
	optimisedPeriods.clear();	

	list<TimePeriodPtr> remaining;
	for(TimePeriodPtr target : periods){
		remaining.push_back(TimePeriodPtr(new TimePeriod(*target)));
	}

	reduce(remaining);	
	optimisedPeriods = remaining;

	Logger::instance()->log("sphirewalld.sfwallcore.timeperiodcritera.refresh", INFO, "Optimised time periods from %d to %d in evaluation size", periods.size(), optimisedPeriods.size());
}

std::string SFwallCore::HttpContentType::key() const {
	return "application.http.contenttype";
}

bool SFwallCore::HttpContentType::match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
		if (a->httpHeadersResponseParsed && a->http_contains(RESPONSE, HTTP_CONTENTTYPE)) {
			for (AliasPtr ua : aliases) {
				if (ua->search(a->http_get(RESPONSE, HTTP_CONTENTTYPE))) {
					return true;
				}
			}
		}
	}

	return false;
}

std::string SFwallCore::HttpUserAgent::key() const {
	return "application.http.useragent";
}

bool SFwallCore::HttpUserAgent::match(SFwallCore::Packet* packet){
	if (aliases.size() > 0) {
		ConnectionApplicationInfo *a = packet->getConnection()->getApplicationInfo();
		if (a->httpHeadersParsed && a->http_contains(REQUEST, HTTP_USERAGENT)) {
			for (AliasPtr ua : aliases) {
				if (ua->search(a->http_get(REQUEST, HTTP_USERAGENT))) {
					return true;
				}
			}
		}
	}

	return false;
}

void SFwallCore::HttpHostname::refresh(){
	cache.clear();
}

std::string SFwallCore::HttpHostname::key() const {
	return "application.http.hostname";
}

void SFwallCore::HttpHostname::BanHitListItem::clear() {
	banned.clear();
}

int SFwallCore::HttpHostname::BanHitListItem::size() {
	return banned.size();
}

bool SFwallCore::HttpHostname::BanHitListItem::check(SFwallCore::ConnectionIp *ip) {
	if (ip->type() == IPV4) {
		ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) ip;
		return banned.find(ipv4->getDstIp()) != banned.end();
	}

	return false;
}


void SFwallCore::HttpHostname::BanHitListItem::insert(SFwallCore::ConnectionIp *ip) {
	if (ip->type() == IPV4) {
		ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) ip;
		banned.insert(ipv4->getDstIp());
	}
}


bool SFwallCore::HttpHostname::match(SFwallCore::Packet* packet){
	if (System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY == 1 && !packet->getConnection()->getApplicationInfo()->tls) {
		int block = System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY;
		int dport = packet->getConnection()->getDestinationPort();
		int tls = packet->getConnection()->getApplicationInfo()->tls;
		ConnectionIp *ip = packet->getConnection()->getIp();

		if (block == 1 && dport == 443 && !tls && nonTlsBannedItems.check(ip)) {
			return true;
		}
	}

	if (aliases.size() > 0) {
		if (!packet->getConnection()->getApplicationInfo()->http_hostNameSet) {
			return false;
		}

		const string& host = packet->getConnection()->getApplicationInfo()->http_hostName;
		for (AliasPtr websiteList :  aliases) {
			if (websiteList->search(host)) {
				if (System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY == 1 && packet->getConnection()->getApplicationInfo()->tls) {
					if (Logger::instance()->criticalPathEnabled) {
						ConnectionIpV4 *ip = (ConnectionIpV4 *) packet->getConnection()->getIp();
						string hostname = packet->getConnection()->getApplicationInfo()->http_hostName;
						string ipaddr = IP4Addr::ip4AddrToString(ip->getDstIp());
						Logger::instance()->log("sphirewalld.firewall.webfiltering", INFO, "Adding dstip to ban list for website " + hostname + " under " + ipaddr + " %s");
					}

					if (System::getInstance()->getFirewall()->BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD < nonTlsBannedItems.size()) {
						Logger::instance()->log("sphirewalld.firewall.webfiltering", INFO, "Clearing nonTlsBannedItems as it has exceeded the max threhold");
						nonTlsBannedItems.clear();
					}

					nonTlsBannedItems.insert(packet->getConnection()->getIp());
				}

				return true;
			}
		}
	}

	return false;
}

std::string SFwallCore::SourceNetworkDeviceCriteria::key() const {
	return "source.device";
}

bool SFwallCore::SourceNetworkDeviceCriteria::match(SFwallCore::Packet* packet){
	if(!devicePtr){
		return false;	
	}

	if(packet->getSourceDev() != devicePtr->ifid){
		return false;
	}

	return true;
}

std::string SFwallCore::DestinationNetworkDeviceCriteria::key() const {
	return "destination.device";
}

bool SFwallCore::DestinationNetworkDeviceCriteria::match(SFwallCore::Packet* packet){
	if(!devicePtr){
		return false;	
	}

	if(packet->getDestDev() != devicePtr->ifid){
		return false;
	}

	return true;
}

std::string SFwallCore::ProtocolCriteria::key() const {
	return "protocol";
}

bool SFwallCore::ProtocolCriteria::match(SFwallCore::Packet* packet){
	if(packet->getProtocol() != type){
		return false;
	}

	return true;
}

std::string SFwallCore::UsersCriteria::key() const {
	return "source.user";
}

bool SFwallCore::UsersCriteria::match(SFwallCore::Packet* packet){
	if(!packet->getUser()){
		return false;
	}

	if(users.find(packet->getUser()) == users.end()){
		return false;
	}

	return true;
}

std::string SFwallCore::MacAddressCriteria::key() const {
	return "source.mac";
}

bool SFwallCore::MacAddressCriteria::match(SFwallCore::Packet* packet){
	if(macs.find(packet->getHw()) == macs.end()){
		return false;
	}

	return true;
}

std::string SFwallCore::SignatureCriteria::key() const {
	return "signature";
}

bool SFwallCore::SignatureCriteria::match(SFwallCore::Packet* packet){
	if(!signature){
		return false;
	}

	return signature->matches(packet->getConnection(), packet);
}

void SFwallCore::SignatureCriteria::refresh(){
	signature = System::getInstance()->getFirewall()->signatureStore->get(signatureKey);
}

void SFwallCore::DestinationNetworkDeviceCriteria::refresh(){
	devicePtr = System::getInstance()->get_interface_manager()->get_interface(device);
}

void SFwallCore::SourceNetworkDeviceCriteria::refresh(){
	devicePtr = System::getInstance()->get_interface_manager()->get_interface(device);
}


std::string SFwallCore::UserQuotaExceededCriteria::key() const {
        return "user.quota";
}

bool SFwallCore::UserQuotaExceededCriteria::match(SFwallCore::Packet* packet){
        if(!packet->getSession()){
                return false;
        }

	return packet->getSession()->getUser()->isQuotaExceeded();
}

std::string SFwallCore::GlobalQuotaExceededCriteria::key() const {
        return "global.quota";
}

bool SFwallCore::GlobalQuotaExceededCriteria::match(SFwallCore::Packet* packet){
        return System::getInstance()->getQuotaManager()->isGlobalQuotaExceeded();
}

std::string SFwallCore::HostQuarantinedCriteria::key() const {
        return "source.quarantined";
}

bool SFwallCore::HostQuarantinedCriteria::match(SFwallCore::Packet* packet){
	if(packet->getHostDiscoveryServiceEntry()){
		return packet->getHostDiscoveryServiceEntry()->quarantined;
	}
	return false;
}

std::string SFwallCore::AuthenticatedCriteria::key() const {
        return "session.active";
}

bool SFwallCore::AuthenticatedCriteria::match(SFwallCore::Packet* packet){
	if(packet->getSession()){
		return true;
	}
	return false;
}

std::string SFwallCore::NotAuthenticatedCriteria::key() const {
        return "session.inactive";
}

bool SFwallCore::NotAuthenticatedCriteria::match(SFwallCore::Packet* packet){
	if(!packet->getSession()){
		return true;
	}
	return false;
}


void SFwallCore::GeoIPCriteriaBase::refresh(){
        Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loading Maxmind GeoIP alias for criteria for country '%s'", country.c_str());
        HttpRequestWrapper *request = new HttpRequestWrapper("http://mirror.sphirewall.net/resources/GeoIPCountryWhois.csv", GET);
	IpRangeAlias* new_alias = new IpRangeAlias();
        try {
		int loaded_entries = 0;
                vector<string> items;
                split(request->execute(), '\n', items);

                for (string item : items) {
                        vector<string> csv;
                        split(item, ',', csv);
                        std::string startIp = StringUtils::trim(csv[0], "\"");
                        std::string endIp = StringUtils::trim(csv[1], "\"");

                        std::string targetCountryCode = StringUtils::trim(csv[4], "\"");
                        std::string targetCountry = StringUtils::trim(csv[5], "\"");

                        if (targetCountry.compare(country) == 0 || targetCountryCode.compare(country) == 0) {
                                stringstream ss;
                                ss << startIp << "-" << endIp;
                                new_alias->addEntry(ss.str());
				loaded_entries++;
                        }
                }

		Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loaded Maxmind GeoIP alias for criteria, %d entries added", loaded_entries);
		delete alias;
		alias = new_alias;
        }
        catch (const HttpRequestWrapperException &ex) {
                Logger::instance()->log("sphirewalld.firewall.aliases", ERROR, "Failed to load Maxmind GeoIP country for %s, reason %s", country.c_str(), ex.what());
                delete request;
		delete new_alias;
                return;
        }
	
        delete request;
}

std::string SFwallCore::GeoIPSourceCriteria::key() const{
	return "geoip.source";
}

bool SFwallCore::GeoIPSourceCriteria::match(Packet* packet){
        if(alias){
                if(packet->type() == IPV4){
                        if(alias->searchForNetworkMatch(((PacketV4*) packet)->getSrcIp())){
                                return true;
                        }
                }
        }       

        return false;
}

std::string SFwallCore::GeoIPDestinationCriteria::key() const{
	return "geoip.destination";
}

bool SFwallCore::GeoIPDestinationCriteria::match(Packet* packet){
	if(alias){
		if(packet->type() == IPV4){
			if(alias->searchForNetworkMatch(((PacketV4*) packet)->getDstIp())){
				return true;
			}
		}
	}

	return false;
}

ObjectContainer* SFwallCore::CriteriaBuilder::serialize(Criteria* criteria){
	ObjectContainer* ret = NULL;

	if(criteria->key().compare("source.ipv4") == 0){
		SourceIpCriteria* rc = dynamic_cast<SourceIpCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->ip)));
		ret->put("mask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->mask)));
	}

	if(criteria->key().compare("destination.ipv4") == 0){
		DestinationIpCriteria* rc = dynamic_cast<DestinationIpCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("ip", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->ip)));
		ret->put("mask", new ObjectWrapper((string) IP4Addr::ip4AddrToString(rc->mask)));
	}

	if(criteria->key().compare("source.ipv6") == 0){
		SourceIp6Criteria* rc = dynamic_cast<SourceIp6Criteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("ip", new ObjectWrapper((string) IP6Addr::toString(rc->addr)));
		ret->put("cidr", new ObjectWrapper((double) rc->cidr));
	}

	if(criteria->key().compare("destination.ipv6") == 0){
		DestinationIp6Criteria* rc = dynamic_cast<DestinationIp6Criteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("ip", new ObjectWrapper((string) IP6Addr::toString(rc->addr)));
		ret->put("cidr", new ObjectWrapper((double) rc->cidr));
	}


	if(criteria->key().compare("group") == 0){
		GroupCriteria* rc = dynamic_cast<GroupCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (GroupPtr  group : rc->groups) {
			ret->put(new ObjectWrapper((double) group->getId()));
		}
	}

	if(criteria->key().compare("application.http.hostname") == 0){
		HttpHostname* rc = dynamic_cast<HttpHostname*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			ret->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("application.http.useragent") == 0){
		HttpUserAgent* rc = dynamic_cast<HttpUserAgent*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			ret->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("application.http.contenttype") == 0){
		HttpContentType* rc = dynamic_cast<HttpContentType*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			ret->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("source.ipv4.alias") == 0){
		SourceIpAliasCriteria* rc = dynamic_cast<SourceIpAliasCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			ret->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("destination.ipv4.alias") == 0){
		DestinationIpAliasCriteria* rc = dynamic_cast<DestinationIpAliasCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (AliasPtr alias : rc->aliases) {
			ret->put(new ObjectWrapper((std::string) alias->id));
		}
	}

	if(criteria->key().compare("timeperiod") == 0){
		TimePeriodCriteria* rc = dynamic_cast<TimePeriodCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);

		for (TimePeriodPtr period : rc->periods) {
			ret->put(new ObjectWrapper((string) period->uuid));
		}
	}

	if(criteria->key().compare("source.device") == 0){
		SourceNetworkDeviceCriteria* rc = dynamic_cast<SourceNetworkDeviceCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("name", new ObjectWrapper((string) rc->device));
	}

	if(criteria->key().compare("destination.device") == 0){
		DestinationNetworkDeviceCriteria* rc = dynamic_cast<DestinationNetworkDeviceCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("name", new ObjectWrapper((string) rc->device));
	}

	if(criteria->key().compare("destination.transport.port") == 0){
		DestinationPortCriteria* rc = dynamic_cast<DestinationPortCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);
		for(unsigned int port : rc->ports){
			ret->put(new ObjectWrapper((double) port));
		}
	}

	if(criteria->key().compare("source.transport.port") == 0){
		SourcePortCriteria* rc = dynamic_cast<SourcePortCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);
		for(unsigned int port : rc->ports){
			ret->put(new ObjectWrapper((double) port));
		}
	}

	if(criteria->key().compare("destination.transport.portrange") == 0){
		DestinationPortRangeCriteria* rc = dynamic_cast<DestinationPortRangeCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("startPort", new ObjectWrapper((double) rc->startPort));
		ret->put("endPort", new ObjectWrapper((double) rc->endPort));
	}

	if(criteria->key().compare("source.transport.portrange") == 0){
		SourcePortRangeCriteria* rc = dynamic_cast<SourcePortRangeCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("startPort", new ObjectWrapper((double) rc->startPort));
		ret->put("endPort", new ObjectWrapper((double) rc->endPort));
	}

	if(criteria->key().compare("protocol") == 0){
		ProtocolCriteria* rc = dynamic_cast<ProtocolCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("type", new ObjectWrapper((double) rc->type));
	}

	if(criteria->key().compare("source.user") == 0){
		UsersCriteria* rc = dynamic_cast<UsersCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);
		for(UserPtr user: rc->users){
			ret->put(new ObjectWrapper((string) user->getUserName()));
		}
	}

	if(criteria->key().compare("source.mac") == 0){
		MacAddressCriteria* rc = dynamic_cast<MacAddressCriteria*>(criteria);
		ret = new ObjectContainer(CARRAY);
		for(std::string i : rc->macs){
			ret->put(new ObjectWrapper((string) i));
		}
	}

	if(criteria->key().compare("signature") == 0){
		SignatureCriteria* rc = dynamic_cast<SignatureCriteria*>(criteria);
		ret = new ObjectContainer(CREL);
		ret->put("type", new ObjectWrapper((std::string) rc->signatureKey));
	}

	if(criteria->key().compare("user.quota") == 0){
		ret = new ObjectContainer(CREL);
	}

	if(criteria->key().compare("global.quota") == 0){
		ret = new ObjectContainer(CREL);
	}

	if(criteria->key().compare("source.quarantined") == 0){
		ret = new ObjectContainer(CREL);
	}

	if(criteria->key().compare("session.active") == 0){
		ret = new ObjectContainer(CREL);
	}

	if(criteria->key().compare("session.inactive") == 0){
		ret = new ObjectContainer(CREL);
	}

        if(criteria->key().compare("geoip.destination") == 0){
                GeoIPDestinationCriteria* rc = dynamic_cast<GeoIPDestinationCriteria*>(criteria);
                ret = new ObjectContainer(CREL);
                ret->put("country", new ObjectWrapper((std::string) rc->country));
        }

        if(criteria->key().compare("geoip.source") == 0){
                GeoIPSourceCriteria* rc = dynamic_cast<GeoIPSourceCriteria*>(criteria);
                ret = new ObjectContainer(CREL);
                ret->put("country", new ObjectWrapper((std::string) rc->country));
        }


	return ret;
}

SFwallCore::Criteria* SFwallCore::CriteriaBuilder::parse(std::string type, ObjectContainer* options){
	if(type.compare("source.ipv4") == 0){
		SourceIpCriteria* ret = new SourceIpCriteria();
		ret->ip = IP4Addr::stringToIP4Addr(options->get("ip")->string());
		ret->mask= IP4Addr::stringToIP4Addr(options->get("mask")->string());
		return ret;
	}

	if(type.compare("user.quota") == 0){
		UserQuotaExceededCriteria* ret = new UserQuotaExceededCriteria();
		return ret;
	}

	if(type.compare("global.quota") == 0){
		GlobalQuotaExceededCriteria* ret = new GlobalQuotaExceededCriteria();
		return ret;
	}

	if(type.compare("destination.ipv4") == 0){
		DestinationIpCriteria* ret = new DestinationIpCriteria();
		ret->ip = IP4Addr::stringToIP4Addr(options->get("ip")->string());
		ret->mask= IP4Addr::stringToIP4Addr(options->get("mask")->string());
		return ret;
	}

	if(type.compare("source.ipv6") == 0){
		SourceIp6Criteria* ret = new SourceIp6Criteria();
		IP6Addr::fromString(ret->addr, options->get("ip")->string());
		ret->cidr = options->get("cidr")->number();
		return ret;
	}

	if(type.compare("destination.ipv6") == 0){
		DestinationIp6Criteria* ret = new DestinationIp6Criteria();
		IP6Addr::fromString(ret->addr, options->get("ip")->string());
		ret->cidr = options->get("cidr")->number();
		return ret;
	}
	if(type.compare("source.device") == 0){
		SourceNetworkDeviceCriteria* ret = new SourceNetworkDeviceCriteria();
		ret->device = options->get("name")->string();
		ret->refresh();
		return ret;
	}

	if(type.compare("destination.device") == 0){
		DestinationNetworkDeviceCriteria* ret = new DestinationNetworkDeviceCriteria();
		ret->device = options->get("name")->string();
		ret->refresh();
		return ret;
	}

	if(type.compare("group") == 0){
		GroupCriteria* ret = new GroupCriteria();

		for (int y = 0; y < options->size(); y++) {
			GroupPtr g = System::getInstance()->getGroupDb()->getGroup(options->get(y)->number());
			if (g) ret->groups.push_back(g);
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("application.http.hostname") == 0){
		HttpHostname* ret = new HttpHostname();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("application.http.useragent") == 0){
		HttpUserAgent* ret = new HttpUserAgent();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("application.http.contenttype") == 0){
		HttpContentType* ret = new HttpContentType();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("source.ipv4.alias") == 0){
		SourceIpAliasCriteria* ret = new SourceIpAliasCriteria();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("destination.ipv4.alias") == 0){
		DestinationIpAliasCriteria* ret = new DestinationIpAliasCriteria();

		for (int y = 0; y < options->size(); y++) {
			AliasPtr alias = System::getInstance()->getFirewall()->aliases->get(options->get(y)->string());
			if (alias) ret->aliases.push_back(alias);
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("timeperiod") == 0){
		TimePeriodCriteria* ret = new TimePeriodCriteria();

		for (int y = 0; y < options->size(); y++) {
			TimePeriodPtr target = System::getInstance()->periods->getById(options->get(y)->string());
			if(target){
				ret->periods.push_back(target);
			}
		}
		ret->refresh();
		return ret;
	}

	if(type.compare("destination.transport.port") == 0){
		DestinationPortCriteria* ret = new DestinationPortCriteria();

		for (int y = 0; y < options->size(); y++) {
			ret->ports.insert(options->get(y)->number());
		}
		return ret;
	}

	if(type.compare("source.transport.port") == 0){
		SourcePortCriteria* ret = new SourcePortCriteria();

		for (int y = 0; y < options->size(); y++) {
			ret->ports.insert(options->get(y)->number());
		}
		return ret;
	}

	if(type.compare("source.transport.portrange") == 0){
		SourcePortRangeCriteria* ret = new SourcePortRangeCriteria();
		ret->startPort = options->get("startPort")->number();
		ret->endPort = options->get("endPort")->number();
		return ret;
	}

	if(type.compare("destination.transport.portrange") == 0){
		DestinationPortRangeCriteria* ret = new DestinationPortRangeCriteria();
		ret->startPort = options->get("startPort")->number();
		ret->endPort = options->get("endPort")->number();
		return ret;
	}

	if(type.compare("protocol") == 0){
		ProtocolCriteria* ret = new ProtocolCriteria();
		ret->type = (proto) options->get("type")->number();
		return ret;
	}

	if(type.compare("source.user") == 0){
		UsersCriteria* ret = new UsersCriteria();
		for (int y = 0; y < options->size(); y++) {
			ret->users.insert(System::getInstance()->getUserDb()->getUser(options->get(y)->string()));
		}
		return ret;
	}

	if(type.compare("source.mac") == 0){
		MacAddressCriteria* ret = new MacAddressCriteria();
		for (int y = 0; y < options->size(); y++) {
			ret->macs.insert(options->get(y)->string());
		}
		return ret;
	}

	if(type.compare("signature") == 0){
		SignatureCriteria* ret = new SignatureCriteria();
		ret->signatureKey =  options->get("type")->string();
		ret->refresh();
		return ret;
	}

	if(type.compare("source.quarantined") == 0){
		HostQuarantinedCriteria* ret = new HostQuarantinedCriteria();
		return ret;
	}

	if(type.compare("session.active") == 0){
		AuthenticatedCriteria* ret = new AuthenticatedCriteria();
		return ret;
	}

	if(type.compare("session.inactive") == 0){
		NotAuthenticatedCriteria* ret = new NotAuthenticatedCriteria();
		return ret;
	}

        if(type.compare("geoip.destination") == 0){
                GeoIPDestinationCriteria* ret = new GeoIPDestinationCriteria();
		ret->country = options->get("country")->string();
		ret->refresh();
                return ret;
        }

        if(type.compare("geoip.source") == 0){
		GeoIPSourceCriteria* ret = new GeoIPSourceCriteria();
		ret->country = options->get("country")->string();
		ret->refresh();
		return ret;
	}


	return NULL;
}

