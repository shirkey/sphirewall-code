/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <vector>
#include <time.h>
#include <sys/types.h>
#include <sstream>
#include <map>
#include <fstream>
#include <string>

using namespace std;

#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Alias.h"
#include "Utils/Utils.h"
#include "Utils/StringUtils.h"
#include "Json//JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"
#include "SFwallCore/TimePeriods.h"
#include "Packet.h"
#include "Auth/User.h"
#include "SFwallCore/Criteria.h"

SFwallCore::Nat::Nat() : type(NO_NAT) {}

SFwallCore::ACLStore::ACLStore(UserDb *userdb, GroupDb *groupdb, SessionDb *sessiondb, SFwallCore::AliasDb *aliases, TimePeriodStore *tps)
	: userDb(userdb),
	  groupDb(groupdb),
	  sessionDb(sessiondb),
	  aliases(aliases),
	  tps(tps) {
}

SFwallCore::ACLStore::~ACLStore() {
	for (FilterRule * entry : entries) {
		if (entry) delete entry;
	}
}

SFwallCore::ACLStore::ACLStore() {}

void SFwallCore::ACLStore::setInterfaceManager(IntMgr* interfaceManager) {
	this->interfaceManager = interfaceManager;
}

std::vector< SFwallCore::FilterRule * > &SFwallCore::ACLStore::listFilterRules() {
	return entries;
}

std::vector< SFwallCore::PriorityRule * > &SFwallCore::ACLStore::listPriorityRules() {
	return priorityQosRules;
}

SFwallCore::FilterRule::FilterRule()
	: Rule(), log(false)
{}

SFwallCore::FilterRule::~FilterRule()
{}

SFwallCore::Rule::Rule()
	: count(0),
	  enabled(true),
	  valid(true),
	  ignoreconntrack(false),
	  id(),
	  action(0),
	  comment()
{
	ignore_application_layer_filters = false;
}

SFwallCore::PriorityRule *SFwallCore::ACLStore::matchPriorityQos(Packet *packet) {
	for (PriorityRule * pr : priorityQosRules) {
		/*Packet should never be null: TODO: Find why its sometimes null and resolve*/
		if (packet == NULL) {
			return NULL;
		}

		if (pr->supermatch(packet, groupDb)) {
			pr->count++;
			return pr;
		}
	}

	return NULL;
}

SFwallCore::FilterRule *SFwallCore::ACLStore::matchFilterAcls(Packet *packet) {
	if (packet == NULL) {
		return NULL;
	}

	for (ProvidedFilterRule* fr : provided_rules) {
		if (fr->supermatch(packet, groupDb)) {
			fr->count++;
			return fr;
		}
	}


	for (FilterRule* fr : entries) {
		if (fr->supermatch(packet, groupDb)) {
			fr->count++;
			return fr;
		}
	}

	return NULL;
}


bool SFwallCore::Rule::isActive() {
	return valid && enabled;
}

bool SFwallCore::Rule::valueInRange(int lower, int value, int higher) {
	return (value >= lower && value <= higher);
}

bool SFwallCore::Rule::supermatch(Packet *packet, GroupDb *groupDb) {
	if(!isActive()){
		return false;
	}
	
	for(Criteria* c : criteria){
		if(!c->match(packet)){
			return false;
		}
	}

	return true;
}

void SFwallCore::ACLStore::deserializeRule(ObjectContainer *source, SFwallCore::Rule *entry) {
	if (source->has("id")) {
		entry->id = source->get("id")->string();
	}

	if (source->has("enabled")) {
		entry->enabled = source->get("enabled")->boolean();
	}

	if(source->has("criteria")){
		ObjectContainer* criteria = source->get("criteria")->container();
		for(int y = 0; y < criteria->size(); y++){
			ObjectContainer* individualCriteria = criteria->get(y)->container();
			entry->criteria.push_back(CriteriaBuilder::parse(individualCriteria->get("type")->string(), individualCriteria->get("conditions")->container()));
		}
	}
}

void SFwallCore::ACLStore::loadPriorityFile() {
	priorityQosRules.clear();
	ObjectContainer *root;

	if (configurationManager->has("priorityAcls")) {
		root = configurationManager->getElement("priorityAcls");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			PriorityRule *entry = new PriorityRule();

			ObjectContainer *source = rules->get(x)->container();
			deserializeRule(source, entry);

			entry->nice = source->get("nice")->number();
			loadAclEntry(entry);
		}
	}
	else {
		Logger::instance()->log("sphirewalld.firewall.acls", INFO, "Could not find any nat rules in configuration");
	}
}

void SFwallCore::ACLStore::loadFilterFile() {
	int loaded = 0;

	ObjectContainer *root;
	entries.clear();

	if (configurationManager->has("filterAcls")) {
		root = configurationManager->getElement("filterAcls");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			FilterRule *entry = new FilterRule();
			ObjectContainer *source = rules->get(x)->container();

			deserializeRule(source, entry);

			if (source->has("log")) {
				entry->log = source->get("log")->boolean();
			}

			if (source->has("action")) {
				entry->action = source->get("action")->number();
			}

			if (source->has("comment")) {
				entry->comment = source->get("comment")->string();
			}

			if (source->has("ignoreconntrack")) {
				entry->ignoreconntrack = source->get("ignoreconntrack")->boolean();
			}

			loadAclEntry(entry);
			loaded++;
		}
	}

	if (loaded == 0) {
		Logger::instance()->log("sphirewalld.firewall.acls", INFO, "No filter nat rules found, loading default accept all ruleset");
		FilterRule *def= new FilterRule();
		def->action = SQ_ACCEPT;
		def->enabled = true;

		createAclEntry(def);
	}
}

void SFwallCore::ACLStore::serializeRule(ObjectContainer *rule, SFwallCore::Rule *target) {
	if (target->comment.size() > 0) {
		rule->put("comment", new ObjectWrapper((string) target->comment));
	}

	rule->put("id", new ObjectWrapper((string) target->id));
	ObjectContainer* criteria = new ObjectContainer(CARRAY);
	for(Criteria* c: target->criteria){
		ObjectContainer* singleCriteria = new ObjectContainer(CREL);
		singleCriteria->put("type", new ObjectWrapper(c->key()));
		singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(c)));
		criteria->put(new ObjectWrapper(singleCriteria));
	}
	rule->put("criteria", new ObjectWrapper(criteria));
}

void SFwallCore::ACLStore::savePriorityFile() {
	int loaded = 0;

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (int x = 0; x < (int) priorityQosRules.size(); x++) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		serializeRule(rule, priorityQosRules[x]);
		rule->put("id", new ObjectWrapper((string) priorityQosRules[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) priorityQosRules[x]->enabled));
		rule->put("nice", new ObjectWrapper((double) priorityQosRules[x]->nice));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));
	configurationManager->setElement("priorityAcls", root);
	configurationManager->save();
}

void SFwallCore::ACLStore::saveFilterFile() {
	int loaded = 0;

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (int x = 0; x < (int) entries.size(); x++) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		serializeRule(rule, entries[x]);
		rule->put("id", new ObjectWrapper((string) entries[x]->id));
		rule->put("enabled", new ObjectWrapper((bool) entries[x]->enabled));
		rule->put("action", new ObjectWrapper((double) entries[x]->action));

		rule->put("ignoreconntrack", new ObjectWrapper((bool) entries[x]->ignoreconntrack));

		rule->put("log", new ObjectWrapper((bool) entries[x]->log));
		loaded++;

		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));
	configurationManager->setElement("filterAcls", root);
	configurationManager->save();
}

void SFwallCore::ACLStore::initLazyObjects(Rule *rule) {
	rule->valid = true;

	for(Criteria* c : rule->criteria){
		c->refresh();
	}
}

void SFwallCore::ACLStore::add_provided_rule(ProvidedFilterRule* rule){
	initLazyObjects(rule);
	provided_rules.push_back(rule);	
}

int SFwallCore::ACLStore::loadAclEntry(Rule *rule) {
	initLazyObjects(rule);

	if (FilterRule *r = dynamic_cast<FilterRule *> (rule)) {
		for(uint x = 0; x < entries.size(); x++) {
			if(entries[x]->id.compare(rule->id) == 0) {
				r->enabled = entries[x]->enabled;
				entries[x] = r;

				return 0;
			}
		}

		entries.push_back(r);
	}
	else if(PriorityRule *r = dynamic_cast<PriorityRule *>(rule)) {
		for(uint x = 0; x < priorityQosRules.size(); x++) {
			if(priorityQosRules[x]->id.compare(rule->id) == 0) {
				r->enabled = priorityQosRules[x]->enabled;
				priorityQosRules[x] = r;
				return 0;
			}
		}

		priorityQosRules.push_back(r);
	}

	return 0;
}

int SFwallCore::ACLStore::unloadAclEntry(Rule *rule) {
	if (FilterRule *e = dynamic_cast<FilterRule *>(rule)) {
		for (uint x = 0; x < entries.size(); x++) {
			if (e == entries[x]) {
				entries.erase(entries.begin() + x);

				delete e;
				break;
			}
		}

	}
	else if (PriorityRule *e = dynamic_cast<PriorityRule *>(rule)) {
		for (uint x = 0; x < priorityQosRules.size(); x++) {
			if (e == priorityQosRules[x]) {
				priorityQosRules.erase(priorityQosRules.begin() + x);

				delete e;
				break;
			}
		}

	}

	return 0;
}

	SFwallCore::PriorityRule::PriorityRule()
: Rule(), nice(1)
{}

SFwallCore::PriorityRule::~PriorityRule()
{}

int SFwallCore::ACLStore::createAclEntry(Rule *e) {
	if (e->id.size() == 0) {
		e->id = StringUtils::genRandom();
	}

	holdLock();
	loadAclEntry(e);
	save();
	releaseLock();
	return 0;
}

int SFwallCore::ACLStore::deleteAclEntry(Rule *e) {
	holdLock();
	unloadAclEntry(e);
	save();
	releaseLock();
	return 0;
}

int SFwallCore::ACLStore::deleteAclEntryByPos(int rulePos) {
	holdLock();
	Rule *rule;
	int result;
	if ((rulePos >= 0) && (((unsigned) rulePos) < entries.size())) {
		rule = entries[rulePos];
		releaseLock();
		result = this->deleteAclEntry(rule);
	}
	else {
		releaseLock();
		result = -1;
	}

	return result;
}

void SFwallCore::ACLStore::save() {
	saveFilterFile();
	savePriorityFile();
}

void SFwallCore::ACLStore::refresh_all_filtering_rule_providers(){
	provided_rules.clear();
	for(FilteringRuleProvider* provider : filtering_rule_providers){
		provider->add_rules(this);
	}	
}

bool SFwallCore::ACLStore::load() {
	loadFilterFile();
	loadPriorityFile();
}


void SFwallCore::ACLStore::movedownByPos(int rulePos) {
	holdLock();
	if ((rulePos + 1) < (int) entries.size()) {
		FilterRule *a = entries[rulePos];
		FilterRule *b = entries[rulePos + 1];

		entries[rulePos + 1] = a;
		entries[rulePos]     = b;
	}

	save();
	releaseLock();
}

void SFwallCore::ACLStore::movedown(Rule *rule) {
	int x = findPos(rule);

	if (dynamic_cast<FilterRule *>(rule)) {
		this->movedownByPos(x);
	}
	else {
		holdLock();

		if ((x + 1) < (int) priorityQosRules.size()) {
			PriorityRule *a = priorityQosRules[x];
			PriorityRule *b = priorityQosRules[x + 1];

			priorityQosRules[x + 1] = a;
			priorityQosRules[x]     = b;
		}
		releaseLock();
	}

	save();
}

void SFwallCore::ACLStore::moveupByPos(int rulePos) {
	holdLock();	

	if ((rulePos - 1) >= 0) {
		FilterRule *a = entries[rulePos];
		FilterRule *b = entries[rulePos - 1];

		entries[rulePos - 1] = a;
		entries[rulePos]     = b;
	}

	save();
	releaseLock();
}

void SFwallCore::ACLStore::moveup(Rule *rule) {
	int x = findPos(rule);

	if (dynamic_cast<FilterRule *>(rule)) {
		this->moveupByPos(x);
	}
	else {
		holdLock();

		if ((x - 1) >= 0) {
			PriorityRule *a = priorityQosRules[x];
			PriorityRule *b = priorityQosRules[x - 1];

			priorityQosRules[x - 1] = a;
			priorityQosRules[x]     = b;
		}
		releaseLock();
	}

	save();
}

int SFwallCore::ACLStore::findPos(Rule *rule) {
	holdLock();

	for (uint x = 0; x < entries.size(); x++) {
		Rule *target = entries[x];

		if (target == rule) {
			releaseLock();
			return x;
		}
	}

	for (uint x = 0; x < priorityQosRules.size(); x++) {
		Rule *target = priorityQosRules[x];

		if (target == rule) {
			releaseLock();
			return x;
		}
	}

	releaseLock();
	return -1;
}

SFwallCore::Rule *SFwallCore::ACLStore::getRuleById(std::string id) {
	holdLock();

	for (uint x = 0; x < entries.size(); x++) {
		Rule *target = entries[x];

		if (target->id.compare(id) == 0) {
			releaseLock();
			return target;
		}
	}

	for (uint x = 0; x < priorityQosRules.size(); x++) {
		Rule *target = priorityQosRules[x];

		if (target->id.compare(id) == 0) {
			releaseLock();
			return target;
		}
	}

	releaseLock();
	return NULL;
}

void SFwallCore::ACLStore::InterfaceChangeListener::interface_change() {
	store->holdLock();

	for (uint x = 0; x < store->entries.size(); x++) {
		store->initLazyObjects(store->entries[x]);
	}

	store->releaseLock();
}

void SFwallCore::ACLStore::aliasRemoved(AliasPtr alias) {
}

	SFwallCore::ACLStore::InterfaceChangeListener::InterfaceChangeListener(SFwallCore::ACLStore *store)
: store(store)
{}


