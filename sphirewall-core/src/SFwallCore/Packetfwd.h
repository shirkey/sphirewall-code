/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PACKETFWD_H
#define PACKETFWD_H

#include <iosfwd>

#define IPV4 0
#define IPV6 1

#define AF_NDEF 0
#define AF_HTTP 1
#define AF_HTTPS 2
#define AF_HTTP_PROXY 3

class NetDevice;
class Session;
namespace SFwallCore {

	class Connection;

	enum proto {
		ICMP = 1,
		IGMP = 2,
		GGP = 3,
		TCP = 6,
		EGP = 8,
		IGP = 9,
		UDP = 17,
		ESP = 50,
		AH = 51,
		ICMPv6 = 58,
		EIGRP = 88,
		OSPF = 89
	};

	enum pDirection {
		n,
		UP,
		DOWN
	};

	enum PacketDirection {
		DIR_SAME = 1,
		DIR_OPPOSITE = 2,
		DIR_ANY = 3
	};

	std::string stringProtocol(proto p);

	class SNatConnTracker;
	class DNatConnTracker;
	class SNatConn;
	class DNatConn;
	class NatRule;
	class ApplicationFrame ;
	class TransportFrame ;
	class Packet;
	class PacketV4;
	class PacketV6;
	class TcpUdpPacket;
	class TcpPacket;
	class UdpPacket;
	class IcmpPacket;
}

#endif
