#ifndef CRITERIA_H
#define CRITERIA_H

#include "SFwallCore/Packet.h"
#include "Auth/Group.h"
#include "SFwallCore/Alias.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/ApplicationLevel/Signatures.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/TimePeriods.h"
#include <boost/unordered_set.hpp>

class TimePeriod;

namespace SFwallCore {
	class CriteriaBuilder;
	class ConnectionIp;
	
	enum CriteriaType {IP, SESSION, APPLICATION, APPLICATION_EXPENSIVE};

	class Criteria {
		public:
			virtual std::string key() const = 0;
			virtual bool match(Packet* packet) = 0;	

			virtual void refresh(){}
			virtual CriteriaType type() const {
				return IP;
			}		

			virtual ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::NDEF;
			}
	};

	class SourceIpCriteria : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			unsigned int ip;
			unsigned int mask;
	};

	class DestinationIpCriteria : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			unsigned int ip;
			unsigned int mask;
	};

	class SourceIp6Criteria : public Criteria{
		public:
			SourceIp6Criteria(){
				addr = (struct in6_addr*) malloc(sizeof(struct in6_addr));
				cidr = 0;
			}		

			std::string key() const;
			bool match(Packet* packet);

			struct in6_addr *addr;
			int cidr;
	};

	class DestinationIp6Criteria : public Criteria{
		public:
			DestinationIp6Criteria(){
				addr = (struct in6_addr*) malloc(sizeof(struct in6_addr));
				cidr = 0;
			}

			std::string key() const;
			bool match(Packet* packet);

			struct in6_addr *addr;
			int cidr;
	};


	class GroupCriteria : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			GroupPtr group;
			std::list<GroupPtr> groups;

			CriteriaType type() const {
				return SESSION;
			}
	};

	class SourceIpAliasCriteria : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::list<AliasPtr> aliases;		
	};

	class DestinationIpAliasCriteria : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::list<AliasPtr> aliases;		
	};

	class TimePeriodCriteria : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::list<TimePeriodPtr> periods;
			std::list<TimePeriodPtr> optimisedPeriods;

			void refresh();
		private:
			void reduce(list<TimePeriodPtr>& remaining);
			void reduce(TimePeriodPtr newPeriod, list<TimePeriodPtr>& remaining);
	};

	class HttpContentType : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::list<AliasPtr> aliases;

			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}

	};	

	class HttpUserAgent : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::list<AliasPtr> aliases;
			CriteriaType type() const {
				return APPLICATION;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}
	};

	class HttpHostname : public Criteria{
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::list<AliasPtr> aliases;
			CriteriaType type() const {
				return APPLICATION;
			}

			void refresh();
			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				return ConnectionApplicationInfo::Classifier::HTTP;
			}
		private:

			class BanHitListItem {
				public:
					void insert(ConnectionIp *ip);
					bool check(ConnectionIp *ip);
					int size();
					void clear();
				private:
					std::unordered_set<unsigned int> banned;
			} nonTlsBannedItems;

			boost::unordered_map<std::string, int> cache;
	};

	class DestinationPortCriteria : public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			boost::unordered_set<unsigned int> ports;
	};

	class SourcePortCriteria : public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			boost::unordered_set<unsigned int> ports;
	};

	class DestinationPortRangeCriteria : public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			int startPort, endPort;
	};

	class SourcePortRangeCriteria : public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			int startPort, endPort;
	};

	class SourceNetworkDeviceCriteria: public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::string device;
			InterfacePtr devicePtr;

			void refresh();
	};

	class DestinationNetworkDeviceCriteria: public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			std::string device;
			InterfacePtr devicePtr;

			void refresh();
	};

	class ProtocolCriteria: public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			proto type;
	};

	class UsersCriteria: public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			boost::unordered_set<UserPtr> users;
			CriteriaType type() const {
				return SESSION;
			}


	};

	class MacAddressCriteria: public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			boost::unordered_set<std::string> macs;
	};

	class SignatureCriteria: public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);

			SignaturePtr signature;
			std::string signatureKey;

			void refresh();
			CriteriaType type() {
				return APPLICATION_EXPENSIVE;
			}

			ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const {
				if(signature){
					if(signature->lowPassApplicationFilterType() != ConnectionApplicationInfo::Classifier::NDEF){
						return signature->lowPassApplicationFilterType();
					}
				}

				return ConnectionApplicationInfo::Classifier::UNKNOWN;
			}
	};

	class UserQuotaExceededCriteria : public Criteria {
		public:
			std::string key() const;
			bool match(Packet* packet);
	};

        class GlobalQuotaExceededCriteria : public Criteria {
                public:
                        std::string key() const;
                        bool match(Packet* packet);
        };

        class HostQuarantinedCriteria: public Criteria {
                public:
                        std::string key() const;
                        bool match(Packet* packet);
        };

        class AuthenticatedCriteria: public Criteria {
                public:
                        std::string key() const;
                        bool match(Packet* packet);
        };


        class NotAuthenticatedCriteria: public Criteria {
                public:
                        std::string key() const;
                        bool match(Packet* packet);
        };

        class GeoIPCriteriaBase: public Criteria {
                public:
			GeoIPCriteriaBase(){
				this->alias = NULL;
			}
			void refresh();
			std::string country;

		protected:
			IpRangeAlias* alias;
        };

	class GeoIPSourceCriteria : public GeoIPCriteriaBase {
		public:
			std::string key() const;
			bool match(Packet* packet);
	};

	class GeoIPDestinationCriteria : public GeoIPCriteriaBase {
		public:
			std::string key() const;
			bool match(Packet* packet);
	};

	class CriteriaBuilder {
		public:
			static ObjectContainer* serialize(Criteria* criteria);
			static Criteria* parse(std::string type, ObjectContainer* options);
	};

	typedef boost::shared_ptr<Criteria> CriteriaPtr;
};

#endif
