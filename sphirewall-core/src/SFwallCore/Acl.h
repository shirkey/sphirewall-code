/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ACL_H
#define ACL_H

#include <vector>
#include "Packetfwd.h"
#include "SFwallCore/Alias.h"
#include "Core/Logger.h"
#include "Auth/UserDb.h"
#include "Auth/GroupDb.h"
#include "Auth/SessionDb.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/Interfaces.h"

class ConfigurationManager;
class ObjectContainer;
class User;
namespace SFwallCore {
	class ACLStore;
	class FilteringRuleProvider;

	class Criteria;
	enum NAT_TYPE {
		NO_NAT = -1, DNAT = 0, SNAT = 1
	};

	class AliasDb;

	class Nat {
		public:
			Nat();
			int target;
			NAT_TYPE type;
	};

	class Rule {
		public:
			virtual ~Rule(){}
			Rule();
			std::string id;
			int action;

			string comment;
			std::list<Criteria*> criteria;
			bool enabled;
			bool valid;
			int count;
			bool ignoreconntrack;
			bool ignore_application_layer_filters;

			bool supermatch(SFwallCore::Packet *packet, GroupDb *groupDb);
			bool isActive();

		private:
			bool valueInRange(int lower, int value, int higher);
	};

	class FilterRule : public Rule {
		public:
			FilterRule();
			virtual ~FilterRule();
			bool log;
	};

	class ProvidedFilterRule : public FilterRule {
		public:
			ProvidedFilterRule(FilteringRuleProvider* provider) :
				provider(provider){}

			ProvidedFilterRule();
			FilteringRuleProvider* provider;	
	};

	class PriorityRule : public Rule {
		public:
			PriorityRule();
			virtual ~PriorityRule();

			int nice;
	};

	class FilteringRuleProvider {
		public:
			virtual void add_rules(ACLStore* store) = 0;
			virtual void remove_all_rules(ACLStore* store){
					
			}
	};

	class ACLStore : public Lockable, public Configurable, public AliasRemovedListener {
		public:
			class InterfaceChangeListener : public virtual IntMgrChangeListener {
				public:
					InterfaceChangeListener(ACLStore *store);
					void interface_change();
				private:
					ACLStore *store;
			};

			ACLStore(UserDb *userdb, GroupDb *groupdb, SessionDb *sessiondb, AliasDb *aliases, TimePeriodStore *tps);
			ACLStore();
			virtual ~ACLStore();
			int loadAclEntry(Rule *);
			int unloadAclEntry(Rule *);

			Rule *getRuleById(std::string id);
			void movedownByPos(int rulePos);
			void movedown(Rule *);
			void moveupByPos(int rulePos);
			void moveup(Rule *);

			FilterRule *matchFilterAcls(Packet *);
			PriorityRule *matchPriorityQos(Packet *packet);

			void loadFilterFile();
			void initLazyObjects(SFwallCore::Rule *);

			void add_provided_rule(ProvidedFilterRule* rule);
			int createAclEntry(Rule *e);
			int deleteAclEntry(Rule *e);
			int deleteAclEntryByPos(int rulePos);

			void save();
			bool load();
			void setInterfaceManager(IntMgr* im);
			void aliasRemoved(AliasPtr alias);

			vector<ProvidedFilterRule*> listProvidedFilterRules(){
				return provided_rules;
			}

			vector<FilterRule *> &listFilterRules();
			vector<PriorityRule *> &listPriorityRules();

			void loadPriorityFile();
			const char* getConfigurationSystemName(){
				return "Filtering rule store";
			}

			void register_filtering_rule_provider(FilteringRuleProvider* provider){
				filtering_rule_providers.push_back(provider);
			}

			void refresh_all_filtering_rule_providers();
	
		private:
			void saveFilterFile();
			void savePriorityFile();
			int findPos(Rule *rule);
			UserDb *userDb;
			GroupDb *groupDb;
			SessionDb *sessionDb;
			TimePeriodStore *tps;
			AliasDb *aliases;

			IntMgr* interfaceManager;

			vector<FilterRule*> entries;
			vector<ProvidedFilterRule*> provided_rules;
			vector<PriorityRule*> priorityQosRules;

			void serializeRule(ObjectContainer *rule, SFwallCore::Rule *target);
			void deserializeRule(ObjectContainer *source, SFwallCore::Rule *entry);

			list<FilteringRuleProvider*> filtering_rule_providers;
	};
}

#endif
