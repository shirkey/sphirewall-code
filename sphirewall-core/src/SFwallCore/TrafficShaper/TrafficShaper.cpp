/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/TrafficShaper/TrafficShaper.h"
#include "Core/System.h"
#include "SFwallCore/Packet.h"
#include "Core/Logger.h"

using namespace std;

void TrafficShaper::addBucket(TokenBucketPtr b) {
	bucket.push_back(b);
}

TrafficShaper::TrafficShaper(GroupDb *groupDb, SessionDb *sessionDb, SFwallCore::PlainConnTracker *connections) 
	: groupDb(groupDb), sessionDb(sessionDb), connections(connections) {
}

bool TrafficShaper::load(){
        Logger::instance()->log("sphirewalld.firewall.ts", EVENT, "Loading Rules from file: " + ruleFile);

        ObjectContainer *root;
        rules.clear();
        if (configurationManager->has("qos")) {

                root = configurationManager->getElement("qos");
                ObjectContainer *arr = root->get("rules")->container();

                for (int x = 0; x < arr->size(); x++) {
                        ObjectContainer *source = arr->get(x)->container();

                        TsRulePtr target = TsRulePtr(new TsRule(source->get("id")->string()));
                        target->uploadRate = source->get("upload")->number();
                        target->downloadRate = source->get("download")->number();

                        target->name = source->get("name")->string();
                        target->cumulative = source->get("cumulative")->boolean();

                        if(source->has("criteria")){
                                ObjectContainer* criteria = source->get("criteria")->container();
                                for(int y = 0; y < criteria->size(); y++){
                                        ObjectContainer* individualCriteria = criteria->get(y)->container();
                                        target->filter->criteria.push_back(SFwallCore::CriteriaPtr(SFwallCore::CriteriaBuilder::parse(individualCriteria->get("type")->string(), individualCriteria->get("conditions")->container())));
                                }
                        }

                        //Blocks
                        rules.push_back(target);
                }
        }
	return true;	
}

void TrafficShaper::save(){
        if (configurationManager == NULL) {
                return;
        }

        ObjectContainer *root = new ObjectContainer(CREL);
        ObjectContainer *arr = new ObjectContainer(CARRAY);

        for (TsRulePtr b: rules) {
                ObjectContainer *o = new ObjectContainer(CREL);
                o->put("upload", new ObjectWrapper((double) b->uploadRate));
                o->put("download", new ObjectWrapper((double) b->downloadRate));
                o->put("id", new ObjectWrapper((string) b->id));
                o->put("cumulative", new ObjectWrapper((bool) b->cumulative));
                o->put("name", new ObjectWrapper((string) b->name));

                ObjectContainer* criteria = new ObjectContainer(CARRAY);
                for(SFwallCore::CriteriaPtr c: b->filter->criteria){
                        ObjectContainer* singleCriteria = new ObjectContainer(CREL);
                        singleCriteria->put("type", new ObjectWrapper(c->key()));
                        singleCriteria->put("conditions", new ObjectWrapper(SFwallCore::CriteriaBuilder::serialize(c.get())));
                        criteria->put(new ObjectWrapper(singleCriteria));
                }
                o->put("criteria", new ObjectWrapper(criteria));

                arr->put(new ObjectWrapper(o));
        }

        root->put("rules", new ObjectWrapper(arr));
        configurationManager->setElement("qos", root);
        configurationManager->save();
}


void TrafficShaper::start() {
	new boost::thread(boost::bind(&TrafficShaper::loop, this));
}

TrafficShaper::~TrafficShaper() {
}

void TrafficShaper::loop() {
	Logger::instance()->log("sphirewalld.firewall.ts", INFO, "Starting rate limiter background task");
	while (System::getInstance()->ALIVE) {
		if (bucket.size() > 0) {
			for (std::list<TokenBucketPtr>::iterator iter = bucket.begin(); iter != bucket.end(); iter++) {
				TokenBucketPtr tb = (*iter);
				tb->rollBucket();

				if (tb->hasExpired()) {
					iter = bucket.erase(iter);	
				}
			}

			usleep(100);
		}
		else {   // No shaping specified.  Wait and see if situation changes.
			sleep(1);
		}
	}
}

TokenBucketPtr TrafficShaper::match(SFwallCore::Packet *packet) {
        for (auto tb : bucket) {
                if (tb->filter->check(packet)) {
                        return tb;
                }
        }

        for (TsRulePtr rule : rules) {
                if (rule->filter->check(packet)) {
                        TokenFilterPtr filter = rule->filter;

                        TokenBucketPtr bucket;
                        /*What kind of rule are we dealing with? Is this a cumulative rule?*/
                        if(!rule->cumulative){
                                //Create a new Filter Object
                                TokenFilterPtr newFilter(new TokenFilter());

                                //Translate filters that are required
                                for(SFwallCore::CriteriaPtr criteria : filter->criteria){
                                        if(criteria->key().compare("group") == 0){
                                                SFwallCore::UsersCriteria* uc = new SFwallCore::UsersCriteria();
                                                uc->users.insert(packet->getUser());
                                                newFilter->criteria.push_back(SFwallCore::CriteriaPtr(uc));
                                        }else{
                                                newFilter->criteria.push_back(criteria);
                                        }
                                }

                                bucket = TokenBucketPtr(new TokenBucket(rule->uploadRate, rule->downloadRate, newFilter));

                        }else{
                                bucket = TokenBucketPtr(new TokenBucket(rule->uploadRate, rule->downloadRate, filter));
                        }

                        addBucket(bucket);

                        rule->incrementHits();
                        return bucket;
                }
        }

        return TokenBucketPtr();
}

void TrafficShaper::add(TsRulePtr rule) {
	Logger::instance()->log("sphirewalld.firewall.ts", EVENT, "Added a new TSRule");
	rules.push_back(rule);
}

TsRulePtr TrafficShaper::get(std::string id) {
	for(TsRulePtr rule : rules){
		if(rule->id.compare(id) == 0){
			return rule;
		}
	}	

	return TsRulePtr();
}

void TrafficShaper::del(TsRulePtr rule) {
	Logger::instance()->log("sphirewalld.firewall.ts", EVENT, "Deleting a TSRule");
	rules.remove(rule);
}

list<TsRulePtr> TrafficShaper::list() {
	return rules;
}

void TrafficShaper::invalidate(){
	holdLock();
	for(TokenBucketPtr tb : bucket){
		tb->invalidate();
	}
	bucket.clear();
	releaseLock();
}

void TrafficShaper::delBucket(TokenBucketPtr tkb) {
	if (tkb) {
		holdLock();
		for (std::list<TokenBucketPtr>::iterator iter = bucket.begin();
				iter != bucket.end();
				++iter) {
			if (*iter == tkb) {
				bucket.erase(iter);
				break;
			}
		}
		releaseLock();
	}
}

