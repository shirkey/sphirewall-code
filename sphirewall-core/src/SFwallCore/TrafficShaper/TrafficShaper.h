/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRAFFICSHAPER_H
#define TRAFFICSHAPER_H

#include <map>
#include "TokenBucket.h"
#include "Utils/Lock.h"
#include "SFwallCore/Packetfwd.h"
#include "TSRule.h"
#include "Core/Logger.h"
#include "Core/Config.h"
#include "Auth/SessionDb.h"
#include "Auth/UserDb.h"

class TSRuleStore;

namespace SFwallCore {
	class PlainConnTracker;
};

class TrafficShaper : public Configurable, public Lockable {
	public:
		TrafficShaper(GroupDb *groupDb, SessionDb *sessionDb, SFwallCore::PlainConnTracker *connections);
		TrafficShaper() {}
		~TrafficShaper();

		void addBucket(TokenBucketPtr);
		void delBucket(TokenBucketPtr);
		TokenBucketPtr match(SFwallCore::Packet *);
                void invalidate();
                std::list<TokenBucketPtr> listBuckets(){
			return bucket;
                }

		void push(TokenBucketPtr bucket, struct sq_packet *packet);

		/*Top up all the buckets*/
		void topup();

		/*Run through everything*/
		void loop();

		void start();
		void stop();
		bool load();
		void save();

		/*Manage rules*/
		void add(TsRulePtr b);
		TsRulePtr get(std::string id);
		void del(TsRulePtr);
		std::list<TsRulePtr> list();

		const char* getConfigurationSystemName(){
			return "Traffic shaper";
		}

	private:
		std::list<TokenBucketPtr> bucket;
		std::list<TsRulePtr> rules;
		std::string ruleFile;

		GroupDb *groupDb;
		SessionDb *sessionDb;
		SFwallCore::PlainConnTracker *connections;
};

#endif
