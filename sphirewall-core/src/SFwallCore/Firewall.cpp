/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string.h>
#include <time.h>
#include <sstream>
#include <vector>

using namespace std;

#include "Core/System.h"
#include "SFwallCore/Capture.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/ConnTracker.h"
#include "Core/Config.h"
#include "Core/Logger.h"
#include "Ids/Ids.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "SFwallCore/NatAcls.h"

#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"
#include "SFwallCore/ApplicationLevel/CapturePortal.h"
#include "SFwallCore/ApplicationLevel/ManagementLocator.h"

int SFwallCore::Firewall::TCP_CONNECTION_TIMEOUT_THESHOLD = 7200;
int SFwallCore::Firewall::UDP_CONNECTION_TIMEOUT_THESHOLD = 10;
int SFwallCore::Firewall::ICMP_CONNECTION_TIMEOUT_THESHOLD = 10;
int SFwallCore::Firewall::SYN_SYNACK_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::SYN_ACKACK_TIMEOUT = 60;
int SFwallCore::Firewall::FIN_FINACK_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::FIN_WAIT_TIMEOUT = 60 * 2;
int SFwallCore::Firewall::TIME_WAIT_TIMEOUT = 20;
int SFwallCore::Firewall::CLOSE_WAIT_TIMEOUT = 120;
int SFwallCore::Firewall::LAST_ACK_TIMEOUT = 30;
int SFwallCore::Firewall::CACHE_TTL_MAX = 20;
int SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_TIMEOUT = 10000;
int SFwallCore::Firewall::SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL = 1000;

int SFwallCore::Firewall::BLOCK_LIST_ENABLED = 1;
int SFwallCore::Firewall::PREROUTING_ENABLED = 1;
int SFwallCore::Firewall::POSTROUTING_ENABLED = 1;
int SFwallCore::Firewall::QOS_ENABLED = 0;
int SFwallCore::Firewall::BANDWIDTHDB_ENABLED = 1;
int SFwallCore::Firewall::REWRITE_ENABLED = 1;
int SFwallCore::Firewall::REWRITE_PROXY_REQUESTS_ENABLED = 1;
int SFwallCore::Firewall::HTTP_FILTERING_ENABLED = 1;
int SFwallCore::Firewall::BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY = 1;
int SFwallCore::Firewall::IGNORE_MAC_ADDRESS = 0;

int SFwallCore::Firewall::FIREWALL_IPV4_ENABLED = 1;
int SFwallCore::Firewall::FIREWALL_IPV6_ENABLED = 0;
int SFwallCore::Firewall::AGGRESIVE_APPLICATION_FILTERING = 1;
int SFwallCore::Firewall::BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD = 10000;
int SFwallCore::Firewall::CONN_TRACKER_DEADLOCK_PROTECTION_ENABLED = 1;
int SFwallCore::Firewall::TCP_CONNECTION_TRACKER_COLLECT_DETAILED_STATS = 0;
int SFwallCore::Firewall::MULTIPATH_WAN_LOADBALANCING = 0;
int SFwallCore::Firewall::RESIZE_TCP_ADJUST_SEQ_ENABLED = 1;
int SFwallCore::Firewall::ANONYMIZE_USER_DATA=0;
int SFwallCore::Firewall::ANONYMIZE_WEBSITE_DATA=0;
int SFwallCore::Firewall::APPLICATION_FILTERING_MIN_PAYLOAD_SIZE=20;
int SFwallCore::Firewall::APPLICATION_LAYER_FILTERS_ENABLED=1;
int SFwallCore::Firewall::APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED=1;
int SFwallCore::Firewall::WEBSITE_ALIAS_CACHE_ENABLED=1;
int SFwallCore::Firewall::ALIAS_CACHE_SIZE_MAX=10000;

SFwallCore::Firewall::Firewall() {}
SFwallCore::Firewall::Firewall(Config *config)
	: config(config){

	/*Install the runtime configuration*/
	config->getRuntime()->loadOrPut("TCP_CONNECTION_TIMEOUT_THESHOLD", &TCP_CONNECTION_TIMEOUT_THESHOLD);
	config->getRuntime()->loadOrPut("UDP_CONNECTION_TIMEOUT_THESHOLD", &UDP_CONNECTION_TIMEOUT_THESHOLD);
	config->getRuntime()->loadOrPut("ICMP_CONNECTION_TIMEOUT_THESHOLD", &ICMP_CONNECTION_TIMEOUT_THESHOLD);
	config->getRuntime()->loadOrPut("SYN_SYNACK_WAIT_TIMEOUT", &SYN_SYNACK_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("SYN_ACKACK_TIMEOUT", &SYN_ACKACK_TIMEOUT);
	config->getRuntime()->loadOrPut("FIN_FINACK_WAIT_TIMEOUT", &FIN_FINACK_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("FIN_WAIT_TIMEOUT", &FIN_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("TIME_WAIT_TIMEOUT", &TIME_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("CLOSE_WAIT_TIMEOUT", &CLOSE_WAIT_TIMEOUT);
	config->getRuntime()->loadOrPut("LAST_ACK_TIMEOUT", &LAST_ACK_TIMEOUT);
	config->getRuntime()->loadOrPut("TCPCONN_CACHE_TTL_MAX", &CACHE_TTL_MAX);
	config->getRuntime()->loadOrPut("SQUEUE_DEQUEUE_YIELD_TIMEOUT", &SQUEUE_DEQUEUE_YIELD_TIMEOUT);
	config->getRuntime()->loadOrPut("SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL", &SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL);
	config->getRuntime()->loadOrPut("BLOCK_LIST_ENABLED", &BLOCK_LIST_ENABLED);
	config->getRuntime()->loadOrPut("PREROUTING_ENABLED", &PREROUTING_ENABLED);
	config->getRuntime()->loadOrPut("POSTROUTING_ENABLED", &POSTROUTING_ENABLED);
	config->getRuntime()->loadOrPut("QOS_ENABLED", &QOS_ENABLED);
	config->getRuntime()->loadOrPut("BANDWIDTHDB_ENABLED", &BANDWIDTHDB_ENABLED);
	config->getRuntime()->loadOrPut("REWRITE_ENABLED", &REWRITE_ENABLED);
	config->getRuntime()->loadOrPut("REWRITE_PROXY_REQUESTS_ENABLED", &REWRITE_PROXY_REQUESTS_ENABLED);
	config->getRuntime()->loadOrPut("HTTP_FILTERING_ENABLED", &HTTP_FILTERING_ENABLED);
	config->getRuntime()->loadOrPut("FIREWALL_IPV4_ENABLED", &FIREWALL_IPV4_ENABLED);
	config->getRuntime()->loadOrPut("FIREWALL_IPV6_ENABLED", &FIREWALL_IPV6_ENABLED);
	config->getRuntime()->loadOrPut("BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY", &BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY);
	config->getRuntime()->loadOrPut("IGNORE_MAC_ADDRESS", &IGNORE_MAC_ADDRESS);
	config->getRuntime()->loadOrPut("AGGRESIVE_APPLICATION_FILTERING", &AGGRESIVE_APPLICATION_FILTERING);
	config->getRuntime()->loadOrPut("BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD", &BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD);
	config->getRuntime()->loadOrPut("TCP_CONNECTION_TRACKER_COLLECT_DETAILED_STATS", &TCP_CONNECTION_TRACKER_COLLECT_DETAILED_STATS);
	config->getRuntime()->loadOrPut("CONN_TRACKER_DEADLOCK_PROTECTION_ENABLED", &CONN_TRACKER_DEADLOCK_PROTECTION_ENABLED);
	config->getRuntime()->loadOrPut("MULTIPATH_WAN_LOADBALANCING", &MULTIPATH_WAN_LOADBALANCING);
	config->getRuntime()->loadOrPut("RESIZE_TCP_ADJUST_SEQ_ENABLED", &RESIZE_TCP_ADJUST_SEQ_ENABLED);
	config->getRuntime()->loadOrPut("ANONYMIZE_USER_DATA", &ANONYMIZE_USER_DATA);
	config->getRuntime()->loadOrPut("ANONYMIZE_WEBSITE_DATA", &ANONYMIZE_WEBSITE_DATA);
	config->getRuntime()->loadOrPut("APPLICATION_FILTERING_MIN_PAYLOAD_SIZE", &APPLICATION_FILTERING_MIN_PAYLOAD_SIZE);
	config->getRuntime()->loadOrPut("APPLICATION_LAYER_FILTERS_ENABLED", &APPLICATION_LAYER_FILTERS_ENABLED);
	config->getRuntime()->loadOrPut("APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED", &APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED);
	config->getRuntime()->loadOrPut("WEBSITE_ALIAS_CACHE_ENABLED", &WEBSITE_ALIAS_CACHE_ENABLED);
	config->getRuntime()->loadOrPut("ALIAS_CACHE_SIZE_MAX", &ALIAS_CACHE_SIZE_MAX);

	totalTcpPackets = 0;
	totalUdpPackets = 0;
	totalIcmpPackets = 0;
	totalDefaultAction = 0;

	debug = false;
	defaultAction = -1;

	totalTcpUp = 0;
	totalTcpDown = 0;
	totalUdpUp = 0;
	totalUdpDown = 0;

	avgPacketProcess = new AverageSampler(60);
	transferSampler = new FlexibleSecondIntervalSampler();
	connectionTracker = new PlainConnTracker(this);

	totalAllowed =0;
	totalDenied = 0;
	totalReset=0;
	totalRewrites=0;
	totalQueued=0;
	totalAllowed=0;
	totalDenied=0;
	totalReset=0;
	totalRewrites=0;
	totalQueued=0;
	totalTcpWindowResized=0;

	}

SFwallCore::Firewall::~Firewall() {
	delete acls;
	delete trafficShaper;
	delete aliases;
	delete garbageCron;
	delete connectionTracker;
}

void SFwallCore::Firewall::registerConfigurables(ConfigurationManager* configurationManager){
        aliases = new AliasDb();
        capturePortalEngine = new CapturePortalEngine(aliases);
        managementLocator = new ManagementLocator();
        httpFilter = new ApplicationFilter(aliases, System::getInstance()->getGroupDb(), System::getInstance()->getEventDb(), System::getInstance()->getUserDb());
        httpFilter->setEventDb(System::getInstance()->getEventDb());
        acls = new ACLStore(System::getInstance()->getUserDb(), System::getInstance()->getGroupDb(), System::getInstance()->getSessionDb(), aliases, System::getInstance()->periods);
        acls->setInterfaceManager(interfaceManager);
        natAcls = new NatAclStore(interfaceManager);
        trafficShaper = new TrafficShaper(System::getInstance()->getGroupDb(), System::getInstance()->getSessionDb(), connectionTracker);
        signatureStore = new SignatureStore();

	configurationManager->registerConfigurable(aliases);
	configurationManager->registerConfigurable(signatureStore);
	configurationManager->registerConfigurable(capturePortalEngine);
	configurationManager->registerConfigurable(managementLocator);
	configurationManager->registerConfigurable(httpFilter);
	configurationManager->registerConfigurable(acls);
	configurationManager->registerConfigurable(natAcls);
	configurationManager->registerConfigurable(trafficShaper);
}

bool SFwallCore::Firewall::load(){
	applicationLayerTracker = new ApplicationLayerTracker(configurationManager);
	applicationLayerTracker->initializeHandlers();
	connectionTracker->setApplicationLayerTracker(applicationLayerTracker);	

	garbageCron = new GarbageCollectorCron(connectionTracker);
	googleSafeSearchEngine = new GoogleSafeSearchEngine(configurationManager);

	interfaceManager->register_change_listener(new ACLStore::InterfaceChangeListener(acls));
	interfaceManager->register_change_listener(httpFilter);
	System::getInstance()->getGroupDb()->registerRemovedListener(httpFilter);
	System::getInstance()->periods->registerDeleteListener(httpFilter);

	applicationLayerFilteringHandlers.push_back(googleSafeSearchEngine);
	applicationLayerFilteringHandlers.push_back(capturePortalEngine);
	applicationLayerFilteringHandlers.push_back(managementLocator);
	applicationLayerFilteringHandlers.push_back(httpFilter);

	aliases->setAclStore(acls);
	aliases->registerRemovedListeners(httpFilter);
	aliases->registerRemovedListeners(acls);

	connectionTracker->registerDeleteHook(System::getInstance()->getBandwidthDbPrimer());
	connectionTracker->registerDeleteHook(System::getInstance()->getQuotaManager());
	System::getInstance()->getCronManager()->registerJob(garbageCron);
	System::getInstance()->getCronManager()->registerJob(new ApplicationFilter::CleanupCronJob(httpFilter));

	return true;
}

void SFwallCore::Firewall::start() {
	acls->refresh_all_filtering_rule_providers();
	trafficShaper->start();
	boost::thread th(&startCapture);
}

void SFwallCore::Firewall::stop() {
	sqClient.disconnect();
}

void SFwallCore::GarbageCollectorCron::run() {
	waiting = true;

	Logger::instance()->log("sphirewalld.firewall", INFO, "Starting cleanup on the Plain Connection Tracker");
	int ct_count = connectionTracker->cleanup();
	Logger::instance()->log("sphirewalld.firewall", INFO, "Finished cleanup on the Plain Connection Tracker: Removed connection count %d", ct_count);

	Logger::instance()->log("sphirewalld.firewall", INFO, "Flushing aliasdb cache");
	System::getInstance()->getFirewall()->aliases->cleanUpCaches();

	waiting = false;
}

void SFwallCore::Firewall::sample(map<string, double> &input) {
	input["firewall.transfer.persec"] = transferSampler->value();
	input["firewall.capture.tcp"] = System::getInstance()->getFirewall()->totalTcpPackets;
	input["firewall.capture.udp"] = System::getInstance()->getFirewall()->totalUdpPackets;
	input["firewall.capture.icmp"] = System::getInstance()->getFirewall()->totalIcmpPackets;

	input["firewall.capture.action.default"] = System::getInstance()->getFirewall()->totalDefaultAction;
	input["firewall.capture.action.allowed"] = System::getInstance()->getFirewall()->totalAllowed;
	input["firewall.capture.action.denied"] = System::getInstance()->getFirewall()->totalDenied;
	input["firewall.capture.action.reset"] = System::getInstance()->getFirewall()->totalReset;
	input["firewall.capture.action.rewrite"] = System::getInstance()->getFirewall()->totalRewrites;
	input["firewall.capture.action.queued"] = System::getInstance()->getFirewall()->totalQueued;
	input["firewall.capture.action.tcpwindworesized"] = System::getInstance()->getFirewall()->totalTcpWindowResized;

	input["firewall.capture.cp.queries"] = System::getInstance()->getFirewall()->capturePortalEngine->queries;
	input["firewall.capture.cp.hits"] = System::getInstance()->getFirewall()->capturePortalEngine->hits;
	input["firewall.aliasdb.cache.size"] = aliases->cacheSize();
	connectionTracker->sample(input);
}
