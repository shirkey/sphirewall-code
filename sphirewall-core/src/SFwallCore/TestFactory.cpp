/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>
#include <iostream>
#include <algorithm>
#include "SFwallCore/ConnTracker.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <linux/icmp.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <cstdlib>

#include "Utils/IP6Addr.h"
#include "SFwallCore/TestFactory.h"
#include "SFwallCore/Packet.h"

using namespace SFwallCore;

Packet *PacketBuilder::createTcp(std::string sourceIp, std::string destIp, int sourcePort, int destPort, std::string data) {
	char *packet = (char *) calloc(1, RAWSIZE);
	struct iphdr *iphdr = (struct iphdr *)(packet);
	struct tcphdr *tcphdr = (struct tcphdr *)(packet + sizeof(struct iphdr));

	iphdr->version = 4;
	iphdr->ihl = 5;
	iphdr->tos = 0;
	iphdr->tot_len = htons(sizeof(struct iphdr) + sizeof(struct tcphdr) + data.size());
	iphdr->id = 0;
	iphdr->frag_off = 0;
	iphdr->ttl = 255;
	iphdr->protocol = IPPROTO_TCP;
	iphdr->check = 0;
	iphdr->saddr     = htonl(IP4Addr::stringToIP4Addr(sourceIp));
	iphdr->daddr     = htonl(IP4Addr::stringToIP4Addr(destIp));

	tcphdr->source = htons(sourcePort);
	tcphdr->dest   = htons(destPort);

	tcphdr->syn = 0;
	tcphdr->rst = 0;
	tcphdr->ack = 0;
	tcphdr->fin = 0;
	tcphdr->doff = 5;

	memcpy(packet + (tcphdr->doff * 4) + (iphdr->ihl << 2), data.c_str(), data.size());
	struct sq_packet *sqp = (struct sq_packet *) calloc(1, sizeof(struct sq_packet));
	sqp->type = 0;
	sqp->len = sizeof(struct iphdr) + sizeof(struct tcphdr) + data.size();
	sqp->raw_packet = (unsigned char *) packet;
	Packet *ret = Packet::parsePacket(sqp);
	return ret;
}

Packet *PacketBuilder::createTcpV6(std::string sourceIp, std::string destIp, int sourcePort, int destPort) {
	char *packet = (char *) calloc(1, RAWSIZE);
	struct ip6_hdr *iphdr = (struct ip6_hdr *)(packet);
	struct tcphdr *tcphdr = (struct tcphdr *)(packet + sizeof(struct ip6_hdr));

	iphdr->ip6_nxt	 = IPPROTO_TCP;
	IP6Addr::fromString(&iphdr->ip6_src, sourceIp);
	IP6Addr::fromString(&iphdr->ip6_dst, destIp);

	tcphdr->source = htons(sourcePort);
	tcphdr->dest   = htons(destPort);

	tcphdr->syn = 0;
	tcphdr->rst = 0;
	tcphdr->ack = 0;
	tcphdr->fin = 0;

	struct sq_packet *sqp = (struct sq_packet *) calloc(1, sizeof(struct sq_packet));
	sqp->type = 1;
	sqp->raw_packet = (unsigned char *) packet;
	Packet *ret = Packet::parsePacket(sqp);
	return ret;
}

void PacketBuilder::setSourceDev(Packet *packet, int device) {
	struct sq_packet *sqp = packet->getInternalSqp();
	sqp->indev = device;
}

void PacketBuilder::setDestDev(Packet *packet, int device) {
	struct sq_packet *sqp = packet->getInternalSqp();
	sqp->outdev = device;
}

Packet *PacketBuilder::createUdp(std::string sourceIp, std::string destIp, int sourcePort, int destPort, char *data, size_t len) {
	char *packet = (char *) calloc(1, RAWSIZE);
	struct iphdr *iphdr = (struct iphdr *)(packet);
	struct udphdr *udphdr = (struct udphdr *)(packet + sizeof(struct iphdr));

	iphdr->version = 4;
	iphdr->ihl = 5;
	iphdr->tos = 0;
	iphdr->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr) + len;
	iphdr->id = 0;
	iphdr->frag_off = 0;
	iphdr->ttl = 255;
	iphdr->protocol = IPPROTO_UDP;
	iphdr->check = 0;
	iphdr->saddr     = htonl(IP4Addr::stringToIP4Addr(sourceIp));
	iphdr->daddr     = htonl(IP4Addr::stringToIP4Addr(destIp));

	udphdr->source = htons(sourcePort);
	udphdr->dest   = htons(destPort);
	udphdr->len    = htons(len);

	if (len) {
		memcpy(packet + (sizeof(struct iphdr) + sizeof(struct udphdr)), data, len);
	}

	struct sq_packet *sqp = (struct sq_packet *) calloc(1, sizeof(struct sq_packet));

	sqp->raw_packet = (unsigned char *) packet;

	sqp->type = 0;

	Packet *ret = Packet::parsePacket(sqp);

	return ret;
}

Packet *PacketBuilder::createIcmp(std::string sourceIp, std::string destIp, int icmpid) {
	char *packet = (char *) calloc(1, RAWSIZE);
	struct iphdr *iphdr = (struct iphdr *)(packet);
// 	struct icmphdr *udphdr = (struct icmphdr *)(packet + sizeof(struct iphdr));

	iphdr->version = 4;
	iphdr->ihl = 5;
	iphdr->tos = 0;
	iphdr->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
	iphdr->id = 0;
	iphdr->frag_off = 0;
	iphdr->ttl = 255;
	iphdr->protocol = IPPROTO_ICMP;
	iphdr->check = 0;
	iphdr->saddr     = htonl(IP4Addr::stringToIP4Addr(sourceIp));
	iphdr->daddr     = htonl(IP4Addr::stringToIP4Addr(destIp));

	struct icmphdr *icmp = (struct icmphdr *)(packet + (iphdr->ihl << 2));
	icmp->un.echo.id = htons(icmpid);

	struct sq_packet *sqp = (struct sq_packet *) calloc(1, sizeof(struct sq_packet));
	sqp->raw_packet = (unsigned char *) packet;
	sqp->type = 0;
	Packet *ret = Packet::parsePacket(sqp);
	return ret;
}

void PacketBuilder::setAck(TcpPacket *tcpPacket) {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) tcpPacket->getInternal();
	packet_tcp_header->ack = 1;
}

void PacketBuilder::setSyn(TcpPacket *tcpPacket) {
	struct tcphdr *packet_tcp_header = (struct tcphdr *) tcpPacket->getInternal();
	packet_tcp_header->syn = 1;
}
