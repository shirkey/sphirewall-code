/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <boost/regex.hpp>
#include <map>

using namespace std;

#include "Core/System.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/ConnTracker.h"
#include "Utils/Utils.h"
#include "Utils/NetworkUtils.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include <curl/curl.h>
#include "Api/JsonManagementService.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "Utils/HttpRequestWrapper.h"

void SFwallCore::AliasDb::setAclStore(SFwallCore::ACLStore *store) {
	this->store = store;
}

void SFwallCore::AliasDb::setResolve(bool r) {
	this->resolve = r;
}

void SFwallCore::AliasDb::registerRemovedListeners(SFwallCore::AliasRemovedListener *target) {
	this->removedListeners.push_back(target);
}

SFwallCore::AliasDb::AliasDb() : resolve(true)
{}


void SFwallCore::AliasDb::cleanUpCaches(){
	for(auto alias : aliases){
		if(alias.second->cacheSize() > System::getInstance()->getFirewall()->ALIAS_CACHE_SIZE_MAX){
			alias.second->flushCache();
		}
	}	
}

int SFwallCore::AliasDb::cacheSize(){
	int ret = 0;
	for(auto alias : aliases){
		ret += alias.second->cacheSize();
	}	
	return ret;
}

SFwallCore::AliasPtr SFwallCore::AliasDb::get(string id) {
	for (map<string, AliasPtr>::iterator iter = aliases.begin();
			iter != aliases.end();
			++iter) {
		if (iter->first.compare(id) == 0) {
			return iter->second;
		}
	}

	return AliasPtr();
}

SFwallCore::AliasPtr SFwallCore::AliasDb::getByName(string id) {
	for (map<string, AliasPtr>::iterator iter = aliases.begin();
			iter != aliases.end();
			++iter) {
		if (iter->second->name.compare(id) == 0) {
			return iter->second;
		}
	}

	return AliasPtr();
}

bool SFwallCore::AliasDb::load() {
	ObjectContainer *root;
	aliases.clear();		
	if (configurationManager->has("aliasDb")) {
		root = configurationManager->getElement("aliasDb");
		ObjectContainer *arr = root->get("aliases")->container();

		for (int x = 0; x < arr->size(); x++) {
			Alias *alias = NULL;
			ObjectContainer *o = arr->get(x)->container();
			int type = o->get("type")->number();
			std::string name = o->get("name")->string();
			std::string id = o->get("id")->string();

			switch ((AliasType)type) {
				case IP_RANGE: {
						       alias = new IpRangeAlias();
						       break;
					       }

				case IP_SUBNET: {
							alias = new IpSubnetAlias();
							break;
						}

				case WEBSITE_LIST : {
							    alias = new WebsiteListAlias();
							    break;
						    }

				case STRING_WILDCARD_LIST: {
								   alias = new StringWildcardListAlias();
								   break;
							   }
			}

			if (alias) {
				alias->name = name;
				alias->id = id;

				if (o->has("source")) {
					AliasListSourceType source = (AliasListSourceType) o->get("source")->number();

					switch (source) {
						case DNS: {
								  alias->source = new DnsListSource();
								  alias->source->detail = o->get("detail")->string();
								  break;
							  }

						case HTTP_FILE: {
									alias->source = new HttpFileSource();
									alias->source->detail = o->get("detail")->string();
									break;
								}
					};

				}
				else {
					ObjectContainer *values = o->get("values")->container();

					for (int z = 0; z < values->size(); z++) {
						alias->addEntry(values->get(z)->string());
					}
				}
			}

			alias->load();
			aliases[alias->id] = AliasPtr(alias);
		}
	}
	return true;
}

void SFwallCore::AliasDb::save() {
	ObjectContainer *root = new ObjectContainer(CREL);

	map<string, AliasPtr>::iterator iter = aliases.begin();
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	while (iter != aliases.end()) {
		ObjectContainer *o = new ObjectContainer(CREL);
		AliasPtr temp = iter->second;
		o->put("id", new ObjectWrapper((string) temp->id));
		o->put("name", new ObjectWrapper((string) temp->name));
		o->put("type", new ObjectWrapper((double) temp->type()));

		if (temp->source != NULL) {
			o->put("source", new ObjectWrapper((double) temp->source->type()));
			o->put("detail", new ObjectWrapper((string) temp->source->detail));
		}
		else {
			ObjectContainer *values = new ObjectContainer(CARRAY);

			for (std::string aliasEntry : temp->listEntries()) {
				values->put(new ObjectWrapper((string) aliasEntry));
			}

			o->put("values", new ObjectWrapper(values));
		}

		arr->put(new ObjectWrapper(o));
		iter++;
	}

	root->put("aliases", new ObjectWrapper(arr));

	if (configurationManager) {
		configurationManager->setElement("aliasDb", root);
		configurationManager->save();
	}
}


int SFwallCore::AliasDb::create(AliasPtr alias) {
	alias->id = StringUtils::genRandom();
	aliases[alias->id] = alias;

	int ret = alias->load();
	save();
	return ret;
}

int SFwallCore::AliasDb::del(AliasPtr target) {
	std::string key = target->id;

	for (list<AliasRemovedListener *>::iterator iter = removedListeners.begin();
			iter != removedListeners.end();
			iter++) {

		(*iter)->aliasRemoved(target);
	}

	map<string, AliasPtr>::iterator iter = aliases.find(key);

	if (iter != aliases.end()) {
		aliases.erase(iter);
	}

	save();
	return 0;
}

bool SFwallCore::IpRangeAlias::searchForNetworkMatch(unsigned int ip) {
	if (lock->tryLock()) {
		Range target;
		target.start = ip;
		bool res = items.find(target) != items.end();
		lock->unlock();
		return res;
	}

	return false;
}

bool SFwallCore::IpRangeAlias::searchForNetworkMatch(struct in6_addr *ip) {
	return false;
}

SFwallCore::Range::Range(std::string value) {
	static boost::regex exp("([0-9,\\.]+)(\\/|\\-)([0-9,\\.]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		start = IP4Addr::stringToIP4Addr(what[1]);
		end = IP4Addr::stringToIP4Addr(what[3]);
	}
	else {
		start = -1;
		end = -1;
	}
}

bool SFwallCore::Range::valid(std::string value) {
	static boost::regex exp("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)(\\/|\\-)([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		return true;
	}

	return false;
}

SFwallCore::RangeV6::RangeV6(std::string value) {
	static boost::regex exp("([0-9,a-f,A-F,\\:]+)\\/([0-9]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		start = (struct in6_addr *) malloc(sizeof(struct in6_addr));
		IP6Addr::fromString(start, what[1]);
		string temp = what[2];
		cidr = stoi(temp);
	}
}

bool SFwallCore::RangeV6::valid(std::string value) {
	static boost::regex exp("([0-9,a-f,A-F,\\:]+)\\/([0-9]+)");
	boost::smatch what;

	if (boost::regex_match(value, what, exp)) {
		return true;
	}

	return false;
}

void SFwallCore::IpRangeAlias::addEntry(const std::string& entry) {
	if (Range::valid(entry)) {
		items.insert(Range(entry));
	}
}

std::list<std::string> SFwallCore::IpRangeAlias::listEntries() {
	std::list<std::string> ret;
	std::multiset<Range>::iterator iter;

	for (iter = items.begin(); iter != items.end(); iter++) {
		stringstream ss;
		ss << IP4Addr::ip4AddrToString((*iter).start);
		ss << "-";
		ss << IP4Addr::ip4AddrToString((*iter).end);
		ret.push_back(ss.str());
	}

	return ret;
}

void SFwallCore::IpRangeAlias::removeEntry(const std::string& entry) {
	Range target(entry);

	std::multiset<Range>::iterator iter;

	for (iter = items.begin(); iter != items.end(); iter++) {
		if (target.start == (*iter).start && target.end == (*iter).end) {
			items.erase(iter);
			break;
		}
	}
}

bool SFwallCore::IpSubnetAlias::searchForNetworkMatch(unsigned int ip) {
	if (lock->tryLock()) {
		Range target;
		target.start = ip;
		bool res = items.find(target) != items.end();

		lock->unlock();
		return res;
	}

	return false;
}

bool SFwallCore::IpSubnetAlias::searchForNetworkMatch(struct in6_addr *ip) {
	if (lock->tryLock()) {
		for (list<RangeV6>::iterator iter = itemsV6.begin(); iter != itemsV6.end(); iter++) {
			if (IP6Addr::matchNetwork(ip, (*iter).start, (*iter).cidr)) {
				lock->unlock();
				return true;
			}
		}

		lock->unlock();
	}

	return false;
}

void SFwallCore::IpSubnetAlias::addEntry(const std::string& entry) {
	if (Range::valid(entry)) {
		items.insert(Range(entry));
	}

	if (RangeV6::valid(entry)) {
		itemsV6.push_back(RangeV6(entry));
	}
}

std::list<std::string> SFwallCore::IpSubnetAlias::listEntries() {
	std::list<std::string> ret;
	std::multiset<Range>::iterator iter;

	for (iter = items.begin(); iter != items.end(); iter++) {
		stringstream ss;
		ss << IP4Addr::ip4AddrToString((*iter).start);
		ss << "/";
		ss << IP4Addr::ip4AddrToString((*iter).end);
		ret.push_back(ss.str());
	}

	for (list<RangeV6>::iterator iter = itemsV6.begin(); iter != itemsV6.end(); iter++) {
		stringstream ss;
		ss << IP6Addr::toString((*iter).start) << "/" << (*iter).cidr;
		ret.push_back(ss.str());
	}

	return ret;
}

void SFwallCore::IpSubnetAlias::removeEntry(const std::string& entry) {
	if (Range::valid(entry)) {
		Range target(entry);

		std::multiset<Range>::iterator iter;

		for (iter = items.begin(); iter != items.end(); iter++) {
			if (target.start == (*iter).start && target.end == (*iter).end) {
				items.erase(iter);
				break;
			}
		}
	}
	else if (RangeV6::valid(entry)) {
		RangeV6 target(entry);

		std::list<RangeV6>::iterator iter;

		for (iter = itemsV6.begin(); iter != itemsV6.end(); iter++) {
			if (memcmp(target.start, (*iter).start, sizeof(struct in6_addr)) == 0 && target.cidr == (*iter).cidr) {
				iter = itemsV6.erase(iter);
			}
		}
	}
}

bool SFwallCore::WebsiteListAlias::search(const std::string& url) {
	lock->lock();
	int cacheEnabled = System::getInstance()->getFirewall()->WEBSITE_ALIAS_CACHE_ENABLED;

	if(cacheEnabled == 1){
		boost::unordered_map<std::string, bool>::iterator cacheSearch = cache.find(url);
		if(cacheSearch != cache.end()){
			lock->unlock();
			return cacheSearch->second;
		}
	}

	list<std::string> chunks;
	chunks.push_back(url);

	for (uint x = 0; x < url.size(); x++) {
		if (url[x] == '.') {
			string target = url.substr(x + 1, url.size() - x + 1);
			chunks.push_back(target);
		}
	}

	for (list<string>::reverse_iterator rit = chunks.rbegin(); rit != chunks.rend(); ++rit) {
		std::string target = (*rit);

		if (items.find(target) != items.end()) {

			if (cacheEnabled == 1){
				cache.insert(pair<std::string, bool>(target, true));
			}

			lock->unlock();
			return true;
		}
	}

	if(cacheEnabled == 1){
		cache.insert(pair<std::string, bool>(url, false));
	}
	lock->unlock();
	return false;
}

void SFwallCore::WebsiteListAlias::addEntry(const std::string& entry) {
	if (entry.size() == 0) {
		return;
	}

	lock->lock();
	cache.clear();
	items.insert(entry);
	lock->unlock();
}

std::list<std::string> SFwallCore::WebsiteListAlias::listEntries() {
	lock->lock();
	std::list<std::string> ret;
	unordered_set<std::string>::iterator iter;

	for (iter = items.begin(); iter != items.end(); iter++) {
		ret.push_back((*iter));
	}

	lock->unlock();
	return ret;
}

void SFwallCore::WebsiteListAlias::removeEntry(const std::string& entry) {
	lock->lock();
	items.erase(entry);
	cache.clear();
	lock->unlock();
}

void SFwallCore::StringWildcardListAlias::addEntry(const string& entry) {
	lock->lock();
	items.insert(entry);
	lock->unlock();
}

list<string> SFwallCore::StringWildcardListAlias::listEntries() {
	std::list<std::string> l;

	for (std::string s : items) {
		l.emplace_back(s);
	}

	return l;
}

void SFwallCore::StringWildcardListAlias::removeEntry(const string& entry) {
	lock->lock();
	items.erase(entry);
	lock->unlock();
}

bool SFwallCore::StringWildcardListAlias::search(const string& s) {
	for (std::string m : items) {
		if (s.find(m) != std::string::npos) {
			return true;
		}
	}

	return false;
}


int SFwallCore::DnsListSource::load(Alias *alias) {
	if (alias->type() != IP_SUBNET) {
		Logger::instance()->log("sphirewalld.firewall.aliases", INFO, " Tried to add a alias with source as dns with invalid format");
		return -2;
	}

	Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loading Hostname alias for " + detail);
	vector<unsigned int> hosts = hostToIPAddresses(detail);

	for (int x = 0; x < (int) hosts.size(); x++) {
		unsigned int ip = hosts[x];
		stringstream ss;
		ss << IP4Addr::ip4AddrToString(ip) << "/255.255.255.255";
		alias->addEntry(ss.str());
	}

	return 0;
}

int SFwallCore::HttpFileSource::load(Alias *alias) {
	Logger::instance()->log("sphirewalld.firewall.aliases", INFO, "Loading HttpList alias from " + detail);

	HttpRequestWrapper *request = new HttpRequestWrapper(detail.c_str(), GET);

	try {
		vector<string> items;
		split(request->execute(), '\n', items);

		for (string item : items) {
			alias->addEntry(item);
		}
	}
	catch (const HttpRequestWrapperException &ex) {
		Logger::instance()->log("sphirewalld.firewall.aliases", ERROR, "Failed to load HttpList alias from " + detail);
		delete request;
		return -1;
	}

	delete request;
	return 0;
}

void SFwallCore::Alias::clear() {
	list<string> values = listEntries();

	for (list<string>::iterator iter = values.begin();
			iter != values.end();
			iter++) {

		removeEntry((*iter));
	}
}

bool SFwallCore::IpRangeOperator::operator()(SFwallCore::Range const &n1, SFwallCore::Range const &n2) const {
	if (n1.end == (unsigned int) - 1) {
		bool res = (n1.start < n2.start);
		return res;
	}

	if (n2.end == (unsigned int) - 1) {
		bool res = n1.end < n2.start;
		return res;
	}

	if (n1.start < n2.start) {
		return true;
	}

	return false;
}

bool SFwallCore::IpSubnetOperator::operator()(SFwallCore::Range const &n1, SFwallCore::Range const &n2) const {
	if (n1.end == (unsigned int) - 1) {
		return (n1.start < n2.start);
	}

	if (n2.end == (unsigned int) - 1) {
		return (n1.start | (~ n1.end)) < n2.start;
	}

	if (n1.start < n2.start) {
		return true;
	}

	return false;
}


