/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FIREWALL_H
#define FIREWALL_H

#include <map>
#include <list>
#include <vector>
#include <queue>

#include "Core/Logger.h"
#include "Core/Config.h"
#include "Acl.h"
#include "Alias.h"
#include "client.h"
#include "TrafficShaper/TrafficShaper.h"
#include "Core/Cron.h"
#include "Core/SysMonitor.h"

class ConfigurationManager;
class Sampler;
namespace SFwallCore {
	class PlainConnTracker;
	class SNatConnTracker;
	class DNatConnTracker;
	class ApplicationLayerTracker;
	class ApplicationLayerFilterHandler;
	class CapturePortalEngine;
	class ManagementLocator;
	class ApplicationFilter;
	class GoogleSafeSearchEngine;
	class MultipathLoadBalancer;
	class NatAclStore;
	class SignatureStore;

	class GarbageCollectorCron : public CronJob {
		public:

			GarbageCollectorCron(PlainConnTracker *connectionTracker) :
				CronJob(60 * 5, "CONNTRACKER_GARBAGECOLLECTOR_CRON", true),
				connectionTracker(connectionTracker) {
				waiting = false;
			}

			void run();
			bool waiting;

		private:
			PlainConnTracker *connectionTracker;
	};

	class Firewall : public MetricSampler, public Configurable {
		public:
			static int TCP_CONNECTION_TIMEOUT_THESHOLD;
			static int UDP_CONNECTION_TIMEOUT_THESHOLD;
			static int ICMP_CONNECTION_TIMEOUT_THESHOLD;

			static int SYN_SYNACK_WAIT_TIMEOUT;
			static int SYN_ACKACK_TIMEOUT;
			static int FIN_FINACK_WAIT_TIMEOUT;
			static int FIN_WAIT_TIMEOUT;
			static int TIME_WAIT_TIMEOUT;
			static int CLOSE_WAIT_TIMEOUT;
			static int LAST_ACK_TIMEOUT;
			static int CACHE_TTL_MAX;
			static int SQUEUE_DEQUEUE_YIELD_TIMEOUT;
			static int SQUEUE_DEQUEUE_YIELD_SLEEP_INTERVAL;

			static int POST_PROCESSOR_ENABLED;
			static int BLOCK_LIST_ENABLED;
			static int PREROUTING_ENABLED;
			static int POSTROUTING_ENABLED;
			static int QOS_ENABLED;
			static int FIREWALL_FORCE_AUTHENTICATION;
			static int BANDWIDTHDB_ENABLED;
			static int REWRITE_ENABLED;
			static int REWRITE_PROXY_REQUESTS_ENABLED;
			static int HTTP_FILTERING_ENABLED;

			static int FIREWALL_IPV4_ENABLED;
			static int FIREWALL_IPV6_ENABLED;

			static const int PLAIN_CONN_HASH_MAX = 200000;
			static int BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY;
			static int IGNORE_MAC_ADDRESS;
			static int AGGRESIVE_APPLICATION_FILTERING;
			static int BLOCK_HTTP_FILTERING_TLS_SSLV3_RETRY_THRESHOLD;
			static int CONN_TRACKER_DEADLOCK_PROTECTION_ENABLED;
			static int TCP_CONNECTION_TRACKER_COLLECT_DETAILED_STATS;
			static int MULTIPATH_WAN_LOADBALANCING;
			static int ANONYMIZE_WEBSITE_DATA;
			static int ANONYMIZE_USER_DATA;
			static int RESIZE_TCP_ADJUST_SEQ_ENABLED;
			static int APPLICATION_FILTERING_MIN_PAYLOAD_SIZE;
			static int APPLICATION_LAYER_FILTERS_ENABLED;
			static int APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED;
			static int WEBSITE_ALIAS_CACHE_ENABLED;
			static int ALIAS_CACHE_SIZE_MAX;

			TrafficShaper *trafficShaper;
			SqClient sqClient;
			ACLStore *acls;
			PlainConnTracker *connectionTracker;
			GarbageCollectorCron *garbageCron;
			Firewall(Config *config);
			Firewall();
			virtual ~Firewall();

			void start();
			void stop();

			string filterFile;
			string natFile;
			string aliasFilename;
			int defaultAction;
			int defaultCount;

			AliasDb *aliases;

			int totalTcpUp;
			int totalTcpDown;
			int totalUdpUp;
			int totalUdpDown;

			bool debug;

			long totalTcpPackets;
			long totalUdpPackets;
			long totalIcmpPackets;
			long totalDefaultAction;
			long totalAllowed;
			long totalDenied;
			long totalReset;
			long totalRewrites;
			long totalQueued;
			long totalTcpWindowResized;

			Sampler *avgPacketProcess;

			PlainConnTracker *getPlainConnTracker() const {
				return connectionTracker;
			}

			void setInterfaceManager(IntMgr* i) {
				this->interfaceManager = i;
			}

			Sampler *transferSampler;
			void sample(map<string, double> &input);
			ApplicationLayerTracker *applicationLayerTracker;
			std::list<ApplicationLayerFilterHandler *> applicationLayerFilteringHandlers;

			CapturePortalEngine *capturePortalEngine;
			ManagementLocator *managementLocator;
			ApplicationFilter *httpFilter;
			GoogleSafeSearchEngine *googleSafeSearchEngine;
			MultipathLoadBalancer *multiPathWanLoadBalancer;
			NatAclStore *natAcls;
			SignatureStore* signatureStore;

			bool load();
			void registerConfigurables(ConfigurationManager* configurationManager);
			const char* getConfigurationSystemName(){
				return "Firewall core";
			}
		private:
			IntMgr* interfaceManager;
			Config* config;
	};
}
#endif
