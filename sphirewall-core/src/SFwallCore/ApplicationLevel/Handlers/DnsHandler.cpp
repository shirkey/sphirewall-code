/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "DnsHandler.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"
#include "Utils/DnsType.h"
#include <Core/System.h>
#include <Core/Logger.h>
#include <memory>

using namespace SFwallCore;


DnsHandler::DnsHandler() {

}

DnsHandler::~DnsHandler()
{}

bool DnsHandler::supportsProto(int p) {
	return (p == UDP || p == TCP) ? true : false;
}

void DnsHandler::handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	if (frame->getSize() <= (int) sizeof(dns_packet_header)) {
		return;
	}

	unsigned char *packet = frame->getInternal();
	dns_packet_header *hdr = (dns_packet_header *) packet;

	for (int i = 0; i < ntohs(hdr->nquestions); i++) {
		const char *query = dns_get_q_nth(hdr, i);
		ConnectionApplicationInfo *cai = connection->getApplicationInfo();
		cai->dnsQueryName = query;
		cai->dnsQueryNameSet = true;
		cai->type = SFwallCore::ConnectionApplicationInfo::Classifier::DNS;
		delete query;
	}
}

bool DnsHandler::tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame) {
	return false;
}
