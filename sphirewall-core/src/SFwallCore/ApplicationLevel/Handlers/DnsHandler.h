/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DNSHANDLER_H
#define DNSHANDLER_H

#include "Core/ConfigurationManager.h"
#include <SFwallCore/ApplicationLevel/Handlers.h>

using namespace SFwallCore;

class DnsHandler : public SFwallCore::ApplicationLayerTrackerHandler {
	public:
		DnsHandler();
		virtual ~DnsHandler();
		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

class DNSConfigurationCallback : public ConfigurationCallback {
		ApplicationLayerTracker *applt;
		ConfigurationManager *cm;
	public:
		DNSConfigurationCallback(ApplicationLayerTracker *applt, ConfigurationManager *cm)
			: applt(applt), cm(cm)
		{};
		virtual ~DNSConfigurationCallback() {};

		virtual void run() {
			if (cm->get("general:disableGoogleSearchSSL")->boolean()) {
				applt->addHandler(53, new DnsHandler());
			}
			else {
				applt->removeHandler(53);
			}
		};
};


#endif // DNSHANDLER_H
