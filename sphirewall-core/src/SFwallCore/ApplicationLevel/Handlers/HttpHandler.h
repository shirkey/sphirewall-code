/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HTTPHANDLER_H
#define HTTPHANDLER_H

#include <SFwallCore/ApplicationLevel/Handlers.h>

using namespace SFwallCore;

class HttpHandler : public SFwallCore::ApplicationLayerTrackerHandler {
	public:
		HttpHandler();
		HttpHandler(const HttpHandler &other);
		~HttpHandler();
		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

class HttpsHandler : public SFwallCore::ApplicationLayerTrackerHandler {
	public:
		HttpsHandler();
		HttpsHandler(const HttpsHandler &other);
		~HttpsHandler();
		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

class HttpConfigurationCallback : public ConfigurationCallback {
		ApplicationLayerTracker *applt;
		ConfigurationManager *cm;

public:
		HttpConfigurationCallback(ApplicationLayerTracker *applt, ConfigurationManager *cm)
			: applt(applt), cm(cm)
		{};
		virtual ~HttpConfigurationCallback() {};

		virtual void run() {
			if (cm->hasByPath("general:httpApplicationTracker") && cm->get("general:httpApplicationTracker")->boolean()) {
				applt->addHandler(80, new HttpHandler());
				applt->addHandler(443, new HttpsHandler());
			}
			else {
				applt->removeHandler(80);
				applt->removeHandler(443);
			}
		};
};


#endif // HTTPHANDLER_H
