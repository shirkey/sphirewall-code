/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HTTPPROXYHANDLER_H
#define HTTPPROXYHANDLER_H

#include <SFwallCore/ApplicationLevel/Handlers.h>
#include <Core/ConfigurationManager.h>

using namespace SFwallCore;

class HttpProxyHandler : public SFwallCore::ApplicationLayerTrackerHandler {
	public:
		HttpProxyHandler();
		HttpProxyHandler(const HttpProxyHandler &other);
		~HttpProxyHandler();
		virtual bool supportsProto(int protocol);
		virtual void handle(SFwallCore::PacketDirection direction, SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
		virtual bool tls(SFwallCore::Connection *connection, SFwallCore::ApplicationFrame *frame);
};

class HttpProxyConfigurationCallback : public ConfigurationCallback {
		ApplicationLayerTracker *applt;
		ConfigurationManager *cm;

public:
		HttpProxyConfigurationCallback(ApplicationLayerTracker *applt, ConfigurationManager *cm)
			: applt(applt), cm(cm)
		{};
		virtual ~HttpProxyConfigurationCallback() {};

		virtual void run() {
			if (cm->hasByPath("general:proxyApplicationTracker") && cm->get("general:proxyApplicationTracker")->boolean()) {
				applt->addHandler(8080, new HttpProxyHandler());
				applt->addHandler(3128, new HttpProxyHandler());
				applt->addHandler(1080, new HttpProxyHandler());
			}
			else {
				applt->removeHandler(8080);
				applt->removeHandler(3128);
				applt->removeHandler(1080);
			}
		};
};

#endif // HTTPPROXYHANDLER_H
