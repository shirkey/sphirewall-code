#ifndef SIGNATURE_FILTER_H
#define SIGNATURE_FILTER_H

#include <boost/shared_ptr.hpp>
#include <boost/regex.hpp>
#include <list>
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Connection.h"

namespace SFwallCore {
	class Packet;

	class Signature {
		public:
			Signature(){}

			virtual std::string type() = 0;
			virtual std::string id(){
				return type();
			}
			virtual std::string name() = 0;
			virtual std::string description() = 0;
			
			virtual bool matches(Connection* connection, Packet* packet) = 0;

			virtual ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const{
				return ConnectionApplicationInfo::Classifier::NDEF;
			}
	};	

	class HttpTrafficSignature : public Signature {
		public:
			std::string type(){
				return "signatures.http";
			};

			std::string name(){
				return "Http/Https Traffic";
			};

			std::string description(){
				return "Matches all website traffic, including proxied and ssl.";
			};

			bool matches(Connection* connection, Packet* packet);

                        ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const{
                                return ConnectionApplicationInfo::Classifier::HTTP;
                        }

	};

	class HttpDirectIpSignature : public Signature {
		public:
			std::string type(){
				return "signatures.http.directip";
			};

			std::string name(){
				return "Direct IP Http Traffic";
			};

			std::string description(){
				return "Matches http traffic with ip address requests rather than host";
			};

			bool matches(Connection* connection, Packet* packet);
                        ConnectionApplicationInfo::Classifier lowPassApplicationFilterType() const{
                                return ConnectionApplicationInfo::Classifier::HTTP;
                        }
	};

	class SshSignature : public Signature {
		public:
			std::string type(){
				return "signatures.ssh";
			};

			std::string name(){
				return "SSH Session";
			};

			std::string description(){
				return "Matches ssh session traffic, on any port";
			};

			bool matches(Connection* connection, Packet* packet);
	};

	class BitTorrentSignature: public Signature {
		public:
			std::string type(){
				return "signatures.bittorrent";
			};

			std::string name(){
				return "Bittorrent peer traffic";
			};

			std::string description(){
				return "Bit torrent";
			};

			bool matches(Connection* connection, Packet* packet);
	};

	class SkypeSignature: public Signature {
		public:
			std::string type(){
				return "signatures.skype";
			};

			std::string name(){
				return "Skype traffic";
			};

			std::string description(){
				return "Skype http traffic";
			};

			bool matches(Connection* connection, Packet* packet);
	};

	class RegexSignature : public Signature {
		public:
			std::string type(){
				return "signatures.regex";
			};

			std::string id(){
				return uniqueId;
			}			

			std::string name(){
				return userDefinedName;
			}

			std::string description(){
				return userDefinedDescription;
			};

			bool matches(Connection* connection, Packet* packet);

			std::string uniqueId;
			std::string userDefinedName;
			std::string userDefinedDescription;
			std::string expression;

			void refresh(){
				r = boost::regex(expression);
			}
		private:
			boost::regex r;
	};


	typedef boost::shared_ptr<Signature> SignaturePtr;
	class SignatureStore : public Configurable {
		public:
			SignatureStore();
			SignaturePtr get(std::string key);
			std::list<SignaturePtr> available();

			void add(SignaturePtr signature);
			void remove(SignaturePtr signature);

			bool load();
			void save();
			const char* getConfigurationSystemName(){
				return "Layer7 Signature Store";
			}

			void loadStandardSignatures();
		private:
			std::list<SignaturePtr> availableSignatures;
	};	
};

#endif
