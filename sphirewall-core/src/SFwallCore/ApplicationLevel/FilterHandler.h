#ifndef APPLICATION_LAYER_FILTER_HANDLER
#define APPLICATION_LAYER_FILTER_HANDLER

namespace SFwallCore {
	class Packet;
	class Connection;
	class ApplicationLayerFilterHandler {
		public:
			virtual void process(Connection *conn, Packet *packet) = 0;
			virtual bool matchesIpCriteria(Connection *conn, Packet *packet) = 0;
			virtual bool enabled() = 0;
	};
};
#endif
