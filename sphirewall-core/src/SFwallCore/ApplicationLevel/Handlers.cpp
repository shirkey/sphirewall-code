/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

using namespace std;

#include <SFwallCore/ApplicationLevel/Handlers/HttpHandler.h>
#include <SFwallCore/ApplicationLevel/Handlers/HttpProxyHandler.h>
#include <SFwallCore/ApplicationLevel/Handlers/DnsHandler.h>
#include "SFwallCore/Connection.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Packet.h"
#include "Core/System.h"
#include "SFwallCore/ApplicationLevel/Handlers.h"
#include <mutex>
#include <assert.h>

ApplicationLayerTrackerHandler::~ApplicationLayerTrackerHandler() {}

std::mutex mtx;

void SFwallCore::ApplicationLayerTracker::update(SFwallCore::Connection *connection, SFwallCore::Packet *incomming) {
	if (!connection->getApplicationInfo()->afVerdictApplied && connection->getDestinationPort() != -1) {
		std::lock_guard<std::mutex> lck(mtx);
		if(connection->getDestinationPort() < MAX_PORTS){
			ApplicationLayerTrackerHandler *handler = handlers[connection->getDestinationPort()];

			if (handler && handler->supportsProto(connection->getProtocol())) {
				handler->handle(connection->checkPacket(incomming, DIR_SAME) ? DIR_SAME : DIR_OPPOSITE, connection, incomming->getTransport()->getApplication());
			}
		}
	}
}

SFwallCore::ApplicationLayerTracker::ApplicationLayerTracker(ConfigurationManager *cm) : cm(cm) {
	if(System::getInstance()->getFirewall()->APPLICATION_LAYER_PROTOCOL_HANDLERS_ENABLED == 1){
		for (int i = 0; i < MAX_PORTS; i++) {
			handlers[i] = NULL;
		}
	}
}

void SFwallCore::ApplicationLayerTracker::initializeHandlers() {
	if (cm) {
		cm->registerChangeHandler("general:httpApplicationTracker", std::shared_ptr<ConfigurationCallback>(new HttpConfigurationCallback(this, cm)));
		cm->registerChangeHandler("general:proxyApplicationTracker", std::shared_ptr<ConfigurationCallback>(new HttpProxyConfigurationCallback(this, cm)));
		cm->registerChangeHandler("general:disableGoogleSearchSSL", std::shared_ptr<ConfigurationCallback>(new DNSConfigurationCallback(this, cm)));
		cm->triggerChangeHandlers();
	}
}

void ApplicationLayerTracker::addHandler(int port, ApplicationLayerTrackerHandler *handler) {
	std::lock_guard<std::mutex> lck(mtx);
	if (handlers[port] != NULL) {
		Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Replacing an old application layer tracker handler. [port %d]", port);
		delete handlers[port];
	}
	else {
		Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Adding an application layer tracker handler. [port %d]", port);
	}
	handlers[port] = handler;
}

void ApplicationLayerTracker::removeHandler(int port) {
	if(port < MAX_PORTS){
		std::lock_guard<std::mutex> lck(mtx);
		if (handlers[port]) {
			Logger::instance()->log("sphirewalld.firewall.applicationlayer", INFO, "Removing an application layer tracker handler. [port %d]", port);
			delete handlers[port];
		}
		handlers[port] = NULL;
	}else{
		Logger::instance()->log("sphirewalld.firewall.applicationlayer", ERROR, " a handler with a insane port number was operated on");
	}
}
