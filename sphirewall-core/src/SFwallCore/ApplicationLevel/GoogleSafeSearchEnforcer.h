#ifndef GOOGLE_SAFE_SEARCH_ENFORCER_H 
#define GOOGLE_SAFE_SEARCH_ENFORCER_H 

#include "SFwallCore/ApplicationLevel/FilterHandler.h"

namespace SFwallCore {
	class Packet;
	class Connection;
	class GoogleSafeSearchEngine : public ApplicationLayerFilterHandler {
		public:
			GoogleSafeSearchEngine(ConfigurationManager *config,std::string from = "www.google.co", std::string to = "nosslsearch.google.com");
			virtual void process(Connection *conn, Packet *packet);
			bool matchesIpCriteria(Connection *conn, Packet *packet);
			bool enabled();

			static bool requestNeedsRewrite(unsigned char *request);

		private:
			ConfigurationManager *config;
			std::string from;
			std::string to;
	};
};
#endif
