/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstring>
#include <boost/algorithm/string.hpp>
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Packet.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "Core/Cloud.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "Utils/DnsType.h"
#include <Utils/Dump.h>
#include "SFwallCore/Alias.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"

using namespace std;
SFwallCore::HttpEnforceGoogleSafeSearch::HttpEnforceGoogleSafeSearch() {}
SFwallCore::HttpEnforceGoogleSafeSearch::~HttpEnforceGoogleSafeSearch() {}

SFwallCore::GoogleSafeSearchEngine::GoogleSafeSearchEngine(ConfigurationManager *config, string from, string to)
        : config(config), from(from), to(to) {
}

bool SFwallCore::GoogleSafeSearchEngine::matchesIpCriteria(Connection *conn, Packet *packet) {
	return conn->getDestinationPort() == 53 || conn->getDestinationPort() == 80;
}

bool SFwallCore::GoogleSafeSearchEngine::enabled() {
	return true;
}


void SFwallCore::GoogleSafeSearchEngine::process(SFwallCore::Connection *conn, SFwallCore::Packet *packet) {
	ConnectionApplicationInfo *cai = conn->getApplicationInfo();

	if (cai->dnsQueryNameSet && cai->dnsQueryName.find(from) != std::string::npos && conn->checkPacket(packet, DIR_OPPOSITE)) {
		cai->afVerdictApplied= true;
		conn->setRewriteRule(new DNSRecaster(set<string> {from}, to));
		return;
	}
	else if (cai->http_contains(REQUEST, HTTP_HOSTNAME) && cai->http_contains(REQUEST, HTTP_REQUEST) && conn->checkPacket(packet, DIR_SAME)) {
		string host = cai->http_get(REQUEST, HTTP_HOSTNAME);
		unsigned char *req = packet->getTransport()->getApplication()->getInternal();

		if (host.find("google.co") != std::string::npos && packet->getTransport()->getApplication()->getSize() > 5) {
			if (GoogleSafeSearchEngine::requestNeedsRewrite(req)) {
				for (uint i = 5; i < packet->getTransport()->getApplication()->getSize(); i++) {
					if (req[i] == ' ') {
						return;
					}
					else if (req[i] == '?') {
						break;
					}
				}

				cai->afVerdictApplied= true;
				conn->setRewriteRule(new HttpEnforceGoogleSafeSearch());
				return;
			}
		}
	}
}

bool SFwallCore::GoogleSafeSearchEngine::requestNeedsRewrite(unsigned char *request) {
	return (request[0] == 'G' && request[4] == '/' && (request[5] == 's' || (request[5] == 'i' && request[6] == 'm')));
}


