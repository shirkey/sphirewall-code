/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <list>
#include <boost/regex.hpp>

#include "SFwallCore/Packet.h"
#include "SFwallCore/Firewall.h"
#include "SFwallCore/Connection.h"
#include "Core/Config.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Alias.h"
#include "Utils/TimeWrapper.h"
#include "Core/Event.h"
#include "SFwallCore/TimePeriods.h"
#include "Utils/StringUtils.h"

bool SFwallCore::ApplicationFilter::load() {
	holdLock();
	filters.clear();

	ObjectContainer *root;

	if (configurationManager->has("webfilter")) {
		root = configurationManager->getElement("webfilter");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			ObjectContainer *source = rules->get(x)->container();
			FilterCriteria *target = new FilterCriteria();
			target->id = source->get("id")->string();

			if(source->has("criteria")){
				ObjectContainer* criteria = source->get("criteria")->container();	
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					target->criteria.push_back(CriteriaBuilder::parse(individualCriteria->get("type")->string(), individualCriteria->get("conditions")->container()));
				}
			}

			if (source->has("fireEvent")) {
				target->fireEvent = source->get("fireEvent")->boolean();
			}
			if (source->has("name")) {
				target->name = source->get("name")->string();
			}

			if (source->has("action")) {
				target->action = (ApplicationFilterAction) source->get("action")->number();
			}

			if (source->has("redirect")) {
				target->redirect = source->get("redirect")->boolean();
				target->redirectUrl = source->get("redirectUrl")->string();
			}

			if (source->has("enabled")) {
				target->enabled  = source->get("enabled")->boolean();
			}

			if (source->has("metadata")) {
				target->metadata = source->get("metadata")->string();
			}

			if(source->has("temp_rule")){
				target->temp_rule = source->get("temp_rule")->boolean();
				target->expiry_timestamp = source->get("expiry_timestamp")->number();
			}

			filters.push_back(target);
		}
	}

	refresh();
	releaseLock();
	return true;
}

void SFwallCore::ApplicationFilter::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rulesArray = new ObjectContainer(CARRAY);

	for (FilterCriteria *source : filters) {
		ObjectContainer *target = new ObjectContainer(CREL);
		target->put("id", new ObjectWrapper((string) source->id));

		ObjectContainer* criteria = new ObjectContainer(CARRAY);
		for(Criteria* target : source->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(target->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(target)));		
			criteria->put(new ObjectWrapper(singleCriteria));	
		}

		target->put("criteria", new ObjectWrapper(criteria));
		target->put("fireEvent", new ObjectWrapper((bool) source->fireEvent));
		target->put("action", new ObjectWrapper((double) source->action));
		target->put("redirect", new ObjectWrapper((bool) source->redirect));
		target->put("redirectUrl", new ObjectWrapper((string) source->redirectUrl));
		target->put("metadata", new ObjectWrapper((string) source->metadata));
		target->put("name", new ObjectWrapper((string) source->name));
		target->put("enabled", new ObjectWrapper((bool) source->enabled));

		target->put("temp_rule", new ObjectWrapper((bool) source->temp_rule));
		target->put("expiry_timestamp", new ObjectWrapper((double) source->expiry_timestamp));
		rulesArray->put(new ObjectWrapper(target));
	}

	root->put("rules", new ObjectWrapper(rulesArray));
	configurationManager->setElement("webfilter", root);
	configurationManager->save();
}

void SFwallCore::ApplicationFilter::refresh(){
	requires = 0;	
	enabled_filters.clear();
	
	for (FilterCriteria *source : filters) {
		if(source->enabled){
			for(Criteria* target : source->criteria){
				if(target->lowPassApplicationFilterType() != ConnectionApplicationInfo::Classifier::NDEF){
					requires = ( requires | target->lowPassApplicationFilterType());	

					Logger::instance()->log("sphirewalld.firewall.applicationfilter.refresh", INFO, 
							"Adding low pass filter for connection info type %d", target->lowPassApplicationFilterType());
				}
			}

			source->refresh();
			enabled_filters.push_back(source);
		}
	}
}


SFwallCore::AliasDb *SFwallCore::ApplicationFilter::getAliasDb() {
	return aliases;
}

void SFwallCore::ApplicationFilter::setEventDb(EventDb *eventDb) {
	this->eventDb = eventDb;
}

SFwallCore::ApplicationFilter::ApplicationFilter(SFwallCore::AliasDb *aliases, GroupDb *groupDb, EventDb *eventDb, UserDb * userDb)
: aliases(aliases), groupDb(groupDb), eventDb(eventDb), userDb(userDb)
{}


void SFwallCore::ApplicationFilter::timePeriodDeleted(TimePeriodPtr removed) {
	holdLock();
	refresh();
	releaseLock();
}

void SFwallCore::ApplicationFilter::aliasRemoved(AliasPtr alias) {
        holdLock();
        refresh();
        releaseLock();
}

bool SFwallCore::ApplicationFilter::matchesIpCriteria(Connection *conn, Packet *packet) {
	if(packet->getTransport()->getApplication()->getSize() < System::getInstance()->getFirewall()->APPLICATION_FILTERING_MIN_PAYLOAD_SIZE){
		return false;
	}

	if(ConnectionApplicationInfo::Classifier::UNKNOWN != (ConnectionApplicationInfo::Classifier::UNKNOWN & requires) && 
			conn->getApplicationInfo()->type != (conn->getApplicationInfo()->type & requires)){

		return false;
	}

	return true;
}

bool SFwallCore::ApplicationFilter::enabled() {
	return System::getInstance()->getFirewall()->HTTP_FILTERING_ENABLED == 1;
}

void SFwallCore::ApplicationFilter::process(Connection *conn, SFwallCore::Packet *packet) {
	holdLock();

	if(conn->checkPacket(packet, DIR_SAME)){
		conn->getApplicationInfo()->afProcessedRequest= true;	
	}else{
		conn->getApplicationInfo()->afProcessedResponse= true;	
	}

	for (FilterCriteria * filter : enabled_filters) {
		if (filter->match(packet)) {
			releaseLock();
			packet->getConnection()->getApplicationInfo()->afVerdictApplied = true;

			if (filter->action != ALLOW) {
				if (conn->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP 
					&& !conn->getApplicationInfo()->tls && !conn->getApplicationInfo()->sslv3 && filter->redirect) {

					conn->setRewriteRule(new HttpRedirectRewrite(filter->redirectUrl));
				}
				else {
					conn->terminate();
				}
			}

			if (filter->fireEvent && eventDb) {
				EventParams params;
				params["host"] = packet->getConnection()->getApplicationInfo()->http_hostName;
				params["sourceIp"] = packet->getConnection()->getIp()->getSrcIpString();
				params["macAddress"] = packet->getHw();
				params["metadata"] = filter->metadata;
				params["policy.name"] = filter->name;
				params["policy.id"] = filter->id;

				if (packet->getUser()) {
					params["user"] = packet->getUser()->getUserName();
				}

				eventDb->add(new Event(FIREWALL_WEBFILTER_HIT, params));
			}


			return;
		}
	}

	releaseLock();
	return;
}

SFwallCore::FilterCriteria::FilterCriteria() 
: log(false), fireEvent(false), action(DENY), redirect(false), enabled(false)
{
	this->temp_rule = false;	
	this->expiry_timestamp = -1;	
}

bool SFwallCore::FilterCriteria::match(SFwallCore::Packet *packet) {
	if(!enabled || criteria.size() == 0){
		return false;
	}

	return optimized.match(packet);
}

bool SFwallCore::OptimizedFilterCriteria::match(SFwallCore::Packet* packet){
	for(Criteria* target : ipCriteria){
		if(!target->match(packet)){
			return false;
		}
	}

	for(Criteria* target : sessionCriteria){
		if(!target->match(packet)){
			return false;
		}
	}

	for(Criteria* target : applicationCriteria){
		if(!target->match(packet)){
			return false;
		}
	}

	for(Criteria* target : applicationExpensiveCriteria){
		if(!target->match(packet)){
			return false;
		}
	}

	return true;
}

void SFwallCore::OptimizedFilterCriteria::load(std::list<Criteria*> criteria){
	ipCriteria.clear();
	sessionCriteria.clear();
	applicationCriteria.clear();
	applicationExpensiveCriteria.clear();

	for(Criteria* c: criteria){
		switch(c->type()){
			case IP:
				ipCriteria.push_back(c);
				break;
			case SESSION:
				sessionCriteria.push_back(c);
				break;
			case APPLICATION:
				applicationCriteria.push_back(c);
				break;
			case APPLICATION_EXPENSIVE:
				applicationExpensiveCriteria.push_back(c);
				break;
			default:
				break;
		};
	}
}

void SFwallCore::ApplicationFilter::addRule(SFwallCore::FilterCriteria *rule) {
	filters.push_back(rule);
}

void SFwallCore::ApplicationFilter::removeRule(SFwallCore::FilterCriteria *rule) {
	filters.remove(rule);
	delete rule;
}

std::list<SFwallCore::FilterCriteria *> &SFwallCore::ApplicationFilter::getRules() {
	return filters;
}

int SFwallCore::ApplicationFilter::findpos(FilterCriteria *target) {
	int n = 0;
	for (auto x : filters) {
		if (x == target) {
			return n;
		}
		n++;
	}

	return -1;
}

void SFwallCore::ApplicationFilter::moveup(FilterCriteria *target) {
	int currentpos = findpos(target);

	if (currentpos <= 0) {
		return;
	}

	filters.remove(target);
	insert(currentpos - 1, target);
}

void SFwallCore::ApplicationFilter::insert(int pos, FilterCriteria *target) {
	int n = 0;

	for (list<FilterCriteria *>::iterator iter = filters.begin();
			iter != filters.end();
			iter++) {

		if (n == pos) {
			filters.insert(iter, target);
			return;
		}

		n++;
	}

	filters.push_back(target);
}

void SFwallCore::ApplicationFilter::movedown(FilterCriteria *target) {
	int currentpos = findpos(target);

	if ((size_t)currentpos == filters.size() - 1) {
		return;
	}

	filters.remove(target);
	insert(currentpos + 1, target);
}

SFwallCore::FilterCriteria *SFwallCore::ApplicationFilter::get(std::string id) {
	for (FilterCriteria * filter : filters) {
		if (filter->id.compare(id) == 0) {
			return filter;
		}
	}

	return NULL;
}

void SFwallCore::ApplicationFilter::groupRemoved(GroupPtr group) {
	holdLock();
	refresh();
	releaseLock();
}

void SFwallCore::ApplicationFilter::CleanupCronJob::run(){
	filter->holdLock();
	list<FilterCriteria*>::iterator iter;
	for(iter = filter->getRules().begin(); iter != filter->getRules().end(); iter++){
		FilterCriteria* rule = (*iter);
		if(rule->temp_rule && time(NULL) > rule->expiry_timestamp){
			Logger::instance()->log("sphirewalld.firewall.applicationfilter.cleanup", INFO, 
				"Removing layer7 rule with id '%s', it was temporary and has expired", rule->id.c_str());

			delete rule;
			iter = filter->getRules().erase(iter);
		}
	}

	filter->save();
	filter->releaseLock();
}

