#ifndef CAPTURE_PORTAL_H 
#define CAPTURE_PORTAL_H 

namespace SFwallCore {
        class TcpConnection;
        class Packet;
        class Connection;
        class ConnectionIp;
        class Alias;
        class AliasDb;

        class CapturePortalEngine: public ApplicationLayerFilterHandler, public Configurable {
                public:
                        CapturePortalEngine(AliasDb *aliases);

                        virtual void process(Connection *conn, Packet *packet);
                        bool matchesIpCriteria(Connection *conn, Packet *packet);
                        bool enabled();

                        static int CAPTURE_PORTAL_MODE;
                        static int CAPTURE_PORTAL_FORCE;
                        static int CAPTURE_PORTAL_REQUIRES_HOSTNAME;
                        static int CAPTURE_PORTAL_FORCE_AUTH_WEBSITE_EXCEPTIONS;
                        static int REWRITE_PROXY_REQUESTS_ENABLED;
                        static int CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION;
                        static int CAPTURE_PORTAL_DMZ_MODE_ENABLED;

                        std::list<AliasPtr> &getForceAuthRanges();
                        std::list<AliasPtr> &getForceAuthWebsiteExceptions();
                        std::list<AliasPtr> &getForceAuthDestIpExceptions();

                        void save();
                        bool load();
                        const char* getConfigurationSystemName() {
                                return "Capture portal engine";
                        }

                        long queries;
                        long hits;
                private:
                        AliasDb *aliases;

                        //Force auth rules:
                        std::list<AliasPtr> forceAuthSources;
                        std::list<AliasPtr> forceAuthWebsiteExceptions;
                        std::list<AliasPtr> forceAuthDestIpExceptions;
                        bool isAddressInAliases(int direction, const std::list<AliasPtr> &aliases, ConnectionIp *connectionIp);
                        bool isWebsiteInAliases(const std::list<AliasPtr> &aliases, const std::string &website);
        };
};

#endif
