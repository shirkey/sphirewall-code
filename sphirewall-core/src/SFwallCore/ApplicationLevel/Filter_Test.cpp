/*
   Copyright Michael Lawson
   This file is part of Sphirewall.

   Sphirewall is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Sphirewall is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <iostream>
#include <list>

#include "Auth/GroupDb.h"
#include "SFwallCore/TestFactory.h"
#include "Utils/IP4Addr.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Alias.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/ApplicationLevel/ApplicationFilter.h"
#include "SFwallCore/ApplicationLevel/Handlers.h"
#include "Core/Event.h"
#include "test.h"
#include "Core/System.h"

using namespace SFwallCore;

TEST(HttpFilter, addFilter) {
	ApplicationFilter *filter = new ApplicationFilter(NULL, NULL, NULL, NULL);
	filter->addRule(new FilterCriteria());

	EXPECT_TRUE(filter->getRules().size() == 1);
}

TEST(HttpFilter, removeFilter) {
	ApplicationFilter *filter = new ApplicationFilter(NULL,  NULL, NULL, NULL);
	FilterCriteria *rule1 = new FilterCriteria();
	rule1->enabled = true;

	filter->addRule(rule1);
	filter->addRule(new FilterCriteria());

	EXPECT_TRUE(filter->getRules().size() == 2);

	filter->removeRule(rule1);
	EXPECT_TRUE(filter->getRules().size() == 1);
}

TEST(HttpFilter, filter_noMatchingRule) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "sf");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->increment(packet);

	ApplicationFilter *filter = new ApplicationFilter(NULL,  NULL, NULL, NULL);
	filter->process(connection, packet);
	EXPECT_FALSE(connection->isTerminating());
}

TEST(HttpFilter, filter_disabledRuleMissed) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "--");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	connection->getApplicationInfo()->setHttpHost("testing.com");
	packet->setConnection(connection);
	connection->increment(packet);
	ApplicationFilter *filter = new ApplicationFilter(NULL, NULL, NULL, NULL);
        HttpHostname* hc = new HttpHostname(); hc->aliases.push_back(AliasPtr(new SFwallCore::WebsiteListAlias( {"testing.com"})));
	SFwallCore::FilterCriteria *matchingRule = new SFwallCore::FilterCriteria();
	matchingRule->enabled = false;
	matchingRule->criteria.push_back(hc);

	filter->addRule(matchingRule);
	filter->process(connection, packet);

	EXPECT_FALSE(connection->isTerminating());
}

TEST(HttpFilter, whiteListOverRidesBlackList) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "--");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	connection->getApplicationInfo()->setHttpHost("testing.com");
	connection->increment(packet);

	ApplicationFilter *filter = new ApplicationFilter(NULL, NULL, NULL, NULL);
	SFwallCore::WebsiteListAlias *list = new SFwallCore::WebsiteListAlias( {"testing.com"});

	SFwallCore::FilterCriteria *matchingRule = new SFwallCore::FilterCriteria();
        HttpHostname* hc = new HttpHostname(); hc->aliases.push_back(AliasPtr(new SFwallCore::WebsiteListAlias( {"testing.com"})));
	matchingRule->enabled = true;
	matchingRule->action = ALLOW;
	matchingRule->criteria.push_back(hc);
	filter->addRule(matchingRule);

	SFwallCore::FilterCriteria *blackListRule = new SFwallCore::FilterCriteria();
	blackListRule->enabled = true;
	blackListRule->criteria.push_back(hc);
	blackListRule->action = DENY;

	filter->addRule(blackListRule);

	filter->process(connection, packet);
	EXPECT_FALSE(connection->isTerminating());
}

TEST(HttpFilter, filter_match_redirect) {
	SFwallCore::Packet *packet = (SFwallCore::Packet *) PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 12332, 80, "blank data");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	connection->getApplicationInfo()->setHttpHost("testing.com");
	packet->setConnection(connection);

	ApplicationFilter *filter = new ApplicationFilter(NULL,NULL, NULL, NULL);
	SFwallCore::WebsiteListAlias *list = new SFwallCore::WebsiteListAlias( {"testing.com"});

	SFwallCore::FilterCriteria *matchingRule = new SFwallCore::FilterCriteria();
        HttpHostname* hc = new HttpHostname(); hc->aliases.push_back(AliasPtr(new SFwallCore::WebsiteListAlias( {"testing.com"})));

	matchingRule->enabled = true;
	matchingRule->redirect = true;
	matchingRule->redirectUrl = "blocked.com";
	matchingRule->criteria.push_back(hc);
	filter->addRule(matchingRule);
	filter->refresh();

	filter->process(connection, packet);
	EXPECT_FALSE(connection->isTerminating());
	EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(HttpFilter, sni_paramater_reject_ssl_backoff_attempt) {
        SFwallCore::Packet *packet= (SFwallCore::Packet *) PacketBuilder::createTcp("10.1.1.1", "31.13.77.65", 12332, 443, "--");
	SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	connection->getApplicationInfo()->setHttpHost("facebook.com");
	packet->setConnection(connection);

	ApplicationFilter *filter = new ApplicationFilter(NULL, NULL, NULL, NULL);
	SFwallCore::FilterCriteria *matchingRule = new SFwallCore::FilterCriteria();
        HttpHostname* hc = new HttpHostname(); hc->aliases.push_back(AliasPtr(new SFwallCore::WebsiteListAlias( {"facebook.com"})));
	matchingRule->enabled = true;
	matchingRule->criteria.push_back(hc);
	filter->addRule(matchingRule);
	filter->refresh();

	filter->process(connection, packet);
	EXPECT_TRUE(connection->isTerminating());

	SFwallCore::Packet *nontlspacket = (SFwallCore::Packet *) PacketBuilder::createTcp("10.1.1.1", "31.13.77.65", 12332, 443, "asds");

	SFwallCore::TcpConnection *nontlsconnection = new SFwallCore::TcpConnection(nontlspacket);
	nontlspacket->setConnection(nontlsconnection);

	filter->process(connection, packet);
	EXPECT_TRUE(connection->isTerminating());
}

