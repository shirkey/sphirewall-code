/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstring>
#include <boost/algorithm/string.hpp>
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Packet.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "Core/Cloud.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "Utils/DnsType.h"
#include <Utils/Dump.h>
#include "SFwallCore/Alias.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"
#include "SFwallCore/ApplicationLevel/CapturePortal.h"

using namespace std;

int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_FORCE_AUTH_WEBSITE_EXCEPTIONS = 1;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_FORCE = 0;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_REQUIRES_HOSTNAME = 0;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION= 0;
int SFwallCore::CapturePortalEngine::REWRITE_PROXY_REQUESTS_ENABLED = 1;
int SFwallCore::CapturePortalEngine::CAPTURE_PORTAL_DMZ_MODE_ENABLED= 0;

SFwallCore::CapturePortalEngine::CapturePortalEngine(SFwallCore::AliasDb *aliases) :
        aliases(aliases) {

        System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_FORCE", &CAPTURE_PORTAL_FORCE);
        System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_REQUIRES_HOSTNAME", &CAPTURE_PORTAL_REQUIRES_HOSTNAME);
        System::getInstance()->config.getRuntime()->loadOrPut("REWRITE_PROXY_REQUESTS_ENABLED", &REWRITE_PROXY_REQUESTS_ENABLED);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_FORCE_AUTH_WEBSITE_EXCEPTIONS", &REWRITE_PROXY_REQUESTS_ENABLED);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION", &CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION);
	System::getInstance()->config.getRuntime()->loadOrPut("CAPTURE_PORTAL_DMZ_MODE_ENABLED", &CAPTURE_PORTAL_DMZ_MODE_ENABLED);

	queries = 0;
	hits = 0;
	}

void SFwallCore::CapturePortalEngine::save() {
	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *websites = new ObjectContainer(CARRAY);
	ObjectContainer *sources = new ObjectContainer(CARRAY);
	ObjectContainer *destExceptions = new ObjectContainer(CARRAY);

	for (AliasPtr ap : forceAuthSources) {
		sources->put(new ObjectWrapper((string) ap->id));
	}

	for (AliasPtr ap : forceAuthWebsiteExceptions) {
		websites->put(new ObjectWrapper((string) ap->id));
	}

	for (AliasPtr ap : forceAuthDestIpExceptions) {
		destExceptions->put(new ObjectWrapper((string) ap->id));
	}

	root->put("websites_exceptions", new ObjectWrapper(websites));
	root->put("included_networks", new ObjectWrapper(sources));
	root->put("network_exceptions", new ObjectWrapper(destExceptions));
	configurationManager->setElement("authentication:ranges", root);
	configurationManager->save();
}

bool SFwallCore::CapturePortalEngine::load() {
	ObjectContainer *root;
	forceAuthWebsiteExceptions.clear();
	forceAuthSources.clear();
	forceAuthDestIpExceptions.clear();

	if (configurationManager->has("authentication:ranges")) {
		root = configurationManager->getElement("authentication:ranges");
		ObjectContainer *websites = root->get("websites_exceptions")->container();

		for (int x = 0; x < websites->size(); x++) {
			string id = websites->get(x)->string();

			if (aliases->get(id)) {
				forceAuthWebsiteExceptions.push_back(aliases->get(id));
			}
		}

		ObjectContainer *sources = root->get("included_networks")->container();
		for (int x = 0; x < sources->size(); x++) {
			string id = sources->get(x)->string();

			if (aliases->get(id)) {
				forceAuthSources.push_back(aliases->get(id));
			}
		}

		if (root->has("network_exceptions")) {
			ObjectContainer *sources = root->get("network_exceptions")->container();

			for (int x = 0; x < sources->size(); x++) {
				string id = sources->get(x)->string();

				if (aliases->get(id)) {
					forceAuthDestIpExceptions.push_back(aliases->get(id));
				}
			}
		}
	}
	return true;
}

#define SOURCE 1
#define DESTINATION 2
#define BOTH 3

bool SFwallCore::CapturePortalEngine::isAddressInAliases(int direction, const list<AliasPtr> &aliases, SFwallCore::ConnectionIp *connectionIp) {
	for (AliasPtr ap : aliases) {
		if (connectionIp->type() == IPV4) {
			ConnectionIpV4 *ipv4 = (ConnectionIpV4 *) connectionIp;
			if ((direction == DESTINATION || direction == BOTH) && ap->searchForNetworkMatch(ipv4->getDstIp())) {
				return true;
			}

			if ((direction == SOURCE || direction == BOTH) && ap->searchForNetworkMatch(ipv4->getSrcIp())) {
				return true;
			}
		}

		if (connectionIp->type() == IPV6) {
			ConnectionIpV6 *ipv6 = (ConnectionIpV6 *) connectionIp;

			if ((direction == DESTINATION || direction == BOTH) && ap->searchForNetworkMatch(ipv6->getDstIp())) {
				return true;
			}

			if ((direction == SOURCE || direction == BOTH) && ap->searchForNetworkMatch(ipv6->getSrcIp())) {
				return true;
			}
		}
	}

	return false;
}

bool SFwallCore::CapturePortalEngine::isWebsiteInAliases(const std::list< AliasPtr > &aliases, const std::string &website) {
	vector<std::string> chunks;
	chunks.push_back(website);

	for (uint x = 0; x < website.size(); x++) {
		if (website[x] == '.') {
			string target = website.substr(x + 1, website.size() - x + 1);
			chunks.push_back(target);
		}
	}

	for (AliasPtr websiteTarget : forceAuthWebsiteExceptions) {
		for (int x = (chunks.size() - 1); x >= 0; x--) {
			if (websiteTarget->search(chunks[x])) {
				return true;
			}
		}
	}

	return false;
}

void SFwallCore::CapturePortalEngine::process(SFwallCore::Connection *conn, SFwallCore::Packet *packet) {
	queries++;
	if (CAPTURE_PORTAL_FORCE == 1) {
		if (isAddressInAliases(SOURCE, forceAuthSources, conn->getIp())) {
			if (isAddressInAliases(DESTINATION, forceAuthDestIpExceptions, conn->getIp())) {
				conn->getApplicationInfo()->ignore_cp = true;
				return;
			}

			if (conn->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
				if (CAPTURE_PORTAL_FORCE_AUTH_WEBSITE_EXCEPTIONS == 1) {
					if (forceAuthWebsiteExceptions.size() > 0) {
						if (conn->getApplicationInfo()->http_hostNameSet) {
							std::string host = conn->getApplicationInfo()->http_hostName;

							if (isWebsiteInAliases(forceAuthWebsiteExceptions, host)) {
								conn->getApplicationInfo()->ignore_cp = true;
								return;
							}
						}else{
							conn->getApplicationInfo()->ignore_cp = true;
							return;

						} 
					}
				}
			}
			hits++;	
			if(conn->getApplicationInfo()->tls || conn->getApplicationInfo()->sslv3 || conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP){
				conn->terminate();
			}else{
				conn->setRewriteRule(new HttpRedirectRewrite(configurationManager->get("authentication:cp_url")->string()));
			}
		}
	}

	conn->getApplicationInfo()->ignore_cp = true;
	return;
}

std::list< SFwallCore::AliasPtr> &SFwallCore::CapturePortalEngine::getForceAuthDestIpExceptions() {
	return forceAuthDestIpExceptions;
}

std::list< SFwallCore::AliasPtr> &SFwallCore::CapturePortalEngine::getForceAuthRanges() {
	return forceAuthSources;
}

std::list< SFwallCore::AliasPtr> &SFwallCore::CapturePortalEngine::getForceAuthWebsiteExceptions() {
	return forceAuthWebsiteExceptions;
}

bool SFwallCore::CapturePortalEngine::matchesIpCriteria(Connection *conn, Packet *packet) {
	if (conn->getApplicationInfo()->ignore_cp) {
		return false;
	}

	if(CAPTURE_PORTAL_DMZ_MODE_ENABLED == 1){
		if(conn->getProtocol() == TCP){
			if(conn->getDestinationPort() == 80 || conn->getDestinationPort() == 8080 || conn->getDestinationPort() == 3128 || conn->getDestinationPort() == 1080 || conn->getDestinationPort() == 443){
				//This is strange, but basically means that we are unsure what the connection is yet, so we need to let it through
				if (conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
					return false;
				}
			}
		}
	}else{
		if (conn->getProtocol() != TCP) {
			return false;
		}

		if (((TcpConnection *) conn)->getState()->state != TCPUP) {
			return false;
		}

		if (conn->getApplicationInfo()->type != SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
			return false;
		}
	}

	if (conn->getSession()) {
		return false;
	}

	return true;
}

bool SFwallCore::CapturePortalEngine::enabled() {
	return CAPTURE_PORTAL_FORCE == 1 && (CAPTURE_PORTAL_REQUIRES_CLOUD_CONNECTION == 0 || System::getInstance()->getCloudConnection()->connected());
}



