#include <iostream>
#include <boost/regex.hpp>

#include "SFwallCore/ApplicationLevel/Signatures.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/Packet.h"

using namespace std;
using namespace SFwallCore;

SignatureStore::SignatureStore(){
}

void SignatureStore::loadStandardSignatures(){
        availableSignatures.push_back(SignaturePtr(new HttpTrafficSignature()));
        availableSignatures.push_back(SignaturePtr(new HttpDirectIpSignature()));
        availableSignatures.push_back(SignaturePtr(new SshSignature()));
        availableSignatures.push_back(SignaturePtr(new BitTorrentSignature()));
        availableSignatures.push_back(SignaturePtr(new SkypeSignature()));
}

void SignatureStore::save(){
	if(configurationManager){
		ObjectContainer* container = new ObjectContainer(CARRAY);
		for(SignaturePtr signature: availableSignatures){
			if(signature->type().compare("signatures.regex") == 0){
				RegexSignature *entry = (RegexSignature*) signature.get();
				ObjectContainer* target = new ObjectContainer(CREL);
				target->put("id", new ObjectWrapper(entry->uniqueId));				
				target->put("name", new ObjectWrapper(entry->userDefinedName));				
				target->put("description", new ObjectWrapper(entry->userDefinedDescription));				
				target->put("expression", new ObjectWrapper(entry->expression));				

				container->put(new ObjectWrapper(target));
			}
		}

		configurationManager->setElement("webfilter:signatures", container);
		configurationManager->save();
	}
}

bool SignatureStore::load(){
	availableSignatures.clear();
	if(configurationManager){

		ObjectContainer *root;
		if (configurationManager->has("webfilter:signatures")) {
			root = configurationManager->getElement("webfilter:signatures");

			for (int x = 0; x < root->size(); x++) {
				RegexSignature *entry = new RegexSignature();
				ObjectContainer *source = root->get(x)->container();

				entry->uniqueId= source->get("id")->string();
				entry->userDefinedName = source->get("name")->string();
				entry->userDefinedDescription= source->get("description")->string();
				entry->expression= source->get("expression")->string();

				entry->refresh();	
				availableSignatures.push_back(SignaturePtr(entry));
			}
		}
	}

	loadStandardSignatures();
}

SignaturePtr SignatureStore::get(string id){
	for(SignaturePtr signature : availableSignatures){
		if(signature->id().compare(id) == 0){
			return signature;
		}
	}

	return SignaturePtr();
}

std::list<SignaturePtr> SignatureStore::available(){
	return availableSignatures;
}

bool HttpTrafficSignature::matches(Connection* connection, Packet* packet){
	return connection->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
}

bool HttpDirectIpSignature::matches(Connection* connection, Packet* packet){
	if(!connection->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP){
		return false;
	}

	static boost::regex ipregex("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");
	boost::smatch what;

	return boost::regex_search(connection->getApplicationInfo()->http_hostName, what, ipregex);
}

bool RegexSignature::matches(Connection* connection, Packet* packet){
	std::string packetData = (const char *) packet->getTransport()->getApplication()->getInternal();
	boost::smatch what;
	return boost::regex_search(packetData, what, r);
}


bool SshSignature::matches(Connection* connection, Packet* packet){
	if(connection->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP){
		return false;
	}

	if(packet->getTransport()->getApplication()->getSize() > 4){
		unsigned char* data = packet->getTransport()->getApplication()->getInternal();
		if(data[0] == 'S' && data[1] == 'S' && data[2] == 'H'){
			return true;
		}		
	}		
	return false;
}

bool BitTorrentSignature::matches(Connection* connection, Packet* packet){
	if(packet->getTransport()->getApplication()->getSize() > 3){
		unsigned char* data = packet->getTransport()->getApplication()->getInternal();
		if(data[0] == '\x13' && data[1] == 'B' && data[2] == 'i'){
			return true;
		}		
	}		

	return false;
}

bool SkypeSignature::matches(Connection* connection, Packet* packet){
	static boost::regex ipregex("^..\\x02.............");
	boost::smatch what;
	std::string packetData = (const char *) packet->getTransport()->getApplication()->getInternal();
	if(boost::regex_search(packetData, what, ipregex)){
		return true;
	}	
	return false;

}

void SignatureStore::add(SignaturePtr signature){
	availableSignatures.push_back(signature);		
}

void SignatureStore::remove(SignaturePtr signature){
	availableSignatures.remove(signature);
}

