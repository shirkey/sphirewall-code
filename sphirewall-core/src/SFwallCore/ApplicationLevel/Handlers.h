/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APP_LEVEL_HANDLERS
#define APP_LEVEL_HANDLERS

#include <vector>
#include <iosfwd>
#include "SFwallCore/Packetfwd.h"
#include "Core/ConfigurationManager.h"
# include <mutex>

#define HTTP_HOST_LEN 5
#define HTTP_CR '\r'
#define HTTP_SPACE ' '
#define HTTP_HEADER_SEP ':'
#define MAX_PORTS 65535

namespace SFwallCore {
	class ApplicationLayerTrackerHandler {
		public:
			virtual ~ApplicationLayerTrackerHandler();
			virtual void handle(SFwallCore::PacketDirection direction, Connection *connection, ApplicationFrame *frame) = 0;
			virtual bool supportsProto(int protocol_number) = 0;
			virtual bool tls(Connection *connection, ApplicationFrame *frame) = 0;
	};

	class ApplicationLayerTracker {
		public:
			ApplicationLayerTracker(ConfigurationManager *cm); 
			void initializeHandlers();
			void addHandler(int port, ApplicationLayerTrackerHandler*);
			void removeHandler(int port);
			void update(Connection *connection, Packet *incomming);
		private:
			ApplicationLayerTrackerHandler * handlers[MAX_PORTS];
			ConfigurationManager *cm;
	};
}

#endif
