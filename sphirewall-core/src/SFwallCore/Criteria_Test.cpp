#include <iostream>
#include <gtest/gtest.h>
#include "SFwallCore/TestFactory.h"
#include "Utils/IP4Addr.h"
#include "Utils/IP6Addr.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Criteria.h"
#include "SFwallCore/Connection.h"
#include "Utils/Utils.h"
#include "test.h"

using namespace std;
using namespace SFwallCore;


TEST(Criteria, source_ip_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

	SourceIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_TRUE(criteria.match(packet));       	
}

TEST(Criteria, source_ip_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");

	SourceIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_FALSE(criteria.match(packet));       	
}

TEST(Criteria, destination_ip_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

	DestinationIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("2.1.33.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_TRUE(criteria.match(packet));       	
}

TEST(Criteria, destination_ip_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");

	DestinationIpCriteria criteria;
	criteria.ip = IP4Addr::stringToIP4Addr("192.168.1.1");
	criteria.mask = IP4Addr::stringToIP4Addr("255.255.255.255");
	EXPECT_FALSE(criteria.match(packet));       	
}

TEST(Criteria, source_ip6_matches) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:b::10", "2443:f0d0:2001:b::2", 10, 20);

	SourceIp6Criteria criteria;
        criteria.cidr = 64;
        IP6Addr::fromString(criteria.addr, "2607:f0d0:2001:b::10");
	EXPECT_TRUE(criteria.match(packet));       	
}

TEST(Criteria, source_ip6_mismatch) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:d::10", "2443:f0d0:2001:b::2", 10, 20);

	SourceIp6Criteria criteria;
        criteria.cidr = 128;
        IP6Addr::fromString(criteria.addr, "2607:f0d0:2001:b::10");
	EXPECT_FALSE(criteria.match(packet));       	
}

TEST(Criteria, destination_ip6_matches) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:b::10", "2443:f0d0:2001:b::2", 10, 20);

	DestinationIp6Criteria criteria;
        criteria.cidr = 64;
        IP6Addr::fromString(criteria.addr, "2443:f0d0:2001:b::2");
	EXPECT_TRUE(criteria.match(packet));       	
}

TEST(Criteria, destination_ip6_mismatch) {
        Packet *packet = PacketBuilder::createTcpV6("2607:f0d0:2001:d::10", "2443:f0d0:2001:b::2", 10, 20);

	DestinationIp6Criteria criteria;
        criteria.cidr = 128;
        IP6Addr::fromString(criteria.addr, "2222:f0d0:2001:b::10");
	EXPECT_FALSE(criteria.match(packet));       	
}

TEST(Criteria, group_matches) {
        MockFactory *mocks = new MockFactory();
	Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	
        SessionDb *sessionDb = mocks->givenSessionDb();
	sessionDb->setConfigurationManager(new ConfigurationManager());
        UserPtr user(new User());
        GroupPtr group(new Group());
	group->setId(10);
        user->addGroup(group);

        System::getInstance()->setSessionDb(sessionDb);
        SessionPtr session = sessionDb->create(user, "aa:bb:cc:dd:ee:ff", IP4Addr::stringToIP4Addr("10.1.1.1"));
	packet->getConnection()->setSession(session);

	GroupCriteria criteria;
	criteria.groups.push_back(group);
	EXPECT_TRUE(criteria.match(packet));       	
}

TEST(Criteria, group_mismatch) {
        MockFactory *mocks = new MockFactory();
	Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.2", "2.1.33.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
	packet->setConnection(connection);
	
        SessionDb *sessionDb = mocks->givenSessionDb();
	sessionDb->setConfigurationManager(new ConfigurationManager());
        UserPtr user(new User());
        GroupPtr group(new Group());
	group->setId(10);
        user->addGroup(group);

        System::getInstance()->setSessionDb(sessionDb);
        SessionPtr session = sessionDb->create(user, "aa:bb:cc:dd:ee:ff", IP4Addr::stringToIP4Addr("10.1.1.1"));
	packet->getConnection()->setSession(session);

	GroupPtr other(new Group());
	other->setId(1);
	GroupCriteria criteria;
	criteria.groups.push_back(other);
	EXPECT_FALSE(criteria.match(packet));       	
}

TEST(Criteria, source_alias_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "2.1.33.1", 12332, 80, "");

        SourceIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, source_alias_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        SourceIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, destination_alias_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");

        DestinationIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, destination_alias_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        DestinationIpAliasCriteria criteria;
	SFwallCore::IpRangeAlias *alias = new SFwallCore::IpRangeAlias( {"10.1.1.1-10.1.1.10", "192.168.1.1-192.168.1.1"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_FALSE(criteria.match(packet));
}


TEST(Criteria, http_host_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "blah.google.com";

        HttpHostname criteria;
	SFwallCore::WebsiteListAlias *alias = new SFwallCore::WebsiteListAlias( {"yahoo.com", "google.com"});
	criteria.aliases.push_back(AliasPtr(alias));

	EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, http_host_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.1", "10.1.1.1", 12332, 80, "");
        SFwallCore::TcpConnection *connection = new SFwallCore::TcpConnection(packet);
        packet->setConnection(connection);
	connection->getApplicationInfo()->http_hostNameSet = true;
	connection->getApplicationInfo()->http_hostName = "fuck.com";

        HttpHostname criteria;
	SFwallCore::WebsiteListAlias *alias = new SFwallCore::WebsiteListAlias( {"yahoo.com", "google.com"});
	criteria.aliases.push_back(AliasPtr(alias));
	
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, destination_port_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        DestinationPortCriteria criteria;
	criteria.ports.insert(80);	
	criteria.ports.insert(443);	
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, destination_port_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        DestinationPortCriteria criteria;
	criteria.ports.insert(81);	
	criteria.ports.insert(443);	
	
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, source_port_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        SourcePortCriteria criteria;
	criteria.ports.insert(12332);	
	criteria.ports.insert(443);	
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, source_port_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        SourcePortCriteria criteria;
	criteria.ports.insert(81);	
	criteria.ports.insert(443);	
	
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, destination_port_range_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        DestinationPortRangeCriteria criteria;
	criteria.startPort = 21;
	criteria.endPort = 81;
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, destination_port_range_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        DestinationPortRangeCriteria criteria;
        criteria.startPort = 21;
        criteria.endPort = 31;
	
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, source_port_range_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

        SourcePortRangeCriteria criteria;
	criteria.startPort = 21;
	criteria.endPort = 81;
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, source_port_range_mismatches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 12332, 80, "");

        SourcePortRangeCriteria criteria;
        criteria.startPort = 21;
        criteria.endPort = 31;
	
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, source_device_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
	SFwallCore::PacketBuilder::setSourceDev(packet, 10);

        SourceNetworkDeviceCriteria criteria;
	criteria.devicePtr = InterfacePtr(new Interface("eth10"));
	criteria.devicePtr->ifid = 10;
	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, source_device_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
        SFwallCore::PacketBuilder::setSourceDev(packet, 10);

        SourceNetworkDeviceCriteria criteria;
        criteria.devicePtr = InterfacePtr(new Interface("eth10"));
        criteria.devicePtr->ifid = 11;

        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, destination_device_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
	SFwallCore::PacketBuilder::setDestDev(packet, 10);

        DestinationNetworkDeviceCriteria criteria;
        criteria.devicePtr = InterfacePtr(new Interface("eth10"));
        criteria.devicePtr->ifid = 10;

        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, destination_device_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");
        SFwallCore::PacketBuilder::setDestDev(packet, 10);

        DestinationNetworkDeviceCriteria criteria;
        criteria.devicePtr = InterfacePtr(new Interface("eth10"));
        criteria.devicePtr->ifid = 11;

        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, protocol_matches) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

        ProtocolCriteria criteria;
	criteria.type = TCP;	
        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, protocol_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

        ProtocolCriteria criteria;
        criteria.type = UDP;
        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, periods_reduction) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("192.168.1.100", "2.1.33.1", 80, 11111, "");

        TimePeriodCriteria criteria;
	TimePeriod* period1 = new TimePeriod(); period1->uuid= "1";
	TimePeriod* period2 = new TimePeriod(); period2->uuid = "2";
	TimePeriod* period3 = new TimePeriod(); period3->uuid = "3";
	TimePeriod* period4 = new TimePeriod(); period4->uuid = "4";
	
	period1->mon = true; period1->startTime = 1; period1->endTime = 23;
	period2->tue = true; period2->startTime = 1; period2->endTime = 23;
	period3->fri = true; period3->startTime = 1; period3->endTime = 23;
	period4->mon = true; period4->startTime = 22; period4->endTime = 23;

	criteria.periods.push_back(TimePeriodPtr(period1));
	criteria.periods.push_back(TimePeriodPtr(period2));
	criteria.periods.push_back(TimePeriodPtr(period3));
	criteria.periods.push_back(TimePeriodPtr(period4));

	criteria.refresh();	
	EXPECT_TRUE(criteria.optimisedPeriods.size() == 2);
	
	TimePeriodPtr newPeriod1 = (*criteria.optimisedPeriods.begin());
	EXPECT_TRUE(newPeriod1->startTime == 1);
	EXPECT_TRUE(newPeriod1->endTime == 23);
	EXPECT_TRUE(newPeriod1->mon);
	EXPECT_TRUE(newPeriod1->tue);
	EXPECT_TRUE(newPeriod1->fri);

	TimePeriodPtr newPeriod2 = (*(++criteria.optimisedPeriods.begin()));
	EXPECT_TRUE(newPeriod2->startTime == 22);
	EXPECT_TRUE(newPeriod2->endTime == 23);
	EXPECT_TRUE(newPeriod2->mon);

	//And check that the existing list was not fucked with:
	EXPECT_TRUE(criteria.periods.size() == 4);
}

TEST(Criteria, geoip_source_match) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("14.128.4.3", "2.1.33.1", 80, 11111, "");

        GeoIPSourceCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, geoip_source_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 80, 11111, "");

        GeoIPSourceCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, geoip_destination_match) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "14.128.4.3", 80, 11111, "");

        GeoIPDestinationCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

        EXPECT_TRUE(criteria.match(packet));
}

TEST(Criteria, geoip_destination_mismatch) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 80, 11111, "");

        GeoIPDestinationCriteria criteria;
	criteria.country = "New Zealand";
	criteria.refresh();

        EXPECT_FALSE(criteria.match(packet));
}

TEST(Criteria, geoip_invalid_country) {
        SFwallCore::Packet *packet = (SFwallCore::Packet *) SFwallCore::PacketBuilder::createTcp("10.1.1.1", "2.1.33.1", 80, 11111, "");

        GeoIPDestinationCriteria criteria;
	criteria.country = "Michael Land";
	criteria.refresh();

        EXPECT_FALSE(criteria.match(packet));
}

