#ifndef NAT_ACLS_H
#define NAT_ACLS_H

#define UNSET (unsigned int) -1

#include "SFwallCore/Criteria.h"
#include <boost/unordered_set.hpp>
namespace SFwallCore {
	class Packet;

	class PortForwardingRule {
		public:
			PortForwardingRule() :
				forwardingDestination(UNSET),
				forwardingDestinationPort(UNSET),
				enabled(false) {}

			std::string id;
			std::list<Criteria*> criteria;

			unsigned int forwardingDestination;
			unsigned int forwardingDestinationPort;
			bool enabled;

			bool match(SFwallCore::Packet *packet);
	};

	class MasqueradeRule {
		public:
			MasqueradeRule() :
				enabled(false),
				natTargetIp(UNSET)
			{}

			in_addr_t getUsableNatTargetIp() {
				if (natTargetDevicePtr) {
					return natTargetDevicePtr->get_primary_ipv4_address();
				}
				else if (natTargetIp) {
					return natTargetIp;
				}

				return -1;
			}
	
			std::list<Criteria*> criteria;
	
			std::string natTargetDevice;
			InterfacePtr destinationDevicePtr;
			InterfacePtr natTargetDevicePtr;
			unsigned int natTargetIp;
			std::string id;
			bool enabled;

			bool match(SFwallCore::Packet *packet);
	};

	typedef boost::shared_ptr<PortForwardingRule> PortForwardingRulePtr;
	typedef boost::shared_ptr<MasqueradeRule> MasqueradeRulePtr;

	enum {
		DISABLED = -1, SINGLE=0, AUTOWAN_MODE_LOADBALANCING = 1, FAILOVER = 2
	};

	class AutowanInterface {
		public:
			int failover_index;
			std::string interface;

			InterfacePtr resolved_interface;
			std::list<Criteria*> criteria;
	};	
	typedef boost::shared_ptr<AutowanInterface> AutowanInterfacePtr;	

	class NatAclStore : public Configurable, public Lockable {
		public:
			class InterfaceChangeListener : public virtual IntMgrChangeListener {
				public:
					InterfaceChangeListener(NatAclStore *store) :
						store(store) {}

					void interface_change() {
						store->initRules();
					}
				private:
					NatAclStore *store;
			};


			NatAclStore(IntMgr *interfaceManager):
				interfaceManager(interfaceManager) {

				interfaceManager->register_change_listener(new InterfaceChangeListener(this));
			}
			NatAclStore(){}

			void addMasqueradeRule(MasqueradeRulePtr rule);
			void delMasqueradeRule(MasqueradeRulePtr rule);
			std::list<MasqueradeRulePtr> listMasqueradeRules();
			MasqueradeRulePtr getMasqueradeRule(std::string id);

			void addForwardingRule(PortForwardingRulePtr rule);
			void delPortForwardingRule(PortForwardingRulePtr rule);
			std::list<PortForwardingRulePtr> listPortForwardingRules();
			PortForwardingRulePtr getPortForwardingRule(std::string id);

			bool load();
			void save();

			MasqueradeRulePtr matchMasqueradeRule(SFwallCore::Packet *packet);
			PortForwardingRulePtr matchForwardingRule(SFwallCore::Packet *packet);

			void initRules();

			//Autowan code
			int get_autowan_mode();
			void set_autowan_mode(int mode);
			int determine_routing_fwmark(SFwallCore::Packet* incomming_packet);
			bool determine_autowan_matches(SFwallCore::Packet* incomming_packet);
			std::vector<AutowanInterfacePtr>& get_autowan_rules();
			
			const char* getConfigurationSystemName(){
				return "Forwarding and Filtering system";
			}

		private:
			std::list<MasqueradeRulePtr> masqueradeRules;
			std::list<PortForwardingRulePtr> portFowardingRules;

			vector<AutowanInterfacePtr> autowan_rules;
			std::set<InterfacePtr> autowan_active_interfaces;
			int autowan_load_balancing_device_pool_last;
			int autowan_mode;

			IntMgr* interfaceManager;
	};
};

#endif
