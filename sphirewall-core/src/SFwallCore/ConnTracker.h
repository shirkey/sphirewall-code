/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_TSHASHSET_H_INCLUDED
#define SPHIREWALL_TSHASHSET_H_INCLUDED

#include <list>
#include <vector>

#include "SFwallCore/Connectionfwd.h"
#include "Packetfwd.h"
#include "Acl.h"
#include "Utils/Lock.h"
#include "Auth/User.h"
#include "TrafficShaper/TokenBucket.h"
#include "Utils/Utils.h"
#include "Core/SysMonitor.h"
#include "ApplicationLevel/ApplicationFilter.h"
#include "ApplicationLevel/Handlers.h"
#include "Firewall.h"
#include "SFwallCore/ConnTrackerDeleteHook.h"

#define MAX_HANDLERS 256
namespace SFwallCore {

	class ConnectionCriteria {
		public:

			ConnectionCriteria() {
				this->dest = -1;
				this->destPort = -1;
				this->source = -1;
				this->sourcePort = -1;
				this->type = -1;
			}

			void setSession(SessionPtr s) {
				session = s;
			}

			void setSource(unsigned int source) {
				this->source = source;
			}

			void setDest(unsigned int dest) {
				this->dest = dest;
			}

			void setSourcePort(int sourcePort) {
				this->sourcePort = sourcePort;
			}

			void setDestPort(int destPort) {
				this->destPort = destPort;
			}

			void setProtocol(proto type) {
				this->type = type;
			}

			bool match(Connection *connection);

		private:
			unsigned int source, dest;
			int sourcePort, destPort;
			int type;

			SessionPtr session;
	};

	class TrackerMetrics {
		public:
			double firewall_conntracker_tcp_synsynack;
			double firewall_conntracker_tcp_synackack;
			double firewall_conntracker_tcp_tcpup;
			double firewall_conntracker_tcp_finwait;
			double firewall_conntracker_tcp_closewait;
			double firewall_conntracker_tcp_lastack;
			double firewall_conntracker_tcp_timewait;
			double firewall_conntracker_tcp_finfinack;
			double firewall_conntracker_tcp_closed;
			double firewall_conntracker_tcp_reset;

			double firewall_conntracker_tcp_size;
			double firewall_conntracker_tcp_valid;
			double firewall_conntracker_udp_size;
			double firewall_conntracker_udp_valid;
			double firewall_conntracker_icmp_size;
			double firewall_conntracker_icmp_valid;

			TrackerMetrics() {
				firewall_conntracker_tcp_synsynack = 0;
				firewall_conntracker_tcp_synackack = 0;
				firewall_conntracker_tcp_tcpup = 0;
				firewall_conntracker_tcp_finwait = 0;
				firewall_conntracker_tcp_closewait = 0;
				firewall_conntracker_tcp_lastack = 0;
				firewall_conntracker_tcp_timewait = 0;
				firewall_conntracker_tcp_finfinack = 0;
				firewall_conntracker_tcp_closed = 0;
				firewall_conntracker_tcp_reset = 0;

				firewall_conntracker_tcp_size = 0;
				firewall_conntracker_tcp_valid = 0;
				firewall_conntracker_udp_size = 0;
				firewall_conntracker_udp_valid = 0;
				firewall_conntracker_icmp_size = 0;
				firewall_conntracker_icmp_valid = 0;
			}

	};

	class ProtocolHandler {
		public:
			ProtocolHandler(Firewall *firewall) :
				firewall(firewall) {

					numberConnections = 0;
					hashCollisions = 0;
					hashMatchWithoutCollision = 0;
					newConnections = 0;
					inlineRemovedConnections = 0;
					gbRemovedConnections = 0;
					searchRequests = 0;
					successSearchRequests = 0;
					failedSearchRequests = 0;
				}

			virtual ~ProtocolHandler() {}
			Connection *create(Packet *packet);

			//Virtual Overloaded Methods:
			virtual Connection *offer(Packet *packet) = 0;
			virtual int cleanup() = 0;
			virtual Connection *findConn(Packet *packet, int hash) = 0;
			virtual void sample(map<string, double> &items) = 0;

			void add(Connection *conn);
			int size() {
				return numberConnections;
			}
			std::vector<Connection *> listConnections();
			std::list<Connection *> listConnections(ConnectionCriteria criteria);

			std::list<Connection *> conn_map[Firewall::PLAIN_CONN_HASH_MAX];
			void erase(Connection *conn);

			void deleteHook(Connection *connection);
			void registerDeleteHook(DeleteConnectionHook *hook);

			double hashCollisions;
			double hashMatchWithoutCollision;
			double newConnections;
			double inlineRemovedConnections;
			double gbRemovedConnections;
			double searchRequests;
			double successSearchRequests;
			double failedSearchRequests;

			void setApplicationLayerTracker(ApplicationLayerTracker *applicationLayerTracker){
				this->applicationLayerTracker = applicationLayerTracker;
			}
		protected:
			Firewall *firewall;
			ApplicationLayerTracker *applicationLayerTracker;

			vector<DeleteConnectionHook *> deleteHooks;
			int numberConnections;
	};

	class ProtocolHandlerV4 : public ProtocolHandler {
		public:
			ProtocolHandlerV4(Firewall *firewall) :
				ProtocolHandler(firewall) {
				}
			virtual ~ProtocolHandlerV4() {}

			virtual Connection *offer(Packet *packet);
			virtual int cleanup();
			virtual Connection *findConn(Packet *packet, int hash);
			virtual void sample(map<string, double> &items) = 0;
	};

	class TcpProtocolHandler : public ProtocolHandlerV4 {
		public:
			TcpProtocolHandler(Firewall *firewall) :
				ProtocolHandlerV4(firewall) {
				}

			~TcpProtocolHandler() {}
			Connection *offer(Packet *packet);
			int cleanup();
			void sample(map<string, double> &items);
			TrackerMetrics *metrics();
	};

	class UdpProtocolHandler : public ProtocolHandlerV4 {
		public:
			UdpProtocolHandler(Firewall *firewall) :
				ProtocolHandlerV4(firewall) {}

			~UdpProtocolHandler() {}
			void sample(map<string, double> &items);
	};


	class IcmpProtocolHandler : public ProtocolHandlerV4 {
		public:
			IcmpProtocolHandler(Firewall *firewall) :
				ProtocolHandlerV4(firewall) {}

			~IcmpProtocolHandler() {};
			void sample(map<string, double> &items);
	};

	class PlainConnTracker : public MetricSampler, public Lockable {
		public:
			PlainConnTracker(Firewall *firewall)
				: firewall(firewall) {
					for (int x = 0; x < MAX_HANDLERS; x++) {
						handlers[x] = NULL;
					}

					handlers[TCP] = new TcpProtocolHandler(firewall);
					handlers[UDP] = new UdpProtocolHandler(firewall);
					handlers[ICMP] = new IcmpProtocolHandler(firewall);
				}

			~PlainConnTracker();

			void sample(map<string, double> &items);
			void deleteTokenBucket(TokenBucket *);

			Connection *create(Packet *packet);
			Connection *offer(Packet *packet);
			void add(Connection *conn);
			int size();
			std::vector<Connection *> listConnections();
			std::list<Connection *> listConnections(ConnectionCriteria criteria);
			int cleanup();

			Sampler *cacheIterations;

			void terminate(Connection *connection);
			void registerDeleteHook(DeleteConnectionHook *hook) {
				for (int x = 0; x < MAX_HANDLERS; x++) {
					if (handlers[x]) {
						handlers[x]->registerDeleteHook(hook);
					}
				}
			}

			void setApplicationLayerTracker(ApplicationLayerTracker *applicationLayerTracker){
				for (int x = 0; x < MAX_HANDLERS; x++) {
					if (handlers[x]) {
						handlers[x]->setApplicationLayerTracker(applicationLayerTracker);
					}
				}
			}

		protected:
			Connection *findConn(Packet *packet, int hash);

		private:
			void erase(Connection *conn);

			Firewall *firewall;
			ProtocolHandler *handlers[MAX_HANDLERS];
	};
}

#endif
