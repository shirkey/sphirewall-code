/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ALIAS_H_INCLUDED
#define ALIAS_H_INCLUDED

#include <vector>
#include <set>
#include <string>
#include <map>
#include <unordered_set>
#include <boost/unordered_map.hpp>

#include "Core/Logger.h"
#include "Core/ConfigurationManager.h"
#include "Utils/Lock.h"
#include "Utils/IP4Addr.h"

using std::unordered_set;
class ConfigurationManager;

namespace SFwallCore {
	class ACLStore;
	class Alias;
	typedef std::shared_ptr<Alias> AliasPtr;

	enum AliasListSourceType {
		DNS = 0,
		HTTP_FILE = 1
	};

	enum AliasType {
		IP_RANGE = 0,
		IP_SUBNET = 1,
		WEBSITE_LIST = 2,
		STRING_WILDCARD_LIST = 3
	};

	class ListSource {
		public:
			std::string detail;
			virtual int load(Alias *) = 0;
			virtual AliasListSourceType type() = 0;
			virtual std::string description() = 0;
	};

	class DnsListSource : public ListSource {
		public:
			int load(Alias *);

			AliasListSourceType type() {
				return DNS;
			}

			std::string description() {
				return "Dns Hostname Source: " + detail;
			}

	};

	class HttpFileSource : public ListSource {
		public:
			int load(Alias *);
			AliasListSourceType type() {
				return HTTP_FILE;
			}

			std::string description() {
				return "Http File: " + detail;
			}
	};

	class Range {
		public:
			Range() {
				start = -1;
				end = -1;
			}
			Range(std::string value);
			static bool valid(std::string value);

			unsigned int start;
			unsigned int end;
	};

	class RangeV6 {
		public:
			RangeV6() {
				start = NULL;
				end = NULL;
				cidr = -1;
			}

			RangeV6(std::string value);
			static bool valid(std::string value);

			struct in6_addr *start;
			struct in6_addr *end;
			int cidr;
	};


	struct IpRangeOperator : public std::binary_function<Range, Range, bool> {
		bool operator()(Range const &n1, Range const &n2) const;
	};

	struct IpSubnetOperator : public std::binary_function<Range, Range, bool> {
		bool operator()(Range const &n1, Range const &n2) const;
	};

	class Alias {
		public:
			std::string id;
			std::string name;
			ListSource *source;

			Alias() {
				lock = new Lock();
				source = NULL;
			}

			virtual ~Alias() {
				delete lock;
			}

			virtual bool searchForNetworkMatch(unsigned int ip) {
				return false;
			}
			virtual bool searchForNetworkMatch(struct in6_addr *ip) {
				return false;
			}

			virtual bool search(const std::string& s) {
				return false;
			}

			virtual void addEntry(const std::string& entry) = 0;
			virtual std::list<std::string> listEntries() = 0;
			virtual void removeEntry(const std::string& entry) = 0;
			virtual AliasType type() const = 0;

			//Cache helper methods
                        virtual int cacheSize(){
				return 0;
			}
                        virtual void flushCache(){

			}

			int load() {
				if (source != NULL) {
					clear();
					return source->load(this);
				}

				return 0;
			}
			virtual const std::string description() const = 0;
			void clear();

		protected:
			Lock *lock;
	};

	class IpRangeAlias : public Alias {
		public:
			IpRangeAlias() {};
			IpRangeAlias(std::list<string> items) {
				for (auto item : items) {
					addEntry(item);
				}
			}
			~IpRangeAlias() {}

			bool searchForNetworkMatch(unsigned int ip);
			bool searchForNetworkMatch(struct in6_addr *ip);
			void addEntry(const std::string& entry);
			std::list<std::string> listEntries();
			void removeEntry(const std::string& entry);

			AliasType type() const {
				return IP_RANGE;
			}
			const std::string description() const {
				return "Ip range alias";
			}

                        int cacheSize(){
                                return cache.size();
                        }

                        void flushCache(){
				cache.clear();
			}
		private:
			std::multiset<Range, IpRangeOperator> items;
			unordered_set<unsigned int> cache;
	};

	class IpSubnetAlias : public Alias {
		public:
			~IpSubnetAlias() {}
			IpSubnetAlias() {};
			IpSubnetAlias(std::list<string> items) {
				for (auto item : items) {
					addEntry(item);
				}
			}
			bool searchForNetworkMatch(unsigned int ip);
			bool searchForNetworkMatch(struct in6_addr *ip);

			void addEntry(const std::string& entry);
			std::list<std::string> listEntries();
			void removeEntry(const std::string& entry);

			AliasType type() const {
				return IP_SUBNET;
			}
			const std::string description() const {
				return "Ip subnet alias";
			}

		private:
			std::multiset<Range, IpSubnetOperator> items;
			std::list<RangeV6> itemsV6;
	};

	class WebsiteListAlias : public Alias {
		public:
			WebsiteListAlias() {};
			WebsiteListAlias(std::list<string> items): items(items.begin(), items.end()) {};
			~WebsiteListAlias() {}
			AliasType type() const {
				return WEBSITE_LIST;
			}

			const std::string description() const {
				return "Custom website list";
			}

			bool search(const std::string& term);
			void addEntry(const std::string& entry);
			std::list<std::string> listEntries();
			void removeEntry(const std::string& entry);

			int cacheSize(){
				return cache.size();
			}

			void flushCache(){
				cache.clear();
			}
		private:
			unordered_set<std::string> items;
			boost::unordered_map<std::string, bool> cache;
	};

	class StringWildcardListAlias : public Alias {
		public:
			StringWildcardListAlias() {}
			StringWildcardListAlias(std::list<string> items) : items(items.begin(), items.end()) {}
			~StringWildcardListAlias() {}
			AliasType type() const {
				return STRING_WILDCARD_LIST;
			}

			bool search(const std::string& s);
			void addEntry(const std::string& entry);
			void removeEntry(const std::string& entry);

			std::list<std::string> listEntries();

			const std::string description() const {
				return "Custom Wildcard List";
			}
		private:
			unordered_set<std::string> items;
	};

	class AliasRemovedListener {
		public:
			virtual void aliasRemoved(AliasPtr alias) = 0;
	};


	class AliasDb : public Configurable {
		public:
			AliasDb();
			AliasPtr get(std::string);
			AliasPtr getByName(string id);
			int create(AliasPtr alias);
			int del(AliasPtr target);
			bool load();
			void save();
			void registerRemovedListeners(AliasRemovedListener *target);
			void setAclStore(ACLStore *store);
			void setResolve(bool r);

			std::map<std::string, AliasPtr> aliases;
			const char* getConfigurationSystemName(){
				return "Alias store";
			}

			void cleanUpCaches();
			int cacheSize();
		private:
			ACLStore *store;
			bool resolve;

			std::list<AliasRemovedListener *> removedListeners;
	};
}

#endif
