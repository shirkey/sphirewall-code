#include <gtest/gtest.h>
#include <iostream>
#include <time.h>

using namespace std;

#include "Utils/TimeWrapper.h"
#include "SFwallCore/TimePeriods.h"

TEST(TimePeriod, match) {
	Time *t = new Time("2013-12-13 07", "%Y-%m-%d %H");

	TimePeriod period;
	period.fri = true;
	period.startTime = 600;
	period.endTime = 900;

	EXPECT_TRUE(period.matches(t->timestamp()));
}

TEST(TimePeriod, nonMatch) {
	Time *t = new Time("2013-12-13 07", "%Y-%m-%d %H");

	TimePeriod period;
	period.mon = true;
	period.startTime = 600;
	period.endTime = 900;

	EXPECT_FALSE(period.matches(t->timestamp()));
}

TEST(TimePeriod, nonMatch_outsideTimePeriod) {
	Time *t = new Time("2013-12-13 07", "%Y-%m-%d %H");

	TimePeriod period;
	period.fri = true;
	period.startTime = 1000;
	period.endTime = 1100;

	EXPECT_FALSE(period.matches(t->timestamp()));
}

TEST(TimePeriod, check_listOfPeriods) {
	Time *t = new Time("2013-12-13 07", "%Y-%m-%d %H");

	TimePeriod* invalidPeriod = new TimePeriod();
	invalidPeriod->fri = true;
	invalidPeriod->startTime = 1000;
	invalidPeriod->endTime = 1100;

	TimePeriod* period1 = new TimePeriod();
	period1->fri = true;
	period1->startTime = 600;
	period1->endTime = 800;

	list<TimePeriodPtr> periods;
	periods.push_back(TimePeriodPtr(invalidPeriod));
	periods.push_back(TimePeriodPtr(period1));

	EXPECT_TRUE(TimePeriodStore::checkMatch(periods, t->timestamp()));
}

TEST(TimePeriod, check_listOfPeriods_noMatch) {
	Time *t = new Time("2013-12-12 07", "%Y-%m-%d %H");

        TimePeriod* invalidPeriod = new TimePeriod();
	invalidPeriod->fri = true;
	invalidPeriod->startTime = 1000;
	invalidPeriod->endTime = 1100;

        TimePeriod* period1 = new TimePeriod();
	period1->fri = true;
	period1->startTime = 600;
	period1->endTime = 800;

	list<TimePeriodPtr> periods;
	periods.push_back(TimePeriodPtr(invalidPeriod));
	periods.push_back(TimePeriodPtr(period1));

	EXPECT_FALSE(TimePeriodStore::checkMatch(periods, t->timestamp()));
}



