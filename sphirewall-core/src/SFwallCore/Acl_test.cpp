/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iosfwd>
#include <gtest/gtest.h>
#include "SFwallCore/TestFactory.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Acl.h"
#include "SFwallCore/Criteria.h"
#include "Core/System.h"
#include "Utils/IP6Addr.h"
#include "Auth/User.h"

using namespace SFwallCore;


TEST(AclStore_Filter, criteria_reject) {
	ACLStore *store = new ACLStore();
	FilterRule *filter = new FilterRule();
	filter->enabled = true;

	ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = TCP;
	filter->criteria.push_back(pc);
		
	store->listFilterRules().push_back(filter);

	Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet) == NULL);
}

TEST(AclStore_Filter, accept_no_criteria) {
	ACLStore *store = new ACLStore();
	FilterRule *filter = new FilterRule();
	filter->enabled = true;
	store->listFilterRules().push_back(filter);

	Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet) != NULL);
}

TEST(AclStore_Filter, accept_criteria_matches) {
	ACLStore *store = new ACLStore();
	FilterRule *filter = new FilterRule();
	filter->enabled = true;
	store->listFilterRules().push_back(filter);

        ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = ICMP;
        filter->criteria.push_back(pc);

	Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet) != NULL);
}

TEST(AclStore_Filter, reject_one_criteria_not_matched) {
	ACLStore *store = new ACLStore();
	FilterRule *filter = new FilterRule();
	filter->enabled = true;
	store->listFilterRules().push_back(filter);

        ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = ICMP;
        filter->criteria.push_back(pc);

        SourceIpCriteria* sc = new SourceIpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("192.168.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
        filter->criteria.push_back(sc);

	Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet) == NULL);
}

TEST(AclStore_Filter, accept_all_criteria_matched) {
	ACLStore *store = new ACLStore();
	FilterRule *filter = new FilterRule();
	filter->enabled = true;
	store->listFilterRules().push_back(filter);

        ProtocolCriteria* pc = new ProtocolCriteria(); pc->type = ICMP;
        filter->criteria.push_back(pc);

        SourceIpCriteria* sc = new SourceIpCriteria(); sc->ip = IP4Addr::stringToIP4Addr("10.1.1.1"); sc->mask = IP4Addr::stringToIP4Addr("255.255.255.0");
        filter->criteria.push_back(sc);

	Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet) != NULL);
}

TEST(AclStore_Filter, enabled_disabled) {
	ACLStore *store = new ACLStore();
	FilterRule *filter = new FilterRule();
	filter->enabled = true;
	store->listFilterRules().push_back(filter);

	Packet *packet = PacketBuilder::createIcmp("10.1.1.1", "10.1.1.2", 10);
	EXPECT_TRUE(store->matchFilterAcls(packet) != NULL);
	
	filter->enabled = false;
	EXPECT_TRUE(store->matchFilterAcls(packet) == NULL);
}


