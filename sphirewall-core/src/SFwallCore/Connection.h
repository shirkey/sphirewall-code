/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPHIREWALL_CONNECTION_H
#define SPHIREWALL_CONNECTION_H

#include <netinet/in.h>
#include "SFwallCore/Connectionfwd.h"
#include "SFwallCore/Packetfwd.h"
#include "Utils/Interfaces.h"
#include "Core/Host.h"
#include <Auth/Session.h>
#include <boost/unordered_map.hpp>

namespace SFwallCore {

	using namespace std;
	class ConnectionApplicationInfo {
		public:
			enum Classifier {
				NDEF=0, HTTP=1, DNS=2, UNKNOWN=4
			} type;

			ConnectionApplicationInfo();
			std::string http_get(int direction, int type);
			bool http_contains(int direction, int type);
			void http_set(int direction, std::string key, std::string value);
			void http_clear(int direction);

			void setHttpHost(std::string hostname);
			bool tls;
			bool sslv3;
			bool httpHeadersParsed;
			bool httpHeadersResponseParsed;
			string dnsQueryName;
			bool dnsQueryNameSet;

			bool afVerdictApplied;
			bool afProcessedRequest;
			bool afProcessedResponse;

			bool ignore_cp;
			bool ignore_all_filtering;

			std::string http_hostName;
			std::string http_useragent;
			std::string http_contenttype;
			std::string http_request;

			//isset
			bool http_hostNameSet;
			bool http_useragentSet;
			bool http_contenttypeSet;
			bool http_requestSet;
	};

	class ConnectionIp {
		public:
			virtual ~ConnectionIp() {}
			virtual bool checkPacket(Packet *packet, PacketDirection filter) = 0;
			virtual int hash() = 0;
			virtual int type() const = 0;

			virtual std::string getSrcIpString() = 0;
	};

	class ConnectionIpV4 : public ConnectionIp {
		public:
			ConnectionIpV4(unsigned int sourceIp, unsigned int destIp);
			~ConnectionIpV4();

			int hash();
			int type() const;
			in_addr_t getSrcIp() const;
			in_addr_t getDstIp() const;
			std::string getSrcIpString();
			bool checkPacket(Packet *packet, PacketDirection filter);
		private:
			unsigned int m_destIp;
			unsigned int m_sourceIp;
	};

	class ConnectionIpV6 : public ConnectionIp {
		public:
			ConnectionIpV6(struct in6_addr *source, struct in6_addr *dest);
			~ConnectionIpV6();

			int hash();
			int type() const;
			struct in6_addr *getSrcIp();
			struct in6_addr *getDstIp();
			std::string getSrcIpString();
			bool checkPacket(Packet *packet, PacketDirection filter);
		private:
			struct in6_addr source;
			struct in6_addr destination;
	};

	class Connection {
		public:
			Connection();
			Connection(Packet *);
			virtual ~Connection();
			virtual std::string toString() = 0;

			int hash() const;
			static Connection *parseConnection(Packet *packet);
			bool checkPacket(Packet *packet, PacketDirection filter) ;
			//bool checkPacketIp(Packet* p, PacketDirection filter);
			void terminate();
			//void terminateNext();
			static int hash(Packet *tcpPacket);

			void increment(Packet *packet);
			void setQosBucket(TokenBucketPtr bucket);
			void setSession(SessionPtr session);
			void setMustLog(bool i);
			void setRewriteRule(RewriteRule *rewriteRule);
			void setRewriteVerdictApplied();

			int getTime() const;
			int getDestDev() const;
			int getSourceDev() const;
			int getSourcePort() const;
			int getDestinationPort() const;
			int idle() const;
			int getNoPackets() const;
			//     int getUpTransfer() const;
			//     int getDownTransfer() const;
			bool mustLog();
			bool hasExpired();
			bool isTerminating();
			bool isRewriteVerdictApplied();
			double getDownload() const;
			double getUpload() const;
			int getProtocol() const;
			string getHwAddress() const;
			SessionPtr getSession() const;
			InterfacePtr getDestNetDevice();
			InterfacePtr getSourceNetDevice();
			RewriteRule *getRewriteRule() const;
			TokenBucketPtr getQosBucket() const;
			ConnectionIp *getIp();
			virtual ConnectionApplicationInfo *getApplicationInfo();
			void setHostDiscoveryServiceEntry(HostPtr host);
			HostPtr getHostDiscoveryServiceEntry();

			bool capturePortal;
			int nice;

			virtual void resizePayload(PacketDirection, int diff) {};
		protected:
			int pleaseTerminate;
			int noPackets;
			int sourceDev;
			int destDev;
			int m_sport;
			int m_dport;
			int m_hash;
			int m_time_stamp;
			int m_icmpId;
			long m_last_packet;
			double upload;
			double download;
			bool log;
			bool rewriteVerdictApplied;
			void updateTransferRate(SFwallCore::Packet *);
			int protocol;
			HostPtr hostptr;
			string hwAddress;
			SessionPtr sessionPtr;
			RewriteRule *rewriteRule;
			TokenBucketPtr qos_bucket;
			ConnectionIp *ip;
			ConnectionApplicationInfo *applicationInfo;
	};

	class TcpConnection : public Connection {
		public:
			TcpConnection(Packet *tcpPacket);
			~TcpConnection();
			TcpTrackable *getTcpTrackable() const;
			TcpState *getState();
			std::string toString();
			TcpTrackable *tcpTrackable;

			void resizePayload(PacketDirection, int diff);
			bool adjustSeqAckWindow(SFwallCore::Packet *);
		private:
			int seqack_recv_offset;
			int seqack_send_offset;

	};


	class UdpConnection : public Connection {
		public:
			UdpConnection(Packet *udpPacket);
			~UdpConnection();
			std::string toString();
			void resizePayload(PacketDirection, int diff) {}
	};


	class IcmpConnection : public Connection {
		public:
			IcmpConnection(Packet *icmpPacket);
			~IcmpConnection();
			std::string toString();
			void resizePayload(PacketDirection, int diff) {}
	};
};

#endif
