#include <iostream>
#include <algorithm>

using namespace std;

#include "SFwallCore/NatAcls.h"
#include "Utils/StringUtils.h"
#include "Core/ConfigurationManager.h"
#include "SFwallCore/Packet.h"

void SFwallCore::NatAclStore::addMasqueradeRule(MasqueradeRulePtr rule) {
	holdLock();
	rule->id = StringUtils::genRandom();
	masqueradeRules.push_back(rule);
	releaseLock();
}

void SFwallCore::NatAclStore::delMasqueradeRule(MasqueradeRulePtr rule) {
	holdLock();
	masqueradeRules.remove(rule);
	releaseLock();
}

std::list<SFwallCore::MasqueradeRulePtr>  SFwallCore::NatAclStore::listMasqueradeRules() {
	return masqueradeRules;
}

void  SFwallCore::NatAclStore::addForwardingRule(PortForwardingRulePtr rule) {
	holdLock();
	rule->id = StringUtils::genRandom();
	portFowardingRules.push_back(rule);
	releaseLock();
}

void  SFwallCore::NatAclStore::delPortForwardingRule(PortForwardingRulePtr rule) {
	holdLock();
	portFowardingRules.remove(rule);
	releaseLock();
}

std::list<SFwallCore::PortForwardingRulePtr>  SFwallCore::NatAclStore::listPortForwardingRules() {
	return portFowardingRules;
}

bool SFwallCore::NatAclStore::load() {
	holdLock();

	portFowardingRules.clear();
	masqueradeRules.clear();
	autowan_mode = -1;
	autowan_active_interfaces.clear();
	autowan_rules.clear();

	ObjectContainer *root;
	if (configurationManager->has("forwarding")) {
		root = configurationManager->getElement("forwarding");
		ObjectContainer *rules = root->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			ObjectContainer *source = rules->get(x)->container();

			PortForwardingRulePtr rule(new PortForwardingRule());
			rule->id = source->get("id")->string();
			rule->forwardingDestination = source->get("forwardingDestination")->number();

			if(source->has("criteria")){
				ObjectContainer* criteria = source->get("criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					rule->criteria.push_back(CriteriaBuilder::parse(individualCriteria->get("type")->string(), individualCriteria->get("conditions")->container()));
				}
			}

			if (source->has("forwardingDestinationPort")) {
				rule->forwardingDestinationPort = source->get("forwardingDestinationPort")->number();
			}

			rule->enabled = source->get("enabled")->boolean();
			portFowardingRules.push_back(rule);
		}
	}

	ObjectContainer *masq;
	if (configurationManager->has("masquerading")) {
		masq = configurationManager->getElement("masquerading");
		ObjectContainer *rules = masq->get("rules")->container();

		for (int x = 0; x < rules->size(); x++) {
			ObjectContainer *source = rules->get(x)->container();

			MasqueradeRulePtr rule(new MasqueradeRule());
			rule->id = source->get("id")->string();
			rule->enabled = source->get("enabled")->boolean();

			if(source->has("criteria")){
				ObjectContainer* criteria = source->get("criteria")->container();
				for(int y = 0; y < criteria->size(); y++){
					ObjectContainer* individualCriteria = criteria->get(y)->container();
					rule->criteria.push_back(CriteriaBuilder::parse(individualCriteria->get("type")->string(), individualCriteria->get("conditions")->container()));
				}
			}

			rule->natTargetDevice = source->get("natTargetDevice")->string();
			if(source->has("natTargetIp")){
				rule->natTargetIp = source->get("natTargetIp")->number();
			}
			masqueradeRules.push_back(rule);
		}
	}

	if(configurationManager->has("autowan2")){
		ObjectContainer* obj = configurationManager->getElement("autowan2");
		this->autowan_mode = obj->get("mode")->number();

		ObjectContainer* devices= obj->get("devices")->container();
		for(int y = 0; y < devices->size(); y++){
			ObjectContainer* criteria = devices->get(y)->container();
			AutowanInterface* aw = new AutowanInterface();
			aw->failover_index = criteria->get("failover_index")->number();	
			aw->interface = criteria->get("interface")->string();	

			autowan_rules.push_back(AutowanInterfacePtr(aw));	
		}
	}

	releaseLock();
	initRules();
	return true;
}
void  SFwallCore::NatAclStore::save() {
	holdLock();

	ObjectContainer *root = new ObjectContainer(CREL);
	ObjectContainer *rules = new ObjectContainer(CARRAY);

	for (PortForwardingRulePtr target : portFowardingRules) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		rule->put("id", new ObjectWrapper((string) target->id));
		rule->put("enabled", new ObjectWrapper((bool) target->enabled));
		rule->put("forwardingDestination", new ObjectWrapper((double) target->forwardingDestination));

		if (target->forwardingDestinationPort != UNSET) {
			rule->put("forwardingDestinationPort", new ObjectWrapper((double) target->forwardingDestinationPort));
		}

		ObjectContainer* criteria = new ObjectContainer(CARRAY);
		for(Criteria* crit : target->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(crit->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(crit)));
			criteria->put(new ObjectWrapper(singleCriteria));
		}

		rule->put("criteria", new ObjectWrapper(criteria));
		rules->put(new ObjectWrapper(rule));
	}

	root->put("rules", new ObjectWrapper(rules));
	configurationManager->setElement("forwarding", root);

	ObjectContainer *masq = new ObjectContainer(CREL);
	ObjectContainer *masqRules = new ObjectContainer(CARRAY);

	for (MasqueradeRulePtr target : masqueradeRules) {
		ObjectContainer *rule = new ObjectContainer(CREL);
		rule->put("id", new ObjectWrapper((string) target->id));
		rule->put("enabled", new ObjectWrapper((bool) target->enabled));
		rule->put("natTargetDevice", new ObjectWrapper((string) target->natTargetDevice));
		rule->put("natTargetIp", new ObjectWrapper((double) target->natTargetIp));

		ObjectContainer* criteria = new ObjectContainer(CARRAY);
		for(Criteria* crit : target->criteria){
			ObjectContainer* singleCriteria = new ObjectContainer(CREL);
			singleCriteria->put("type", new ObjectWrapper(crit->key()));
			singleCriteria->put("conditions", new ObjectWrapper(CriteriaBuilder::serialize(crit)));
			criteria->put(new ObjectWrapper(singleCriteria));
		}
		rule->put("criteria", new ObjectWrapper(criteria));
		masqRules->put(new ObjectWrapper(rule));
	}

	masq->put("rules", new ObjectWrapper(masqRules));
	configurationManager->setElement("masquerading", masq);

	ObjectContainer *autowanObj = new ObjectContainer(CREL);
	autowanObj->put("mode", new ObjectWrapper((double) autowan_mode));
	ObjectContainer* devices = new ObjectContainer(CARRAY);
	for(AutowanInterfacePtr device : autowan_rules){
		ObjectContainer* device_container = new ObjectContainer(CREL);
		device_container->put("interface", new ObjectWrapper((string) device->interface));
		device_container->put("failover_index", new ObjectWrapper((double) device->failover_index));
		devices->put(new ObjectWrapper(device_container));
	}
	autowanObj->put("devices", new ObjectWrapper(devices));
	configurationManager->setElement("autowan2", autowanObj);

	configurationManager->save();
	releaseLock();
}

SFwallCore::MasqueradeRulePtr  SFwallCore::NatAclStore::matchMasqueradeRule(SFwallCore::Packet *packet) {
	for (MasqueradeRulePtr target : masqueradeRules) {
		if (target->match(packet)) {
			return target;
		}
	}

	return MasqueradeRulePtr();
}

SFwallCore::PortForwardingRulePtr  SFwallCore::NatAclStore::matchForwardingRule(SFwallCore::Packet *packet) {
	for (PortForwardingRulePtr target : portFowardingRules) {
		if (target->match(packet)) {
			return target;
		}
	}

	return PortForwardingRulePtr();
}

SFwallCore::MasqueradeRulePtr SFwallCore::NatAclStore::getMasqueradeRule(std::string id) {
	for (MasqueradeRulePtr target : masqueradeRules) {
		if (target->id.compare(id) == 0) {
			return target;
		}
	}

	return MasqueradeRulePtr();
}

SFwallCore::PortForwardingRulePtr SFwallCore::NatAclStore::getPortForwardingRule(std::string id) {
	for (PortForwardingRulePtr target : portFowardingRules) {
		if (target->id.compare(id) == 0) {
			return target;
		}
	}

	return PortForwardingRulePtr();
}

bool sort_by_priority(const SFwallCore::AutowanInterfacePtr& x, const SFwallCore::AutowanInterfacePtr& y){
	return x->failover_index < y->failover_index;
}

void SFwallCore::NatAclStore::initRules() {
	holdLock();

	for (MasqueradeRulePtr target : masqueradeRules) {
		target->natTargetDevicePtr = interfaceManager->get_interface(target->natTargetDevice);

		for(Criteria* criteria : target->criteria){
			criteria->refresh();
		}
	}

	for (PortForwardingRulePtr target : portFowardingRules) {
		for(Criteria* criteria : target->criteria){
			criteria->refresh();
		}
	}

	autowan_active_interfaces.clear();
	autowan_load_balancing_device_pool_last = 0;

	sort(autowan_rules.begin(), autowan_rules.end(), sort_by_priority);
	for (AutowanInterfacePtr target : autowan_rules) {
		auto device = interfaceManager->get_interface(target->interface);
		if(device){
			target->resolved_interface = device;
			autowan_active_interfaces.insert(device);	
		}
	}

	releaseLock();
}

bool SFwallCore::PortForwardingRule::match(SFwallCore::Packet *packet) {
	if (!enabled) {
		return false;
	}

	for(Criteria* target: criteria){
		if(!target->match(packet)){
			return false;
		}
	}

	return true;
}

bool SFwallCore::MasqueradeRule::match(SFwallCore::Packet *packet) {
	if (!enabled) {
		return false;
	}

	for(Criteria* target: criteria){
		if(!target->match(packet)){
			return false;
		}
	}

	return true;
}

int SFwallCore::NatAclStore::determine_routing_fwmark(SFwallCore::Packet* incomming_packet){
	if(autowan_mode == DISABLED || autowan_mode == SINGLE){
		return -1;
	}

	if(autowan_mode == AUTOWAN_MODE_LOADBALANCING){
		int size = autowan_rules.size();
		if (size == 0) {
			return -1;
		}

		if (autowan_load_balancing_device_pool_last == size) {
			autowan_load_balancing_device_pool_last = 0;
		}

		bool looped=false;		
		while(true){
			if (autowan_load_balancing_device_pool_last == size) {
				autowan_load_balancing_device_pool_last = 0;
				if(looped){
					break;
				}

				looped = true;
			}

			AutowanInterfacePtr ptr = autowan_rules[autowan_load_balancing_device_pool_last++];
			if(ptr && ptr->resolved_interface && ptr->resolved_interface->state){
				bool criteria_matched = true;
				for(Criteria* criteria : ptr->criteria){
					if(!criteria->match(incomming_packet)){
						criteria_matched = false;
						break;
					}
				}

				if(criteria_matched){
					autowan_load_balancing_device_pool_last;
					return ptr->resolved_interface->ifid;
				}
			}
		}
	}


	if(autowan_mode == FAILOVER){
		int size = autowan_rules.size();
		if (size == 0) {
			return -1;
		}

		//Find a suitable host
		for(int x =0; x < size; x++){
			AutowanInterfacePtr ptr = autowan_rules[x];
			if(ptr && ptr->resolved_interface->state){

                                bool criteria_matched = true;
                                for(Criteria* criteria : ptr->criteria){
                                        if(!criteria->match(incomming_packet)){
                                                criteria_matched = false;
                                                break;
                                        }
                                }

				if(criteria_matched){
					return ptr->resolved_interface->ifid;
				}
			}
		}
	}

	return -1;
}

bool SFwallCore::NatAclStore::determine_autowan_matches(SFwallCore::Packet* incomming_packet){
	if(autowan_mode != DISABLED){
		PacketV4* ipv4_packet = (PacketV4*) incomming_packet;
		if(autowan_active_interfaces.find(incomming_packet->getDestNetDevice()) != autowan_active_interfaces.end() &&
				ipv4_packet->getDstIp() != incomming_packet->getDestNetDevice()->get_primary_ipv4_address() &&
				ipv4_packet->getSrcIp() != incomming_packet->getDestNetDevice()->get_primary_ipv4_address()){

			return true;	
		}
	}
	return false;
}

int SFwallCore::NatAclStore::get_autowan_mode(){
	return autowan_mode; 
}

void SFwallCore::NatAclStore::set_autowan_mode(int mode){
	autowan_mode = mode;	
}

std::vector<SFwallCore::AutowanInterfacePtr>& SFwallCore::NatAclStore::get_autowan_rules(){
	return autowan_rules;
}

