#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "test.h"
#include "SFwallCore/TestFactory.h"
#include "SFwallCore/Rewrite.h"
#include "SFwallCore/Connection.h"
#include "SFwallCore/State.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/ApplicationLevel/Handlers/DnsHandler.h"
#include "SFwallCore/ApplicationLevel/Handlers/HttpHandler.h"
#include "SFwallCore/ApplicationLevel/FilterHandler.h"

#include "SFwallCore/ApplicationLevel/CapturePortal.h"
#include "SFwallCore/ApplicationLevel/GoogleSafeSearchEnforcer.h"
#include <Utils/DnsType.h>
#include <Core/System.h>
#include <Utils/Utils.h>
#include "Core/Cloud.h"

using namespace SFwallCore;

HttpHandler h;

TEST(CapturePortalEngine, ignoreIfSessionExists) {
	MockFactory *tester = new MockFactory();
	SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->getState()->state = TCPUP;
        connection->setSession(SessionPtr(new Session()));

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, ignoreIfNotTcp) {
	MockFactory *tester = new MockFactory();
	SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	Connection *connection = new UdpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->setSession(SessionPtr());

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, ignoreIfStateNotUp) {
	MockFactory *tester = new MockFactory();

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->setSession(SessionPtr());
        connection->getState()->state = TIME_WAIT;

        EXPECT_FALSE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, matches) {
	MockFactory *tester = new MockFactory();
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;
        connection->setSession(SessionPtr());
        connection->getState()->state = TCPUP;

        EXPECT_TRUE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, matches_dmz_othertraffic) {
        MockFactory *tester = new MockFactory();
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	cp->CAPTURE_PORTAL_DMZ_MODE_ENABLED = 1;

        Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 53);
        UdpConnection *connection = new UdpConnection(packet);
        connection->setSession(SessionPtr());

        EXPECT_TRUE(cp->matchesIpCriteria(connection, packet));
}

TEST(CapturePortalEngine, redirectToAuthPage) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();
        config->put("authentication:cp_url", "http://google.com");

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	cp->setConfigurationManager(config);
        cp->CAPTURE_PORTAL_FORCE = 1;

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->getForceAuthRanges().push_back(std::shared_ptr<Alias>(alias));

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        connection->setSession(SessionPtr(new Session()));
        connection->capturePortal = false;
        connection->getState()->state = TCPUP;
	connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(CapturePortalEngine, ignoreIfDestIpInRage) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();
        config->put("authentication:cp_url", "http://google.com");

        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
	cp->setConfigurationManager(config);
        cp->CAPTURE_PORTAL_FORCE = 1;

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->getForceAuthRanges().push_back(shared_ptr<Alias>(alias));

	Alias *ignoreAlias = new IpSubnetAlias();
        ignoreAlias->addEntry("192.168.1.1/255.255.255.255");
	cp->getForceAuthDestIpExceptions().push_back(shared_ptr<Alias>(ignoreAlias));

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        connection->setSession(SessionPtr(new Session()));
        connection->capturePortal = false;
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(CapturePortalEngine, ignoreIfWebsiteInAuthExceptionList) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();
        config->put("authentication:cp_url", "http://google.com");
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());
        cp->setConfigurationManager(config);
        cp->CAPTURE_PORTAL_FORCE = 1;

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->getForceAuthRanges().push_back(shared_ptr<Alias>(alias));

	Alias *ignoreAlias = new WebsiteListAlias();
        ignoreAlias->addEntry("google.com");
        ignoreAlias->addEntry("yahoo.com");
	cp->getForceAuthWebsiteExceptions().push_back(shared_ptr<Alias>(ignoreAlias));

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
	connection->getApplicationInfo()->setHttpHost("www.google.com");
        connection->setSession(SessionPtr(new Session()));
        connection->capturePortal = false;
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(CapturePortalEngine, ignoreIfWebsiteInAuthExceptionList_wildCards) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());        
        cp->setConfigurationManager(config);

        cp->CAPTURE_PORTAL_FORCE = 1;

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->getForceAuthRanges().push_back(shared_ptr<Alias>(alias));

	Alias *ignoreAlias = new WebsiteListAlias();
        ignoreAlias->addEntry("google.com");
	cp->getForceAuthWebsiteExceptions().push_back(shared_ptr<Alias>(ignoreAlias));

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());

        connection->setSession(SessionPtr(new Session()));
        connection->capturePortal = false;
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

        cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(CapturePortalEngine, websiteNotInIgnoredList) {
	MockFactory *tester = new MockFactory();
	ConfigurationManager *config = new ConfigurationManager();
        config->put("authentication:cp_url", "http://google.com");
        SFwallCore::CapturePortalEngine *cp = new SFwallCore::CapturePortalEngine(new SFwallCore::AliasDb());        
        cp->setConfigurationManager(config);
        cp->CAPTURE_PORTAL_FORCE = 1;

	Alias *alias = new IpSubnetAlias();
        alias->addEntry("10.1.2.1/255.255.255.255");
        alias->addEntry("10.1.66.1/255.255.255.255");
        alias->addEntry("10.1.13.1/255.255.255.255");
        alias->addEntry("10.1.44.1/255.255.255.255");
        alias->addEntry("10.1.66.1/255.255.255.255");
        alias->addEntry("10.1.22.1/255.255.255.255");
        alias->addEntry("10.1.1.1/255.255.255.255");
	cp->getForceAuthRanges().push_back(shared_ptr<Alias>(alias));

	Alias *ignoreAlias = new WebsiteListAlias();
        ignoreAlias->addEntry("google.com");
        ignoreAlias->addEntry("facebook.com");
        ignoreAlias->addEntry("poo.com");
        ignoreAlias->addEntry("bitch.com");
        ignoreAlias->addEntry("fuckme.com");
	cp->getForceAuthWebsiteExceptions().push_back(shared_ptr<Alias>(ignoreAlias));

	Alias *ignoreAlias2 = new WebsiteListAlias();
        ignoreAlias2->addEntry("google.com");
        ignoreAlias2->addEntry("facebook.com");
        ignoreAlias2->addEntry("poo.com");
        ignoreAlias2->addEntry("bitch.com");
        ignoreAlias2->addEntry("fuckme.com");
	cp->getForceAuthWebsiteExceptions().push_back(shared_ptr<Alias>(ignoreAlias2));

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.1", 12334, 80);
	TcpConnection *connection = new TcpConnection(packet);
        connection->getApplicationInfo()->http_hostNameSet = true;
        connection->getApplicationInfo()->http_hostName = "bitching.boobs.fuck.yahoo.com";

        connection->setSession(SessionPtr(new Session()));
        connection->capturePortal = false;
        connection->getState()->state = TCPUP;
        connection->getApplicationInfo()->type = SFwallCore::ConnectionApplicationInfo::Classifier::HTTP;

	cp->process(connection, packet);
        EXPECT_TRUE(connection->getRewriteRule() != NULL);
}

TEST(DNSRecaster, responseNotReplacedWithCNameWhenDisabled) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", false);
	SFwallCore::GoogleSafeSearchEngine *GSS = new GoogleSafeSearchEngine(config);

	Packet *packet = PacketBuilder::createUdp("10.1.1.1", "192.168.1.2", 53, 10240);
	UdpConnection *connection = new UdpConnection(PacketBuilder::createUdp("192.168.1.2", "10.1.1.1",  10240, 53));
        packet->setConnection(connection);

        connection->getApplicationInfo()->dnsQueryName = "www.google.com";
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(DNSRecaster, responseShouldBeReplacedWithCName) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);
        DnsHandler handler;

	DnsMessage *m = new DnsMessage();
        m->setRecursionDesired(true);
        m->setQuery(false);
        m->setId(734);
        m->getQuestions().push_back(new DnsQuestion("www.google.com", DnsRecordTypes::A, DnsRecordClasses::INTERNET));

        size_t len = 0;
	char *message = m->toCString(&len);

	Packet *packet = PacketBuilder::createUdp("10.1.1.1", "192.168.1.2", 53, 10240, message, len);
	UdpConnection *connection = new UdpConnection(PacketBuilder::createUdp("192.168.1.2", "10.1.1.1",  10240, 53));
        packet->setConnection(connection);

        handler.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();
        ASSERT_TRUE(rwr != NULL);

        if (rwr) {
                rwr->rewrite(connection, packet);
		m = new DnsMessage((char *)packet->getTransport()->getApplication()->getInternal());
		DnsAnswer *a = m->getAnswers().at(0);
		ASSERT_STREQ("nosslsearch.google.com", ((DnsCNAMERecord *)a->getResourceRecord())->getCanonicalName().c_str());
        }
}

TEST(DNSRecaster, responseShouldNotBeReplacedWithCNameWhenEnabledButNotGoogle) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);
        DnsHandler handler;

	DnsMessage *m = new DnsMessage();
        m->setRecursionDesired(true);
        m->setQuery(false);
        m->setId(734);
        m->getQuestions().push_back(new DnsQuestion("bitbucket.org", DnsRecordTypes::A, DnsRecordClasses::INTERNET));
        m->getAnswers().push_back(new DnsAnswer("bitbucket.org", DnsRecordTypes::A, DnsRecordClasses::INTERNET, 60, new DnsARecord(htonl(IP4Addr::stringToIP4Addr("10.0.0.1")))));

        size_t len = 0;
	char *message = m->toCString(&len);

	Packet *packet = PacketBuilder::createUdp("10.1.1.1", "192.168.1.2", 53, 10240, message, len);
	UdpConnection *connection = new UdpConnection(PacketBuilder::createUdp("192.168.1.2", "10.1.1.1",  10240, 53));
        packet->setConnection(connection);

        handler.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

        EXPECT_TRUE(connection->getRewriteRule() == NULL);
}

TEST(DNSRecaster, responseIsReplacedWithCName) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);
        DnsHandler handler;

	DnsMessage *m = new DnsMessage();

        m->setRecursionAllowed(false);
        m->setId(10023);
        m->setOpCode(DnsOpCodes::QUERY);
        m->setQuery(false);
        m->setRCode(DnsResponseCodes::NOERROR);

        m->getQuestions().push_back(new DnsQuestion("www.google.com", DnsRecordTypes::A, DnsRecordClasses::INTERNET));

        m->getAnswers().push_back(new DnsAnswer("www.google.com", DnsRecordTypes::A, DnsRecordClasses::INTERNET, 7200, new DnsARecord(IP4Addr::stringToIP4Addr("201.201.201.201"))));

        size_t len = 0;
	char *data = m->toCString(&len);

	Packet *packet = PacketBuilder::createUdp("10.1.1.1", "192.168.1.2", 53, 10240, data, len);
	UdpConnection *connection = new UdpConnection(PacketBuilder::createUdp("192.168.1.2", "10.1.1.1",  10240, 53));
        packet->setConnection(connection);

        handler.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();
        ASSERT_TRUE(rwr != NULL);

        if (rwr) {
                rwr->rewrite(connection, packet);
		m = new DnsMessage((char *)packet->getTransport()->getApplication()->getInternal());
		DnsAnswer *a = m->getAnswers().at(0);
		ASSERT_STREQ("nosslsearch.google.com", ((DnsCNAMERecord *)a->getResourceRecord())->getCanonicalName().c_str());
        }
}

TEST(HttpEnforceGoogleSafeSearch, packetsShouldBeFlaggedAndModifiedForAllSearchesWithGoogle) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.2", 10240, 80, "GET /search?q=hi HTTP/1.1\r\nHost: www.google.com\r\n\r\n");
	TcpConnection *connection = new TcpConnection(packet);
        packet->setConnection(connection);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();

        ASSERT_TRUE(rwr != NULL);
        rwr->rewrite(connection, packet);
	string s = (char *) packet->getTransport()->getApplication()->getInternal();
        ASSERT_TRUE(s.find("?safe=vss&") != std::string::npos);
}

TEST(HttpEnforceGoogleSafeSearch, packetsShouldBeFlaggedAndModifiedForAllSearchesWithGoogleImages) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.2", 10240, 80, "GET /images?q=hi HTTP/1.1\r\nHost: www.google.com\r\n\r\n");
	TcpConnection *connection = new TcpConnection(packet);
        packet->setConnection(connection);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();

        ASSERT_TRUE(rwr != NULL);
        rwr->rewrite(connection, packet);
	string s = (char *) packet->getTransport()->getApplication()->getInternal();
        ASSERT_TRUE(s.find("?safe=vss&") != std::string::npos);
}

TEST(HttpEnforceGoogleSafeSearch, packetsShouldBeFlaggedAndModifiedForAllSearchesWithGoogleS) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.2", 10240, 80, "GET /s?q=hi HTTP/1.1\r\nHost: www.google.com\r\n\r\n");
	TcpConnection *connection = new TcpConnection(packet);
        packet->setConnection(connection);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();

        ASSERT_TRUE(rwr != NULL);
        rwr->rewrite(connection, packet);
	string s = (char *) packet->getTransport()->getApplication()->getInternal();
        ASSERT_TRUE(s.find("?safe=vss&") != std::string::npos);
}

TEST(HttpEnforceGoogleSafeSearch, packetsShouldBeFlaggedAndModifiedForAllSearchesWithGoogleOther) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();
        string t = "GET /search?output=search&sclient=psy-ab&q=porn+&oq=porn+&gs_l=hp.3...3229.5871.0.6329.5.5.0.0.0.0.429.1351.2-2j0j2.4.0....0...1c.1.32.psy-ab..3.2.690.k_SNgpBVTFg&pbx=1&bav=on.2,or.r_qf.&bvm=bv.60444564%2Cd.dGI%2Cpv.xjs.s.en_US.GDFJ-w8tMYk.O&fp=6b9250809a372138&biw=1366&bih=632&dpr=1&tch=1&ech=1&psi=vXnsUoSOOsfEkgWni4CIAQ.1391229374441.3 HTTP/1.1\r\nHost: www.google.com\r\n\r\n";

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.2", 10240, 80, t);
	TcpConnection *connection = new TcpConnection(packet);
        packet->setConnection(connection);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();

        ASSERT_TRUE(rwr != NULL);
        rwr->rewrite(connection, packet);
	string s = (char *) packet->getTransport()->getApplication()->getInternal();
        ASSERT_TRUE(s.find("?safe=vss&") != std::string::npos);
        ASSERT_TRUE(s.find("?safe=vss&output=search") != std::string::npos);
        ASSERT_TRUE(s.find("?safe=vss& ") == std::string::npos);
}

TEST(HttpEnforceGoogleSafeSearch, packetsShouldNotBeFlaggedAndModifiedForGoogleRoot) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.2", 10240, 80, "GET / HTTP/1.1\r\nHost: www.google.com\r\n\r\n");
	TcpConnection *connection = new TcpConnection(packet);
        packet->setConnection(connection);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();

        ASSERT_TRUE(rwr == NULL);
}

TEST(HttpEnforceGoogleSafeSearch, packetsShouldNotBeFlaggedAndModifiedForNonQuery) {
	ConfigurationManager *config = System::getInstance()->getConfigurationManager();

        config->put("general:disableGoogleSearchSSL", true);
	SFwallCore::GoogleSafeSearchEngine *GSS = new SFwallCore::GoogleSafeSearchEngine(config);
        string t = "GET /search&output=search&sclient=psy-ab&q=porn+&oq=porn+&gs_l=hp.3...3229.5871.0.6329.5.5.0.0.0.0.429.1351.2-2j0j2.4.0....0...1c.1.32.psy-ab..3.2.690.k_SNgpBVTFg&pbx=1&bav=on.2,or.r_qf.&bvm=bv.60444564%2Cd.dGI%2Cpv.xjs.s.en_US.GDFJ-w8tMYk.O&fp=6b9250809a372138&biw=1366&bih=632&dpr=1&tch=1&ech=1&psi=vXnsUoSOOsfEkgWni4CIAQ.1391229374441.3 HTTP/1.1\r\nHost: www.google.com\r\n\r\n";

	Packet *packet = PacketBuilder::createTcp("10.1.1.1", "192.168.1.2", 10240, 80, t);
	TcpConnection *connection = new TcpConnection(packet);
        packet->setConnection(connection);
        h.handle(SFwallCore::DIR_SAME, connection, packet->getTransport()->getApplication());
        connection->setSession(SessionPtr(new Session()));

        if (GSS->enabled()) {
                GSS->process(connection, packet);
        }

	RewriteRule *rwr = connection->getRewriteRule();

        ASSERT_TRUE(rwr == NULL);
}

