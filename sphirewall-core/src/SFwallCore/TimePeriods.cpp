#include <iostream>

using namespace std;

#include "Utils/StringUtils.h"
#include "Utils/TimeWrapper.h"
#include "SFwallCore/TimePeriods.h"
#include "Core/ConfigurationManager.h"

TimePeriod::TimePeriod()
	: startTime(-1),
	  endTime(-1),
	  startDate(-1),
	  endDate(-1),
	  any(false),
	  mon(false),
	  tue(false),
	  wed(false),
	  thu(false),
	  fri(false),
	  sat(false),
	  sun(false)
{}


TimePeriod::TimePeriod(std::string name, float startTime, float endTime, float startDate, float endDate, bool any, bool mon, bool tue, bool wed, bool thu, bool fri, bool sat, bool sun)
	: name(name),
	  startTime(startTime),
	  endTime(endTime),
	  startDate(startDate),
	  endDate(endDate),
	  any(any),
	  mon(mon),
	  tue(tue),
	  wed(wed),
	  thu(thu),
	  fri(fri),
	  sat(sat),
	  sun(sun) {
}

TimePeriod::~TimePeriod()
{}


bool TimePeriodStore::load() {
	store.clear();
	if (configurationManager) {
		ObjectContainer *root;

		if (configurationManager->has("periods")) {
			root = configurationManager->getElement("periods");

			for (int x = 0; x < root->size(); x++) {
				ObjectContainer *elem = root->get(x)->container();

				TimePeriod *period = new TimePeriod();
				period->uuid = elem->get("id")->string();
				period->name = elem->get("name")->string();
				period->startTime = elem->get("startTime")->number();
				period->endTime = elem->get("endTime")->number();
				period->mon = elem->get("mon")->boolean();
				period->tue = elem->has("tue") ? elem->get("tue")->boolean() : elem->get("tues")->boolean();
				period->wed = elem->get("wed")->boolean();
				period->thu = elem->has("thu") ? elem->get("thu")->boolean() : elem->get("thurs")->boolean();
				period->fri = elem->get("fri")->boolean();
				period->sat = elem->get("sat")->boolean();
				period->sun = elem->get("sun")->boolean();

				if (elem->has("any")) {
					period->startDate = elem->get("startDate")->number();
					period->endDate = elem->get("endDate")->number();
					period->any = elem->get("any")->boolean();
				}
				else {
					period->any = period->mon &&
								  period->tue &&
								  period->wed &&
								  period->thu &&
								  period->fri &&
								  period->sat &&
								  period->sun;
					period->startDate = -1;
					period->endDate = -1;
				}

				add(TimePeriodPtr(period));
			}
		}
	}
	return true;
}

void TimePeriodStore::save() {
	if (configurationManager) {
		ObjectContainer *root = new ObjectContainer(CARRAY);

		for (auto x :  store) {
			TimePeriodPtr period = x.second;
			if (period) {
				ObjectContainer *target = new ObjectContainer(CREL);

				target->put("startTime", new ObjectWrapper((double) period->startTime));
				target->put("endTime", new ObjectWrapper((double) period->endTime));
				target->put("startDate", new ObjectWrapper((double) period->startDate));
				target->put("endDate", new ObjectWrapper((double) period->endDate));
				target->put("any", new ObjectWrapper((bool) period->any));
				target->put("mon", new ObjectWrapper((bool) period->mon));
				target->put("tue", new ObjectWrapper((bool) period->tue));
				target->put("wed", new ObjectWrapper((bool) period->wed));
				target->put("thu", new ObjectWrapper((bool) period->thu));
				target->put("fri", new ObjectWrapper((bool) period->fri));
				target->put("sat", new ObjectWrapper((bool) period->sat));
				target->put("sun", new ObjectWrapper((bool) period->sun));

				target->put("id", new ObjectWrapper((string) period->uuid));
				target->put("name", new ObjectWrapper((string) period->name));
				root->put(new ObjectWrapper(target));
			}
		}
		configurationManager->setElement("periods", root);
		configurationManager->save();
	}
}

void TimePeriodStore::add(TimePeriodPtr period) {
	if (period->uuid.size() == 0) {
		period->uuid = StringUtils::genRandom();
	}

	store[period->uuid] = period;
}

void TimePeriodStore::remove(TimePeriodPtr period) {
	store.erase(period->uuid);

	for (TimePeriodDeleteListener * tpd : deleteListeners) {
		tpd->timePeriodDeleted(period);
	}
}

TimePeriodPtr TimePeriodStore::getById(std::string id) {
	if(store.find(id) != store.end()){
		return store[id];
	}
	
	return TimePeriodPtr();
}

TimePeriodPtr TimePeriodStore::getByName(std::string name) {
	for (auto period : store) {
		TimePeriodPtr target = period.second;

		if (target->name.compare(name) == 0) {
			return target;
		}
	}

	return TimePeriodPtr();
}

std::list<TimePeriodPtr> TimePeriodStore::list() {
	std::list<TimePeriodPtr> ret;
	for (auto period : store) {
		ret.push_back(period.second);
	}

	return ret;
}

void TimePeriodStore::registerDeleteListener(TimePeriodDeleteListener *listener) {
	deleteListeners.push_back(listener);
}

TimePeriodStore::TimePeriodStore()
{}


TimePeriodStore::~TimePeriodStore()
{}


/*
   If this method returns true, then we have matched atleast one of the periods
 */
bool TimePeriodStore::checkMatch(const std::list<TimePeriodPtr> &periods, int inputTime) {
	if (periods.size() == 0) {
		return true;
	}

	for (auto target : periods) {
		if (target->matches(inputTime)) {
			return true;
		}
	}
	return false;
}

bool TimePeriod::matches(int currentTimeStamp) {
	if ((startDate == -1) || (currentTimeStamp >= startDate && currentTimeStamp <= endDate)) {
		Time now(currentTimeStamp);
		int wday = now.wday();
		int hour = now.extractHour();
		int minutes = now.extractMins();
		int current = 100 * hour + minutes;

		// 100 * hour + minutes means [0, 2400) where t-1 is 2359 or 23 hours, 59 mins
		if ((current >= startTime && current < endTime)) {
			if (any) {
				return true;
			}

			switch (wday) {
			case 1:
				return mon;

			case 2:
				return tue;

			case 3:
				return wed;

			case 4:
				return thu;

			case 5:
				return fri;

			case 6:
				return sat;

			case 7:
				return sun;
			}
		}
	}

	return false;
}

