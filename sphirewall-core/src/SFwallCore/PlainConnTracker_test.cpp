/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <iostream>
#include <algorithm>
#include "SFwallCore/ConnTracker.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <linux/icmp.h>
#include <netinet/ip.h>

#include "SFwallCore/TestFactory.h"
#include "SFwallCore/Packet.h"
#include "SFwallCore/Connection.h"

using namespace SFwallCore;


TEST(PlainConnTracker, createTcpConnection) {
	Packet *tcp = PacketBuilder::createTcp("10.1.1.1", "192.168.0.1", 34538, 80);
	PacketBuilder::setAck((TcpPacket *) tcp->getTransport());

	PlainConnTracker *tracker = new PlainConnTracker(NULL);
	Connection *tcpConnection = tracker->create(tcp);
	EXPECT_TRUE(tcpConnection);
	EXPECT_TRUE((tcpConnection->getProtocol() == TCP));
}


TEST(PlainConnTracker, createUdpConnection) {
	Packet *udp = PacketBuilder::createUdp("192.1.1.1", "192.168.0.1", 3453, 80);

	Config * c = new Config();
	SFwallCore::Firewall * fw = new Firewall(c);

	PlainConnTracker *tracker = new PlainConnTracker(fw);
	Connection *udpConnection = tracker->create(udp);
	EXPECT_TRUE(udpConnection);
	EXPECT_EQ(UDP,  udpConnection->getProtocol());
	Connection *temp = tracker->offer(udp);
	EXPECT_EQ(udpConnection, temp);
}


TEST(PlainConnTracker, createIcmpConnection) {
	Packet *icmp = PacketBuilder::createIcmp("192.1.1.1", "192.168.0.1", 100);
	PlainConnTracker *tracker = new PlainConnTracker(NULL);

	Connection *icmpConnection = tracker->create(icmp);
	EXPECT_TRUE(icmpConnection);
	EXPECT_EQ(ICMP, icmpConnection->getProtocol());
	Connection *temp = tracker->offer(icmp);
	EXPECT_EQ(icmpConnection, temp);
}


TEST(ConnectionCriteria, matchTcp_AllCriteria) {
	unsigned int source = IP4Addr::stringToIP4Addr("10.1.1.1");
	unsigned int dest = IP4Addr::stringToIP4Addr("192.168.0.1");

	Packet *tcp = PacketBuilder::createTcp("10.1.1.1", "192.168.0.1", 40000, 80);

	PlainConnTracker *tracker = new PlainConnTracker(NULL);
	Connection *connection = tracker->create(tcp);

	ConnectionCriteria *criteria = new ConnectionCriteria();
	criteria->setSource(source);
	criteria->setSourcePort(40000);
	criteria->setDest(dest);
	criteria->setDestPort(80);
	criteria->setProtocol(TCP);

	EXPECT_TRUE(criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_AllCriteria) {
	unsigned int source = IP4Addr::stringToIP4Addr("10.1.1.1");
	unsigned int dest = IP4Addr::stringToIP4Addr("192.168.0.1");

	Packet *udp = PacketBuilder::createUdp("10.1.1.1", "192.168.0.1", 40000, 80);
	PlainConnTracker *tracker = new PlainConnTracker(NULL);
	Connection *connection = tracker->create(udp);

	ConnectionCriteria *criteria = new ConnectionCriteria();
	criteria->setSource(source);
	criteria->setSourcePort(40000);
	criteria->setDest(dest);
	criteria->setDestPort(80);
	criteria->setProtocol(UDP);

	EXPECT_TRUE(criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_WrongType) {
	Packet *udp = PacketBuilder::createUdp("10.1.1.1", "192.168.0.1", 3453, 80);
	PlainConnTracker *tracker = new PlainConnTracker(NULL);
	Connection *connection = tracker->create(udp);

	ConnectionCriteria *criteria = new ConnectionCriteria();
	criteria->setProtocol(TCP);

	EXPECT_TRUE(!criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_WrongSource) {
	unsigned int source = IP4Addr::stringToIP4Addr("10.1.1.1");

	Packet *udp = PacketBuilder::createUdp("10.1.1.2", "192.168.0.1", 3453, 80);
	PlainConnTracker *tracker = new PlainConnTracker(NULL);
	Connection *connection = tracker->create(udp);

	ConnectionCriteria *criteria = new ConnectionCriteria();
	criteria->setSource(source);
	criteria->setProtocol(UDP);

	EXPECT_TRUE(!criteria->match(connection));
}


TEST(ConnectionCriteria, matchUdp_WrongDest) {
	unsigned int dest = IP4Addr::stringToIP4Addr("10.1.1.1");

	Packet *udp = PacketBuilder::createUdp("10.1.1.2", "192.168.0.1", 3453, 80);
	PlainConnTracker *tracker = new PlainConnTracker(NULL);
	Connection *connection = tracker->create(udp);

	ConnectionCriteria *criteria = new ConnectionCriteria();
	criteria->setSource(dest);
	criteria->setProtocol(UDP);

	EXPECT_TRUE(!criteria->match(connection));
}
