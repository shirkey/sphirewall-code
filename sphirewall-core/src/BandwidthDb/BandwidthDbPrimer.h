/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SPHIREWALL_BANDWIDTH_H_INCLUDED
#define SPHIREWALL_BANDWIDTH_H_INCLUDED

#include <list>
#include <vector>
#include "SFwallCore/ConnTracker.h"
#include "SFwallCore/Connectionfwd.h"

class ConnectionStatistics;
class BandwidthListener;
class AnalyticsClient;
class Lock;
class Config;
class CronJob;
class UserDb;

class BandwidthDbPrimer : public SFwallCore::DeleteConnectionHook, public CronJob {
	public:
		void close();
		void setMaxBufferSize(int i);
		void run();

		BandwidthDbPrimer(UserDb *userDb, Config *config);
		~BandwidthDbPrimer();

		void hook(SFwallCore::Connection *connection);
		AnalyticsClient *db();

		void registerListener(BandwidthListener *listener) {
			bandwidthListeners.push_back(listener);
		}
	private:
		//Performance configuration items
		static int BANDWIDTHDB_PRIMER_MAX_BUFSIZE;
		static int EXCLUDE_NON_WEB;
		static int MIN_BANDWIDTH_TRANSFER_THRESHOLD;

		UserDb *userDb;
		Config *conf;
		Lock *transferLock;
		Lock *transferToAnaLock;
		std::vector<ConnectionStatistics *> bufferedConnections;
		void push(ConnectionStatistics *statistic);

		std::list<BandwidthListener *> bandwidthListeners;
		bool primerAlreadyRunning;
};
#endif

