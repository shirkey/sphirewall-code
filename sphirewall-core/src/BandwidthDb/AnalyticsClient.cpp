/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <boost/asio.hpp>
#include <vector>
#include <sstream>
#include "Json/JSON.h"
#include "Core/SysMonitor.h"
#include "Utils/TimeWrapper.h"
#include "Core/System.h"
#include "Utils/IP4Addr.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "BandwidthDb/ConnectionStatistics.h"
#include "BandwidthDb/BandwidthException.h"

using namespace std;
using boost::asio::ip::tcp;

AnalyticsClient::AnalyticsClient()
{}

bool AnalyticsClient::enabled() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("general:analyticsenabled")) {
		return System::getInstance()->getConfigurationManager()->get("general:analyticsenabled")->boolean();
	}

	return true;
}

std::string AnalyticsClient::name() {
	return "AnalyticsClient";
}

int AnalyticsClient::push(std::vector<ConnectionStatistics *> input) {
	ObjectContainer *obj = new ObjectContainer(CREL);
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	for (ConnectionStatistics * connectionStatistics : input) {
		arr->put(new ObjectWrapper(connectionStatistics->serialize()));
	}

	ObjectContainer *args = new ObjectContainer(CREL);

	if (System::getInstance()->configurationManager.hasByPath("general:bandwidth_threshold") && System::getInstance()->configurationManager.hasByPath("general:bandwidth_retension")) {
		args->put("byteThreshold", new ObjectWrapper((double) System::getInstance()->configurationManager.get("general:bandwidth_threshold")->number()));
		args->put("retensionPeriod", new ObjectWrapper((double) System::getInstance()->configurationManager.get("general:bandwidth_retension")->number()));
		args->put("daySwitchPeriod", new ObjectWrapper((double) System::getInstance()->configurationManager.get("general:bandwidth_day_switch_period")->number()));
	}

	args->put("items", new ObjectWrapper(arr));
	obj->put("args", new ObjectWrapper(args));
	obj->put("request", new ObjectWrapper((string) "stats/bandwidth/insert"));
	ObjectWrapper *root = new ObjectWrapper(obj);

	std::string sMsg = JsonSerialiser::serializeToJsonString(root);
	delete root;
	sendAndRecv(sMsg);

	return 0;
}

std::string AnalyticsClient::sendAndRecv(std::string input) {
	string hostname = System::getInstance()->configurationManager.get("general:bandwidth_hostname")->string();
	int port = System::getInstance()->configurationManager.get("general:bandwidth_port")->number();

	boost::asio::io_service io_service;

	stringstream ss; //create a stringstream
	ss << port;

	tcp::resolver resolver(io_service);
	tcp::resolver::query query(tcp::v4(), hostname, ss.str());
	tcp::resolver::iterator iterator = resolver.resolve(query);

	tcp::socket s(io_service);
	s.connect(*iterator);

	input += "\n";
	boost::asio::write(s, boost::asio::buffer(input, input.size()));

	boost::asio::streambuf response;
	boost::asio::read_until(s, response, "\n");
	string buffer = string((istreambuf_iterator<char>(&response)),
						   istreambuf_iterator<char>());

	s.close();
	io_service.stop();
	return buffer;
}

void AnalyticsClient::addMetricSnapshot(std::map<std::string, double> metrics) {
	ObjectContainer *obj = new ObjectContainer(CREL);
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	for (map<std::string, double>::iterator iter = metrics.begin();
			iter != metrics.end();
			iter++) {

		ObjectContainer *i = new ObjectContainer(CREL);
		i->put("key", new ObjectWrapper((std::string) iter->first));
		i->put("value", new ObjectWrapper((double) iter->second));
		arr->put(new ObjectWrapper(i));
	}

	ObjectContainer *args = new ObjectContainer(CREL);
	args->put("items", new ObjectWrapper(arr));

	obj->put("args", new ObjectWrapper(args));
	obj->put("request", new ObjectWrapper((string) "stats/metrics/insert"));
	ObjectWrapper *root = new ObjectWrapper(obj);

	std::string sMsg = JsonSerialiser::serializeToJsonString(root);
	delete root;
	sendAndRecv(sMsg);
}

long AnalyticsClient::getBandwidthByUser(std::string username, Time start, Time finish) {
	JSONObject args;
	args.put(L"filter_user", new JSONValue((string) username));
	args.put(L"startTime", new JSONValue((string) start.format("%Y-%m-%d")));
	args.put(L"endTime", new JSONValue((string) finish.format("%Y-%m-%d")));
	args.put(L"metric", new JSONValue((string) "bytes"));

	JSONValue *root = request("analytics/stats/bandwidth/query/transfer/sum", args);

	if (root && root->AsObject().has(L"response")) {
		JSONObject response = root->AsObject()[L"response"]->AsObject();

		double total = response[L"tcpUp"]->AsNumber() + response[L"tcpDown"]->AsNumber() + response[L"udpUp"]->AsNumber() + response[L"udpDown"]->AsNumber();
		delete root;
		return total;
	}

	return -1;
}

long AnalyticsClient::getTotalTransfer(Time start, Time finish) {
	JSONObject args;
	args.put(L"startTime", new JSONValue((string) start.format("%Y-%m-%d")));
	args.put(L"endTime", new JSONValue((string) finish.format("%Y-%m-%d")));
	args.put(L"metric", new JSONValue((string) "bytes"));

	JSONValue *root = request("analytics/stats/bandwidth/query/transfer/sum", args);

	if (root && root->AsObject().has(L"response")) {
		JSONObject response = root->AsObject()[L"response"]->AsObject();
		double total = response[L"tcpUp"]->AsNumber() + response[L"tcpDown"]->AsNumber() + response[L"udpUp"]->AsNumber() + response[L"udpDown"]->AsNumber();
		delete root;
		return total;
	}

	return -1;
}

JSONValue *AnalyticsClient::request(std::string uri, JSONObject args) {
	JSONObject obj, responseRoot;
	JSONValue *root = NULL;
	JSONValue *value = NULL;
	string response;

	obj.put(L"request", new JSONValue((string) uri));
	obj.put(L"args", new JSONValue(args));
	root = new JSONValue(obj);
	response = sendAndRecv(WStringToString(root->Stringify()));

	if (!(value = JSON::Parse(response.c_str()))) {
		return NULL;
	}

	responseRoot = value->AsObject();
	return value;
}
