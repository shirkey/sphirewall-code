#include <iostream>
#include <time.h>
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "SFwallCore/Connection.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "Utils/Lock.h"

#include "BandwidthDb/AnalyticsClient.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "BandwidthDb/ConnectionStatistics.h"

using boost::asio::ip::tcp;
using namespace boost::algorithm;
using namespace std;

ConnectionStatistics::ConnectionStatistics(SFwallCore::Connection *connection) {
	upload = connection->getUpload();
	download = connection->getDownload();

	SFwallCore::ConnectionIpV4 *ipv4 = (SFwallCore::ConnectionIpV4 *) connection->getIp();
	m_sourceIp = ipv4->getSrcIp();
	m_destIp = ipv4->getDstIp();

	m_sport = connection->getSourcePort();
	m_dport = connection->getDestinationPort();

	if (connection->getApplicationInfo()->type == SFwallCore::ConnectionApplicationInfo::Classifier::HTTP) {
		httpHost = connection->getApplicationInfo()->http_hostName;
		useragent = connection->getApplicationInfo()->http_get(REQUEST, HTTP_USERAGENT);
		contenttype = connection->getApplicationInfo()->http_get(RESPONSE, HTTP_CONTENTTYPE);
	}

	protocol = connection->getProtocol();
	startTime = connection->getTime();
	hwAddress = connection->getHwAddress();

	InterfacePtr sourceDev;
	InterfacePtr destDev;

	if ((sourceDev = connection->getSourceNetDevice()) != NULL) {
		this->sourceDev = sourceDev->name;
	}

	if ((destDev = connection->getDestNetDevice()) != NULL) {
		this->destDev = destDev->name;
	}

	if (connection->getSession()) {
		user = connection->getSession()->getUserName();
	}

	if (connection->getHostDiscoveryServiceEntry()) {
		hostname = connection->getHostDiscoveryServiceEntry()->hostname();
	}
}

ObjectContainer *ConnectionStatistics::serialize() {
	ObjectContainer *target = new ObjectContainer(CREL);
	target->put("sourceIp", new ObjectWrapper(IP4Addr::ip4AddrToString(m_sourceIp)));
	target->put("destIp", new ObjectWrapper(IP4Addr::ip4AddrToString(m_destIp)));
	target->put("sourcePort", new ObjectWrapper((double) m_sport));
	target->put("destPort", new ObjectWrapper((double) m_dport));
	target->put("time", new ObjectWrapper((double) startTime));
	target->put("upload", new ObjectWrapper((double) upload));
	target->put("download", new ObjectWrapper((double) download));
	target->put("useragent", new ObjectWrapper((string) useragent));
	target->put("contenttype", new ObjectWrapper((string) contenttype));
	target->put("inputDev", new ObjectWrapper((string) sourceDev));
	target->put("outputDev", new ObjectWrapper((string) destDev));
	target->put("protocol", new ObjectWrapper((double) protocol));

	if(System::getInstance()->getFirewall()->ANONYMIZE_USER_DATA == 0){
		target->put("sourceHostname", new ObjectWrapper((string) hostname));
		target->put("hwAddress", new ObjectWrapper((string) hwAddress));
		target->put("user", new ObjectWrapper(user));
	}

	if(System::getInstance()->getFirewall()->ANONYMIZE_WEBSITE_DATA == 0){
		target->put("httpHost", new ObjectWrapper((string) httpHost));
	}else{
		if(httpHost.size() > 0){
			target->put("httpHost", new ObjectWrapper((string) "fake.com"));
		}else{
			target->put("httpHost", new ObjectWrapper((string) ""));
		}
	}

	return target;
}

