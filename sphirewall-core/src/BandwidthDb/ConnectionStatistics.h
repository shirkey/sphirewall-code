#ifndef SPHIREWALL_CONNECTIONSTATISTICS_H_INCLUDED
#define SPHIREWALL_CONNECTIONSTATISTICS_H_INCLUDED

#include <string>

class ObjectContainer;
namespace SFwallCore {
	class Connection;
	enum proto;
}

class ConnectionStatistics {
	public:
		ConnectionStatistics(SFwallCore::Connection *connection);
		ObjectContainer *serialize();

		double upload, download;
		unsigned int m_sourceIp, m_destIp;
		int m_sport, m_dport, startTime;

		int protocol;

		std::string httpHost;
		std::string sourceDev;
		std::string destDev;
		std::string user;
		std::string hwAddress;
		std::string contenttype;
		std::string useragent;
		std::string hostname;
};

#endif
