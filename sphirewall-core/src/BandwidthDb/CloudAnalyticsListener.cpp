/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <time.h>
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <queue>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include "Core/ConfigurationManager.h"
#include "Json/JSON.h"
#include "Json/JSONValue.h"
#include "Core/System.h"
#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"
#include "Utils/Lock.h"
#include "BandwidthDb/BandwidthDbPrimer.h"
#include "BandwidthDb/AnalyticsClient.h"
#include "BandwidthDb/CloudAnalyticsListener.h"
#include "BandwidthDb/ConnectionStatistics.h"
#include "Utils/HttpRequestWrapper.h"

using boost::asio::ip::tcp;
using namespace boost::algorithm;
using namespace std;

int CloudAnalyticsListener::CLOUD_EVENTS_PUSH_THRESHOLD = 120;
int CloudAnalyticsListener::CLOUD_EVENTS_ENABLED = 1;

CloudAnalyticsListener::CloudAnalyticsListener() {
	System::getInstance()->config.getRuntime()->loadOrPut("CLOUD_EVENTS_PUSH_THRESHOLD", &CLOUD_EVENTS_PUSH_THRESHOLD);
	System::getInstance()->config.getRuntime()->loadOrPut("CLOUD_EVENTS_ENABLED", &CLOUD_EVENTS_ENABLED);
}


std::string CloudAnalyticsListener::name() {
	return "CloudAnalyticsListener";
}


int CloudAnalyticsListener::push(std::vector<ConnectionStatistics *> input) {
	ObjectContainer *obj = new ObjectContainer(CREL);
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	for (ConnectionStatistics * stats : input) {
		arr->put(new ObjectWrapper(stats->serialize()));
	}

	for (ConnectionStatistics * stats : errors) {
		arr->put(new ObjectWrapper(stats->serialize()));
	}

	obj->put("items", new ObjectWrapper(arr));
	obj->put("deviceid", new ObjectWrapper((string) getDeviceId()));
	obj->put("token", new ObjectWrapper((string) getToken()));
	obj->put("type", new ObjectWrapper((string) "CONNECTIONS"));
	ObjectWrapper *root = new ObjectWrapper(obj);

	std::string sMsg = JsonSerialiser::serializeToJsonString(root);
	delete root;

	try {
		HttpRequestWrapper http(getAnalyticsUrl(), POST);
		http.postdata = sMsg;
		http.execute();

		for (ConnectionStatistics * stats : errors) {
			delete stats;
		}

		errors.clear();
	}
	catch (exception &e) {
		//Something went wrong during communication with cloud service - we need to buffer the input connections and add them to the next attempt
		if (errors.size() < 20000) {
			for (ConnectionStatistics * stats : input) {
				errors.push_back(new ConnectionStatistics(*stats));
			}
		}

		throw e; //Rethrow exception so root handler can deal with it
	}

	return 0;
}

bool CloudAnalyticsListener::enabled() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("cloud:enabled")) {
		return System::getInstance()->getConfigurationManager()->get("cloud:enabled")->boolean() && getToken().size() > 0;;
	}

	return false;
}

std::string CloudAnalyticsListener::getHostname() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("cloud:hostname")) {
		return System::getInstance()->getConfigurationManager()->get("cloud:hostname")->string();
	}

	return "api.linewize.net";
}

std::string CloudAnalyticsListener::getDeviceId() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("cloud:deviceid") && System::getInstance()->getConfigurationManager()->get("cloud:deviceid")->string().size() > 0) {
		return System::getInstance()->getConfigurationManager()->get("cloud:deviceid")->string();
	}

	string ret = FileUtils::read("/sys/class/net/eth0/address");
	trim(ret);
	return ret;
}

std::string CloudAnalyticsListener::getToken() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("cloud:ana-token")) {
		return System::getInstance()->getConfigurationManager()->get("cloud:ana-token")->string();
	}

	return "";
}


std::string CloudAnalyticsListener::getAnalyticsUrl() {
	if (System::getInstance()->getConfigurationManager()->hasByPath("cloud:ana-url")) {
		return System::getInstance()->getConfigurationManager()->get("cloud:ana-url")->string();
	}

	return "https://api.linewize.net:8184";
}

void CloudAnalyticsListener::handle(EventPtr event) {
	if (CLOUD_EVENTS_ENABLED != 1) {
		return;
	}

	holdLock();
	bufferedEvents.push_back(event);
	releaseLock();

	if (bufferedEvents.size() > (size_t) CLOUD_EVENTS_PUSH_THRESHOLD) {
		flushEvents();
	}
}

void CloudAnalyticsListener::operate(map<string, double> metrics){
	Logger::instance()->log("sphirewalld.cloudanalyticslistener.operate", INFO, "pushing metrics to cloud");	

	ObjectContainer *obj = new ObjectContainer(CREL);
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	for (pair<string, double> metric : metrics) {
		ObjectContainer *sevent = new ObjectContainer(CREL);
		sevent->put("time", new ObjectWrapper((double) time(NULL)));
		sevent->put("key", new ObjectWrapper((string) metric.first));
		sevent->put("value", new ObjectWrapper((double) metric.second));
		arr->put(new ObjectWrapper(sevent));
	}

	obj->put("items", new ObjectWrapper(arr));
	obj->put("deviceid", new ObjectWrapper((string) getDeviceId()));
	obj->put("token", new ObjectWrapper((string) getToken()));
	obj->put("type", new ObjectWrapper((string) "METRICS"));
	ObjectWrapper *root = new ObjectWrapper(obj);

	std::string sMsg = JsonSerialiser::serializeToJsonString(root);
	delete root;

	try {
		HttpRequestWrapper http(getAnalyticsUrl(), POST);
		http.postdata = sMsg;
		http.execute();

		Logger::instance()->log("sphirewalld.cloudanalyticslistener.operate", INFO, "finished pushing metrics to cloud");
	}
	catch (exception &e) {
		Logger::instance()->log("sphirewalld.cloudanalyticslistener.operate", ERROR,
				"an error occurred while pushing metrics, reason = %s", e.what());
	}
}

void CloudAnalyticsListener::flushEvents() {
	holdLock();

	if (bufferedEvents.size() == 0) {
		releaseLock();
		return;
	}

	Logger::instance()->log("sphirewalld.cloudanalyticslistener.flushEvents", INFO,
			"flushing event system, %d begin pushed", bufferedEvents.size());

	ObjectContainer *obj = new ObjectContainer(CREL);
	ObjectContainer *arr = new ObjectContainer(CARRAY);

	for (EventPtr event : bufferedEvents) {
		ObjectContainer *sevent = new ObjectContainer(CREL);
		sevent->put("time", new ObjectWrapper((double) event->t));
		sevent->put("key", new ObjectWrapper((string) event->key));

		ObjectContainer *data = new ObjectContainer(CARRAY);
		map<string, Param> params = event->params.getParams();

		for (map<string, Param>::iterator piter = params.begin(); piter != params.end(); piter++) {
			Param p = piter->second;
			ObjectContainer *po = new ObjectContainer(CREL);

			if (p.isString()) {
				po->put(piter->first, new ObjectWrapper((string) p.string()));
			}

			if (p.isNumber()) {
				po->put(piter->first, new ObjectWrapper((double) p.number()));
			}

			data->put(new ObjectWrapper(po));
		}

		sevent->put("params", new ObjectWrapper(data));
		arr->put(new ObjectWrapper(sevent));
	}

	obj->put("items", new ObjectWrapper(arr));
	obj->put("deviceid", new ObjectWrapper((string) getDeviceId()));
	obj->put("token", new ObjectWrapper((string) getToken()));
	obj->put("type", new ObjectWrapper((string) "EVENTS"));
	ObjectWrapper *root = new ObjectWrapper(obj);

	std::string sMsg = JsonSerialiser::serializeToJsonString(root);
	delete root;

	try {
		HttpRequestWrapper http(getAnalyticsUrl(), POST);
		http.postdata = sMsg;
		http.execute();

		Logger::instance()->log("sphirewalld.cloudanalyticslistener.flushEvents", INFO,
				"finished flushing event system, %d begin pushed", bufferedEvents.size());

		bufferedEvents.clear();
	}
	catch (exception &e) {
		Logger::instance()->log("sphirewalld.cloudanalyticslistener.flushEvents", ERROR,
				"an error occurred while flushing events, reason = %s", e.what());
	}

	releaseLock();
}
