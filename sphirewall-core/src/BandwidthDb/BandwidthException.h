/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BANDWITHEXCEPTION_H
#define BANDWITHEXCEPTION_H

#include <exception>
#include <string>

class BandwithException : std::exception {
	public:
		BandwithException();
		BandwithException(const BandwithException &other);
		BandwithException(std::string what);
		~BandwithException() throw();
		BandwithException &operator=(const BandwithException &other);
		bool operator==(const BandwithException &other);
		std::string what();
		std::string exception_value;
};

#endif // BANDWITHEXCEPTION_H
