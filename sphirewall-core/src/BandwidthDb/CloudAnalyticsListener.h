#include <map>
#include <list>
#include <vector>
#include <queue>

#ifndef SPHIREWALL_CLOUD_ANA_H_INCLUDED
#define SPHIREWALL_CLOUD_ANA_H_INCLUDED
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include "BandwidthDb/BandwidthListener.h"
#include "Core/Event.h"
#include "Utils/Lock.h"
#include "Core/Cron.h"
#include "Core/SysMonitor.h"

class ConnectionStatistics;
class AnalyticsClient;
class Lock;

class CloudAnalyticsListenerException : public std::exception {
	public:
		CloudAnalyticsListenerException() noexcept {}
		virtual ~CloudAnalyticsListenerException() noexcept {}
		virtual const char *what() const noexcept {
			return "The remote service could not persist the data";
		}
};

class CloudAnalyticsListener : public Lockable, public virtual MetricHandler, public virtual EventHandler, public virtual BandwidthListener {
	public:
		CloudAnalyticsListener();
		int push(std::vector<ConnectionStatistics *> input);
		void operate(std::map<std::string, double> input);

		bool enabled();
		std::string getHostname();
		std::string getAnalyticsUrl();
		std::string getDeviceId();
		std::string getToken();
		int getPort();

		bool verify_certificate(bool preverified, boost::asio::ssl::verify_context &ctx);

		std::string name();

		//EventHandler Methods:
		CloudAnalyticsListener(const CloudAnalyticsListener &h) : EventHandler(h) {}
		void handle(EventPtr event);
		int getId() {
			return 99;
		}

		std::string key() const {
			return "handler.cloud";
		}

		void flushEvents();
		bool dontremove(){
			return true;
		}
	private:
		std::vector<ConnectionStatistics *> errors;
		std::string sendAndRecv(std::string);
		std::list<EventPtr> bufferedEvents;

		static int CLOUD_EVENTS_PUSH_THRESHOLD;
		static int CLOUD_EVENTS_ENABLED;
};

class CloudAnalyticsListenerEventFlushCron : public CronJob {
	public:
		CloudAnalyticsListenerEventFlushCron(CloudAnalyticsListener *root)
			: CronJob(60 * 5, "CLOUD_EVENTS", true) {

			this->root = root;
		}
		void run() {
			root->flushEvents();
		}

	private:
		CloudAnalyticsListener *root;
};

#endif
