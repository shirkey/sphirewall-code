/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ANALYTICS_CLIENT
#define ANALYTICS_CLIENT
#include <vector>
#include <map>
#include <string>
#include "BandwidthDb/BandwidthListener.h"

class taInputSlot;
class InterfaceManager;
class SysMonitorInstance;
class Time;
class JSONObject;
class JSONValue;
class ConnectionStatistics;


class AnalyticsClient : public virtual BandwidthListener {
	public:
		AnalyticsClient();
		bool enabled();

		void addTaInputSlot(std::vector<ConnectionStatistics *> &input);
		void addMetricSnapshot(std::map<std::string, double> metrics);
		long getBandwidthByUser(std::string username, Time start, Time finish);
		long getTotalTransfer(Time start, Time finish);

		std::string sendAndRecv(std::string input);
		int push(std::vector<ConnectionStatistics *> input);
		JSONValue *request(std::string uri, JSONObject args);

		std::string name();
};

#endif
