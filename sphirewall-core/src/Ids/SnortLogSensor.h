/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SNOR_LOG_SENSOR
#define SNORT_LOG_SENSOR

#include "Ids.h"
#include <boost/regex.hpp>

class SnortLogSensor : public IdsLogSensor {
	public:

		SnortLogSensor(IDS *ids) : IdsLogSensor(ids) {
		}

		std::string description() {
			return "Snort alert log sensor";
		}

		std::string type();
		std::string filename();
		bool active();
		boost::regex rx() {
			return boost::regex("");
		}

		void parseIn(std::string line);

	private:
		void alert();
};

#endif
