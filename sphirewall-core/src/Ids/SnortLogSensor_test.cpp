/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtest/gtest.h>

using namespace std;

#include "Ids/SnortLogSensor.h"
#include "Core/Logger.h"
#include "Core/Event.h"

TEST(SnortLogSensor, parseIn) {
	IDS *ids = new IDS(new EventDb());

	SnortLogSensor *sensor = new SnortLogSensor(ids);
	sensor->parseIn("");

	sensor->parseIn("[**] [1:1418:11] SNMP request tcp [**]");
	sensor->parseIn("[Classification: Attempted Information Leak] [Priority: 2]");
	sensor->parseIn("07/30-03:10:19.995171 10.0.0.1:44473 -> 10.0.0.42:161");
	sensor->parseIn("TCP TTL:51 TOS:0x0 ID:13654 IpLen:20 DgmLen:44");

	sensor->parseIn("[**] [1:1418:11] SNMP request tcp [**]");
	sensor->parseIn("[Classification: Attempted Information Leak] [Priority: 2]");
	sensor->parseIn("07/30-03:10:19.995171 10.0.0.1:44473 -> 10.0.0.42:161");
	sensor->parseIn("TCP TTL:51 TOS:0x0 ID:13654 IpLen:20 DgmLen:44");

}
