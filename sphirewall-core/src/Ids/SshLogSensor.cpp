/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>

using namespace std;

#include "Ids/SshLogSensor.h"
#include "Ids/Ids.h"
#include "Core/Event.h"
#include "Utils/IP4Addr.h"

#include <boost/regex.hpp>

SshLogSensor::SshLogSensor(IDS *ids)
	: IdsLogSensor(ids)
{}

string SshLogSensor::description()  {
	return "SSH Brute Force Attack";
}

string SshLogSensor::filename()  {
	return "/var/log/auth.log";
}

boost::regex SshLogSensor::rx() {
	static boost::regex e("(.*)Failed password for\\s+([a-z]+)\\sfrom\\s+([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9])\\s(.*)");
	return e;
}

class SshAttempt {
	public:
		const static int THRESHOLD = 3;

		SshAttempt() {
			noAttempts = 0;
			noAttempts++;
			attemptTime = time(NULL);
		}

		std::string source;

		bool hasExpired() {
			if ((time(NULL) - attemptTime) > THRESHOLD) {
				return true;
			}

			return false;
		}

		int noAttempts;
	private:
		time_t attemptTime;
};

map<std::string, SshAttempt *> attempts;

void SshLogSensor::parseIn(std::string line) {

	boost::smatch what;

	if (boost::regex_match(line, what, rx(), boost::match_extra)) {
		SshAttempt *attempt = attempts[what[3]];

		if (attempt && !attempt->hasExpired()) {
			if (++attempt->noAttempts >= SshAttempt::THRESHOLD) {
				alert(IP4Addr::stringToIP4Addr(what[3]), 0);
			}
		}
		else {
			if (attempt) {//It must have expired
				map<string, SshAttempt *>::iterator iter = attempts.find(what[3]);
				attempts.erase(iter);

				delete attempt;
			}

			attempt = new SshAttempt();
			attempt->source = what[3];
			attempts[what[3]] = attempt;
		}
	}
}

std::string SshLogSensor::type() {
	return IDS_SSHD_BRUTEFORCE;
}
