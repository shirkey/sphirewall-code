/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

#include "Ids/SnortLogSensor.h"
#include "Ids/Ids.h"
#include "Core/ConfigurationManager.h"
#include "Core/System.h"
#include "Core/Event.h"
#include "Utils/IP4Addr.h"

struct SnortAlert {
	std::string description;
	int priority;
	std::string source;
	std::string destination;
};

boost::regex descLine("\\[\\*\\*\\]\\s*\\s*\\[([\\d:]+)\\]\\s*([A-Z,a-z,\\(,\\),\\s]+)\\s*\\[\\*\\*\\]");
boost::regex priorityLine("\\[Classification: ([a-z,A-Z,\\s]+)\\] \\[Priority: ([\\d]+)\\]");
boost::regex addressLine("([\\d+\\/]+)-([\\d:]+)\\.(\\d+) ([\\d.]+):[0-9]+ -> ([\\d.]+):[0-9]+");

SnortAlert *snortAlert = NULL;

std::string SnortLogSensor::filename() {
	//return System::getInstance()->configurationManager.get("general:snort_file")->string();
	return "/var/log/syslog";
}

bool SnortLogSensor::active() {
	//return System::getInstance()->configurationManager.get("general:snort_enabled")->boolean();
	return true;
}

void SnortLogSensor::alert() {
	unsigned int source = IP4Addr::stringToIP4Addr(snortAlert->source);
	unsigned int dest = IP4Addr::stringToIP4Addr(snortAlert->destination);

	if (!idsPtr->checkExceptions(source) && !idsPtr->checkExceptions(dest)) {
		std::string realType = IDS_SNORT_ALERT;

		switch (snortAlert->priority) {
		case 1:
			realType = IDS_SNORT_ALERT_1;
			break;

		case 2:
			realType = IDS_SNORT_ALERT_2;
			break;

		case 3:
			realType = IDS_SNORT_ALERT_3;
			break;

		case 4:
			realType = IDS_SNORT_ALERT_4;
			break;

		case 5:
			realType = IDS_SNORT_ALERT_5;
			break;

		default:
			realType = IDS_SNORT_ALERT;
		};

		EventParams params;

		params["source"] = IP4Addr::ip4AddrToString(source) ;

		params["destination"] = IP4Addr::ip4AddrToString(dest);

		params["desc"] = snortAlert->description;

		params["priority"] = snortAlert->priority;

		idsPtr->getEventDb()->add(new Event(realType, params));
	}
}

void SnortLogSensor::parseIn(std::string line) {
	if (snortAlert) {
		boost::smatch what;

		if (boost::regex_match(line, what, priorityLine, boost::match_extra)) {
			string pri = what[2];
			snortAlert->priority = stoi(pri);
		}

		if (boost::regex_match(line, what, addressLine, boost::match_extra)) {
			snortAlert->source = what[4];
			snortAlert->destination = what[5];

			alert();
			delete snortAlert;
			snortAlert = NULL;
		}

	}
	else {
		boost::smatch what;

		if (boost::regex_match(line, what, descLine, boost::match_extra)) {
			snortAlert = new SnortAlert();
			snortAlert->description = what[2];

		}
	}
}

std::string SnortLogSensor::type() {
	return IDS_SNORT_ALERT;
}
