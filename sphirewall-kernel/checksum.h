#ifndef CHECKSUM_H
#define CHECKSUM_H

extern long checksum(unsigned short *addr, unsigned int count);
extern void nat_tcp_updatechecksum(struct sk_buff* skb, struct iphdr* ip, struct tcphdr* tcp);
extern void nat_udp_updatechecksum(struct iphdr* ip, struct udphdr* udp);
extern void nat_icmp_updatechecksum(struct iphdr* ip, struct icmphdr* icmp);

#endif
