/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <asm/types.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <linux/kfifo.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <net/sock.h>
#include <net/netfilter/nf_queue.h>
#include <net/tcp.h>

#include "transport.h"

struct tcphdr* tcphdr_ptr(struct sk_buff* skb){
        return (struct tcphdr *) ((unsigned char*) ip_hdr(skb) + (ip_hdr(skb)->ihl << 2));
}

struct udphdr* udphdr_ptr(struct sk_buff* skb){
        return (struct udphdr*) ((unsigned char*) ip_hdr(skb) + (ip_hdr(skb)->ihl << 2));
}

struct icmphdr* icmphdr_ptr(struct sk_buff* skb){
        return (struct icmphdr*) ((unsigned char*) ip_hdr(skb) + (ip_hdr(skb)->ihl << 2));
}
