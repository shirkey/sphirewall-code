/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include <asm/types.h>
#include <linux/module.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/icmp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <linux/msg.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/in_route.h>
#include <linux/kfifo.h>
#include <linux/if_ether.h>
#include <net/ip.h>
#include <net/sock.h>
#include <net/netfilter/nf_queue.h>
#include <net/tcp.h>

#include "time.h"
#include "queue.h"
#include "sphirewall_queue.h"
#include "checksum.h"
#include "transport.h"
#include "nat.h"
#include "deadlock.h"

static int POST_ROUTING_HOOK_ENABLED= 1;
static int PRE_ROUTING_HOOK_ENABLED= 1;
static int FORWARDING_HOOK_ENABLED= 1;

static int IPV4_ENABLED= 1;
static int IPV6_ENABLED= 1;

static int TCP_ENABLED = 1;
static int UDP_ENABLED = 1;
static int ICMP_ENABLED = 1;

module_param(POST_ROUTING_HOOK_ENABLED, int, 0644);
module_param(PRE_ROUTING_HOOK_ENABLED, int, 0644);
module_param(FORWARDING_HOOK_ENABLED, int, 0644);

module_param(IPV4_ENABLED, int, 0644);
module_param(IPV6_ENABLED, int, 0644);

module_param(TCP_ENABLED, int, 0644);
module_param(UDP_ENABLED, int, 0644);
module_param(ICMP_ENABLED, int, 0644);

#define DIFF 0
#define SAME 1

struct qmgr* qmgr_recv = NULL;
struct qmgr* qmgr_send = NULL;

void msg_setup_and_copy_data(struct nf_queue_entry*, struct message*);
void msg_wrap_devices(struct message* msg, struct nf_queue_entry* entry);
void msg_translate_hooks(struct nf_queue_entry* entry, struct message* msg);

unsigned int hook6_func( unsigned int hooknum, 
		struct sk_buff *pskb, 
		const struct net_device *in, 
		const struct net_device *out, 
		int (*okfn)(struct sk_buff *));

/*Queue constants*/
static struct nf_hook_ops in_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_LOCAL_IN,
	.pf       = NFPROTO_IPV4,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops out_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_LOCAL_OUT,
	.pf       = NFPROTO_IPV4,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops prerouting_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_PRE_ROUTING,
	.pf       = NFPROTO_IPV4,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops postrouting_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_POST_ROUTING,
	.pf       = NFPROTO_IPV4 ,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops forward_hook = {
	.hook     = hook_func,
	.hooknum  = NF_INET_FORWARD,
	.pf       = NFPROTO_IPV4 ,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops in6_hook = {
	.hook     = hook6_func,
	.hooknum  = NF_INET_LOCAL_IN,
	.pf       = NFPROTO_IPV6,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops out6_hook = {
	.hook     = hook6_func,
	.hooknum  = NF_INET_LOCAL_OUT,
	.pf       = NFPROTO_IPV6,
	.priority = NF_IP_PRI_FIRST,
};

static struct nf_hook_ops forward6_hook = {
	.hook     = hook6_func,
	.hooknum  = NF_INET_FORWARD,
	.pf       = NFPROTO_IPV6,
	.priority = NF_IP_PRI_FIRST,
};

static const struct nf_queue_handler nfqh = {
	.outfn  = &squeue_outgoing_push,
};

struct nat_engine* nat;

void register_hooks(void){
	nat = init_nat_engine();
	nf_register_hook(&in_hook);
	nf_register_hook(&out_hook);
	nf_register_hook(&forward_hook);
	nf_register_hook(&prerouting_hook);
	nf_register_hook(&postrouting_hook);

	nf_register_hook(&in6_hook);
	nf_register_hook(&out6_hook);
	nf_register_hook(&forward6_hook);

	REGISTER_QUEUE_HANDLER_FOR_ALL_PROTOS(&nfqh);
}

void unregister_hooks(void){
	nf_unregister_hook(&in_hook);
	nf_unregister_hook(&out_hook);
	nf_unregister_hook(&forward_hook);
	nf_unregister_hook(&prerouting_hook);
	nf_unregister_hook(&postrouting_hook);

	nf_unregister_hook(&in6_hook);
	nf_unregister_hook(&out6_hook);
	nf_unregister_hook(&forward6_hook);
	UNREGISTER_QUEUE_HANDLER_FOR_ALL_PROTOS(&nfqh);
}

/*
 * Function stolen from nfnetlink_queue.c:
 * 
 * This function looks after mangling packets.
 */
static int nfqnl_mangle(void *data, int data_len, struct nf_queue_entry *e)
{
	struct sk_buff *nskb;
	int diff;

	diff = data_len - e->skb->len;
	if (diff < 0) {
		if (pskb_trim(e->skb, data_len))
			return -ENOMEM;
	}

	if (diff > 0) {
		if (data_len > 0xFFFF){
			PMESSAGE("data_len > 0xFFFF");
			return -EINVAL;
		}
		if (diff > skb_tailroom(e->skb)) {
			nskb = skb_copy_expand(e->skb, skb_headroom(e->skb),
					diff, GFP_ATOMIC);
			if (!nskb) {
				PMESSAGE("OOM in mangle, dropping packet\n");
				return -ENOMEM;
			}
			kfree_skb(e->skb);
			e->skb = nskb;
		}
		skb_put(e->skb, diff);
	}
	if (!skb_make_writable(e->skb, data_len)){
		PMESSAGE("skb_make_writable failed\n");
		return -ENOMEM;
	}
	skb_copy_to_linear_data(e->skb, data, data_len);
	e->skb->ip_summed = CHECKSUM_NONE;
	return 0;
}

void squeue_incoming_worker(struct work_struct *work)
{
	struct nf_queue_entry* entry = NULL;
	struct message_verdict* msg = NULL;
	struct iphdr* iph = NULL;
	struct natct* ct = NULL;
	struct nat_connection_tracker* tracker = NULL;

	while((msg = queue_peek(qmgr_recv)) != NULL){
		entry = entry_map_get(msg->id);
		if(entry){
			update_deadlock_detection();

			if(entry->hook == NF_INET_POST_ROUTING){
				if(msg->nat.type == NAT_SNAT){
					nat->lock(nat);

					tracker = nat->trackers[NAT_SNAT];
					ct = tracker->check(tracker, entry, SAME);
					if(!ct){
						ct = tracker->create(tracker, entry, msg);
					}
					if(ct){
						tracker->translate(tracker, ct, entry, SAME);	
					}
					nat->unlock(nat);
				}

				nf_reinject(entry, NF_ACCEPT);
				queue_pop(qmgr_recv);
				continue;
			}else if(entry->hook == NF_INET_PRE_ROUTING){	
				if(msg->nat.type == NAT_DNAT){
                                        nat->lock(nat);

					tracker = nat->trackers[NAT_DNAT];
					ct = tracker->create(tracker, entry, msg);
					if(ct){
						tracker->translate(tracker, ct, entry, SAME);	
					}
                                        nat->unlock(nat);
				}else if(msg->nat.type == NAT_ROUTE){
					nat->lock(nat);

					tracker = nat->trackers[NAT_ROUTE];
					ct = tracker->create(tracker, entry, msg);
					if(ct){
						tracker->translate(tracker, ct, entry, SAME);
					}
                                        nat->unlock(nat);
				}

				nf_reinject(entry, NF_ACCEPT);
				queue_pop(qmgr_recv);
				continue;
			}

			//Ok - Normal packet - lets deal wtih the verdict :)
			if(msg->verdict == SQ_VERDICT_ACCEPT){
				if(msg->modified == 1){
					iph = (struct iphdr*) msg->raw_packet;
					nfqnl_mangle((char*) msg->raw_packet, ntohs(iph->tot_len), entry);
					if(iph->protocol == IPPROTO_TCP){ 
						nat_tcp_updatechecksum(entry->skb, ip_hdr(entry->skb), tcphdr_ptr(entry->skb));
					}else if(iph->protocol == IPPROTO_UDP){
						nat_udp_updatechecksum(ip_hdr(entry->skb), udphdr_ptr(entry->skb));
					}else if(iph->protocol == IPPROTO_ICMP){
						nat_icmp_updatechecksum(ip_hdr(entry->skb), icmphdr_ptr(entry->skb));
					}

					nf_reinject(entry, NF_ACCEPT);

				}else{
					nf_reinject(entry, NF_ACCEPT);
				}
			}else if (msg->verdict == SQ_VERDICT_DROP){
				nf_reinject(entry, NF_DROP);
			}else if (msg->verdict == SQ_VERDICT_RESET_OR_DROP){
				iph = (struct iphdr*) ip_hdr(entry->skb);
				if(iph->protocol == IPPROTO_TCP){
					tcphdr_ptr(entry->skb)->rst= 1;
					nat_tcp_updatechecksum(entry->skb, ip_hdr(entry->skb), tcphdr_ptr(entry->skb));
					nf_reinject(entry, NF_ACCEPT);
				}else{ 
					nf_reinject(entry, NF_DROP);
				}
			}

			queue_pop(qmgr_recv);	
		}
	}
}

unsigned int hook6_func(unsigned int hooknum, 
		struct sk_buff *pskb, 
		const struct net_device *in, 
		const struct net_device *out, 
		int (*okfn)(struct sk_buff *)){

	if(IPV6_ENABLED == 0){
		return DEFAULT_ACTION;
	}	

	if(hooknum == NF_INET_POST_ROUTING && POST_ROUTING_HOOK_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(hooknum == NF_INET_PRE_ROUTING && PRE_ROUTING_HOOK_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(hooknum == NF_INET_FORWARD && FORWARDING_HOOK_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(ip_hdr(pskb)->protocol == IPPROTO_TCP && TCP_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(ip_hdr(pskb)->protocol == IPPROTO_UDP && UDP_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(ip_hdr(pskb)->protocol == IPPROTO_ICMP && ICMP_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(qmgr_send->state == STATE_ONLINE && qmgr_recv->state == STATE_ONLINE){
		if((in && in->ifindex == 1) || (out && out->ifindex == 1)){
			return NF_ACCEPT;
		}

		pskb->cb[0] = 1;
		return NF_QUEUE;
	}

	/*
	   This means the module is not active: Perhaps we have some outstanding packets though
	 */

	return DEFAULT_ACTION;
} 

unsigned int hook_func(	unsigned int hooknum, 
		struct sk_buff *pskb, 
		const struct net_device *in, 
		const struct net_device *out, 
		int (*okfn)(struct sk_buff *)){

	//Check enabled hooks:
	if(IPV4_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(hooknum == NF_INET_POST_ROUTING && POST_ROUTING_HOOK_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(hooknum == NF_INET_PRE_ROUTING && PRE_ROUTING_HOOK_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(hooknum == NF_INET_FORWARD && FORWARDING_HOOK_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(ip_hdr(pskb)->protocol == IPPROTO_TCP && TCP_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(ip_hdr(pskb)->protocol == IPPROTO_UDP && UDP_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(ip_hdr(pskb)->protocol == IPPROTO_ICMP && ICMP_ENABLED == 0){
		return DEFAULT_ACTION;
	}

	if(qmgr_send->state == STATE_ONLINE && qmgr_recv->state == STATE_ONLINE){
		if((in && in->ifindex == 1) || (out && out->ifindex == 1)){
			return NF_ACCEPT;
		}
		pskb->cb[0] = 0;
		return NF_QUEUE;
	}

	/*
	   This means the module is not active: Perhaps we have some outstanding packets though
	 */
	return DEFAULT_ACTION;
} 

int squeue_outgoing_push(struct nf_queue_entry *entry, unsigned int queuenum){
	local_bh_disable();
	send_queue_push_entry(entry);
	local_bh_enable();

	queue_work(outgoing_packet_worker_wq, &outgoing_packet_worker);

	return 0;
}

void squeue_outgoing_worker(struct work_struct *work){
	struct natct* ct = NULL;
	struct natct* sct = NULL;
	struct natct* dct = NULL;
	struct nf_queue_entry* entry = NULL;
	struct message* msg = NULL;
	struct nat_connection_tracker* route_tracker = NULL;
	struct nat_connection_tracker* dnat_tracker = NULL;
	struct nat_connection_tracker* tracker = NULL;

	if(qmgr_send->state == STATE_OFFLINE){
		return;
	}

	while(!send_queue_empty()){
		poll_deadlock_detection();
		entry = send_queue_pop_entry();	
		if(entry){	
			if(entry->hook == NF_INET_PRE_ROUTING){
				nat->lock(nat);
				route_tracker = nat->trackers[NAT_ROUTE];
				ct= route_tracker->check(route_tracker, entry, SAME);
				if(ct){
					route_tracker->translate(route_tracker, ct, entry, SAME); 
					nat->unlock(nat);
					nf_reinject(entry, NF_ACCEPT);
					continue;
				}

				dnat_tracker = nat->trackers[NAT_DNAT];
				ct = dnat_tracker->check(dnat_tracker, entry, SAME);
				if(ct){
					dnat_tracker->translate(dnat_tracker, ct, entry, SAME);
					nat->unlock(nat);
					nf_reinject(entry, NF_ACCEPT);
					continue;
				}


				//Check SNAT connection tracker					
				tracker = nat->trackers[NAT_SNAT];
				sct = tracker->check(tracker, entry, DIFF);	
				if(sct){
					tracker->translate(tracker, sct, entry, DIFF);
					nat->unlock(nat);
					nf_reinject(entry, NF_ACCEPT);
					continue;
				}
				nat->unlock(nat);

				//If we are not a new connection, then give it back to the kernel
				if(ip_hdr(entry->skb)->protocol == IPPROTO_TCP){
					if(tcphdr_ptr(entry->skb)->syn != 1){
						nf_reinject(entry, NF_ACCEPT);
						continue;
					}
				}
			}

			if(entry->hook == NF_INET_POST_ROUTING){
				//Check SNAT connection tracker
				nat->lock(nat);

				tracker = nat->trackers[NAT_SNAT];
				ct = tracker->check(tracker, entry, SAME);
				if(ct){
					tracker->translate(tracker, ct, entry, SAME);
					nat->unlock(nat);
					nf_reinject(entry, NF_ACCEPT);
					continue;
				}

				//Check DNAT connection tracker
				dnat_tracker = nat->trackers[NAT_DNAT];
				dct = dnat_tracker->check(dnat_tracker, entry, DIFF);
				if(dct){
					dnat_tracker->translate(dnat_tracker, dct, entry, DIFF);
					nat->unlock(nat);
					nf_reinject(entry, NF_ACCEPT);
					continue;
				}

				nat->unlock(nat);
				if(ip_hdr(entry->skb)->protocol == IPPROTO_TCP){
					if(tcphdr_ptr(entry->skb)->syn != 1){
						nf_reinject(entry, NF_ACCEPT);
						continue;
					}
				}

			}

			if(qmgr_send->state == STATE_OFFLINE){
				nf_reinject(entry, DEFAULT_ACTION);
				break;
			}

			msg = queue_enqueue(qmgr_send);	
			if(msg == NULL){
				break;
			}

			msg_setup_and_copy_data(entry, msg);
			msg_wrap_devices(msg, entry);
			msg_translate_hooks(entry, msg);

			msg->id = entry_map_add(entry);
			if(msg->id == -1){
				msg->id = entry_map_add(entry);
				if(msg->id == -1){
					nf_reinject(entry, NF_DROP);
					return;
				}
			}	

			queue_push(qmgr_send);
		}
	}
}

void msg_setup_and_copy_data(struct nf_queue_entry* entry, struct message* msg){
	static int x = 0;
	msg->packet.id = entry->id = ++x % INT_MAX;
	msg->packet.len = entry->skb->len;
	memcpy( msg->packet.raw_packet, (unsigned char *)skb_network_header(entry->skb),msg->packet.len);
	msg->packet.type = entry->skb->cb[0];
}

void msg_wrap_devices(struct message* msg, struct nf_queue_entry* entry){
	struct net_device* indev = NULL;
	struct net_device* outdev = NULL;
	struct sk_buff* skb = NULL;

	skb = entry->skb;
	indev = entry->indev;
	outdev = entry->outdev;

	if(indev){
		msg->packet.indev = indev->ifindex;
	}else{
		msg->packet.indev = 0;
	}

	if(outdev){
		msg->packet.outdev = outdev->ifindex;
	}else{
		msg->packet.outdev = 0;
	}

	if (indev && skb->dev && skb->network_header != skb->mac_header) {

		msg->packet.hwlen = dev_parse_header(skb, (unsigned char*)&msg->packet.hw_addr);
	}else{
		msg->packet.hwlen = 0;
	}
}

void msg_translate_hooks(struct nf_queue_entry* entry, struct message* msg){
	if(!entry || !msg){
		PMESSAGE("!entry or !msg in msg_translate_hooks\n");
		return;
	}

	switch(entry->hook){
		case NF_INET_PRE_ROUTING:
			msg->hook = 1;
			break;

		case NF_INET_LOCAL_IN:
			msg->hook = 2;
			break;

		case NF_INET_FORWARD:
			msg->hook = 2;
			break;

		case NF_INET_LOCAL_OUT:
			msg->hook = 2;
			break;

		case NF_INET_POST_ROUTING:
			msg->hook = 3;
			break;
	}
}

	
