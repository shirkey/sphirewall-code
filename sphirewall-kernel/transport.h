#ifndef TRANSPORT_H
#define TRANSPORT_H

extern struct tcphdr* tcphdr_ptr(struct sk_buff* skb);
extern struct udphdr* udphdr_ptr(struct sk_buff* skb);
extern struct icmphdr* icmphdr_ptr(struct sk_buff* skb);

#endif
