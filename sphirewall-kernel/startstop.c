/*
Copyright Michael Lawson
This file is part of sphirewall-kernel for Sphirewall.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/module.h>
#include <net/sock.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <asm/types.h>
#include <linux/skbuff.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/in.h>
#include <linux/netfilter_ipv4.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/netfilter.h>
#include <linux/kfifo.h>
#include <net/netfilter/nf_queue.h>
#include <linux/ipc.h>
#include <linux/types.h>
#include <linux/msg.h>
#include <linux/kallsyms.h>
#include <linux/types.h>    // uint64_t //
#include <linux/kthread.h>  // kthread_run, kthread_stop //
#include <linux/delay.h>    // msleep_interruptible //
#include <linux/syscalls.h> // sys_msgget //

#include "queue.h"
#define VERSION "0.9.9.68"

struct workqueue_struct* incomming_packets_worker_wq;
struct workqueue_struct* outgoing_packet_worker_wq;
static int SQVERSION_1 = 9945;

module_param(SQVERSION_1, int, 0600);

static int __init sphirewall_queue_init(void)
{
	PMESSAGE("Initializing Sphirewall Kernel Queues (version: " VERSION ")\n");
	PMESSAGE("GPLv2 The Sphirewall Team\n");
	PMESSAGE("Loading Module Threads\n");
	
	qmgr_send = kmalloc(sizeof(struct qmgr), GFP_KERNEL);
	qmgr_recv = kmalloc(sizeof(struct qmgr), GFP_KERNEL);
	qmgr_send->state = STATE_OFFLINE;
	qmgr_recv->state = STATE_OFFLINE;

	incomming_packets_worker_wq= create_singlethread_workqueue("incomming_packets_worker_wq");
	outgoing_packet_worker_wq = create_singlethread_workqueue("outgoing_packet_worker_wq");

	send_queue_init();
	register_hooks();
	if(open_shm() < 0){
		PMESSAGE("Fatal error loading module, debugfs enabled in the kernel\n");
		return -1;
	}
	PMESSAGE( "Finished Loading Threads: Ready for Connections\n");

	return 0;
}

static void __exit sphirewall_queue_exit(void)
{
	PMESSAGE("Module Unloading\n");
	unregister_hooks();

        close_shm();		
	PMESSAGE("Unloaded\n");
	return;
}

module_init(sphirewall_queue_init);
module_exit(sphirewall_queue_exit);

MODULE_DESCRIPTION("Sphirewall Kernel Queues (version: " VERSION ")");
MODULE_LICENSE("GPL");

