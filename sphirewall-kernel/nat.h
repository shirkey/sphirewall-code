#ifndef NAT_H
#define NAT_H

#define SNAT_PORT_POOL_IP_SIZE 100
#define SNAT_PORT_POOL_PORT_MAX 65000 
#define SNAT_PORT_POOL_PORT_MIN 10000 
#define NOT_CHECKED_OUT 0 
#define CHECKED_OUT 1 

#define NAT_CONNTRACKER_MAXCONNS 90000
#define DIFF 0
#define SAME 1
#define TCPUP 1
#define CLOSING 2

#define MAX_PROTOCOL_HANDLERS 256

struct nat_engine {
	struct nat_connection_tracker* trackers[5];
	void (*lock)(struct nat_engine* _self);
	void (*unlock)(struct nat_engine* _self);
	spinlock_t lock_object;
};

extern struct nat_engine* init_nat_engine(void);

struct nat_connection_tracker_protocol_handler;
struct natct {
        //Translate it to this
        struct list_head list;
        int type;
        long target_address;
        int target_port;
        int protocol;
        int state;

        long source_address;
        long destination_address;
        int source_port;
        int destination_port;
        int icmp_id;

        int timestamp;
        int fin;
        int hash;
        int routemark;

	struct nat_connection_tracker_protocol_handler* protocol_handler;
};

struct nat_connection_tracker_protocol_handler {
        int (*hash)(struct nf_queue_entry* entry, int direction);
        void (*translate)(struct natct* ct, struct nf_queue_entry* entry, int direction);
        struct natct* (*create)(struct nat_connection_tracker_protocol_handler* _self, struct nf_queue_entry* entry, struct message_verdict* m);
        bool (*match)(struct natct* target, struct nf_queue_entry* entry, int direction);
        bool (*expired)(struct nat_connection_tracker_protocol_handler* _self, struct natct* target);
};

struct nat_connection_tracker {
        struct list_head store[NAT_CONNTRACKER_MAXCONNS];
        struct nat_connection_tracker_protocol_handler* protocol_handlers[MAX_PROTOCOL_HANDLERS];
	int last_gc_run;

        struct natct* (*check)(struct nat_connection_tracker* _self, struct nf_queue_entry* entry, int direction);
        void (*translate)(struct nat_connection_tracker* _self, struct natct* ct, struct nf_queue_entry* entry, int direction);
        struct natct* (*create)(struct nat_connection_tracker* _self, struct nf_queue_entry* entry, struct message_verdict* m);

	void (*free_state)(struct nat_connection_tracker* _self, struct natct* ct);
	void (*run_gc)(struct nat_connection_tracker* _self);
	bool (*should_run_gc)(struct nat_connection_tracker* _self);
};

#endif
