/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Main header file for the sphirewall_queue module. Not to exported and used externally */

#ifndef QUEUE_H
#define QUEUE_H

#define DEFAULT_ACTION NF_ACCEPT
#define NETLINK_TEST 19

#define VFW_GROUP 1

#ifdef DEBUG
#define PDEBUG(args...) printk (args)
#else
#define PDEBUG(args...)
#endif

#define PMESSAGE(args...) printk(args)

/* struct message slab cache wrapper functions. 
 * Used to quickly alloc and dealloc memory for the message structure, which is 1600 bytes long.
 */
extern void init_message_cache(void);
extern void destroy_message_cache(void);
extern struct message* get_message_ptr(void);
extern void free_message_ptr(struct message* ptr);

/* Hash map used to store nf_queue_entries based on id, that are waiting on a response from the client */
extern int entry_map_add(struct nf_queue_entry *entry);
extern struct nf_queue_entry *entry_map_get(int x);

/* Setup and tear down the netlink interrupt handler*/
extern void setup_nl_sk(void);
extern void teardown_nl_sk(void);

/* Processing queue*/
extern void send_queue_init(void);
extern int send_queue_empty(void);
extern void send_queue_push_entry(struct nf_queue_entry *entry);
extern struct nf_queue_entry *send_queue_pop_entry(void);


extern void nl_rcv(struct sk_buff *nl_skb);
extern int squeue_send_packet(struct nf_queue_entry *entry);
extern int squeue_process_new_packet(void* data);

static int send_queue_alive = 0;

extern unsigned int hook_func(unsigned int hooknum,
		struct sk_buff *pskb,
		const struct net_device *in,
		const struct net_device *out,
		int (*okfn)(struct sk_buff *));

extern int squeue_add_packet(struct nf_queue_entry *entry,
		unsigned int queuenum);

/* Setup and tear down the queues and hooks*/
extern void register_hooks(void);
extern void unregister_hooks(void);
#endif
