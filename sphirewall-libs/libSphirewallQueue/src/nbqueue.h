/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QQ_QUEUE
#define QQ_QUEUE

#define DO_YIELD() pthread_yield() 

#include <pthread.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <time.h>

extern int send_nbqueue_configfd;
pthread_mutex_t queue_recv_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t queue_recv_lock_wait_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t queue_send_lock = PTHREAD_MUTEX_INITIALIZER;
struct meta {
        volatile int head;
        volatile int tail;
        volatile int capacity;
        volatile int elem_size;
        volatile int offset;
	volatile int closing;
	volatile int pid;
};

struct meta* queue_connect(void* memory_block){
        struct meta* queue = (struct meta*) memory_block;
	queue->pid = getpid();
        return queue;
}

struct message_verdict* queue_enqueue(struct meta* q){
	pthread_mutex_lock(&queue_send_lock);
        while ((q->tail + 1) % (q->capacity + 1) == q->head)
		DO_YIELD();
	return (struct message_verdict*) (((char*) q) + (q->elem_size * q->tail) + q->offset);
}

void queue_enqueue_push(struct meta* q){
	q->tail = (q->tail + 1) % (q->capacity + 1);
        
	char buf[0];
        ioctl(send_nbqueue_configfd, 100, buf);
	pthread_mutex_unlock(&queue_send_lock);
}

void queue_pop(struct meta* queue){
	queue->head = (queue->head + 1) % (queue->capacity + 1);
}

struct message* queue_peek(struct meta* queue){
	static int deSleepTimer = 0;
	while (queue->head == queue->tail && queue->closing != 1){
		return NULL;
	}

	if(queue->closing == 1){
		return NULL;
	}

	void* addr = ((char*)queue) + (queue->elem_size * queue->head) + queue->offset;
	struct message* m = (struct message*) addr;
	m->packet.raw_packet = (unsigned char*) ((char*) addr) + sizeof(struct message);
	return m;
}

#endif

