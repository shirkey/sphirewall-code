/*
   Copyright Michael Lawson
   This file is part of Sphirewall.

   Sphirewall is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Sphirewall is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#include <string.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <queue>
#include <asm/types.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include  <sys/types.h> // key_t //
#include  <sys/ipc.h>   // IPC_CREAT, ftok //
#include  <sys/msg.h>   // msgget, msgbuf //
#include <netinet/udp.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <linux/icmp.h>
#include <linux/if_ether.h>
#include <map>
#include <linux/netfilter.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "../includes/sphirewall_queue.h"
#include "../includes/client.h"
#include "nbqueue.h"

#define SHM_SIZE 4096 * 64 
pthread_mutex_t molock = PTHREAD_MUTEX_INITIALIZER;

/*Static function pointer - callback for processing the packets*/
int (*SqClient::filter_hook)(struct sq_packet* packet, bool &modified) = NULL;
struct NatInstruction* (*SqClient::prerouting_hook)(struct sq_packet* packet) = NULL;
struct NatInstruction* (*SqClient::postrouting_hook)(struct sq_packet* packet) = NULL;

/*Map/Queue for packets that are queued for processing*/
std::map<int, struct message_verdict*> message_store;

void dump_packet(unsigned char* packet);

/*Processing sleep inteval - spinlock styles. Decrease to reduce latency, increase to reduce cpu time. Hard to find the best value*/
int SqClient::sleepInterval = 500;

void* process_packet(void*);
/*Queues*/
struct meta* send_nbqueue;
struct meta* recv_nbqueue;
int send_nbqueue_configfd;
int recv_nbqueue_configfd;

int SqClient::life = -1;
int x = 0;
int* SqClient::yieldCountSetting = NULL;
int* SqClient::usleepInterval = NULL;

void queue_wakeup(int sig){
	if(pthread_mutex_trylock(&queue_recv_lock) == 0){
		process_packet(NULL);	
		pthread_mutex_unlock(&queue_recv_lock);
	}
}

int file_exists (char *filename)
{
	struct stat   buffer;   
	return (stat (filename, &buffer) == 0);
}

int SqClient::connect() {
	int yieldCountSettingInt = 10000;
	int usleepInt = 1000;

	setYieldCountSetting(&yieldCountSettingInt);
	setUsleepInterval(&usleepInt);

	//First check the version variable: This is a protection mech for protocol changes
	if(!file_exists("/sys/module/sphirewall_queue/parameters/SQVERSION_1")) {
		printf("could not find kernel module version param, potential mismatch between versions\n");
		return -1;
	}

	pthread_mutexattr_t attr;
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);

	pthread_mutex_init(&queue_recv_lock, &attr);
	signal(SIGUSR1, queue_wakeup);

	send_nbqueue_configfd = open("/sys/kernel/debug/sphirewall_recv", O_RDWR);
	if(send_nbqueue_configfd < 0) {
		return -1;
	}

	void* send_address = mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, send_nbqueue_configfd, 0);
	if (send_address== MAP_FAILED) {
		return -1;
	}

	recv_nbqueue_configfd = open("/sys/kernel/debug/sphirewall_send", O_RDWR);
	if(recv_nbqueue_configfd < 0) {
		return -1;
	}

	void* recv_address= mmap(NULL, SHM_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, recv_nbqueue_configfd, 0);
	if (recv_address== MAP_FAILED) {
		return -1;
	}

	send_nbqueue = queue_connect(send_address);
	recv_nbqueue = queue_connect(recv_address);

	char buf[0];
	ioctl(send_nbqueue_configfd, 101, buf); 
	ioctl(recv_nbqueue_configfd, 101, buf); 
	return 0;
}

bool process_packet_thread_open = false;

//FLOWS:
#define NO_FLOWS 5
#define CYCLE_COST 100
pthread_mutex_t enqueue_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t enqueue_lock_cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t flowqueuelock = PTHREAD_MUTEX_INITIALIZER;

void waitonenqueue(){
	pthread_mutex_lock(&enqueue_lock);

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += 1;
	pthread_cond_timedwait(&enqueue_lock_cond, &enqueue_lock, &ts);
	pthread_mutex_unlock(&enqueue_lock);
}

void wakedequeue(){
	pthread_mutex_lock(&enqueue_lock);
	pthread_cond_broadcast(&enqueue_lock_cond);
	pthread_mutex_unlock(&enqueue_lock);
}

void send_already_generated_verdict(struct message_verdict* verdict){
	struct message_verdict* verdict_message = queue_enqueue(send_nbqueue);
	memcpy(verdict_message, verdict, sizeof(struct message_verdict));
	if(verdict->modified == 1){
		memcpy(((char*) verdict_message) + sizeof(struct message_verdict), verdict->raw_packet, RAWSIZE);
	}
	queue_enqueue_push(send_nbqueue);
}

std::queue<struct message_verdict*> flows[NO_FLOWS];
void* dequeue_flows(void*){
	while(process_packet_thread_open){	
		//wake up lock
		int yieldCounter = 0;
		for(int x = NO_FLOWS; x-- > 0 ;){
			while(true){
				if(yieldCounter > CYCLE_COST){
					break;
				}	

				pthread_mutex_lock(&flowqueuelock);
				if(flows[x].empty()){
					pthread_mutex_unlock(&flowqueuelock);
					break;
				}

				struct message_verdict* target = flows[x].front();	
				if(target != NULL){
					send_already_generated_verdict(target);
					free(target->raw_packet);
					free(target); 

					flows[x].pop();
					yieldCounter++;

				}
				pthread_mutex_unlock(&flowqueuelock);
			}

			if(yieldCounter > CYCLE_COST){
				break;
			}
		}

		//a little bit inefficient - lets play later
		if(yieldCounter == 0){
			waitonenqueue();
		}		
	}
	return NULL;
}

void SqClient::run() {
	process_packet_thread_open = true;

	pthread_t tid2;
	pthread_create(&tid2, NULL, dequeue_flows, NULL);

	while(process_packet_thread_open){
		sleep(1000);
	}
}

void* process_packet(void*) {
	struct message* msgPtr = NULL; 
	while((msgPtr = queue_peek(recv_nbqueue)) != NULL){
		if(msgPtr->packet.raw_packet == NULL){
			return NULL;
		}

		msgPtr->packet.id = msgPtr->id;
		bool modified = false;
		switch (msgPtr->hook) {
			case PREROUTING:
				{
					struct NatInstruction* ins  = (*SqClient::prerouting_hook)(&msgPtr->packet);
					struct message_verdict* verdict = queue_enqueue(send_nbqueue);
					verdict->id = msgPtr->id;
					verdict->hook = msgPtr->hook;

					if(ins){
						verdict->nat.type = ins->type;
						verdict->nat.address= ins->targetAddress;

						if(ins->type == NAT_ROUTE){
							verdict->nat.field= ins->routemark;
						}else{
							verdict->nat.field= ins->targetPort;
						}

						free(ins);
					}else {
						verdict->nat.type = 0;
					}
					queue_enqueue_push(send_nbqueue);

					break;
				}

			case FILTER:
				{
					int verdict = (*SqClient::filter_hook)(&msgPtr->packet, modified);
					if(verdict == SQ_DENY){
						struct message_verdict* verdict_message = queue_enqueue(send_nbqueue);
						verdict_message->id = msgPtr->id;
						verdict_message->hook = msgPtr->hook; 
						verdict_message->verdict = SQ_VERDICT_DROP;
						queue_enqueue_push(send_nbqueue);

					}else if(verdict == SQ_QUEUE){
						pthread_mutex_lock(&molock);
						struct message_verdict* verdict_message = (struct message_verdict*) malloc(sizeof(struct message_verdict));
						unsigned char* rawpacket = (unsigned char*) malloc(RAWSIZE);

						memcpy(rawpacket, msgPtr->packet.raw_packet, RAWSIZE);	
						verdict_message->raw_packet = rawpacket;	
						verdict_message->verdict = SQ_VERDICT_ACCEPT;	
						verdict_message->id= msgPtr->id;	
						verdict_message->hook = msgPtr->hook;	
						verdict_message->modified= modified ? 1 : 0;	
						message_store[msgPtr->id] = verdict_message;
						pthread_mutex_unlock(&molock);

					}else if(verdict == SQ_ACCEPT){
						struct message_verdict* verdict_message = queue_enqueue(send_nbqueue);
						verdict_message->id = msgPtr->id;       
						verdict_message->hook = msgPtr->hook;
						verdict_message->verdict = SQ_VERDICT_ACCEPT;
						verdict_message->modified= modified ? 1 : 0; 

						if(verdict_message->modified == 1){
							memcpy(((char*) verdict_message) + sizeof(struct message_verdict), msgPtr->packet.raw_packet, RAWSIZE);
						}
						queue_enqueue_push(send_nbqueue);
					}else if(verdict == SQ_RESET){
						struct message_verdict* verdict_message = queue_enqueue(send_nbqueue);
						verdict_message->id = msgPtr->id;
						verdict_message->hook = msgPtr->hook;
						verdict_message->verdict = SQ_VERDICT_RESET_OR_DROP;
						queue_enqueue_push(send_nbqueue);

					}else{
						struct message_verdict* verdict_message = (struct message_verdict*) malloc(sizeof(struct message_verdict));
						unsigned char* rawpacket = (unsigned char*) malloc(RAWSIZE);

						memcpy(rawpacket, msgPtr->packet.raw_packet, RAWSIZE);
						verdict_message->raw_packet = rawpacket;
						verdict_message->verdict = SQ_VERDICT_ACCEPT;
						verdict_message->id= msgPtr->id;
						verdict_message->hook = msgPtr->hook;
						verdict_message->modified= modified ? 1 : 0;

						pthread_mutex_lock(&flowqueuelock);
						int queue = verdict - SQ_PRIORITY_QUEUE;
						flows[verdict - SQ_PRIORITY_QUEUE -1].push(verdict_message);
						pthread_mutex_unlock(&flowqueuelock);
						wakedequeue();
					}		
					break;
				}

			case POSTROUTING:
				{
					struct NatInstruction* ins  = (*SqClient::postrouting_hook)(&msgPtr->packet);
					struct message_verdict* verdict_message = queue_enqueue(send_nbqueue);
					verdict_message->id = msgPtr->id;
					verdict_message->hook = msgPtr->hook;

					if(ins){
						verdict_message->nat.type = ins->type;
						verdict_message->nat.address= ins->targetAddress;
						verdict_message->nat.field= ins->targetPort;
						free(ins);
					}else{
						verdict_message->nat.type = 0;
					}

					queue_enqueue_push(send_nbqueue);
					break;
				}
		}
		queue_pop(recv_nbqueue);
	}
	return NULL;
}

int SqClient::reinject(int id) {
	pthread_mutex_lock(&molock);
	if (message_store.find(id) != message_store.end()) {
		struct message_verdict* msgPtr = message_store[id];
		if (msgPtr != NULL) {
			msgPtr->verdict = 1;
			send_already_generated_verdict(msgPtr);
			message_store.erase(id);

			free(msgPtr->raw_packet);
			free(msgPtr);
		}
	} else {
		pthread_mutex_unlock(&molock);
		return -1;
	}

	pthread_mutex_unlock(&molock);
	return 0;
}

void SqClient::disconnect() {
	process_packet_thread_open = false;
	send_nbqueue->closing = 1;
	recv_nbqueue->closing = 1;

	while(send_nbqueue->closing != 2 && recv_nbqueue->closing != 2){
		sleep(2);
	}

	munmap(send_nbqueue, SHM_SIZE);	
	munmap(recv_nbqueue, SHM_SIZE);	
	close(send_nbqueue_configfd);
	close(recv_nbqueue_configfd);
}

/*
 *	Example and Test Code
 *	
 *	Compile this library as a executable, do fun stuff here, dont test on sphirewall - that would be silly now wouldnt it!!!!
 */
void dump_packet(unsigned char* packet) {
	printf("printing packet at loc: %p\n", packet);

	struct iphdr* ip = (struct iphdr*) packet;

	printf("checksum: %d\n", ntohs(ip->check));

	printf("\n==================================================\n");
	char src_ip[20], dest_ip[20];


	/* Get source and destination addresses */
	strcpy(src_ip, inet_ntoa(*(struct in_addr *) &ip->saddr));
	strcpy(dest_ip, inet_ntoa(*(struct in_addr *) &ip->daddr));
	printf("Type: ");

	if (ip->protocol == IPPROTO_TCP) {
		printf("TCP\n");
		struct tcphdr* packet_tcp_header;
		packet_tcp_header = (struct tcphdr *) (packet + (ip->ihl << 2));
		printf("dport: %d sport: %d\n", ntohs(packet_tcp_header->source), ntohs(packet_tcp_header->dest));

	} else if (ip->protocol == IPPROTO_UDP) {
		printf("UDP\n");
	}

	printf("%s -> %s\n", src_ip, dest_ip);
	printf("Length of IP data: %d\n", ntohs(ip->tot_len));
}

int process(struct sq_packet* sqp, bool & modified) {
	unsigned char* p = (unsigned char*) sqp->raw_packet;

	return SQ_PRIORITY_QUEUE + 2;
}

NatInstruction* processPreRouting(struct sq_packet* sqp) {
	//unsigned char* p = (unsigned char*) sqp->raw_packet;
	//	dump_packet(p);

	return NULL;
}

NatInstruction* processPostRouting(struct sq_packet* sqp) {
	return NULL;
}

int main(int x, char** y) {
	SqClient client;
	if(client.connect() != -1 ){

		client.registerFilterCallback(&process);
		client.registerPreroutingCallback(&processPreRouting);
		client.registerPostroutingCallback(&processPostRouting);

		//	client.setPacketLifeLimit(20);	
		client.run();
		client.disconnect();
	}
	return 0;
}

