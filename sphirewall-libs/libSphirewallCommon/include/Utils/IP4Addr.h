/*
Copyright Michael Lawson, John Deal
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_IP4ADDR_H_INCLUDED
#define SPHIREWALL_IP4ADDR_H_INCLUDED

#include <string>
#include <netinet/in.h>

using namespace std;

/*!
 * IP4Addr
 *
 * Holds information for a single IP4 address.
 *
 */
class IP4Addr {
public:
	IP4Addr();
	IP4Addr(unsigned int addr);

	~IP4Addr() {
	}

	static const long CLASS_C_MIN = 3232235520U;
	const static long CLASS_C_MAX = 3232301055U;
	const static long CLASS_B_MIN = 2886729728U;
	const static long CLASS_B_MAX = 2886795263U;
	const static long CLASS_A_MIN = 167772160;
	const static long CLASS_A_MAX = 184549375;

	const static int CLIENT_PORT_MIN = 32768;
	const static int CLIENT_PORT_MAX = 65535;

	const static long SUBNET_SINGLE_IP = 4294967292U;

	const static long MULTICAST_START = 3758096384U;
	const static long MULTICAST_STOP = 4026531839U;

	static bool isLocal(const unsigned int target);

	void setAddr(unsigned int addr) {
		address = addr;
	};

	unsigned int getAddr() {
		return address;
	};

	static const unsigned int ipAddrError = 0xffffffff;
	static string ip4AddrToString(in_addr_t addr, in_addr_t mask = (in_addr_t) - 1);
	static unsigned int buildIPAddr(const string inStr[]);
	static unsigned int stringToIP4Addr(const string addrStr);
	static int splitIPAddrString(const string ipAddrString,
			string components[], int numComponents);

	static int matchNetwork(unsigned int host, unsigned int net, unsigned int mask);

	static bool isBroadCast(unsigned int host);
	static bool isMultiCast(unsigned int host);

	static unsigned int convert(struct in_addr*);
	static std::string convertHw(const u_int8_t hw[6]);

	static bool isHostName(std::string input);
	static bool isIp(std::string address);
	static bool isHw(std::string address);
	static std::string hostname(unsigned int ip);
private:
	unsigned int address;
};

#endif
