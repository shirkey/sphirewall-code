/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALL_UTILS_H_INCLUDED 
#define SPHIREWALL_UTILS_H_INCLUDED

#include "Utils/FileUtils.h"
#include "Utils/StringUtils.h"
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <map>

class Lock;
class Timer {
public:

	Timer() : v(0) {
	}

	void start();
	void stop();

	int value();
private:
	double tnano();
	int v;

	struct timeval start_value, end_value;

};

class Sampler {
public:
	virtual double value() = 0;
	virtual void input(int i){};

        int interval;
        double buffer;
        int lastRefresh;
        double lastValue;
};

class FlexibleSecondIntervalSampler : public Sampler {
public:
	FlexibleSecondIntervalSampler();
	~FlexibleSecondIntervalSampler();
	double value();
        void input(int i);

private:
	Lock* lock;
};

class TimeSampler : public Sampler {
public:
	TimeSampler(int i);

	double value();
	void input(int i);
};

class AverageSampler : public Sampler {
public:
	AverageSampler(int i);

	double value();
	void input(int i);

private:
	int noElems;
};

class Utils {
public:
	static void systemCall(std::string call);
	static int lastHour(int i);
	static std::string timeStampToStr(int);
};

#endif
