/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPHIREWALLD_LOGGER_H_INCLUDED
#define SPHIREWALLD_LOGGER_H_INCLUDED

#include <map>
#include <queue>
#include <list>
#include <fstream>
#include <syslog.h>
#include <boost/thread.hpp>
#include "Utils/Lock.h"

typedef enum{CRITICAL_ERROR=0,ERROR=1,EVENT=2,INFO=3,DEBUG=4, CONSOLE=5} Priority;
class SyslogPriority {
	public:
		static int getSyslogPriority(Priority p){
			switch(p){
				case CRITICAL_ERROR:
					return LOG_CRIT;
			
				case ERROR:
					return LOG_ERR;
			
				case EVENT:
					return LOG_INFO;
	
				case INFO:
					return LOG_INFO;

				case DEBUG:
					return LOG_DEBUG;
		
				default:
					return LOG_DEBUG;
			};
		}
};

class Logger {
	public:
		static Logger* instance(){
			if(ins == NULL){
				ins = new Logger();
			}
			return ins;
		}

		void start(){
			new boost::thread(boost::bind(&Logger::process, this));
		}

		void process_individual_line(std::string context, Priority p, std::string msg, ...);
		void log(std::string path, Priority priority, std::string str, ...);

		void process();
		void flush();	
	
		void setLevel(Priority level){
			this->level = level;
		}

		Priority getLevel(){
			return this->level;
		}		
		bool criticalPathEnabled;
		void setRegionLevel(std::string key, Priority priority){
			priorities[key] = priority;
		}

		void delRegionLevel(std::string key){
			priorities.erase(key);
		}

		std::map<std::string, Priority> getRegionLevel(){
			return priorities;
		}

	protected:
		class Log{
			public:
				bool syslog;

				Priority priority;
				std::string context;
				std::string message;
				int timestamp;

				std::string getPriority();
		};

	private:
		Logger();
		std::list<Log> logs;

		Priority level;
		std::map<std::string, Priority> priorities;
		static Logger* ins;
		Lock* lock;
};


#endif

