/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <queue>
#include <syslog.h>
#include <sstream>
#include <map>
#include <stdarg.h>


#include "Core/Logger.h"
#include "Utils/Utils.h"
#include "Utils/TimeUtils.h"
using namespace std;

Logger* Logger::ins = NULL;
void Logger::log(std::string context, Priority p, std::string msg, ...) {
        lock->lock();

	vector<string> lines;
	split(msg, '\n', lines);
	for(std::string line : lines){
		va_list args;
		char buffer[BUFSIZ];
		va_start(args, msg);
		vsnprintf(buffer, sizeof buffer, line.c_str(), args);
		va_end(args);

		if(p == CONSOLE){
			cout << context << " :: " << buffer << endl;
		}

		//Find context to log this to:
		if (p <= level || p == CONSOLE) {
			Log nLog;
			nLog.context = context;
			nLog.priority = p;
			nLog.message = buffer;
			nLog.timestamp = time(NULL);

			logs.push_back(nLog);
		}else{
			//Check other priorities:
			for(map<string, Priority>::iterator iter = priorities.begin();
					iter != priorities.end();
					iter++){

				if(context.find(iter->first) != string::npos && p <= iter->second){
					Log nLog;
					nLog.context = context;
					nLog.priority = p;
					nLog.message = buffer;
					nLog.timestamp = time(NULL);

					logs.push_back(nLog);
					break;
				}
			}
		}
	}
	lock->unlock();
}

/*All logging is delayed to prevent slowing down main threads*/
void Logger::process() {
	while (true) {
		flush();
		sleep(1);
	}
}

void Logger::flush(){
	if(lock->tryLock()){
		openlog("sphirewall", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

		for (Log log : logs) {
			std::stringstream msg; msg << log.context << " :: " << log.message;

			int syslogPriority = SyslogPriority::getSyslogPriority(log.priority);
			syslog(syslogPriority, "%s", msg.str().c_str());

		}
		logs.clear();
		closelog();
		lock->unlock();
	}
}

	Logger::Logger()
: criticalPathEnabled(false), level(ERROR), lock(new Lock())
{}
