#include <iostream>
#include <string.h>
#include <netinet/ip6.h>
#include <arpa/inet.h>
#include "Utils/IP6Addr.h"

using namespace std;

bool IP6Addr::matches(struct in6_addr* a, struct in6_addr* b){
	return memcmp(a, b, sizeof(struct in6_addr)) == 0;
}

string IP6Addr::toString(struct in6_addr* a){
	char buffer[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, a, (char*) &buffer, INET6_ADDRSTRLEN);
	return string(buffer);	
}

struct in6_addr IP6Addr::fromString(std::string input){
	struct in6_addr ret;
	inet_pton(AF_INET6, input.c_str(), &ret);
	return ret;
}

void IP6Addr::fromString(struct in6_addr* ret, std::string input){
        inet_pton(AF_INET6, input.c_str(), ret);
}

bool IP6Addr::matchNetwork(struct in6_addr* in, struct in6_addr* subnet, int cidr){
	for(int x= 0; x < cidr / 16; x++){
		if(in->s6_addr[x] != subnet->s6_addr[x]){
			return false;
		}		
	}

	return true;
}

bool IP6Addr::valid(std::string ip){
	struct in6_addr ret;
	return inet_pton(AF_INET6, ip.c_str(), &ret) == 1;
}

int IP6Addr::hash(struct in6_addr* in){
	if(!in){
		return -1;
	}

	int cursor;
	for(uint x = 0; x < sizeof(struct in6_addr); x++){
		cursor += *((unsigned char*) in + x);
	}
	
	return cursor;
}
