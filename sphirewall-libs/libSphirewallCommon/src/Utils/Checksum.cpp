/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <stdarg.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <sstream>
#include <vector>

typedef unsigned char u8;
typedef signed char s8;
typedef unsigned short u16;
typedef signed short s16;
typedef unsigned long u32;
typedef signed long s32;

#include "Utils/Checksum.h"
using namespace std;

long Checksum::checksum(unsigned short *addr, unsigned int count) {
	register long sum = 0;

	while (count > 1) {
		sum += *addr++;
		count -= 2;
	}

	if (count > 0)
		sum += *(unsigned char *) addr;

	while (sum >> 16) {
		sum = (sum & 0xffff) + (sum >> 16);
	}
	return ~sum;
}

unsigned short tcp_sum_calc2(unsigned short len_tcp, unsigned short *src_addr, unsigned short *dest_addr, unsigned short *buff)
{
	unsigned short prot_tcp = 6;
	long sum;
	int i;
	sum = 0;

	/* Check if the tcp length is even or odd.  Add padding if odd. */
	if((len_tcp % 2) == 1){
		buff[len_tcp] = 0;  // Empty space in the ip buffer should be 0 anyway.
		len_tcp += 1; // incrase length to make even.
	}

	/* add the pseudo header */
	sum += ntohs(src_addr[0]);
	sum += ntohs(src_addr[1]);
	sum += ntohs(dest_addr[0]);
	sum += ntohs(dest_addr[1]);
	sum += len_tcp; // already in host format.
	sum += prot_tcp; // already in host format.

	/* 
	 * calculate the checksum for the tcp header and payload
	 * len_tcp represents number of 8-bit bytes, 
	 * we are working with 16-bit words so divide len_tcp by 2. 
	 */
	for(i=0;i<(len_tcp/2);i++){
		sum += ntohs(buff[i]);
	}

	// keep only the last 16 bits of the 32 bit calculated sum and add the carries
	sum = (sum & 0xFFFF) + (sum >> 16);
	sum += (sum >> 16);

	// Take the bitwise complement of sum
	sum = ~sum;

	return htons(((unsigned short) sum));
}

long Checksum::get_tcp_checksum(struct iphdr * ip, struct tcphdr * tcp) {
	int len = ntohs(ip->tot_len) - (ip->ihl << 2);
	return  (unsigned short) tcp_sum_calc2((unsigned short) len, (unsigned short *) &ip->saddr, (unsigned short *) &ip->daddr, (unsigned short *) tcp);
}

