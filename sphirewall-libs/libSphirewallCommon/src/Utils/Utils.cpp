/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <time.h>
#include <string>
#include <ctime>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <resolv.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/wait.h>
#include <signal.h>
#include <map>
#include <vector>
#include <stdio.h>
#include <sstream>
#include <uuid/uuid.h>

using namespace std;
using std::string;

#include "Utils/Utils.h"
#include "Utils/Lock.h"

string Utils::timeStampToStr(int ts) {
	time_t rawtime = ts;
	return StringUtils::trim(ctime(&rawtime));
}

/*This function uses a tm structure to modify the time and make it suitable for use in the hash functions*/
int Utils::lastHour(int i) {
	time_t t = i;
	struct tm* t_s = gmtime(&t);
	t_s->tm_sec = 0;
	//t_s->tm_min = 0;

	int stamp1 = mktime(t_s);
	if (stamp1 < 0)
		stamp1 = stamp1 + stamp1;

	return stamp1;
}

AverageSampler::AverageSampler(int interval){
	buffer = 0;
	noElems = 0;
	lastRefresh = 0;
	lastValue = 0;

	this->interval = interval;
}

double AverageSampler::value() {
	return lastValue;
}

void AverageSampler::input(int i) {
	int currentTime = time(NULL);
	//Shall we flip or add:
	if ((currentTime - lastRefresh) > interval) {
		if (noElems > 0)
			lastValue = buffer / noElems;
		lastRefresh = currentTime;
	} else {
		buffer += i;
		noElems++;
	}
}

void Timer::start() {
	gettimeofday(&start_value, NULL);
}

void Timer::stop() {
	gettimeofday(&end_value, NULL);
}

int Timer::value() {
	double mtime, seconds, useconds;

	seconds = end_value.tv_sec - start_value.tv_sec;
	useconds = end_value.tv_usec - start_value.tv_usec;

	mtime = ((seconds) * 1000 + useconds / 1000.0) + 0.5;
	return mtime;
}

TimeSampler::TimeSampler(int i) {
	this->interval = i;
}

double TimeSampler::value() {
	return lastValue;
}

void TimeSampler::input(int i) {
	int currentTime = time(NULL);
	//Shall we flip or add:
	if ((currentTime - lastRefresh) > interval) {
		lastValue = buffer;
		lastRefresh = currentTime;
	} else {
		buffer += (double) i;
	}
}

FlexibleSecondIntervalSampler::FlexibleSecondIntervalSampler(){
	lastRefresh = time(NULL);	
	lock = new Lock();
}

void FlexibleSecondIntervalSampler::input(int i){
	lock->lock();
	buffer += i;
	lock->unlock();
}

double FlexibleSecondIntervalSampler::value(){
	lock->lock();
	int elapsedTime= time(NULL) - lastRefresh;
	lastRefresh = time(NULL);
	double res = buffer / elapsedTime;
	buffer = 0;
	lock->unlock();
	return res;
}

FlexibleSecondIntervalSampler::~FlexibleSecondIntervalSampler(){
	delete lock;
}
