/*
Copyright Michael Lawson, John Deal
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip> 
#include <boost/regex.hpp>
#include <sstream>
#include <arpa/inet.h>
 #include <netdb.h>
using namespace std;

#include "Utils/Utils.h"
#include "Utils/IP4Addr.h"

using namespace std;

static const int numAddrNibbles = 4; // Number of nibbles in a IP4 address

/*!
 * IP4Addr:
 *     default constructor.
 */
IP4Addr::IP4Addr() {
	address = 0;
}

bool IP4Addr::isLocal(const unsigned int target) {
	if ((target >= CLASS_C_MIN && target <= CLASS_C_MAX) ||
			(target >= CLASS_B_MIN && target <= CLASS_B_MAX) ||
			(target >= CLASS_A_MIN && target <= CLASS_A_MAX)) {
		return true;
	}
	return false;
}

/*!
 * IP4Addr:
 *     constructor with initial IP4 address.
 */
IP4Addr::IP4Addr(unsigned int addr) {
	address = addr;
}

/*!
 * ip4AddrToString:
 *
 * Returns standard dot formatted IP4 address (xxx.xxx.xxx.xxx).  Does not
 * return leading zeros.
 *
 * Paramters:
 *
 *     ip4Addr - IP4 address as a unsigned integer
 *
 * Returns:
 *
 *     Dot formatted version of ip4Addr
 */

string IP4Addr::ip4AddrToString(in_addr_t addr, in_addr_t mask) {
	struct in_addr inaddr;
	string strIp;

	inaddr.s_addr = ntohl(addr & mask);
	strIp = inet_ntoa(inaddr);
	if (mask != (in_addr_t) - 1) {
		inaddr.s_addr = ntohl(mask);
		strIp += "/";
		strIp += inet_ntoa(inaddr);
	}

	return strIp;
}

/*
 * buildIPAddr: get numeric equivalant of a ascii IP Address in xxx.xxx.xxx.xxx
 *     format.
 *
 *     Parameters:
 *         inStr - Array of 4 strings of IP Address (ex. "192","168","1", "1")
 *
 *     Return: 32-bit unsigned integer representing supplied address or
 *         ipAddrError if a conversion error was incountered.
 */
unsigned int IP4Addr::buildIPAddr(const string inStr[]) {
	const int numFields = 4;
	int workInt;
	unsigned int result = 0;

	for (int index = 0; index < numFields; index++) {
		workInt = stoi(inStr[index]);

		if ((workInt < 0) || ((workInt == 0) &&
				((inStr[index].compare("0") != 0) &&
				(inStr[index].compare("00") != 0) &&
				(inStr[index].compare("000") != 0))) || (workInt > 255)) {
			// Invalid field
			result = ipAddrError; // Flag error
			break; // End conversion
		}

		result += (unsigned int) workInt << ((3 - index) << 3);
	}

	return result;
}

/*!
 * stringToIP4Addr:
 *     Converts a string representation of a dot notated IP4 address and
 *     returns the integer equivelant.
 *
 * Parameters:
 *     addStr - IP4 dot notated address string to be converted.
 *
 * Return:
 *     Integer equivelant of address or ipAddrError if invalid address string.
 */
unsigned int IP4Addr::stringToIP4Addr(const string addrStr) {
	string strNibbles[10]; // Add some for possible errors

	if (splitIPAddrString(addrStr, strNibbles, numAddrNibbles) !=
			numAddrNibbles) { // Invalid string represenatation of an IP4 address
		return ipAddrError;
	}

	return buildIPAddr(strNibbles);
}

/*
 * splitIPAddrString: Splits IP Address string in dot notation to its
 *     separate components.
 *
 * Parameters:
 *     ipAddrString - IP Address string in dot notation.
 *     components   - Array to contain components of IP address
 *     numComponents - Number of components contained in address
 *
 * Return:
 *     Number of components found in ipAddrString
 */
int IP4Addr::splitIPAddrString(const string ipAddrString, string components[],
		int numComponents) {
	char workCString[64];
	char workNibble[16];
	char *workCharPtr;
	char *lastToken; // ONLY FOR USE BY strtok_r.
	int componentCnt = 0;


	if (numComponents > 0) {
		strcpy(workCString, ipAddrString.c_str());
		workCharPtr = strtok_r(workCString, ".", &lastToken);

		if (workCharPtr != NULL) { // At least one component.
			strncpy(workNibble, workCharPtr, (strlen(workCharPtr) + 1));
			components[0] = workNibble;

			for (componentCnt = 1; componentCnt < numComponents;
					componentCnt++) { // Process the rest of the tokens
				workCharPtr = strtok_r(NULL, ".",
						&lastToken);

				if (workCharPtr != NULL) { // Have component
					strncpy(workNibble, workCharPtr,
							(strlen(workCharPtr) + 1));
					components[componentCnt] =
							workNibble;
				} else {
					break; // No more components
				}
			}
		} // End if numComponents > 0
	}

	return componentCnt;
}

int IP4Addr::matchNetwork(unsigned int host, unsigned int net, unsigned int mask) {
	if(host == net){
		return 0;
	}

	unsigned int side1 = (mask & host);
	unsigned int side2 = (mask & net);
	if (side1 == side2) {
		return 0;
	} else {
		return -1;
	}
}

unsigned int IP4Addr::convert(struct in_addr* in) {
	return htonl((unsigned int) in->s_addr);
}

std::string IP4Addr::convertHw(const u_int8_t hw[6]) {
	char test[32];
	sprintf(test, "%02x:%02x:%02x:%02x:%02x:%02x", hw[0], hw[1], hw[2], hw[3], hw[4], hw[5]);
	return std::string(test);
}

bool IP4Addr::isHostName(std::string input) {
	vector<string> vec;
	split(input, '.', vec);

	if (vec.size() == 4) {
		return false;
	}
	return true;
}

bool IP4Addr::isBroadCast(unsigned int addr) {
/* Without knowing more about the network interface we cant determine whether the address is broadcast. What we can check however is that the address is not the global multicase 255.255.255.255*/

	unsigned char* ucp = (unsigned char*) &addr;
	int a = ucp[0] & 0xff;
	int b = ucp[1] & 0xff;
	int c = ucp[2] & 0xff;
	int d = ucp[3] & 0xff;

	if (a == 255 && b == 255 && c == 255 && d == 255)
		return true;
	return false;
}

bool IP4Addr::isMultiCast(unsigned int addr) {
	if ((addr >= MULTICAST_START) && (addr <= MULTICAST_STOP)) {
		return true;
	}
	return false;
}

bool IP4Addr::isIp(std::string address) {
	boost::smatch what;
	boost::regex exp("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b");
	if (boost::regex_match(address, what, exp, boost::match_extra))
		return true;

	return false;
}

bool IP4Addr::isHw(std::string address) {
	boost::smatch what;
	boost::regex exp("^([0-9a-f]{2}([:]|$)){6}$");
	if (boost::regex_match(address, what, exp, boost::match_extra))
		return true;

	return false;
}

std::string IP4Addr::hostname(unsigned int ip){
	struct hostent *he;
	struct in_addr ipv4addr;

	inet_pton(AF_INET, IP4Addr::ip4AddrToString(ip).c_str(), &ipv4addr);
	he = gethostbyaddr(&ipv4addr, sizeof ipv4addr, AF_INET);
	if(he){
		return he->h_name;
	}
	return "unknown";
}


