/*
Copyright Michael Lawson
This file is part of Sphirewall.

Sphirewall is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sphirewall is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sphirewall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Utils/TimeWrapper.h"
#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <error.h>

using namespace std;

Time::Time(int intval) {
	t = intval;
}

Time::Time(std::string in) {
	Time(in, "%Y-%m-%d");

	tm* brokenTime = new tm();
	strptime(in.c_str(), "%Y-%m-%d", brokenTime);
	time_t sinceEpoch = mktime(brokenTime);
	this->t = sinceEpoch;

	delete brokenTime;
}

Time::Time(std::string in, const char* f) {
	tm* brokenTime = new tm();
	strptime(in.c_str(), f, brokenTime);
	time_t sinceEpoch = mktime(brokenTime);
	this->t = sinceEpoch;

	delete brokenTime;
}

std::string Time::format(const char* form) {
	char buffer[1024];
	struct tm* timeinfo = localtime(&t);
	strftime(buffer, 1024, form, timeinfo);

	return string(buffer);
}

std::string Time::day() {
	return format("%Y-%m-%d");
}

std::string Time::midnight() {
	return format("%Y-%m-%d 00:00:00");
}

std::string Time::last() {
	return format("%Y-%m-%d 23:59:59");
}

std::string Time::hour(){
	return format("%Y-%m-%d %H:00:00");
}

int Time::extractHour(){
	struct tm* timeinfo = localtime(&t);
	return timeinfo->tm_hour - ((timeinfo->tm_isdst != 0) ? 1 : 0);
}

int Time::extractMins(){
	struct tm* timeinfo = localtime(&t);
	return timeinfo->tm_min;
}

int Time::wday() {
	struct tm* timeinfo = localtime(&t);
	return timeinfo->tm_wday;
}

int Time::month() {
	struct tm* timeinfo = localtime(&t);
	return timeinfo->tm_mon;
}

Time* Time::startOfWeek() {
	time_t t = time(NULL);
	struct tm* timeinfo = localtime(&t);
	int daysToSubtract = timeinfo->tm_wday;
	int secondsToSubtract = 24 * 60 * 60 * daysToSubtract;

	t = time(NULL) - secondsToSubtract;
	return new Time(t);
}

Time* Time::startOfMonth() {
	time_t t = time(NULL);

	struct tm* timeinfo = localtime(&t);
	timeinfo->tm_sec = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_hour = 0;
	timeinfo->tm_mday = 1;

	return new Time(mktime(timeinfo));
}

Time* Time::yesterday() {
	time_t t = time(NULL);

	struct tm* timeinfo = localtime(&t);
	timeinfo->tm_sec = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_hour = 0;
	timeinfo->tm_yday = timeinfo->tm_yday - 1;

	return new Time(mktime(timeinfo));
}

Time* Time::startOfWeek(int base) {
	time_t t = base;
	struct tm* timeinfo = localtime(&t);
	int daysToSubtract = timeinfo->tm_wday;
	int secondsToSubtract = 24 * 60 * 60 * daysToSubtract;

	t = time(NULL) - secondsToSubtract;
	return new Time(t);
}

Time* Time::startOfMonth(int base) {
	time_t t = base;

	struct tm* timeinfo = localtime(&t);
	timeinfo->tm_sec = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_hour = 0;
	timeinfo->tm_mday = 1;

	return new Time(mktime(timeinfo));
}

Time* Time::yesterday(int base) {
	time_t t = base;

	struct tm* timeinfo = localtime(&t);
	timeinfo->tm_sec = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_hour = 0;
	timeinfo->tm_yday = timeinfo->tm_yday - 1;

	return new Time(mktime(timeinfo));
}

Time* Time::getNextDayOfMonth(int base, int day){
	time_t t = base;
	struct tm* timeinfo = localtime(&t);
	timeinfo->tm_sec = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_hour = 0;

	if(timeinfo->tm_mday > day){
		timeinfo->tm_mday = day;
		timeinfo->tm_mon = timeinfo->tm_mon + 1;
	}else{
		timeinfo->tm_mday = day;
	}

	return new Time(mktime(timeinfo));
}

Time* Time::getLastDayOfMonth(int base, int day){
	time_t t = base;
	struct tm* timeinfo = localtime(&t);
	timeinfo->tm_sec = 0;
	timeinfo->tm_min = 0;
	timeinfo->tm_hour = 0;

	if(timeinfo->tm_mday >= day){
		timeinfo->tm_mday = day;
	}else{
                timeinfo->tm_mon = timeinfo->tm_mon - 1;
		timeinfo->tm_mday = day;
	}

	return new Time(mktime(timeinfo));
}

int Time::dayOfYear(){
        struct tm* timeinfo = localtime(&t);
	return timeinfo->tm_yday;
}

int Time::dayDiff(Time* a, Time* b){
	return a->dayOfYear() - b->dayOfYear();
}

Time* Time::addDays(int base, int days){
	time_t t = base;
	struct tm* timeinfo_a = localtime(&t);
	timeinfo_a->tm_yday += days;
	return new Time(mktime(timeinfo_a));
}

//Not absolutely true ;)
int Time::daysLeftInMonth(){
	struct tm* timeinfo = localtime(&t);
	return 30 - timeinfo->tm_mday;
}

TimeWrapper::TimeWrapper() {
	theTime.tv_sec = 0;
	theTime.tv_usec = 0;
}

TimeWrapper::TimeWrapper(const struct timeval& time) : theTime(time) {

}

TimeWrapper TimeWrapper::now() {
	struct timeval tm;
	gettimeofday(&tm, NULL);
	return TimeWrapper(tm);
}

__ULONGWORD_TYPE TimeWrapper::getInMiliseconds() const {
	return ((theTime.tv_sec * MILISECONDS_IN_SECOND) +
			(theTime.tv_usec / MICROSECONDS_IN_MILISECOND));
}



